jQuery(document).ready(function(){

    // function zoom_image(){
    //     //pass the images to Fancybox
    //     $('body').bind('click','', function (e) {
    //         var ez = $('#zoom_03').data('ezPlus');
    //         $.fancyboxPlus(ez.getGalleryList());
    //         return false;
    //     });
    // }

    load_library();
    function load_library(){

        if( $('div').hasClass('filter-claim') ){

            let element_filter = $('.filter-claim');
            
            let bulan = '';
            if( element_filter.find('div').hasClass("bulanpengajuan-vclaim") ){
                bulan = element_filter.find('div').data('bulan');
            }

            let aktif = '';
            if( element_filter.find('div').hasClass("aktifpengajuan-vclaim") ){
                aktif = element_filter.find('div').data('aktif');
            }

            let superuser = $('.super-vclaim').data('value');

            var date;
            if( superuser){
                
                if( aktif == '' || aktif !='' ){
                    date        = new Date(); 
                }

                $('.vclaimtgl').datepicker({
                    format: 'dd/mm/yyyy',
                    autoclose:true,
                });

                console.log('test1');


            }else if( aktif != '' && bulan != '' ){
                date        = new Date(bulan);
                var startDate   = new Date(date.getFullYear(), date.getMonth(), 1);
                var endDate     = new Date(date.getFullYear(), date.getMonth() + 1, 0);
                
                $('.vclaimtgl').datepicker({
                    format: 'dd/mm/yyyy',
                    autoclose:true,
                    startDate : startDate,
                    endDate : endDate,
                });

                // console.log(getUrlParameter('tanggal'));
                
                if(getUrlParameter('tanggal') == ''  ){
                    setTimeout(function(){
                        // $( '.vclaimtgl' ).datepicker( 'setDate', endDate );
                        $("#frm-filter-claim").find('.filter').trigger("click");
                    },100);
                }
                
                

                // console.log('test2');
                
            }else{
                // var firstDay = new Date(date.getFullYear(), date.getMonth(), 1);
                
                // $('.vclaimtgl').datepicker({
                //     format: 'dd/mm/yyyy',
                //     autoclose:true,
                // });

                // $( '.vclaimtgl' ).datepicker( 'setDate', firstDay );
                console.log('test3');
                $('.vclaimtgl').attr('disabled',true);
                $('.vclaimtgl').attr('value','Belum Dibuka RS');
            }
            
            var today       = new Date(date.getFullYear(), date.getMonth(), date.getDate());   

            $( '.vclaimtgl' ).datepicker( 'setDate', today );

        }
    }

    $('body').on('click','.header-tab button',function(){
        let root = $(this);
        let data_t = root.data('t');

        $('.card-body').find('.collapse').removeClass('show');
        
        $('.header-tab button').removeClass('active');
        $('.header-tab button').removeClass('text-white');
        $('.header-tab button').removeClass('btn-warning');
        $('.header-tab button').addClass('btn-primary');

        root.addClass('active');
        root.addClass('text-white');
        root.removeClass('btn-primary');
        root.addClass('btn-warning');

        if( data_t == 'all' ){
            root.closest('.card-body').find('.collapse').addClass('show');             
        }else{
            if( root.closest('.card-body').find('.body-tabs').find('#'+data_t).attr('id') == data_t ){
                root.closest('.card-body').find('#'+data_t).addClass('show');             
            }
        }
        
    });
    
    selected_date();
    function selected_date(){
        if( getUrlParameter('tanggal') ){
            let dt = $('.vclaimtgl');
            dt.datepicker();
            dt.datepicker('setDate', getUrlParameter('tanggal') );
        }
        
    }

    function getUrlParameter(sParam) {
        var sPageURL = window.location.search.substring(1),
            sURLVariables = sPageURL.split('&'),
            sParameterName,
            i;
    
        for (i = 0; i < sURLVariables.length; i++) {
            sParameterName = sURLVariables[i].split('=');
    
            if (sParameterName[0] === sParam) {
                return sParameterName[1] === undefined ? true : decodeURIComponent(sParameterName[1]);
            }
        }
        return false;
    }

    add_pertimbangan_vclaim();
    function add_pertimbangan_vclaim(){
        $('body').on("click",'#addpertimbangan',function(e){
            e.preventDefault();
            let root = $(this);
            let element_input = root.closest('.data-groupadd').find('input[name="addpertimbangan"]');
            let val  = element_input.val();
            let elemen_nonce = $('.nonce');
            let elemen_norm = $('.norm');
            let elemen_tglperiksa = $('.tglperiksa');

            if( val.length == ''  ){
                                
                if( $("div").hasClass("msg-invalid") ){
                    $('.msg-invalid').remove();
                }

                element_input.addClass('input-empty');
                
                $( "<div class='msg-invalid'><p>Silahkan isi terlebih dahulu !</p></div>" ).insertAfter( element_input );
                
                element_input.get(0).focus();

                setTimeout(function() { 
                    $('.msg-invalid').remove();
                    element_input.removeClass('input-empty');
                }, 2000);
                
                return false;
            }else{
                $('.msg-invalid').remove();
                element_input.removeClass('input-empty');
                element_input.val('');
            }

            element_input.get(0).focus();

            let namehash = elemen_nonce.attr('name');
            let hash = elemen_nonce.val();

            let data = {
                'value':val,
                'norm':elemen_norm.val(),
                'tglperiksa':elemen_tglperiksa.val(),
                [namehash]:hash
            };
            
            $.ajax({
                url:base_url+'cvclaims/addpertimbangan',
                data: data,
                type:'POST',
                dataType:'JSON',
                beforeSend:function(){
                    $('body').find('.tabledata').html('<p>Sedang Proses ...</p>');
                },
                success: function(response){
                    
                    if( response.code == '200' ){
                        $('body').find('.tabledata').html(response.hsl);
                    }else{
                        alert('Maaf Gagal Menyimpan Data, Mohon Coba lagi :(');
                        setTimeout(function() { 
                            location.reload();
                        }, 1000);
                    }

                    elemen_nonce.val(response.hash);

                },
                error:function(xhr){
                    alert('Silahkan Contact Administrator Rumah Sakit : '+ xhr.status);
                    return false;
                }
            });


        });
    }

    delete_pertimbangan();
    function delete_pertimbangan(){
        $('body').on('click','.delete-pertimbangan',function(){
            let root = $(this);
            let idpertimbangan = root.data('idpertimbangan');
            let idlist = root.data('idlist');
            let elemen_nonce = $('.nonce');
            let namehash = elemen_nonce.attr('name');
            let hash = elemen_nonce.val();

            let data = {'idlist':idlist,'idpertimbangan':idpertimbangan,[namehash]:hash};
            
                $.ajax({
                    url:base_url+'cvclaims/deletepertimbangan',
                    data: data,
                    type:'POST',
                    dataType:'JSON',
                    success: function(response){
                        
                        if( response.code == '200' ){
                            root.closest('tr').remove();
                        }else{
                            alert('Maaf Gagal Mengahpus Data, Mohon Coba lagi :(');
                            setTimeout(function() { 
                                location.reload();
                            }, 1000);
                        }

                        elemen_nonce.val(response.hash);

                    },
                    error:function(xhr){
                        alert('Silahkan Contact Administrator Rumah Sakit : '+ xhr.status);
                        return false;
                    }
                });

        });
    }

    onlynumber();
    function onlynumber(){
        $('input[name="nokontrol"]').on('keyup',function(e){

            if (/\D/g.test(this.value)){
                // Filter non-digits from input value.
                this.value = this.value.replace(/\D/g, '');
            }
        });
    }

    keyup_suratkontrol();
    function keyup_suratkontrol(){
        let elm_idparent = '#suratkontrol';
        $(elm_idparent).on('blur','input[name="skdp"],input[name="nokontrol"],textarea[name="diagnosa"],textarea[name="terapi"],input[name="tglkontrolkembali"],input[name="tglmulairujukan"],input[name="tglselesairujukan"]',function(e){
            
            let root = $(this);
            let val  = root.val();
            let elemen_nonce = $('.nonce');
            let namehash = elemen_nonce.attr('name');
            let hash = elemen_nonce.val();

            let idunit         = $('body').find('.idunit-detail').data('id');


            let elemen_norm = $('.norm');
            let elemen_tglperiksa = $('.tglperiksa');

            let data = {
                'name_elemen':root.attr('name'),
                'val':val,
                'norm':elemen_norm.val(),
                'tglperiksa':elemen_tglperiksa.val(),
                [namehash]:hash,
                'idunit':idunit
            };

            $.ajax({
                url:base_url+'cvclaims/keyup_suratkontrol',
                data: data,
                type:'POST',
                dataType:'JSON',
                success: function(response){
                   
                    if( response.code == '200' ){
                        console.log(response.msg);
                    }else{
                        alert('Maaf Tidak dapat Menyimpan data');
                        setTimeout(function() { 
                            location.reload();
                        }, 1000);
                    }

                    elemen_nonce.val(response.hash);

                },
                error:function(xhr){
                    alert('Silahkan Contact Administrator Rumah Sakit : '+ xhr.status);
                    return false;
                } 
            });
        });
    }

    surat_kontrolinap();
    function surat_kontrolinap(){
        let elm_idparent = '#suratkontrolinap';
        $(elm_idparent).on('blur','input[name="tglmulaidirawat"],input[name="tglselesaidirawat"],input[name="ruang"],input[name="diagnosis"],input[name="terapi"],input[name="tglkontrol"],input[name="jam"]',function(){
            let root = $(this);
            let val = root.val();
            let name_elemen = root.attr('name');

            let elemen_nonce = $('.nonce');
            let namehash    = elemen_nonce.attr('name');
            let hash        = elemen_nonce.val();

            let idunit         = $('body').find('.idunit-detail').data('id');

            let elemen_norm         = $('.norm');
            let elemen_tglperiksa   = $('.tglperiksa');

            let data = {
                'name_elemen':name_elemen,
                'val':val,
                'norm':elemen_norm.val(),
                'tglperiksa':elemen_tglperiksa.val(),
                [namehash]:hash,
                'idunit':idunit
            };

            $.ajax({
                url:base_url+'cvclaims/keyup_suratkontrolinap',
                data: data,
                type:'POST',
                dataType:'JSON',
                success: function(response){
                    
                    if( response.code == '200' ){
                        console.log(response.msg);
                    }else{
                        alert('Maaf Tidak dapat Menyimpan data');
                        setTimeout(function() { 
                            location.reload();
                        }, 1000);
                    }

                    elemen_nonce.val(response.hash);

                },
                error:function(xhr){
                    alert('Silahkan Contact Administrator Rumah Sakit : '+ xhr.status);
                    return false;
                } 
            });
            
        });
    }

    surat_emergency();
    function surat_emergency(){
        let elm_idparent = '#emergency';
        $(elm_idparent).on('blur','input[name="diagnosa"],input[name="tindakan"],input[name="tglemergency"]',function(){
            let root = $(this);
            let val = root.val();
            let name_elemen = root.attr('name');

            let elemen_nonce = $('.nonce');
            let namehash    = elemen_nonce.attr('name');
            let hash        = elemen_nonce.val();

            let idunit         = $('body').find('.idunit-detail').data('id');


            let elemen_norm         = $('.norm');
            let elemen_tglperiksa   = $('.tglperiksa');

            let data = {
                'name_elemen':name_elemen,
                'val':val,
                'norm':elemen_norm.val(),
                'tglperiksa':elemen_tglperiksa.val(),
                [namehash]:hash,
                'idunit' : idunit,
            };

            // console.log(data);

            $.ajax({
                url:base_url+'cvclaims/keyup_emergency',
                data: data,
                type:'POST',
                dataType:'JSON',
                success: function(response){
                    
                    // console.log(response);
                    if( response.code == '200' ){
                        console.log(response.msg);
                    }else{
                        alert('Maaf Tidak dapat Menyimpan data');
                        setTimeout(function() { 
                            location.reload();
                        }, 1000);
                    }

                    elemen_nonce.val(response.hash);

                },
                error:function(xhr){
                    alert('Silahkan Contact Administrator Rumah Sakit : '+ xhr.status);
                    return false;
                } 
            });
            
        });
    }

    upload_inacbg();
    function upload_inacbg(){
        $('body').on('submit','#frm-inacbg',function(e){
            e.preventDefault();
            let root = $(this);
            let elemen_nonce = $('.nonce');
            let namehash    = elemen_nonce.attr('name');
            let hash        = elemen_nonce.val();

            let elemen_norm         = $('.norm');
            let elemen_tglperiksa   = $('.tglperiksa');
            let elemen_idunit       = root.find('.idunit-hidden');

            let formData =  new FormData();

            formData.append('norm', elemen_norm.val());
            formData.append('tglperiksa', elemen_tglperiksa.val());
            formData.append('idunit', elemen_idunit.val());
            formData.append([namehash], hash);

            // Attach file
            formData.append('fileinacbg', root.find('input[name="fileinacbg"]')[0].files[0]); 

            $.ajax({
                url:base_url+'cvclaims/upload_inacbg',
                type:"post",
                data: formData,
                cache: false,
                contentType: false,
                processData: false,
                dataType:'JSON',
                success: function(response){
                    
                    root.find('input[type="submit"]').attr('disabled',true);

                    // console.log(response);
                    if( response.code == '200' ){
                        

                        Swal.fire({
                            position: 'center',
                            icon: 'success',
                            text: response.txt,
                            showConfirmButton: false,
                            timer: 2000
                        }).then((result) => {
                            location.reload();
                        });

                    }else{
                        Swal.fire({
                            position: 'center',
                            icon: 'warning',
                            text: response.txt,
                            showConfirmButton: false,
                            timer: 2000
                        }).then((result) => {
                            location.reload();
                        });
                    }

                    elemen_nonce.val(response.hash);

                },
                error: function(xhr){
                    alert('Silahkan Contact Administrator Rumah Sakit : '+ xhr.status);
                    return false;
                }
            });
        });
    }

    upload_file_image();
    function upload_file_image(){
        $('body').on('submit','#frm-sep,#frm-rencanakontrol',function(e){
            e.preventDefault();
            let root = $(this);
            let elemen_nonce = $('.nonce');
            let namehash    = elemen_nonce.attr('name');
            let hash        = elemen_nonce.val();

            let elemen_norm         = $('.norm');
            let elemen_tglperiksa   = $('.tglperiksa');
            let elemen_idunit       = root.find('.idunit-hidden');

            console.log(elemen_idunit.val());
            let formData =  new FormData();

            formData.append('norm', elemen_norm.val());
            formData.append('tglperiksa', elemen_tglperiksa.val());
            formData.append('elemen_id', root.attr('id'));
            formData.append('idunit', elemen_idunit.val());
            formData.append([namehash], hash);

            // Attach file
            formData.append('fileimage', root.find('input[name="fileimage"]')[0].files[0]); 

            $.ajax({
                url:base_url+'cvclaims/upload_file_image',
                type:"post",
                data: formData,
                cache: false,
                contentType: false,
                processData: false,
                dataType:'JSON',
                success: function(response){
                    
                    root.find('input[type="submit"]').attr('disabled',true);

                    // console.log(response);
                    if( response.code == '200' ){
                        

                        Swal.fire({
                            position: 'center',
                            icon: 'success',
                            text: response.txt,
                            showConfirmButton: false,
                            timer: 2000
                        }).then((result) => {
                            location.reload();
                        });

                    }else{
                        Swal.fire({
                            position: 'center',
                            icon: 'warning',
                            text: response.txt,
                            showConfirmButton: false,
                            timer: 2000
                        }).then((result) => {
                            location.reload();
                        });
                    }

                    elemen_nonce.val(response.hash);

                },
                error: function(xhr){
                    alert('Silahkan Contact Administrator Rumah Sakit : '+ xhr.status);
                    return false;
                }
            });
        });
    }

    editfile_inacbg();
    function editfile_inacbg()
    {
        $('body').on('click','.editfile-inacbg',function(){
            let root = $(this);
            $('#frm-inacbg').removeClass('d-none');
            $('#frm-inacbg').addClass('d-block');

            root.html('Batal Edit');
            root.removeClass('editfile-inacbg');
            root.addClass('bataleditfile-inacbg');
        });

        $('body').on('click','.bataleditfile-inacbg',function(){
            let root = $(this);
            $('#frm-inacbg').removeClass('d-block');
            $('#frm-inacbg').addClass('d-none');

            root.html('Edit');
            root.removeClass('bataleditfile-inacbg');
            root.addClass('editfile-inacbg');
        });
    }

    editfile_upload_image();
    function editfile_upload_image()
    {
        $('body').on('click','.editfile-upload-image',function(){
            let root = $(this);
            $('#frm-sep,#frm-rencanakontrol').removeClass('d-none');
            $('#frm-sep,#frm-rencanakontrol').addClass('d-block');

            root.html('Batal Edit');
            root.removeClass('editfile-upload-image');
            root.addClass('bataleditfile-upload-image');
        });

        $('body').on('click','.bataleditfile-upload-image',function(){
            let root = $(this);
            $('#frm-sep,#frm-rencanakontrol').removeClass('d-block');
            $('#frm-sep,#frm-rencanakontrol').addClass('d-none');

            root.html('Edit');
            root.removeClass('bataleditfile-upload-image');
            root.addClass('editfile-upload-image');
        });
    }

    

    editfile_rujukanbaru();
    function editfile_rujukanbaru()
    {
        $('body').on('click','.editfile-rujukanbaru',function(){
            let root = $(this);
            $('#frm-rujukanbaru').removeClass('d-none');
            $('#frm-rujukanbaru').addClass('d-block');

            root.html('Batal Edit');
            root.removeClass('editfile-rujukanbaru');
            root.addClass('bataleditfile-rujukanbaru');
        });

        $('body').on('click','.bataleditfile-rujukanbaru',function(){
            let root = $(this);
            $('#frm-rujukanbaru').removeClass('d-block');
            $('#frm-rujukanbaru').addClass('d-none');

            root.html('Edit');
            root.removeClass('bataleditfile-rujukanbaru');
            root.addClass('editfile-rujukanbaru');
        });
    }

    delete_inacbg();
    function delete_inacbg(){
        $('body').on('click','.deletefile-inacbg',function(){
            let root         = $(this);
            let idinacbg     = root.data('id');
            let elemen_nonce = $('.nonce');
            let namehash     = elemen_nonce.attr('name');
            let hash         = elemen_nonce.val();
            
            let data = {'idinacbg':idinacbg,[namehash]:hash};

            Swal.fire({
                text: 'Apakah Anda Yakin Menghapus File ini ?',
                showCancelButton: true,
                confirmButtonText: 'Yes',
                cancelButtonText: 'No',
              }).then((result) => {
                if (result.isConfirmed) {
                    $.ajax({
                        url:base_url+'cvclaims/delete_inacbg',
                        type:"post",
                        data: data,
                        dataType:'JSON',
                        success: function(response){
                            // console.log(response);
                            if( response.code == '200' ){
                                Swal.fire({
                                    position: 'center',
                                    icon: 'success',
                                    text: response.txt,
                                    showConfirmButton: false,
                                    timer: 2000
                                }).then((result) => {
                                    location.reload();
                                });
        
                            }else{
                                Swal.fire({
                                    position: 'center',
                                    icon: 'warning',
                                    text: response.txt,
                                    showConfirmButton: false,
                                    timer: 2000
                                }).then((result) => {
                                    location.reload();
                                });
                            }
                        },
                        error:function(xhr){
                            Swal.fire({
                                position: 'center',
                                icon: 'warning',
                                text: 'Silahkan Contact Administrator Rumah Sakit : '+ xhr.status,
                                showConfirmButton: false,
                                timer: 2000
                            });
                        }
                    });
                }
            });

        });
    }

    delete_upload_image();
    function delete_upload_image(){
        $('body').on('click','.deletefile-upload-image',function(){
            let root         = $(this);
            let elemenid     = root.closest('.show-image').find('.form-group').data('id');
            let idimage      = root.data('id');
            let elemen_nonce = $('.nonce');
            let namehash     = elemen_nonce.attr('name');
            let hash         = elemen_nonce.val();
            
            let data = {'idimage':idimage,'elemenid':elemenid,[namehash]:hash};

            Swal.fire({
                text: 'Apakah Anda Yakin Menghapus File ini ?',
                showCancelButton: true,
                confirmButtonText: 'Yes',
                cancelButtonText: 'No',
              }).then((result) => {
                if (result.isConfirmed) {
                    $.ajax({
                        url:base_url+'cvclaims/delete_upload_image',
                        type:"post",
                        data: data,
                        dataType:'JSON',
                        success: function(response){
                            // console.log(response);
                            if( response.code == '200' ){
                                Swal.fire({
                                    position: 'center',
                                    icon: 'success',
                                    text: response.txt,
                                    showConfirmButton: false,
                                    timer: 2000
                                }).then((result) => {
                                    location.reload();
                                });
        
                            }else{
                                Swal.fire({
                                    position: 'center',
                                    icon: 'warning',
                                    text: response.txt,
                                    showConfirmButton: false,
                                    timer: 2000
                                }).then((result) => {
                                    location.reload();
                                });
                            }
                        },
                        error:function(xhr){
                            Swal.fire({
                                position: 'center',
                                icon: 'warning',
                                text: 'Silahkan Contact Administrator Rumah Sakit : '+ xhr.status,
                                showConfirmButton: false,
                                timer: 2000
                            });
                        }
                    });
                }
            });

        });
    }

    upload_rujukanbaru();
    function upload_rujukanbaru(){
        $('body').on('submit','#frm-rujukanbaru',function(e){
            e.preventDefault();
            let root = $(this);
            let elemen_nonce = $('.nonce');
            let namehash     = elemen_nonce.attr('name');
            let hash         = elemen_nonce.val();

            let elemen_norm         = $('.norm');
            let elemen_tglperiksa   = $('.tglperiksa');
            let elemen_idunit       = root.find('.idunit-hidden');

            let formData =  new FormData();

            formData.append('norm', elemen_norm.val());
            formData.append('tglperiksa', elemen_tglperiksa.val());
            formData.append('idunit', elemen_idunit.val());
            formData.append([namehash], hash);
            // Attach file
            formData.append('filerujukanbaru', root.find('input[name="filerujukanbaru"]')[0].files[0]); 

            $.ajax({
                url:base_url+'cvclaims/upload_rujukanbaru',
                type:"post",
                data: formData,
                cache: false,
                contentType: false,
                processData: false,
                dataType:'JSON',
                success: function(response){
                    
                    // console.log(response);
                    root.find('input[type="submit"]').attr('disabled',true);

                    if( response.code == '200' ){
                        

                        Swal.fire({
                            position: 'center',
                            icon: 'success',
                            text: response.txt,
                            showConfirmButton: false,
                            timer: 2000
                        }).then((result) => {
                            location.reload();
                        });

                    }else{
                        Swal.fire({
                            position: 'center',
                            icon: 'warning',
                            text: response.txt,
                            showConfirmButton: false,
                            timer: 2000
                        }).then((result) => {
                            location.reload();
                        });
                    }

                    elemen_nonce.val(response.hash);

                },
                error: function(xhr){
                    alert('Silahkan Contact Administrator Rumah Sakit : '+ xhr.status);
                    return false;
                }
            });
        });
    }

    delete_rujukanbaru();
    function delete_rujukanbaru(){
        $('body').on('click','.deletefile-rujukanbaru',function(){
            let root         = $(this);
            let idrujukanbaru     = root.data('id');
            let elemen_nonce = $('.nonce');
            let namehash     = elemen_nonce.attr('name');
            let hash         = elemen_nonce.val();
            
            let data = {'idrujukanbaru':idrujukanbaru,[namehash]:hash};

            Swal.fire({
                text: 'Apakah Anda Yakin Menghapus File ini ?',
                showCancelButton: true,
                confirmButtonText: 'Yes',
                cancelButtonText: 'No',
              }).then((result) => {
                if (result.isConfirmed) {
                    $.ajax({
                        url:base_url+'cvclaims/delete_rujukanbaru',
                        type:"post",
                        data: data,
                        dataType:'JSON',
                        success: function(response){
                            console.log(response);
                            if( response.code == '200' ){
                                Swal.fire({
                                    position: 'center',
                                    icon: 'success',
                                    text: response.txt,
                                    showConfirmButton: false,
                                    timer: 2000
                                }).then((result) => {
                                    location.reload();
                                });
        
                            }else{
                                Swal.fire({
                                    position: 'center',
                                    icon: 'warning',
                                    text: response.txt,
                                    showConfirmButton: false,
                                    timer: 2000
                                }).then((result) => {
                                    location.reload();
                                });
                            }
                        },
                        error:function(xhr){
                            Swal.fire({
                                position: 'center',
                                icon: 'warning',
                                text: 'Silahkan Contact Administrator Rumah Sakit : '+ xhr.status,
                                showConfirmButton: false,
                                timer: 2000
                            });
                        }
                    });
                }
            });

        });
    }

    is_tampil();
    function is_tampil(){
        $('body').on('change','#rencana-kontrol input[name="is_tampil"], #suratkontrolinap input[name="is_tampil"],#emergency input[name="is_tampil"],#radiologi input[name="is_tampil"],#dokumenlainlain input[name="is_tampil"],#rujukan-baru input[name="is_tampil"], #suratkontrol input[name="is_tampil"]',function(){
            let root           = $(this);
            let is_checked     = root.is(':checked') ? 1 : '';
            let obj            = getdata_nonce();
            let norm           = obj.norm;
            let tglperiksa     = obj.tglperiksa;
            let namehash       = obj.namehash;
            let hash           = obj.hash;
            let idunit         = $('body').find('.idunit-detail').data('id');

            let elemen_nonce = $('.nonce');

            let data           = {
                [namehash] : hash,
                'norm' : norm,
                'tglperiksa' : tglperiksa,
                'val' : is_checked,
                'name_elemen' : root.attr('name'),
                'idelemen' : root.closest('.collapse').attr('id'),
                'idunit' : idunit
            };

            $('.security-check').find('.nonce').remove();

            // console.log(root.closest('.collapse').attr('id'));
                $.ajax({
                    url:base_url+'cvclaims/is_tampil',
                    data:data,
                    type:'POST',
                    dataType:'JSON',
                    success:function(response){
                        if( response.code == '200' ){
                            Swal.fire({
                                position: 'center',
                                icon: 'success',
                                text: response.txt,
                                showConfirmButton: false,
                                timer: 2000
                            });

                            if( is_checked ){
                                root.closest('.form-check').find('label').html('Tampilkan')
                            }else{
                                root.closest('.form-check').find('label').html('Tidak Tampilkan')
                            }

                            $('.tambah-resumegabung button,.tambah-radiologigabung button,.tambah-billinggabung button').attr('data-hash',response.hash);

                            $('<input type="hidden" class="nonce" name="'+namehash+'" value="'+response.hash+'">').insertBefore('.security-check .norm');
    
                        }
                        // console.log(response);
                        elemen_nonce.val(response.hash);  
                    },
                    error:function(xhr){
                        Swal.fire({
                            position: 'center',
                            icon: 'warning',
                            text: 'Silahkan Contact Administrator Rumah Sakit : '+ xhr.status,
                            showConfirmButton: false,
                            timer: 2000
                        });
                    }
                });
            
        });
    }


    function getdata_nonce(){
        let elemen_nonce = $('.nonce');
        let namehash     = elemen_nonce.attr('name');
        let hash         = elemen_nonce.val();
        let elemen_norm         = $('.norm');
        let elemen_tglperiksa   = $('.tglperiksa');
            
        let data = {
            'norm':elemen_norm.val(),
            'tglperiksa':elemen_tglperiksa.val(),
            'namehash':namehash,
            'hash':hash
        };

        return data;
    }

    upload_dokumenlainlain();
    function upload_dokumenlainlain(){
        $('body').on('submit','#frm-dokumenlainlain',function(e){
            e.preventDefault();
            let root = $(this);
            let elemen_nonce = $('.nonce');
            let namehash     = elemen_nonce.attr('name');
            let hash         = elemen_nonce.val();

            let idunit         = $('body').find('.idunit-detail').data('id');

            let elemen_norm         = $('.norm');
            let elemen_tglperiksa   = $('.tglperiksa');

            let formData =  new FormData();

            formData.append('norm', elemen_norm.val());
            formData.append('tglperiksa', elemen_tglperiksa.val());
            formData.append('idunit', idunit);
            formData.append([namehash], hash);
            
            let getfiles = root.find('.fileimagelainlain')[0].files;

            for (var x = 0; x < getfiles.length; x++) {
                formData.append("fileimagelainlain[]", getfiles[x]);
            }

            $.ajax({
                url:base_url+'cvclaims/upload_dokumenlainlain',
                type:"post",
                data: formData,
                cache: false,
                contentType: false,
                processData: false,
                dataType:'JSON',
                success: function(response){
                    
                    // console.log(response);
                    root.find('input[type="submit"]').attr('disabled',true);

                    if( response.code == '200' ){
                        Swal.fire({
                            position: 'center',
                            icon: 'success',
                            text: response.txt,
                            showConfirmButton: false,
                            timer: 2000
                        }).then((result) => {
                            location.reload();
                        });

                    }else{
                        Swal.fire({
                            position: 'center',
                            icon: 'warning',
                            text: response.txt,
                            showConfirmButton: false,
                            timer: 2000
                        }).then((result) => {
                            location.reload();
                        });
                    }

                    elemen_nonce.val(response.hash);

                },
                error: function(xhr){
                    alert('Silahkan Contact Administrator Rumah Sakit : '+ xhr.status);
                    return false;
                }
            });
        });
    }

    delete_imagelainlain();
    function delete_imagelainlain(){
        $('body').on('click','.delete-imageslain',function(e){
            e.preventDefault();
            let root = $(this);
            let dataid = root.data('id');
            let keyimage = root.data('keyimage');

            let obj            = getdata_nonce();
            let norm           = obj.norm;
            let tglperiksa     = obj.tglperiksa;
            let namehash       = obj.namehash;
            let hash           = obj.hash;

            let elemen_nonce = $('.nonce');

            let data = {
                [namehash] : hash,
                'norm' : norm,
                'tglperiksa' : tglperiksa,
                'dataid' : dataid,
                'keyimage' : keyimage
            };

            Swal.fire({
                text: 'Apakah Anda Yakin Menghapus File ini ?',
                showCancelButton: true,
                confirmButtonText: 'Yes',
                cancelButtonText: 'No',
              }).then((result) => {
                if (result.isConfirmed) {
                    $.ajax({
                        url: base_url+'cvclaims/delete_imagelainlain',
                        data:data,
                        type:'POST',
                        dataType:'JSON',
                        success: function(response){
        
                            // console.log(response);
                            if( response.code == '200' ){
                                Swal.fire({
                                    position: 'center',
                                    icon: 'success',
                                    text: response.txt,
                                    showConfirmButton: false,
                                    timer: 2000
                                }).then((result) => {
                                    location.reload();
                                });
        
                            }else{
                                Swal.fire({
                                    position: 'center',
                                    icon: 'warning',
                                    text: response.txt,
                                    showConfirmButton: false,
                                    timer: 2000
                                }).then((result) => {
                                    location.reload();
                                });
                            }
        
                            elemen_nonce.val(response.hash);
                        },
                        error:function(xhr){
                            Swal.fire({
                                position: 'center',
                                icon: 'warning',
                                text: 'Silahkan Contact Administrator Rumah Sakit : '+ xhr.status,
                                showConfirmButton: false,
                                timer: 2000
                            });
                        }
                    });
                }
            });

            
        });
    }

    modal_gabung();
    function modal_gabung(){
        $('body').on('click','.tambah-resumegabung button,.tambah-radiologigabung button,.tambah-billinggabung button',function(){
            let root = $(this);
            let norm = root.data('norm');
            let tglperiksa = root.data('tglperiksa');
            let namehash = root.data('name');
            let hash = root.data('hash');
            let tipe = root.data('tipe');
            let data = {'norm':norm,'tipe':tipe,'tglperiksa':tglperiksa,[namehash] : hash};

            root.remove();
            
            $('#modalEvelin').modal('show');


            $.ajax({
                url:base_url+'cvclaims/form_gabung',
                type:"post",
                data: data,
                async: true,                
                dataType:'JSON',
                beforeSend:function(){
                    $('.modal-title').html('Mohon Tunggu ...');
                    $('.modal-footer').find('button').remove();
                },
                success: function(response){

                    // console.log(response);

                    $('.modal-title').html('Tambah '+tipe+' Gabung');
                    $('.modal-body').html(response.hsl);
                    $('.modal-footer').html('<button type="button" class="btn btn-secondary modalClose" data-dismiss="modal" data-tipe="'+tipe+'" data-name="'+response.name+'" data-hash="'+response.hash+'">Close</button>')
                    

                    if( $('input').hasClass('ql-dateresume') ){
                        var date    = new Date();
                        var today   = new Date(date.getFullYear(), date.getMonth(), date.getDate());

                        $('.ql-dateresume').datepicker({
                            format: 'dd/mm/yyyy',
                            autoclose:true,
                            todayHighlight: true
                        });

                    }

                  if(tipe == 'resume'){
                    $('.tambah-resumegabung').find('.form-group').html('<button class="btn btn-danger btn-sm" type="button" data-tipe="'+tipe+'" data-tglperiksa="'+tglperiksa+'" data-norm="'+norm+'" data-name="'+namehash+'" data-hash="'+response.hash+'"> <i class="fa fa-plus-circle"></i> Resume Gabung</button>');
                  }else if( tipe == 'radiologi' ){
                    $('.tambah-radiologigabung').find('.form-group').html('<button class="btn btn-danger btn-sm" type="button" data-tipe="'+tipe+'" data-tglperiksa="'+tglperiksa+'" data-norm="'+norm+'" data-name="'+namehash+'" data-hash="'+response.hash+'"> <i class="fa fa-plus-circle"></i> Radiologi Gabung</button>');
                  }else{
                    $('.tambah-billinggabung').find('.form-group').html('<button class="btn btn-danger btn-sm" type="button" data-tipe="'+tipe+'" data-tglperiksa="'+tglperiksa+'" data-norm="'+norm+'" data-name="'+namehash+'" data-hash="'+response.hash+'"> <i class="fa fa-plus-circle"></i> Billing Gabung</button>');
                  }

                },
                error: function(xhr){
                    Swal.fire({
                        position: 'center',
                        icon: 'warning',
                        text: 'Silahkan Contact Administrator Rumah Sakit : '+ xhr.status,
                        showConfirmButton: false,
                        timer: 2000
                    });
                }
            });

        });
    }

    simpan_resumegabung();
    function simpan_resumegabung(){
        $('body').on('submit','#frm-resumegabung',function(e){
            e.preventDefault();
            let root = $(this);
            let data = root.serializeArray();

            $.ajax({
                url:base_url+'cvclaims/simpan_resumegabung',
                type:"post",
                data: data,
                dataType:'JSON',
                success: function(response){
                    
                    root.find('button').attr('disabled',true);

                    if( response.code == '200' ){
                        Swal.fire({
                            position: 'center',
                            icon: 'success',
                            text: response.txt,
                            showConfirmButton: false,
                            timer: 2000
                        }).then((result) => {
                            location.reload();
                        });
                    }else{
                        Swal.fire({
                            position: 'center',
                            icon: 'warning',
                            text: response.txt,
                            showConfirmButton: false,
                            timer: 2000
                        }).then((result) => {
                            location.reload();
                        });
                    }

                },
                error: function(xhr){
                    Swal.fire({
                        position: 'center',
                        icon: 'warning',
                        text: 'Silahkan Contact Administrator Rumah Sakit : '+ xhr.status,
                        showConfirmButton: false,
                        timer: 2000
                    });
                }
            });

        
        });
    }

    delete_resumegabung();
    function delete_resumegabung(){
        $('body').on('click','.delete-resumegabung',function(){
            let root = $(this);
            let idgabung = root.data('id');
            let namehash       = root.data('name');
            let hash           = root.data('hash');
            let tipe           = root.data('tipe');
            let data           = {[namehash]:hash,'idgabung':idgabung,'tipe':tipe};
            
            Swal.fire({
                text: 'Apakah Anda Yakin Menghapus File ini ?',
                showCancelButton: true,
                confirmButtonText: 'Yes',
                cancelButtonText: 'No',
              }).then((result) => {
                if (result.isConfirmed) {
                    
                    $.ajax({
                        url:base_url+'cvclaims/delete_resumegabung',
                        type:"post",
                        data: data,
                        dataType:'JSON',
                        success: function(response){
                            
                            root.attr('disabled',true);
        
                            if( response.code == '200' ){
                                Swal.fire({
                                    position: 'center',
                                    icon: 'success',
                                    text: response.txt,
                                    showConfirmButton: false,
                                    timer: 2000
                                }).then((result) => {
                                    location.reload();
                                });
                            }else{
                                Swal.fire({
                                    position: 'center',
                                    icon: 'warning',
                                    text: response.txt,
                                    showConfirmButton: false,
                                    timer: 2000
                                }).then((result) => {
                                    location.reload();
                                });
                            }
        
                        },
                        error: function(xhr){
                            Swal.fire({
                                position: 'center',
                                icon: 'warning',
                                text: 'Silahkan Contact Administrator Rumah Sakit : '+ xhr.status,
                                showConfirmButton: false,
                                timer: 2000
                            });
                        }
                    });
                }
            });


        });
    }

    modalClose();
    function modalClose(){
        $('body').on('click','.modalClose',function(){
            let root = $(this);
            let tipe = root.data('tipe');
            let namehash = root.data('name');
            let hash = root.data('hash');
            let data = {'tipe':tipe,[namehash] : hash};

            $('.security-check').find('.nonce').remove();

            $.ajax({
                url: base_url+'cvclaims/modal_close',
                data:data,
                type:'POST',
                dataType:'JSON',
                success: function(response){
                    $('.tambah-billinggabung button').attr('data-hash',response.hash);
                    $('.tambah-radiologigabung button').attr('data-hash',response.hash);
                    $('.tambah-resumegabung button').attr('data-hash',response.hash);
                    $('<input type="hidden" class="nonce" name="'+namehash+'" value="'+response.hash+'">').insertBefore('.security-check .norm');
                    $('#modalEvelin').modal('hide');
                },
                error:function(xhr){
                    Swal.fire({
                        position: 'center',
                        icon: 'warning',
                        text: 'Silahkan Contact Administrator Rumah Sakit : '+ xhr.status,
                        showConfirmButton: false,
                        timer: 2000
                    });
                }
            });
            
        });
    }

    download_berkas_pertgl();
    function download_berkas_pertgl(){
        $('body').on('click','.downloadall-berkas-tgl button',function(e){
            e.preventDefault();
            let root = $(this);
            let namehash   =  $('.security-check').find('.nonce').attr('name');
            let hash       =  $('.security-check').find('.nonce').attr('value');
            
            let tglperiksa       = root.data('tgl');
            let jenispemeriksaan = root.data('jns');
            
            let data = {
               [namehash] : hash,
               'tglperiksa':tglperiksa, 
               'jenispemeriksaan':jenispemeriksaan, 
            };

            let param;

            if( getUrlParameter('tanggal')  != '' && 
                getUrlParameter('jenis-layanan') != '' 
            ){
                param = '?tanggal='+getUrlParameter('tanggal')+'&jenis-layanan='+getUrlParameter('jenis-layanan');
            }else{
                param = '';
            }


            $.ajax({
                url: base_url+'cvclaims/download_berkas_pertgl'+param,
                data:data,
                type:'POST',
                dataType:'JSON',
                beforeSend:function(){
                    // root.attr('disabled','disabled');
                    $('body').find('a').addClass('disabled');
                    $('body').find('button').attr('disabled','disabled');
                    $('.downloadall-berkas-tgl').after('<div id="msg-berkas"><span class="proses"><i class="fa fa-spinner fa-spin" style="font-size:12px"></i> Mohon Tunggu Sebentar Sampai Proses Selesai ...</span></div>');
                },
                success: function(response){

                    if( response.msg == 'success' ){

                        $('#msg-berkas').find('span').html('<span class="sukses">Yeay :), Berhasil Didownload</span>');
                        
                        setTimeout(function() {
                            $('#msg-berkas').remove();
                            
                            let formdownload = `<form style="display: inline-block;" method="post" action="`+base_url+`cvclaims/download_zippertanggal/`+`">
                            <input type="hidden" class="noncedownload" name="`+response.name+`" value="`+response.hash+`">
                            <input type="hidden" name="tglPeriksa" value="`+tglperiksa+`">
                            <input type="hidden" name="jnsPeriksa" value="`+jenispemeriksaan+`">
                            <input type="submit" class="btn btn-warning" value="Download `+response.jmlfile+` Berkas">
                            </form>`;
                            $('.downloadall-berkas-tgl').after(formdownload);  
                            // root.removeAttr('disabled');
                            $('body').find('a').removeClass('disabled');
                            $('body').find('button').removeAttr('disabled');
                            
                        }, 5000);
                        
                    }
                    // console.log(response);
                    
                },
                error:function(xhr){
                    $('#msg-berkas').html('<span class="gagal">Opps :(, Maaf Download Gagal</span>');
                    
                    setTimeout(function() {
                        $('#msg-berkas').remove();
                        // root.removeAttr('disabled');
                        $('body').find('a').removeClass('disabled');
                        $('body').find('button').removeAttr('disabled');
                    }, 5000);

                    Swal.fire({
                        position: 'center',
                        icon: 'warning',
                        text: 'Silahkan Contact Administrator Rumah Sakit : '+ xhr.status,
                        showConfirmButton: false,
                        timer: 2000
                    });
                }
            });

        });
    }

});
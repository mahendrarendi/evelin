$(document).ready(function(){

    simpan_pengguna();
    function simpan_pengguna(){

        $('#frm-add-pengguna').on('keyup','input[name="username"]',function(e){
            let key_user = $(this).val().replace(/\s/g, "").replace(/(?!^-)-/g, '');
            let new_username = $(this).val(key_user);
        });

        $('#frm-add-pengguna').on('keyup','input[name="confirm_password"]',function(e){
            let root = $(this);
            let pass = $('#frm-add-pengguna').find('input[name="password"]').val();
            let pass_confirm = root.val();
            
            if( pass.length == '0' ){
                root.val('');
            }else{
                if(pass == pass_confirm){
                    $('.err-msg').html('<small class="text-success msg" style="font-size:11px;"><em>Password Sama</em></small>');
                    $('button.simpan').removeAttr('disabled');
                }else{
                    $('.err-msg').html('<small class="text-danger msg" style="font-size:11px;"><em>Password Tidak Sama</em></small>');
                    $('button.simpan').attr('disabled',true);
                }
            }
        });

        $('body').on('submit','#frm-add-pengguna',function(e){
            e.preventDefault();
            let root = $(this),
                data = root.serializeArray();

            let csrfname = root.find('input[type="hidden"]').attr('name');
            let csrfvalue = root.find('input[name="csrf_evelin_security"]').val();

                data.push({name: [csrfname],value: csrfvalue});
                
                $.ajax({
                    url : base_url+'cusers/simpan',
                    data: data,
                    type: 'POST',
                    dataType: 'JSON',
                    beforeSend: function(){
                        $('.err-msgs').html(`<div class="alert alert-primary alert-dismissible fade show" role="alert">
                            <div class="spinner-border spinner-border-sm" role="status"></div>
                            <span class="sr-only"> Mohon Tunggu Sebentar ...</span>
                        </div>`);
                        root.find('button.simpan').attr('disabled',true);
                    },
                    success: function(response){
                        console.log(response);
                        if( response.msg == 'success' ){
                            $('.err-msgs').html(`<div class="alert alert-`+response.msg+` alert-dismissible fade show" role="alert">
                                <i class="fa fa-check-circle" aria-hidden="true"></i> <span class="txt"> `+response.txt+`</span>
                            </div>`);
                            setTimeout(function() { 
                                window.location.href = response.url;
                            }, 1000);
                        }else{
                            $('.err-msgs').html(`<div class="alert alert-`+response.msg+` alert-dismissible fade show" role="alert">
                                <i class="fa fa-exclamation-triangle" aria-hidden="true"></i> <span class="txt"> `+response.txt+`</span>
                            </div>`);
                            root.find('button.simpan').removeAttr('disabled');

                            return false;
                        }
                    },
                    error:function(xhr){
                        $('.err-msgs').html(`<div class="alert alert-warning alert-dismissible fade show" role="alert">
                                <i class="fa fa-exclamation-triangle" aria-hidden="true"></i> <span class="txt"> Mohon hubungi Developer :`+xhr.status+`</span>
                        </div>`);
                        root.find('button.simpan').removeAttr('disabled');
                        return false;
                    }
                });

        });
    }

    delete_user();
    function delete_user(){
        $('body').on('click','.delete-user',function(e){
            e.preventDefault();
            let root = $(this);
            let id   = root.data('id');
            let nama = root.data('nama');
            let namehash = root.data('namehash');
            let hash = root.data('hash');

            Swal.fire({
                text: "Apakah Anda Yakin Menghapus Pengguna "+nama+ " ?",
                icon: 'warning',
                confirmButtonText: 'Iya',
                cancelButtonText: 'Tidak',
                showCancelButton: true,
                confirmButtonColor: '#fb6340',
                cancelButtonColor: '#f5365c',
              }).then((result) => {
                if (result.isConfirmed) {
                    $.ajax({
                        url:base_url+'cusers/delete',
                        data:{'id':id,[namehash]:hash},
                        type:'POST',
                        dataType:'JSON',
                        beforeSend: function(){
                            root.attr('disabled',true);
                        },
                        success: function(response){
                            console.log(response);
                            if( response.msg == 'success' ){
                                message_success('Dihapus!',response.txt);
                            }else{
                                message_failed('Gagal dihapus',response.txt);
                            }
                        },
                        error: function(xhr){
                            alert('Mohon Hubungi Developer');
                            return false;
                        }
                    });
                }
            });
            
        });
    }

    handle_form_level();
    function handle_form_level(){
        $('body').on('submit','#frm-add-level',function(e){
            e.preventDefault();

            let root = $(this);
            let data = root.serializeArray();

            let csrfname = root.find('input[type="hidden"]').attr('name');
            let csrfvalue = root.find('input[name="csrf_evelin_security"]').val();

            data.push({name: [csrfname],value: csrfvalue});

            $.ajax({
                url : base_url+'cusers/simpan_level',
                data: data,
                type: 'POST',
                dataType: 'JSON',
                beforeSend: function(){
                    $('.err-msgs').html(`<div class="alert alert-primary alert-dismissible fade show" role="alert">
                        <div class="spinner-border spinner-border-sm" role="status"></div>
                        <span class="sr-only"> Mohon Tunggu Sebentar ...</span>
                    </div>`);
                    root.find('button.simpan').attr('disabled',true);
                },
                success: function(response){
                    if( response.msg == 'success' ){
                        $('.err-msgs').html(`<div class="alert alert-`+response.msg+` alert-dismissible fade show" role="alert">
                            <i class="fa fa-check-circle" aria-hidden="true"></i> <span class="txt"> `+response.txt+`</span>
                        </div>`);
                        setTimeout(function() { 
                            window.location.href = response.url;
                        }, 1000);
                    }else{
                        $('.err-msgs').html(`<div class="alert alert-`+response.msg+` alert-dismissible fade show" role="alert">
                            <i class="fa fa-exclamation-triangle" aria-hidden="true"></i> <span class="txt"> `+response.txt+`</span>
                        </div>`);
                        root.find('button.simpan').removeAttr('disabled');

                        return false;
                    }
                },
                error:function(xhr){
                    $('.err-msgs').html(`<div class="alert alert-warning alert-dismissible fade show" role="alert">
                            <i class="fa fa-exclamation-triangle" aria-hidden="true"></i> <span class="txt"> Mohon hubungi Developer :`+xhr.status+`</span>
                    </div>`);
                    root.find('button.simpan').removeAttr('disabled');
                    return false;
                }
            });

        });
    }

    delete_level();
    function delete_level(){
        $('body').on('click','.delete-level',function(e){
            e.preventDefault();
            let root = $(this);
            let id   = root.data('id');
            let nama = root.data('nama');
            let namehash = root.data('namehash');
            let hash = root.data('hash');


            Swal.fire({
                text: "Apakah Anda Yakin Menghapus "+nama+ " ?",
                icon: 'warning',
                confirmButtonText: 'Iya',
                cancelButtonText: 'Tidak',
                showCancelButton: true,
                confirmButtonColor: '#fb6340',
                cancelButtonColor: '#f5365c',
              }).then((result) => {
                if (result.isConfirmed) {
                    $.ajax({
                        url:base_url+'cusers/delete_level',
                        data:{'id':id,[namehash]:hash},
                        type:'POST',
                        dataType:'JSON',
                        beforeSend: function(){
                            root.attr('disabled',true);
                        },
                        success: function(response){
                            if( response.msg == 'success' ){
                                message_success('Dihapus!',response.txt);
                            }else{
                                message_failed('Gagal dihapus',response.txt);
                            }
                        },
                        error: function(xhr){
                            alert('Mohon Hubungi Developer');
                            return false;
                        }
                    });
                }
            });
            
        });
    }

});
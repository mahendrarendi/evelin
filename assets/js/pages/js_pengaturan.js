$(document).ready(function(){
    
    load_library();
    function load_library(){
        var date = new Date();
        var today = new Date(date.getFullYear(), date.getMonth(), date.getDate());
        var startDate = new Date(today.getFullYear(), date.getMonth(), 1);
        // var endDate = new Date(today.getFullYear(), date.getMonth() + 1, 0);
        $('.tglpengajuan').datepicker({
            format: 'mm/yyyy',
            todayHighlight : true,
            autoclose:true,
            // startDate: startDate,
            // endDate: endDate
            viewMode: "months", 
            minViewMode: "months"
        });

        let element_pengajuan = $( '.tglpengajuan' );

        if(  element_pengajuan.val() === null || element_pengajuan.val() === undefined  ){
            $( '.tglpengajuan' ).datepicker( 'setDate', today );
        }else{
            $( '.tglpengajuan' ).datepicker( 'setDate', element_pengajuan.val() );
        }

    }

    simpan_pengajuan();
    function simpan_pengajuan(){
        $('body').on('submit','#frm-pengajuan',function(e){
            e.preventDefault();
            let root = $(this);
            let data = root.serializeArray();
            let nonce = getdata_nonce();

            data.push({name: [nonce.namehash],value: nonce.hash});

            $.ajax({
                url: base_url+'cpengaturan/simpan_pengajuan',
                data:data,
                type:'POST',
                dataType:'JSON',
                success: function(response){

                    // console.log(response);
                    if( response.code == '200' ){
                        Swal.fire({
                            position: 'center',
                            icon: 'success',
                            text: response.txt,
                            showConfirmButton: false,
                            timer: 2000
                        }).then((result) => {
                            location.reload();
                        });

                    }else{
                        Swal.fire({
                            position: 'center',
                            icon: 'warning',
                            text: response.txt,
                            showConfirmButton: false,
                            timer: 2000
                        }).then((result) => {
                            location.reload();
                        });
                    }

                    elemen_nonce.val(response.hash);
                },
                error:function(xhr){
                    Swal.fire({
                        position: 'center',
                        icon: 'warning',
                        text: 'Silahkan Contact Administrator Rumah Sakit : '+ xhr.status,
                        showConfirmButton: false,
                        timer: 2000
                    });
                }
            });

        });
    }

    function getdata_nonce(){
        let elemen_nonce = $('.nonce');
        let namehash     = elemen_nonce.attr('name');
        let hash         = elemen_nonce.val();
            
        let data = {
            'namehash':namehash,
            'hash':hash
        };

        return data;
    }

});
$(document).ready(function(){
    first_load();
    function first_load(){
        if( $("table").hasClass("qldt") ){
            $('.qldt').DataTable({
                "dom": 'lBfrtip',
                "buttons": [
                    'excel'
                ],
                "stateSave": true,
                "responsive" : true,
                "language": {
                    "paginate": {
                    "previous": '<i class="fa fa-caret-left"></i>',
                    "next": '<i class="fa fa-caret-right"></i>'
                    }
                }
                
            });
        }

        if( $('select').hasClass('qlselect2') ){
            $('.qlselect2').select2({
                theme: 'bootstrap4',
            });
        }
        
        if( $('input').hasClass('qldate') ){
            var date = new Date();
            var today = new Date(date.getFullYear(), date.getMonth(), date.getDate());
            var startDate = new Date(today.getFullYear(), date.getMonth(), 1);
            var endDate = new Date(today.getFullYear(), date.getMonth() + 2, 0);

            $('.qldate').datepicker({
                format: 'dd/mm/yyyy',
                todayHighlight : true,
                autoclose:true,
                startDate: startDate,
                endDate: endDate
            });

            $( '.qldate' ).datepicker( 'setDate', today );
        }        
    }

    function message_error($msg){
        Swal.fire({
            position: 'center',
            icon: 'warning',
            text: $msg,
            showConfirmButton: false,
            timer: 2000
        });
    }

});
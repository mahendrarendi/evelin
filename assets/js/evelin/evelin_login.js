$(document).ready(function(){
    handle();
    function handle(){
        $('body').on('submit','#frm-login',function(e){
            e.preventDefault();
            let root = $(this),
                data = root.serializeArray();

            let csrfname = root.find('input[type="hidden"]').attr('name');
            let csrfvalue = root.find('input[name="csrf_evelin_security"]').val();

            data.push({name: [csrfname],value: csrfvalue});
            
            // console.log(data);
            
            $.ajax({
                url:base_url + 'clogin/handle',
                data:data,
                type:'POST',
                dataType:'JSON',
                beforeSend: function(){
                    $('.err-msgs').html(`<div class="alert alert-primary alert-dismissible fade show" role="alert">
                        <div class="spinner-border spinner-border-sm" role="status"></div>
                        <span class="sr-only"> Mohon Tunggu Sebentar ...</span>
                    </div>`);
                    root.find('button').attr('disabled',true);
                },
                success:function(response){
                    
                    // console.log(response);
                    if( response.msg == 'success' ){
                        root.find('button').html('Sedang Memproses ... ');
                        $('.err-msgs').html(`<div class="alert alert-`+response.msg+` alert-dismissible fade show" role="alert">
                                <i class="fa fa-check-circle" aria-hidden="true"></i> <span class="txt"> `+response.txt+`</span>
                            </div>`);
                        setTimeout(function() { 
                            window.location.href = response.url;
                        }, 1000);
                    }else{
                        $('.err-msgs').html(`<div class="alert alert-`+response.msg+` alert-dismissible fade show" role="alert">
                                <i class="fa fa-exclamation-triangle" aria-hidden="true"></i> <span class="txt"> `+response.txt+`</span>
                            </div>`);
                        root.find('button').removeAttr('disabled');
                        setTimeout(function() { 
                            location.reload();
                        }, 1000);
                    }
                },
                error:function(xhr){
                    $('.err-msgs').html(`<div class="alert alert-warning alert-dismissible fade show" role="alert">
                                <i class="fa fa-exclamation-triangle" aria-hidden="true"></i> <span class="txt"> Mohon hubungi Developer :`+xhr.status+`</span>
                        </div>`);
                    setTimeout(function() { 
                        location.reload();
                    }, 1000);
                    return false;
                }
            });
        });
    }

    rememberme();
    function rememberme()
    {
        if( $.cookie('user') && $.cookie('pass') ){
            $('body').find('#rememberMe').prop('checked', true);;
        }

        $('body').on('change','#rememberMe',function(e){
            let root = $(this);
            let user = $('#frm-login').find('input[name="username"]');
            let pass = $('#frm-login').find('input[name="password"]');

            if( root.is(':checked') ){
                $.cookie("user", user.val());
                user.val($.cookie('user'));
            
                $.cookie("pass", pass.val());
                pass.val($.cookie('pass'));
            }else{
                $.removeCookie('user');
                $.removeCookie('pass');
            }


            console.log($.cookie());
        });
    }
});
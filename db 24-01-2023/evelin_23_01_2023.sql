-- phpMyAdmin SQL Dump
-- version 4.9.7
-- https://www.phpmyadmin.net/
--
-- Host: localhost
-- Generation Time: Jan 24, 2023 at 02:32 PM
-- Server version: 10.6.11-MariaDB-0ubuntu0.22.04.1
-- PHP Version: 7.4.33

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `evelin`
--

-- --------------------------------------------------------

--
-- Table structure for table `billing_gabung`
--

CREATE TABLE `billing_gabung` (
  `idbillinggabung` int(11) NOT NULL,
  `norm` int(11) NOT NULL,
  `tglperiksa` date NOT NULL,
  `tglgabung` date NOT NULL,
  `idunit` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

--
-- Dumping data for table `billing_gabung`
--

INSERT INTO `billing_gabung` (`idbillinggabung`, `norm`, `tglperiksa`, `tglgabung`, `idunit`) VALUES
(2, 10010629, '2022-12-07', '2022-12-09', 0),
(5, 10136931, '2022-12-20', '2022-12-23', 15);

-- --------------------------------------------------------

--
-- Table structure for table `dokumenlainlain`
--

CREATE TABLE `dokumenlainlain` (
  `iddokumenlainlain` int(11) NOT NULL,
  `norm` int(11) NOT NULL,
  `tglperiksa` date NOT NULL,
  `name_image` longtext NOT NULL,
  `is_tampil` int(11) NOT NULL,
  `idunit` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

--
-- Dumping data for table `dokumenlainlain`
--

INSERT INTO `dokumenlainlain` (`iddokumenlainlain`, `norm`, `tglperiksa`, `name_image`, `is_tampil`, `idunit`) VALUES
(14, 10161943, '2022-12-03', 'thumb-1920-413842_optimized-1024x750.jpg', 0, 0),
(15, 10008179, '2022-12-20', '', 1, 0),
(17, 10000251, '2022-12-02', '', 0, 27),
(18, 10001907, '2022-12-01', '', 0, 10),
(24, 10055939, '2022-12-03', 'thumb-1920-413842_optimized-1024x750.jpg', 0, 10),
(25, 10006825, '2023-01-02', '', 1, 5),
(26, 10163776, '2023-01-01', '', 1, 19);

-- --------------------------------------------------------

--
-- Table structure for table `inacbg`
--

CREATE TABLE `inacbg` (
  `idinacbg` int(11) NOT NULL,
  `norm` int(11) NOT NULL,
  `tglperiksa` date NOT NULL,
  `name_inacbg` text NOT NULL,
  `idunit` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

--
-- Dumping data for table `inacbg`
--

INSERT INTO `inacbg` (`idinacbg`, `norm`, `tglperiksa`, `name_inacbg`, `idunit`) VALUES
(44, 10055939, '2022-12-03', '10055939_20221203.jpeg', 10),
(45, 10055939, '2022-12-03', '10055939_20221203.jpg', 19),
(46, 10008179, '2022-12-20', '10008179_20221220_8.jpeg', 8),
(47, 10028227, '2022-12-07', '10028227_20221207_8.jpg', 8),
(49, 10006316, '2023-01-10', '10006316_20230110_2.jpeg', 2),
(51, 10005176, '2023-01-10', '10005176_20230110_2.jpeg', 2),
(52, 10007309, '2023-01-24', '10007309_20230124_13.jpeg', 13),
(53, 10163776, '2023-01-01', '10163776_20230101_19.jpeg', 19);

-- --------------------------------------------------------

--
-- Table structure for table `pengaturan`
--

CREATE TABLE `pengaturan` (
  `idpengaturan` int(11) NOT NULL,
  `key_pengaturan` varchar(200) NOT NULL,
  `value_pengaturan` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

--
-- Dumping data for table `pengaturan`
--

INSERT INTO `pengaturan` (`idpengaturan`, `key_pengaturan`, `value_pengaturan`) VALUES
(16, '_bulan_pengajuan', '01/2023'),
(17, '_aktifkan_pengajuan', 'on');

-- --------------------------------------------------------

--
-- Table structure for table `radiologi`
--

CREATE TABLE `radiologi` (
  `idradiologi` int(11) NOT NULL,
  `norm` int(11) NOT NULL,
  `tglperiksa` date NOT NULL,
  `is_tampil` int(11) NOT NULL,
  `idunit` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

--
-- Dumping data for table `radiologi`
--

INSERT INTO `radiologi` (`idradiologi`, `norm`, `tglperiksa`, `is_tampil`, `idunit`) VALUES
(3, 10147600, '2022-11-12', 0, 0),
(4, 10002599, '2022-12-01', 0, 0),
(5, 10017607, '2022-12-15', 0, 0),
(6, 10002972, '2022-12-01', 1, 0),
(7, 10008179, '2022-12-20', 1, 0),
(9, 10147439, '2022-12-02', 0, 7),
(10, 10006825, '2023-01-02', 0, 5),
(11, 10163776, '2023-01-01', 1, 19);

-- --------------------------------------------------------

--
-- Table structure for table `radiologi_gabung`
--

CREATE TABLE `radiologi_gabung` (
  `idradiologigabung` int(11) NOT NULL,
  `norm` int(11) NOT NULL,
  `tglperiksa` date NOT NULL,
  `tglgabung` date NOT NULL,
  `idunit` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

--
-- Dumping data for table `radiologi_gabung`
--

INSERT INTO `radiologi_gabung` (`idradiologigabung`, `norm`, `tglperiksa`, `tglgabung`, `idunit`) VALUES
(2, 10147600, '2022-11-12', '2022-11-15', 0),
(5, 10002972, '2022-12-01', '2022-12-06', 0),
(9, 10136931, '2022-12-20', '2022-12-23', 15),
(16, 10106583, '2022-12-21', '2023-01-21', 10);

-- --------------------------------------------------------

--
-- Table structure for table `rencanakontrol`
--

CREATE TABLE `rencanakontrol` (
  `idrencanakontrol` int(11) NOT NULL,
  `norm` int(11) NOT NULL,
  `tglperiksa` date NOT NULL,
  `name_image` text NOT NULL,
  `is_tampil` int(11) NOT NULL,
  `idunit` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

--
-- Dumping data for table `rencanakontrol`
--

INSERT INTO `rencanakontrol` (`idrencanakontrol`, `norm`, `tglperiksa`, `name_image`, `is_tampil`, `idunit`) VALUES
(2, 10022041, '2022-12-16', '', 0, 0),
(7, 10055939, '2022-12-03', '10055939_20221203_19.jpeg', 1, 10),
(9, 10055939, '2022-12-03', '10055939_20221203_10.jpg', 1, 10),
(10, 10028227, '2022-12-07', '10028227_20221207_8.jpg', 0, 8);

-- --------------------------------------------------------

--
-- Table structure for table `rencanakontrol_new`
--

CREATE TABLE `rencanakontrol_new` (
  `idrencanakontrolnew` int(11) NOT NULL,
  `norm` int(11) NOT NULL,
  `tglperiksa` date NOT NULL,
  `is_tampil` int(11) NOT NULL,
  `idunit` int(11) NOT NULL,
  `tglentri` date NOT NULL,
  `keterangancetak` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

--
-- Dumping data for table `rencanakontrol_new`
--

INSERT INTO `rencanakontrol_new` (`idrencanakontrolnew`, `norm`, `tglperiksa`, `is_tampil`, `idunit`, `tglentri`, `keterangancetak`) VALUES
(1, 10005881, '2022-11-01', 0, 7, '2023-01-18', '12344'),
(2, 10106020, '2022-12-20', 0, 7, '2023-01-11', '343435'),
(3, 10011837, '2022-12-01', 1, 8, '2023-01-11', '12-23-2022 23 wib'),
(4, 10016217, '2023-01-12', 0, 9, '0000-00-00', '1233'),
(5, 10005176, '2023-01-21', 0, 2, '0000-00-00', ''),
(6, 10007309, '2023-01-24', 0, 13, '0000-00-00', ''),
(7, 10163776, '2023-01-01', 1, 19, '0000-00-00', '');

-- --------------------------------------------------------

--
-- Table structure for table `resume_gabung`
--

CREATE TABLE `resume_gabung` (
  `idresumegabung` int(11) NOT NULL,
  `norm` int(11) NOT NULL,
  `tglperiksa` date NOT NULL,
  `tglgabung` date NOT NULL,
  `idunit` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

--
-- Dumping data for table `resume_gabung`
--

INSERT INTO `resume_gabung` (`idresumegabung`, `norm`, `tglperiksa`, `tglgabung`, `idunit`) VALUES
(14, 10136931, '2022-12-20', '2022-12-23', 15),
(15, 10106583, '2022-12-21', '2023-01-22', 15);

-- --------------------------------------------------------

--
-- Table structure for table `rujukanbaru`
--

CREATE TABLE `rujukanbaru` (
  `idrujukanbaru` int(11) NOT NULL,
  `norm` int(11) NOT NULL,
  `tglperiksa` date NOT NULL,
  `name_rujukanbaru` text NOT NULL,
  `idunit` int(11) NOT NULL,
  `is_tampil` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

--
-- Dumping data for table `rujukanbaru`
--

INSERT INTO `rujukanbaru` (`idrujukanbaru`, `norm`, `tglperiksa`, `name_rujukanbaru`, `idunit`, `is_tampil`) VALUES
(25, 10016286, '2022-12-20', 'rjb_10016286_20221220_20.jpeg', 20, 0),
(27, 10008179, '2022-12-20', 'rjb_10008179_20221220_8.jpg', 8, 0),
(29, 10003152, '2022-12-26', 'rjb_10003152_20221226_12.jpg', 12, 0);

-- --------------------------------------------------------

--
-- Table structure for table `sep`
--

CREATE TABLE `sep` (
  `idsep` int(11) NOT NULL,
  `norm` int(11) NOT NULL,
  `tglperiksa` date NOT NULL,
  `name_image` text NOT NULL,
  `idunit` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

--
-- Dumping data for table `sep`
--

INSERT INTO `sep` (`idsep`, `norm`, `tglperiksa`, `name_image`, `idunit`) VALUES
(11, 10055939, '2022-12-03', '10055939_20221203_10.jpg', 10),
(15, 10055939, '2022-12-03', '10055939_20221203_19.jpeg', 19),
(16, 10008179, '2022-12-20', '10008179_20221220_8.jpg', 8),
(17, 10028227, '2022-12-07', '10028227_20221207_8.jpeg', 8),
(18, 10002312, '2023-01-05', '10002312_20230105_8.jpg', 8);

-- --------------------------------------------------------

--
-- Table structure for table `sep_new`
--

CREATE TABLE `sep_new` (
  `idsepnew` int(11) NOT NULL,
  `norm` int(11) NOT NULL,
  `tglperiksa` date NOT NULL,
  `cetakan` text NOT NULL,
  `idunit` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

--
-- Dumping data for table `sep_new`
--

INSERT INTO `sep_new` (`idsepnew`, `norm`, `tglperiksa`, `cetakan`, `idunit`) VALUES
(1, 10023691, '2022-12-01', '2 07-12-2022 13:24:54', 8),
(2, 10106020, '2022-12-20', '1234545667', 7),
(3, 10011837, '2022-12-01', '2 07-12-2022 13:12:00 WIB', 8),
(4, 10016217, '2023-01-12', '', 9),
(5, 10072482, '2023-01-01', 'test', 19),
(6, 10007309, '2023-01-24', '', 13);

-- --------------------------------------------------------

--
-- Table structure for table `sk_emergency`
--

CREATE TABLE `sk_emergency` (
  `idemergency` int(11) NOT NULL,
  `norm` int(11) NOT NULL,
  `tglperiksa` date NOT NULL,
  `diagnosa` text NOT NULL,
  `tindakan` text NOT NULL,
  `tglemergency` date NOT NULL,
  `is_tampil` int(11) NOT NULL,
  `idunit` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

--
-- Dumping data for table `sk_emergency`
--

INSERT INTO `sk_emergency` (`idemergency`, `norm`, `tglperiksa`, `diagnosa`, `tindakan`, `tglemergency`, `is_tampil`, `idunit`) VALUES
(10, 10002599, '2022-12-01', '', '', '0000-00-00', 0, 0),
(11, 10008179, '2022-12-20', '', '', '0000-00-00', 0, 8),
(12, 10147439, '2022-12-02', '', '', '0000-00-00', 1, 7),
(13, 10055939, '2022-12-03', '', '', '0000-00-00', 1, 10),
(14, 10007309, '2023-01-24', '', '', '0000-00-00', 1, 13);

-- --------------------------------------------------------

--
-- Table structure for table `sk_formedit`
--

CREATE TABLE `sk_formedit` (
  `idskformedit` int(11) NOT NULL,
  `skdp` varchar(100) NOT NULL,
  `nokontrol` varchar(100) NOT NULL,
  `diagnosa` text NOT NULL,
  `terapi` text NOT NULL,
  `tglkontrolkembali` date NOT NULL,
  `tglmulairujukan` date NOT NULL,
  `tglselesairujukan` date NOT NULL,
  `norm` int(11) NOT NULL,
  `tglperiksa` date NOT NULL,
  `is_tampil` int(11) NOT NULL,
  `idunit` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

--
-- Dumping data for table `sk_formedit`
--

INSERT INTO `sk_formedit` (`idskformedit`, `skdp`, `nokontrol`, `diagnosa`, `terapi`, `tglkontrolkembali`, `tglmulairujukan`, `tglselesairujukan`, `norm`, `tglperiksa`, `is_tampil`, `idunit`) VALUES
(17, '', '', '', '', '0000-00-00', '0000-00-00', '0000-00-00', 10096264, '2022-12-20', 1, 15),
(18, '', '', '', '', '2022-12-22', '0000-00-00', '0000-00-00', 10010629, '2022-12-07', 0, 0),
(19, '', '', 'post stroke\nlbp', '', '0000-00-00', '0000-00-00', '0000-00-00', 10023691, '2022-12-01', 0, 8),
(20, '', '', '', '', '2023-01-12', '2023-01-18', '2023-01-18', 10005176, '2023-01-14', 0, 8),
(21, '', '', '', '', '0000-00-00', '0000-00-00', '0000-00-00', 10007309, '2023-01-24', 0, 13),
(22, '', '', '', '', '0000-00-00', '0000-00-00', '0000-00-00', 10163776, '2023-01-01', 1, 19);

-- --------------------------------------------------------

--
-- Table structure for table `sk_inap`
--

CREATE TABLE `sk_inap` (
  `idskinap` int(11) NOT NULL,
  `norm` int(11) NOT NULL,
  `tglperiksa` date NOT NULL,
  `tglmulaidirawat` date NOT NULL,
  `tglselesaidirawat` date NOT NULL,
  `ruang` varchar(250) NOT NULL,
  `diagnosis` text NOT NULL,
  `terapi` text NOT NULL,
  `tglkontrol` date NOT NULL,
  `jam` time NOT NULL,
  `is_tampil` int(11) NOT NULL,
  `idunit` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

--
-- Dumping data for table `sk_inap`
--

INSERT INTO `sk_inap` (`idskinap`, `norm`, `tglperiksa`, `tglmulaidirawat`, `tglselesaidirawat`, `ruang`, `diagnosis`, `terapi`, `tglkontrol`, `jam`, `is_tampil`, `idunit`) VALUES
(15, 10010629, '2022-12-07', '0000-00-00', '0000-00-00', '', '', '', '2022-12-06', '00:00:00', 0, 0),
(16, 10161433, '2022-12-02', '2022-12-23', '2022-12-27', '', '', '', '0000-00-00', '00:00:00', 0, 0),
(17, 10055939, '2022-12-03', '0000-00-00', '0000-00-00', '', '', '', '0000-00-00', '00:00:00', 1, 10),
(18, 10016217, '2023-01-12', '0000-00-00', '0000-00-00', 'aaa', 'aa', 'ddd', '0000-00-00', '00:00:00', 0, 9),
(19, 10163776, '2023-01-01', '0000-00-00', '0000-00-00', '', '', '', '0000-00-00', '00:00:00', 1, 19);

-- --------------------------------------------------------

--
-- Table structure for table `sk_list_pertimbangan`
--

CREATE TABLE `sk_list_pertimbangan` (
  `idlistpertimbangan` int(11) NOT NULL,
  `idpertimbangan` int(11) NOT NULL,
  `item` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

-- --------------------------------------------------------

--
-- Table structure for table `sk_person_pertimbangan`
--

CREATE TABLE `sk_person_pertimbangan` (
  `idpersonpertimbangan` int(11) NOT NULL,
  `datecreated` datetime NOT NULL,
  `norm` int(11) NOT NULL,
  `tglperiksa` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

-- --------------------------------------------------------

--
-- Table structure for table `testpdf_convert`
--

CREATE TABLE `testpdf_convert` (
  `idtestpdf` int(11) NOT NULL,
  `norm` int(11) NOT NULL,
  `tglperiksa` int(11) NOT NULL,
  `name_image` int(11) NOT NULL,
  `idunit` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

-- --------------------------------------------------------

--
-- Table structure for table `user`
--

CREATE TABLE `user` (
  `iduser` int(11) NOT NULL,
  `nama` varchar(200) NOT NULL,
  `email` varchar(200) NOT NULL,
  `password` text NOT NULL,
  `username` varchar(200) NOT NULL,
  `created_at` datetime NOT NULL,
  `last_login` datetime NOT NULL,
  `level` int(11) NOT NULL,
  `status` int(11) NOT NULL DEFAULT 0
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

--
-- Dumping data for table `user`
--

INSERT INTO `user` (`iduser`, `nama`, `email`, `password`, `username`, `created_at`, `last_login`, `level`, `status`) VALUES
(2, 'ikhsan', 'ikhsan@mail.com', '$2y$10$4SHOG0Lawp9LThWA8B5GDenHRvEJsuf.rourIq5U9dHL/3reNMGz.', 'ikhsanganteng', '2022-08-09 10:36:25', '2023-01-21 14:49:37', 1, 0),
(6, 'USER BPJS 1', 'bpjs1@gmail.com', '$2y$10$4SHOG0Lawp9LThWA8B5GDenHRvEJsuf.rourIq5U9dHL/3reNMGz.', 'bpjs', '2022-08-10 09:41:00', '2023-01-24 12:41:42', 2, 0),
(11, 'superuser', 'superuser@mail.com', '$2y$10$bDRfeg4anMYMqEnjNXAaLe5N/rSr7O4tqTDuFivU.njvjqalEaAjm', 'superuser', '2022-10-13 14:41:06', '2023-01-24 12:40:45', 1, 0),
(12, 'IGD', 'igd@mail.com', '$2y$10$cMVTQTELry5N5msTqQ/8ce6wFr31Q2D0.2EY5yz.bXjAIEg5wPMFi', 'igd', '2022-11-03 11:15:15', '2023-01-02 11:53:11', 11, 0),
(13, 'Poli Umum', 'poli@mail.com', '$2y$10$tAHIlQ4xeYXvjke7z553DezOnGy6xyJ98yIHigY8VTPkN1tKVOww.', 'poliumum', '2022-11-29 14:24:18', '2022-11-29 19:02:36', 10, 0),
(14, 'Klaim', 'klaim@gmail.com', '$2y$10$AtGbww78CS25lERnhwxqTe4/6ms96LdJ0vnjg5tscA6doJbNScE9q', 'klaim', '2022-11-29 17:42:06', '2023-01-24 13:46:21', 9, 1),
(15, 'pendaftaran', 'pendaftaran@mail.com', '$2y$10$me4JOyLUQm54yEX.URWBqej/IIcoY1ycnuyJi6zuWvLxjj9UV3x62', 'pendaftaran', '2022-12-01 08:33:54', '2023-01-02 11:37:20', 12, 0);

-- --------------------------------------------------------

--
-- Table structure for table `user_level`
--

CREATE TABLE `user_level` (
  `idlevel` int(11) NOT NULL,
  `level_name` varchar(200) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

--
-- Dumping data for table `user_level`
--

INSERT INTO `user_level` (`idlevel`, `level_name`) VALUES
(1, 'superadmin'),
(2, 'bpjs'),
(9, 'klaim'),
(10, 'poliumum'),
(11, 'igd'),
(12, 'pendaftaran');

-- --------------------------------------------------------

--
-- Table structure for table `verif_klaim`
--

CREATE TABLE `verif_klaim` (
  `idverif` int(11) NOT NULL,
  `datecreated` datetime NOT NULL,
  `sep` varchar(200) NOT NULL,
  `tglperiksa` date NOT NULL,
  `norm` int(11) NOT NULL,
  `idunit` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

--
-- Dumping data for table `verif_klaim`
--

INSERT INTO `verif_klaim` (`idverif`, `datecreated`, `sep`, `tglperiksa`, `norm`, `idunit`) VALUES
(29, '2023-01-21 10:19:55', '0179R0140123V000208', '2023-01-02', 10000251, 27),
(30, '2023-01-21 10:19:55', '0179R0140123V000164', '2023-01-02', 10001280, 10),
(32, '2023-01-21 10:19:57', '0179R0140123V000042', '2023-01-02', 10006393, 8),
(33, '2023-01-21 10:19:57', '0179R0140123V000036', '2023-01-02', 10006825, 5),
(35, '2023-01-21 10:22:55', '0179R0140123V000239', '2023-01-02', 10010444, 34),
(36, '2023-01-21 10:22:56', '0179R0140123V000201', '2023-01-02', 10015386, 27),
(37, '2023-01-21 10:22:56', '0179R0140123V000188', '2023-01-02', 10016340, 27),
(38, '2023-01-21 10:22:58', '0179R0140123V000205', '2023-01-02', 10019683, 27),
(39, '2023-01-21 10:23:10', '0179R0140123V000329', '2023-01-02', 10019960, 13),
(40, '2023-01-21 10:23:11', '0179R0140123V000047', '2023-01-02', 10025145, 8),
(57, '2023-01-24 12:29:35', '0179R0140123V005793', '2023-01-24', 10007309, 13),
(58, '2023-01-24 12:32:54', '0179r0140123v000006', '2023-01-01', 10072482, 19),
(59, '2023-01-24 12:32:55', '0179R0140123V000001', '2023-01-01', 10163776, 19);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `billing_gabung`
--
ALTER TABLE `billing_gabung`
  ADD PRIMARY KEY (`idbillinggabung`);

--
-- Indexes for table `dokumenlainlain`
--
ALTER TABLE `dokumenlainlain`
  ADD PRIMARY KEY (`iddokumenlainlain`);

--
-- Indexes for table `inacbg`
--
ALTER TABLE `inacbg`
  ADD PRIMARY KEY (`idinacbg`);

--
-- Indexes for table `pengaturan`
--
ALTER TABLE `pengaturan`
  ADD PRIMARY KEY (`idpengaturan`);

--
-- Indexes for table `radiologi`
--
ALTER TABLE `radiologi`
  ADD PRIMARY KEY (`idradiologi`);

--
-- Indexes for table `radiologi_gabung`
--
ALTER TABLE `radiologi_gabung`
  ADD PRIMARY KEY (`idradiologigabung`);

--
-- Indexes for table `rencanakontrol`
--
ALTER TABLE `rencanakontrol`
  ADD PRIMARY KEY (`idrencanakontrol`);

--
-- Indexes for table `rencanakontrol_new`
--
ALTER TABLE `rencanakontrol_new`
  ADD PRIMARY KEY (`idrencanakontrolnew`);

--
-- Indexes for table `resume_gabung`
--
ALTER TABLE `resume_gabung`
  ADD PRIMARY KEY (`idresumegabung`);

--
-- Indexes for table `rujukanbaru`
--
ALTER TABLE `rujukanbaru`
  ADD PRIMARY KEY (`idrujukanbaru`);

--
-- Indexes for table `sep`
--
ALTER TABLE `sep`
  ADD PRIMARY KEY (`idsep`);

--
-- Indexes for table `sep_new`
--
ALTER TABLE `sep_new`
  ADD PRIMARY KEY (`idsepnew`);

--
-- Indexes for table `sk_emergency`
--
ALTER TABLE `sk_emergency`
  ADD PRIMARY KEY (`idemergency`);

--
-- Indexes for table `sk_formedit`
--
ALTER TABLE `sk_formedit`
  ADD PRIMARY KEY (`idskformedit`);

--
-- Indexes for table `sk_inap`
--
ALTER TABLE `sk_inap`
  ADD PRIMARY KEY (`idskinap`);

--
-- Indexes for table `sk_list_pertimbangan`
--
ALTER TABLE `sk_list_pertimbangan`
  ADD PRIMARY KEY (`idlistpertimbangan`);

--
-- Indexes for table `sk_person_pertimbangan`
--
ALTER TABLE `sk_person_pertimbangan`
  ADD PRIMARY KEY (`idpersonpertimbangan`);

--
-- Indexes for table `user`
--
ALTER TABLE `user`
  ADD PRIMARY KEY (`iduser`);

--
-- Indexes for table `user_level`
--
ALTER TABLE `user_level`
  ADD PRIMARY KEY (`idlevel`);

--
-- Indexes for table `verif_klaim`
--
ALTER TABLE `verif_klaim`
  ADD PRIMARY KEY (`idverif`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `billing_gabung`
--
ALTER TABLE `billing_gabung`
  MODIFY `idbillinggabung` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `dokumenlainlain`
--
ALTER TABLE `dokumenlainlain`
  MODIFY `iddokumenlainlain` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=27;

--
-- AUTO_INCREMENT for table `inacbg`
--
ALTER TABLE `inacbg`
  MODIFY `idinacbg` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=54;

--
-- AUTO_INCREMENT for table `pengaturan`
--
ALTER TABLE `pengaturan`
  MODIFY `idpengaturan` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=18;

--
-- AUTO_INCREMENT for table `radiologi`
--
ALTER TABLE `radiologi`
  MODIFY `idradiologi` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;

--
-- AUTO_INCREMENT for table `radiologi_gabung`
--
ALTER TABLE `radiologi_gabung`
  MODIFY `idradiologigabung` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=17;

--
-- AUTO_INCREMENT for table `rencanakontrol`
--
ALTER TABLE `rencanakontrol`
  MODIFY `idrencanakontrol` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;

--
-- AUTO_INCREMENT for table `rencanakontrol_new`
--
ALTER TABLE `rencanakontrol_new`
  MODIFY `idrencanakontrolnew` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT for table `resume_gabung`
--
ALTER TABLE `resume_gabung`
  MODIFY `idresumegabung` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=16;

--
-- AUTO_INCREMENT for table `rujukanbaru`
--
ALTER TABLE `rujukanbaru`
  MODIFY `idrujukanbaru` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=30;

--
-- AUTO_INCREMENT for table `sep`
--
ALTER TABLE `sep`
  MODIFY `idsep` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=19;

--
-- AUTO_INCREMENT for table `sep_new`
--
ALTER TABLE `sep_new`
  MODIFY `idsepnew` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `sk_emergency`
--
ALTER TABLE `sk_emergency`
  MODIFY `idemergency` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=15;

--
-- AUTO_INCREMENT for table `sk_formedit`
--
ALTER TABLE `sk_formedit`
  MODIFY `idskformedit` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=23;

--
-- AUTO_INCREMENT for table `sk_inap`
--
ALTER TABLE `sk_inap`
  MODIFY `idskinap` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=20;

--
-- AUTO_INCREMENT for table `sk_list_pertimbangan`
--
ALTER TABLE `sk_list_pertimbangan`
  MODIFY `idlistpertimbangan` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=38;

--
-- AUTO_INCREMENT for table `sk_person_pertimbangan`
--
ALTER TABLE `sk_person_pertimbangan`
  MODIFY `idpersonpertimbangan` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=27;

--
-- AUTO_INCREMENT for table `user`
--
ALTER TABLE `user`
  MODIFY `iduser` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=16;

--
-- AUTO_INCREMENT for table `user_level`
--
ALTER TABLE `user_level`
  MODIFY `idlevel` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;

--
-- AUTO_INCREMENT for table `verif_klaim`
--
ALTER TABLE `verif_klaim`
  MODIFY `idverif` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=60;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;

<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Clogin extends CI_Controller 
{
    protected $tb_users = 'user';
    const SESSION_KEY   = 'iduser';
    const SESSION_LEVEL = 'level';

    function __construct(){
	    parent::__construct();

        $this->load->model('mauth');
    

        $this->load->model('msqlbasic');
        $this->load->library('form_validation');
        $this->load->library('encryption'); 
    }

    public function index()
    {
        //remove session
        $this->mauth->logout();

        $csrf = array(
            'name' => $this->security->get_csrf_token_name(),
            'hash' => $this->security->get_csrf_hash()
        );

        $data = [
            'title_page'    => 'Login',
            'mode'          => '',
            'active_menu'   => 'login',
            'active_sub_menu' => '',
            'csrf'           => $csrf,
            'active_menu_level' => '',
            // 'plugins'       => [PLUGIN_DATATABLE,PLUGIN_SELECT2],
            // 'scripts_js'    => ['js_dashboard'],
        ];

        
        $this->load->view('login/v_login',$data);
    }

    public function handle()
    {
      
        $config = array(  
            array(
                    'field' => 'username',
                    'label' => 'Username',
                    'rules' => 'required',
                    'errors' => array(
                        'required' => 'Mohon isi kolom %s Terlebih dahulu ',
                    ),
            ),
            array(
                    'field' => 'password',
                    'label' => 'Password',
                    'rules' => 'required',
                    'errors' => array(
                            'required' => 'Mohon isi kolom %s Terlebih dahulu ',
                    ),
            ),
            
        );

        $this->form_validation->set_rules($config);

        if ($this->form_validation->run() == FALSE){
            $return = ['msg'=>'danger','txt'=>validation_errors()];
            echo json_encode($return);
        }else{
            $post       = $this->input->post();
            $username   = $this->security->sanitize_filename($post['username']);
            $password   = $this->security->sanitize_filename($post['password']);
            
            $cek_username = $this->msqlbasic->row_db($this->tb_users,['username'=>$username]);

            if( empty($cek_username) ){
                $return = ['msg'=>'danger','txt'=>'Maaf User Tidak Ditemukan'];
                echo json_encode($return);
            }else{
                if( $username == $cek_username->username ){
                    if(password_verify($password, $cek_username->password)){
                        
                        $this->session->set_userdata([self::SESSION_KEY => encrypt_url($cek_username->iduser)]);
                        $this->session->set_userdata([self::SESSION_LEVEL => $cek_username->level]);
                        
                        $this->mauth->update_last_login($cek_username->iduser);

                        $has_session = $this->session->has_userdata(self::SESSION_KEY);

                        if( $has_session ){
                            $return = ['msg'=>'success','txt'=>'Berhasil','url'=>base_url('listvclaim')];
                        }else{
                            $return = ['msg'=>'danger','txt'=>'Anda Gagal Login'];
                        }

                        echo json_encode($return);

                    }else{
                        $return = ['msg'=>'danger','txt'=>'Maaf Password Salah'];
                        echo json_encode($return);
                    }// end password
                }else{
                    $return = ['msg'=>'danger','txt'=>'Maaf Username Salah'];
                    echo json_encode($return);
                }   // end username == username
            } // end cek username
        } // end validation
    }

    public function logout()
	{
		$this->mauth->logout();
		redirect(base_url());
	}

} 
?>
<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Cdashboard extends CI_Controller 
{
    function __construct(){
	    parent::__construct();
        
        $this->load->model('mauth');
        
        if(!$this->mauth->current_user()){
			redirect('login_evelin');
		}

        
    }

    public function index()
    {

        $data = [
            'content_view'  => 'beranda/v_dashboard',
            'title_page'    => 'Dashboard',
            'mode'          => '',
            'active_menu'   => 'dashboard',
            'active_sub_menu' => '',
            'active_menu_level' => '',
            'plugins'       => [PLUGIN_DATATABLE,PLUGIN_SELECT2],
            'scripts_js'    => ['js_dashboard'],
        ];

        
        $this->load->view('v_index',$data);
    }
} 
?>
<?php
defined('BASEPATH') OR exit('No direct script access allowed');

use Dompdf\Dompdf;

class Cvclaims extends CI_Controller 
{
    
    protected $key_bulanpengajuan    = '_bulan_pengajuan';
    protected $key_aktifkanpengajuan = '_aktifkan_pengajuan';

    public $pathpdf                  = FCPATH . 'assets/pdf';

    function __construct(){
	    parent::__construct();
        $this->load->model('mauth');
        $this->load->model('mvclaim');
        $this->load->model('msqlbasic');
        $this->load->model('mpengaturan');

        $this->load->library('encryption'); 
        $this->load->library('zip');

        if(!$this->mauth->current_user()){
			redirect('login_evelin');
		}

        $dir = $this->pathpdf.'/rajal';

        // Delete backups older than 7 days
        // 3600*24*7
        // delete 1 jam 3600 seccond
        /** setiap 1 hari sekali */
        delete_older_than($dir, 3600*24);
        
    }

    public function list()
    {

        if( is_klaim() || is_bpjs() ){
            $list_vclaim = $this->qlwebapi->api_vclaim_list_selesai();
        }else{
            $list_vclaim = $this->qlwebapi->api_vclaim_list();
        }        
        
        $get_poli = $this->qlwebapi->api_vclaim_poli()['metadata']['response'];
        
        $csrf = array(
            'name' => $this->security->get_csrf_token_name(),
            'hash' => $this->security->get_csrf_hash()
        );

        $row_pengajuan       = $this->mpengaturan->get_pengaturan($this->key_bulanpengajuan);
        $row_aktifpengajuan  = $this->mpengaturan->get_pengaturan($this->key_aktifkanpengajuan);
        
        

        $bulanpengajuan = '';
        if( !empty( $row_pengajuan ) ){
            $explode_bulan    = explode('/',$row_pengajuan->value_pengaturan);
            $bulan = $explode_bulan[0];
            $tahun = $explode_bulan[1];
            $bulanpengajuan   =  $tahun.'-'.$bulan;
        }

        $aktifpengajuan = '';
        if( !empty( $row_pengajuan ) ){
            $aktifpengajuan   = $row_aktifpengajuan->value_pengaturan;
        }

        $data = [
            'content_view'  => 'vclaim/v_list_claim',
            'title_page'    => 'List Vclaim',
            'mode'          => '',
            'active_menu'   => 'vclaim',
            'active_sub_menu' => '',
            'csrf'          => $csrf,
            'active_menu_level' => '',
            'bulanpengajuan' => $bulanpengajuan,
            'aktifpengajuan' => $aktifpengajuan,
            'listvclaim'    => $list_vclaim,
            'getpoli'       => $get_poli,
            'plugins'       => [PLUGIN_DATATABLE,PLUGIN_SWEETALERT,PLUGIN_SELECT2,PLUGIN_DATEPICKER],
            'scripts_js'    => ['js_vclaims'],
        ];

        $this->load->view('v_index',$data);
    }

    public function detail($norm_tgl = null)
    {

        $decrypt_normtgl = decrypt_url($norm_tgl);

        $csrf = array(
            'name' => $this->security->get_csrf_token_name(),
            'hash' => $this->security->get_csrf_hash()
        );

        $explode_decrypt = explode('_',$decrypt_normtgl);
        $norm            = $explode_decrypt[0];
        $tglperiksa      = date('Y-m-d',$explode_decrypt[1]);
        $idunit          =$explode_decrypt[2];

        $params          = ['norm'=>$norm,'tglperiksa'=>$tglperiksa,'idunit'=>$idunit];
        
        $cek_sep_oldinput       = $this->cek_upload_old( 'sep',$params );
        $cek_inacbg_oldinput    = $this->cek_upload_old( 'inacbg',$params );
        $cek_rencana_oldinput   = $this->cek_upload_old( 'rencanakontrol',$params );
        $cek_rujukanbaru_oldinput   = $this->cek_upload_old( 'rujukanbaru',$params );
        
        $data_bpjs      = $this->qlwebapi->api_vclaim_sep($norm,$tglperiksa,$idunit)['metadata']['response'];
        
        $data_resume = [];
        $jns_rujukan = '';
        $cek_rencana_kontrol = false;
        if( !empty($this->qlwebapi->api_vclaim_resume($norm,$tglperiksa,$idunit) ) ){
            $data_resume    = $this->qlwebapi->api_vclaim_resume($norm,$tglperiksa,$idunit)['metadata']['response']; 
            $jns_rujukan = $data_resume['jenisrujukan'];
            if(akses_jenispemeriksaan($jns_rujukan) != 'rujukanbaru'){
                $data_rencanakontrol = $this->qlwebapi->api_vclaim_rencanakontrol($norm,$tglperiksa)['metadata']['response'];
                $cek_rencana_kontrol = ( $data_rencanakontrol['metaData']['code'] == 200) ? true : false;
            }
        }
        
        // $data_riwayat_resume_skdp = '';
        if( !empty( $this->qlwebapi->api_vclaim_suratkontrol( $norm,$tglperiksa ) ) ){
            $data_riwayat_resume_skdp = $this->qlwebapi->api_vclaim_suratkontrol( $norm,$tglperiksa )['metadata']['response'];
        }else{
            $data_riwayat_resume_skdp = $data_resume;
        }

        $data_radiologi = [];
        $found_ro = '';
        if( !empty($this->qlwebapi->api_vclaim_radiologi($norm,$tglperiksa,$idunit)) ){
            $data_radiologi = $this->qlwebapi->api_vclaim_radiologi($norm,$tglperiksa,$idunit)['metadata']['response'];
            $found_ro = $this->cek_pemerisaanradiologi($data_radiologi);
        }

        $data_nota = [];
        if( !empty($this->qlwebapi->api_vclaim_nota( $norm,$tglperiksa,$idunit )) ){
            $data_nota      = $this->qlwebapi->api_vclaim_nota( $norm,$tglperiksa,$idunit )['metadata']['response'];
        }

        # cek is_tampil
        $is_tampil_skinap       = $this->mvclaim->is_tampil_suratkontrolinap($params);
        $is_tampil_emergensi    = $this->mvclaim->is_tampil_emergensi($params);
        $is_tampil_radiologi    = $this->mvclaim->is_tampil_radiologi($params);
        $is_tampil_dokumenlainllain = $this->mvclaim->is_tampil_dokumenlainlain($params);
        $is_tampil_rencanakontrol   = $this->mvclaim->is_tampil_rencanakontrol($params);
        $is_tampil_rujukanbaru      = $this->mvclaim->is_tampil_rujukanbaru($params);
        $is_tampil_skdp         = $this->mvclaim->is_tampil_skdp($params);


        $cek_resumegabung       = $this->mvclaim->get_resume_gabung($params); 
        $cek_radiologigabung    = $this->mvclaim->get_radiologi_gabung($params); 
        $cek_billinggabung      = $this->mvclaim->get_billing_gabung($params); 

        $template_resumegabung = '';
        if( !empty($cek_resumegabung) ){
            if( !empty( $this->qlwebapi->api_vclaim_resume($norm,$cek_resumegabung->tglgabung,$cek_resumegabung->idunit) ) ){
                $data_resume_gabung     = $this->qlwebapi->api_vclaim_resume($norm,$cek_resumegabung->tglgabung,$cek_resumegabung->idunit)['metadata']['response'];
                $template_resumegabung  = $this->template_resume($data_resume_gabung);
            }
        }
        
        $template_radiologigabung = '';
        $found_ro_gabung = '';
        if( !empty($cek_radiologigabung) ){
            if( !empty($this->qlwebapi->api_vclaim_radiologi($norm,$cek_radiologigabung->tglgabung,$cek_radiologigabung->idunit)) ){
                $data_ro                   = $this->qlwebapi->api_vclaim_radiologi($norm,$cek_radiologigabung->tglgabung,$cek_radiologigabung->idunit)['metadata']['response'];
                $template_radiologigabung  = $this->template_radiologi($data_ro);
                $found_ro_gabung = $this->cek_pemerisaanradiologi($data_ro);
            }
        }

        $template_billinggabung = '';
        if( !empty($cek_billinggabung) ){
            if( !empty($this->qlwebapi->api_vclaim_nota( $norm,$cek_billinggabung->tglgabung,$cek_billinggabung->idunit )) ){
                $data_billing            = $this->qlwebapi->api_vclaim_nota( $norm,$cek_billinggabung->tglgabung,$cek_billinggabung->idunit )['metadata']['response'];
                $template_billinggabung  = $this->template_nota($data_billing);
            }
        }

        $template_sep               = $this->template_sep($data_bpjs,$params);
        $template_sep_new           = $this->template_sep_new($params,$idunit,$cek_sep_oldinput);
        $template_resume            = $this->template_resume($data_resume);

        // $template_rencanakontrol    = $this->template_rencanakontrol($data_rencanakontrol);
        
        $template_rencanakontrol_new= $this->template_rencanakontrol_new($params,$idunit,$cek_rencana_oldinput);
        $template_radiologi         = $this->template_radiologi( $data_radiologi );
        $template_nota              = $this->template_nota( $data_nota );
        $template_suratkontrol      = $this->template_suratkontrol( $data_riwayat_resume_skdp, $params);
        $template_suratkontrolinap  = $this->template_suratkontrolinap($data_resume,$params);
        $template_emergency         = $this->template_emergency($data_resume,$params);
        $template_incbg             = $this->template_incbg($params,$idunit,$cek_inacbg_oldinput);
        $template_rujukanbaru       = $this->template_rujukanbaru($params,$idunit,$cek_rujukanbaru_oldinput);
        $template_dokumenlainlain   = $this->template_dokumenlainlain($params);

        /**
         * TEST PDF
         */
        $template_testpdf           = $this->template_testpdf($params,$idunit);

        $data = [
            'norm' => $norm,
            'tglperiksa' => $tglperiksa,
            'csrf' => $csrf,
            'content_view'  => 'vclaim/v_detail_claim',
            'title_page'    => 'Detail Vclaim',
            'mode'          => '',
            'active_menu'   => 'vclaim',
            'cek_rencana_kontrol' => $cek_rencana_kontrol,
            'jenisrujukan' => $jns_rujukan,
            'template_sep'  => $template_sep,
            'template_resume' => $template_resume,
            'template_radiologi'=> $template_radiologi,
            // 'template_rencanakontrol' => $template_rencanakontrol,
            'template_nota' => $template_nota,
            'template_suratkontrol' => $template_suratkontrol,
            'template_suratkontrolinap' => $template_suratkontrolinap,
            'template_emergency' => $template_emergency,
            'template_incbg' => $template_incbg,
            'template_rujukanbaru' => $template_rujukanbaru,
            'template_sep_new' => $template_sep_new,
            'template_rencanakontrol_new' => $template_rencanakontrol_new,
            'template_dokumenlainlain' => $template_dokumenlainlain,
            'template_resumegabung' => $template_resumegabung,
            'template_radiologigabung' => $template_radiologigabung,
            'template_billinggabung' => $template_billinggabung,
            'is_tampil_skinap' => $is_tampil_skinap,
            'is_tampil_emergensi' => $is_tampil_emergensi,
            'is_tampil_radiologi' => $is_tampil_radiologi,
            'is_tampil_dokumenlainllain' => $is_tampil_dokumenlainllain,
            'is_tampil_rencanakontrol' => $is_tampil_rencanakontrol,
            'is_tampil_rujukanbaru' => $is_tampil_rujukanbaru,
            'is_tampil_skdp'    => $is_tampil_skdp,
            'idunit'   => $idunit,
            'found_ro_gabung' => $found_ro_gabung,
            'found_ro' => $found_ro,
            'active_sub_menu' => '',
            'data_bpjs'     => $data_bpjs,
            'active_menu_level' => '',
            'plugins'       => [PLUGIN_DATATABLE,PLUGIN_SELECT2,PLUGIN_DATEPICKER,PLUGIN_SWEETALERT,PLUGIN_ZOOMIMAGE],
            'scripts_js'    => ['js_vclaims'],
            'template_testpdf' => $template_testpdf,
        ];

        // $this->load->view('v_index',$data);
        $this->parser->parse('v_index',$data);
    }

    public function cek_upload_old( $nama_table,$params )
    {
        $where = [
            'norm'=>$params['norm'],
            'tglperiksa'=> $params['tglperiksa'],
            'idunit'=> 0,
        ];
        $cek_sep  = $this->msqlbasic->row_db($nama_table,$where); 
        $found    = ( empty($cek_sep) ) ? false : true; 
        return $found;
    }

    public function cek_pemerisaanradiologi($data_radiologi)
    {
        $t = $data_radiologi;

        $e = $t['identitas']['keteranganradiologi'] . ("" == $t['identitas']['saranradiologi'] ? "" : "<b>Saran:</b>"). $t['identitas']['saranradiologi'];
        $i = "";
        $d = "";
        
        $cek_saran = ( empty( $t['ketradiologi']['keteranganradiologi'] ) ? $e : $t['ketradiologi']['keteranganradiologi'] );

        foreach( $t['diagnosa'] as $dg ){ $i .= '<span style="display:block;">' . $dg['icd'] . " " . $dg['namaicd']; }
        foreach( $t['elektromedik'] as $em ){ $d .= '<span style="display:block;">' . $em['icd'] . " " . $em['namaicd']; }

        $found = FALSE;
        if( !empty($cek_saran) || !empty($d) ){
            $found = TRUE;
        }

        return $found;
    }

    public function addpertimbangan()
    {
        
        $hash       = $this->security->get_csrf_hash();
        $insert_id  = $this->mvclaim->insert_personpertimbangan();
        
        $post = $this->input->post();
        $params = ['norm'=>$post['norm'],'tglperiksa'=>format_tanggal( str_replace('/','-',$post['tglperiksa']) )];

        $loop_table_pertimbangan = $this->template_pertimbangan_loop($params);

        if( $insert_id ){
            $return = ['msg'=>'success','hsl'=>$loop_table_pertimbangan,'code'=>200,'hash'=>$hash];
        }else{
            $return = ['msg'=>'failed','code'=>201,'hash'=>$hash];
        }

        echo json_encode( $return );
    }

    public function deletepertimbangan(){
        $hash    = $this->security->get_csrf_hash();

        $delete  = $this->mvclaim->delete_pertimbangan();
        
        
        if( $delete ){
            $return = ['msg'=>'success','code'=>200,'hash'=>$hash];
        }else{
            $return = ['msg'=>'failed','code'=>201,'hash'=>$hash];
        }

        echo json_encode( $return );

    }

    public function keyup_suratkontrol()
    {
        $hash    = $this->security->get_csrf_hash();

        $query = $this->mvclaim->insert_sk_formedit();

        $return = ['msg'=>'success','code'=>200,'hash'=>$hash];
        
        echo json_encode( $return );

    }

    public function keyup_suratkontrolinap()
    {
        $hash    = $this->security->get_csrf_hash();

        $query = $this->mvclaim->insert_suratkontrolinap();

        $return = ['msg'=>'success','code'=>200,'test'=>$query,'hash'=>$hash];
        
        echo json_encode( $return );

    }
    
    public function keyup_emergency()
    {
        $hash    = $this->security->get_csrf_hash();

        $query = $this->mvclaim->insert_emergency();

        $return = ['msg'=>'success','code'=>200,'hash'=>$hash];
        
        echo json_encode( $return );
    }

    /**
     * change
     * [+] idunit
     */
    public function upload_file_image()
    {
        $hash    = $this->security->get_csrf_hash();

        if( $this->input->method() == 'post' ){

            if( !empty($_FILES) ){
                $file   = $_FILES['fileimage'];
                $post   = $this->input->post();
                
                $name   = $file['name'];
                $type   = $file['type']; //application/pdf
                $tmp    = $file['tmp_name'];
                $size   = $file['size'];
                $ext    = pathinfo($name, PATHINFO_EXTENSION);

                $foldername = str_replace( 'frm-','',$post['elemen_id'] );
                
                $norm         = $post['norm'];
                $tglperiksa   = format_tanggal( str_replace('/','-',$post['tglperiksa']) ); //Y-m-d
                $idunit       = $post['idunit'];

                $params       = ['norm'=>$norm,'tglperiksa'=>$tglperiksa,'idunit'=>$idunit];
                
                $path         = FCPATH.'assets/'.$foldername;
                
                $file_name    = $norm.'_'.str_replace('-','',$tglperiksa).'_'.$idunit;
                $path_file    = $path.'/'.$file_name.'.'.$ext;

                $config['upload_path']          = $path;
                $config['allowed_types']        = 'jpg|jpeg';
                $config['file_name']            = $file_name;
                $config['overwrite']            = true;
                $config['max_size']             = 8000; //8MB
                
                $row_inacbg = $this->mvclaim->tampil_upload_file_image($foldername,$params,$idunit);

                // print_r( $path_file );
                if(!empty($row_inacbg)){
                    exec( chmod($path_file,0777) );
                    unlink($path_file);
                }

                $this->load->library('upload', $config);

                if ( !$this->upload->do_upload('fileimage') ) {
                    $error  = strip_tags($this->upload->display_errors());
                    $return = ['msg'=>'failed','code'=>'202','txt'=>$error,'hash'=>$hash];
                    
                }else {
                    $uploaded_data = $this->upload->data();
                    
                    $zfile = $uploaded_data['full_path']; // get file path
                    exec(chmod($zfile,0777)); // CHMOD file or any other permission level(s)
        
                    $data_inacbg = [
                        'norm'          => $norm,
                        'tglperiksa'    => $tglperiksa,
                        'name_image'    => $uploaded_data['file_name'],
                        'idunit'        => $idunit,
                    ];

                    $insert = $this->mvclaim->insert_upload_file_image($data_inacbg,$foldername,$params);

                    if( $insert ){
                        $return = ['msg'=>'success','code'=>'200','txt'=>'Berhasil di Upload','hash'=>$hash];
                    }else{
                        $return = ['msg'=>'failed','code'=>'201','txt'=>'Gagal di Upload','hash'=>$hash];
                    }
                }
                
            }else{
                $return = ['msg'=>'failed','code'=>'203','txt'=>'Anda Belum Memasukan File','hash'=>$hash];
            }

        }else{
            $return = ['msg'=>'failed','code'=>'204','txt'=>'Not Found Post','hash'=>$hash];
        }

        echo json_encode( $return );

    }

    /*public function upload_inacbg()
    {
        $hash    = $this->security->get_csrf_hash();

        if( $this->input->method() == 'post' ){

            if( !empty($_FILES) ){
                $file   = $_FILES['fileinacbg'];
                $post   = $this->input->post();
                
                $name   = $file['name'];
                $type   = $file['type']; //application/pdf
                $tmp    = $file['tmp_name'];
                $size   = $file['size'];
                $ext    = pathinfo($name, PATHINFO_EXTENSION);
                
                $norm         = $post['norm'];
                $tglperiksa   = format_tanggal( str_replace('/','-',$post['tglperiksa']) ); //Y-m-d
                $idunit       = $post['idunit'];

                $params       = ['norm'=>$norm,'tglperiksa'=>$tglperiksa,'idunit'=>$idunit];
                
                $path         = FCPATH.'assets/inacbg';
                
                $file_name    = $norm.'_'.str_replace('-','',$tglperiksa).'_'.$idunit;
                $path_file    = $path.'/'.$file_name.'.'.$ext;

                $config['upload_path']          = $path;
                $config['allowed_types']        = 'jpg|jpeg';
                $config['file_name']            = $file_name;
                $config['overwrite']            = true;
                $config['max_size']             = 8000; //8MB
                
                $row_inacbg = $this->mvclaim->tampil_inacbg($params);

                // print_r( $path_file );
                if(!empty($row_inacbg)){
                    exec( chmod($path_file,0777) );
                    unlink($path_file);
                }

                $this->load->library('upload', $config);

                if ( !$this->upload->do_upload('fileinacbg') ) {
                    $error  = strip_tags($this->upload->display_errors());
                    $return = ['msg'=>'failed','code'=>'202','txt'=>$error,'hash'=>$hash];
                    
                }else {
                    $uploaded_data = $this->upload->data();
                    
                    $zfile = $uploaded_data['full_path']; // get file path
                    exec(chmod($zfile,0777)); // CHMOD file or any other permission level(s)
        
                    $data_inacbg = [
                        'norm'          => $norm,
                        'tglperiksa'    => $tglperiksa,
                        'name_inacbg'   => $uploaded_data['file_name'],
                        'idunit'        => $idunit,
                    ];

                    $insert = $this->mvclaim->insert_inacbg($data_inacbg,$params);

                    if( $insert ){
                        $return = ['msg'=>'success','code'=>'200','txt'=>'Berhasil di Upload','hash'=>$hash];
                    }else{
                        $return = ['msg'=>'failed','code'=>'201','txt'=>'Gagal di Upload','hash'=>$hash];
                    }
                }
                
            }else{
                $return = ['msg'=>'failed','code'=>'203','txt'=>'Anda Belum Memasukan File','hash'=>$hash];
            }

        }else{
            $return = ['msg'=>'failed','code'=>'204','txt'=>'Not Found Post','hash'=>$hash];
        }

        echo json_encode( $return );

    }*/

    public function upload_inacbg()
    {
        $hash    = $this->security->get_csrf_hash();

        if( $this->input->method() == 'post' ){

            if( !empty($_FILES) ){
                $file   = $_FILES['fileinacbg'];
                $post   = $this->input->post();
                
                $name   = $file['name'];
                $type   = $file['type']; //application/pdf
                $tmp    = $file['tmp_name'];
                $size   = $file['size'];
                $ext    = pathinfo($name, PATHINFO_EXTENSION);
                
                $norm         = $post['norm'];
                $tglperiksa   = format_tanggal( str_replace('/','-',$post['tglperiksa']) ); //Y-m-d
                $idunit       = $post['idunit'];

                $params       = ['norm'=>$norm,'tglperiksa'=>$tglperiksa,'idunit'=>$idunit];
                
                $path         = FCPATH.'assets/inacbg';
                
                $file_name    = $norm.'_'.str_replace('-','',$tglperiksa).'_'.$idunit;
                // $path_file    = $path.'/'.$file_name.'.'.$ext;
                $file_ext     = $file_name.'.jpeg';
                $path_file    = $path.'/'.$file_ext;

                $image = new Imagick();
                $image->setResolution(300, 300);
                $image->readImage($tmp);
                $image->setImageFormat("jpeg"); 
                $image->setImageCompression(imagick::COMPRESSION_JPEG); 
                $image->setImageCompressionQuality(100);
                $image->flattenImages();
                
                $write_jpeg = $image->writeImage( $path_file); 
                
                $image->clear();
                $image->destroy();

                chmod( $path_file, 0777 );

                if( $write_jpeg ){

                    
                    $data_inacbg = [
                        'norm'          => $norm,
                        'tglperiksa'    => $tglperiksa,
                        'name_inacbg'   => $file_ext,
                        'idunit'        => $idunit,
                    ];
                    
                    $insert = $this->mvclaim->insert_inacbg($data_inacbg,$params);
                    
                    if( $insert ){
                        $return = ['msg'=>'success','code'=>'200','txt'=>'Berhasil di Upload','hash'=>$hash];
                    }else{
                        $return = ['msg'=>'failed','code'=>'201','txt'=>'Gagal di Upload','hash'=>$hash];
                    }
                }else{
                    $return = ['msg'=>'failed','code'=>'202','txt'=>'Maaf Gagal Convert Data','hash'=>$hash];

                }
            }else{
                $return = ['msg'=>'failed','code'=>'203','txt'=>'Anda Belum Memasukan File','hash'=>$hash];
            }

        }else{
            $return = ['msg'=>'failed','code'=>'204','txt'=>'Not Found Post','hash'=>$hash];
        }

        echo json_encode( $return );


    }

    public function delete_inacbg()
    {
        if( $this->mvclaim->delete_inacbg() ){
            $return = ['msg'=>'success','code'=>'200','txt'=>'Berhasil Dihapus'];
        }else{
            $return = ['msg'=>'failed','code'=>'201','txt'=>'Gagal Dihapus'];
        }

        echo json_encode( $return );
        
    }

    public function delete_upload_image()
    {
        if( $this->mvclaim->delete_upload_image() ){
            $return = ['msg'=>'success','code'=>'200','txt'=>'Berhasil Dihapus'];
        }else{
            $return = ['msg'=>'failed','code'=>'201','txt'=>'Gagal Dihapus'];
        }

        echo json_encode( $return );
        
    }


    public function delete_rujukanbaru()
    {
        if( $this->mvclaim->delete_rujukanbaru() ){
            $return = ['msg'=>'success','code'=>'200','txt'=>'Berhasil Dihapus'];
        }else{
            $return = ['msg'=>'failed','code'=>'201','txt'=>'Gagal Dihapus'];
        }

        echo json_encode( $return );
        
    }

    public function upload_rujukanbaru()
    {
        $hash    = $this->security->get_csrf_hash();

        if( $this->input->method() == 'post' ){

            if( !empty($_FILES) ){
                $file   = $_FILES['filerujukanbaru'];
                $post   = $this->input->post();
                
                $name   = $file['name'];
                $type   = $file['type']; //application/pdf
                $tmp    = $file['tmp_name'];
                $size   = $file['size'];
                $ext    = pathinfo($name, PATHINFO_EXTENSION);
                
                $norm         = $post['norm'];
                $tglperiksa   = format_tanggal( str_replace('/','-',$post['tglperiksa']) ); //Y-m-d

                $idunit       = $post['idunit'];

                $params       = ['norm'=>$norm,'tglperiksa'=>$tglperiksa,'idunit'=>$idunit];
                
                $path         = FCPATH.'assets/rujukanbaru';
                
                $file_name    = 'rjb_'.$norm.'_'.str_replace('-','',$tglperiksa).'_'.$idunit;
                $path_file    = $path.'/'.$file_name.'.'.$ext;

                $config['upload_path']          = $path;
                $config['allowed_types']        = 'jpg|jpeg';
                $config['file_name']            = $file_name;
                $config['overwrite']            = true;
                $config['max_size']             = 8000; //8MB
                
                $row_rujukanbaru = $this->mvclaim->tampil_rujukanbaru($params,$idunit);

                if(!empty($row_rujukanbaru)){
                    exec(chmod($path_file,0777));
                    unlink($path_file);
                }
                            
                $this->load->library('upload', $config);

                if ( !$this->upload->do_upload('filerujukanbaru') ) {
                    $error  = strip_tags($this->upload->display_errors());
                    $return = ['msg'=>'failed','code'=>'202','txt'=>$error,'hash'=>$hash];
                    
                }else {
                    $uploaded_data = $this->upload->data();

                    $zfile = $uploaded_data['full_path']; // get file path
                    exec(chmod($zfile,0777)); // CHMOD file or any other permission level(s)
        
                    $data_rujukanbaru = [
                        'norm'               => $norm,
                        'tglperiksa'         => $tglperiksa,
                        'name_rujukanbaru'   => $uploaded_data['file_name'],
                        'idunit'             => $idunit,
                    ];

                    $insert = $this->mvclaim->insert_rujukanbaru($data_rujukanbaru,$params);

                    if( $insert ){
                        $return = ['msg'=>'success','code'=>'200','txt'=>'Berhasil di Upload','hash'=>$hash];
                    }else{
                        $return = ['msg'=>'failed','code'=>'201','txt'=>'Gagal di Upload','hash'=>$hash];
                    }
                }
                
            }else{
                $return = ['msg'=>'failed','code'=>'203','txt'=>'Anda Belum Memasukan File','hash'=>$hash];
            }

        }else{
            $return = ['msg'=>'failed','code'=>'204','txt'=>'Not Found Post','hash'=>$hash];
        }

        echo json_encode( $return );

    }

    public function upload_dokumenlainlain()
    {
        $hash    = $this->security->get_csrf_hash();

        if( $this->input->method() == 'post' ){
            if( !empty($_FILES) ){

                $files   = $_FILES['fileimagelainlain'];

                $count_uploaded_files = count( $files['name'] );

                $post         = $this->input->post();
                
                $norm         = $post['norm'];
                $tglperiksa   = format_tanggal( str_replace('/','-',$post['tglperiksa']) ); //Y-m-d
                $idunit       = $post['idunit'];

                $params       = ['norm'=>$norm,'tglperiksa'=>$tglperiksa,'idunit'=>$idunit];
                
                $path         = FCPATH.'assets/dokumenlainlain';
                $config['upload_path']          = $path;
                $config['allowed_types']        = 'jpg|jpeg';
                $config['overwrite']            = true;
                $config['max_size']             = 8000; //8MB
                $this->load->library('upload',$config);

                $row_dokumenlain = $this->mvclaim->tampil_dokumenlainlain($params);

                // if(!empty($row_dokumenlain)){
                //     $name_images = explode(',',$row_dokumenlain->name_image);
                //     foreach( $name_images as $row ){
                //         $path_file    = $path.'/'.$row;
                //         // exec(chmod($path_file,0777));
                //         unlink($path_file);
                //     }
                // }

                $images = array();
                $found = false;

                foreach ($files['name'] as $key => $image) {
                    $_FILES['fileimagelainlain[]']['name']= $files['name'][$key];
                    $_FILES['fileimagelainlain[]']['type']= $files['type'][$key];
                    $_FILES['fileimagelainlain[]']['tmp_name']= $files['tmp_name'][$key];
                    $_FILES['fileimagelainlain[]']['error']= $files['error'][$key];
                    $_FILES['fileimagelainlain[]']['size']= $files['size'][$key];

                    $fileName = $image;

                    $this->upload->initialize($config);

                    if ($this->upload->do_upload('fileimagelainlain[]')) {
                        $uploaded_data = $this->upload->data();
                        $zfile = $uploaded_data['full_path']; // get file path
                        exec(chmod($zfile,0777)); // CHMOD file or any other permission level(s)

                        $images[] = $fileName;
                        $found = true;
                    } 
                }

                if( $found ){
                    $implode_image = implode(',',$images);
                    
                    $query_insert = $this->mvclaim->insert_dokumenlainlain('name_image ',$implode_image);

                    if( $query_insert ){
                        $return = ['msg'=>'success','code'=>'200','txt'=>'File Berhasil Ditambahkan','hash'=>$hash];
                    }else{
                        $return = ['msg'=>'failed','code'=>'204','txt'=>'Gagal Masukan File','hash'=>$hash];
                    }

                }else{
                    $return = ['msg'=>'failed','code'=>'404','txt'=>'File Not Found','hash'=>$hash];
                }

             
            }else{
                $return = ['msg'=>'failed','code'=>'202','txt'=>'File Belum Dipilh','hash'=>$hash];
            }

            echo json_encode( $return );
        }
    }

    public function delete_imagelainlain()
    {
        $hash    = $this->security->get_csrf_hash();

        $query   = $this->mvclaim->delete_imagelainlain();

        if( $query ){
            $return = ['msg'=>'success','code'=>'200','txt'=>'Hapus File Berhasil','hash'=>$hash];
        }else{
            $return = ['msg'=>'failed','code'=>'204','txt'=>'','hash'=>$hash];
        }

        echo json_encode( $return );
    }

    public function is_tampil(){
        
        $post       = $this->input->post();
        $is_checked =  ( $post['val'] ) ? 'Tidak Ditampilkan' : 'Tampilkan';

        $hash    = $this->security->get_csrf_hash();

        if( $post['idelemen'] == 'radiologi' ){
            $query = $this->mvclaim->insert_radiologi();
        }elseif($post['idelemen'] == 'emergency'){
            $query = $this->mvclaim->insert_emergency();
        }elseif($post['idelemen'] == 'dokumenlainlain'){
            $query = $this->mvclaim->insert_dokumenlainlain();
        }elseif( $post['idelemen'] == 'rencana-kontrol' ){
            $query = $this->mvclaim->insert_rencanakontrol();
        }elseif($post['idelemen'] == 'rujukan-baru') {
            $query = $this->mvclaim->insert_rujukanbaru_showhide();
        }elseif($post['idelemen'] == 'suratkontrol'){
            $query = $this->mvclaim->insert_skdp_showhide();
        }else{
            $query = $this->mvclaim->insert_suratkontrolinap();
        }
        
        $return = ['msg'=>'success','code'=>200,'txt'=>$is_checked,'hash'=>$hash];
        
        echo json_encode( $return );
    }

    public function downloadfiles($norm_tgl,$download_pertgl=FALSE)
    {
        $decrypt_normtgl = decrypt_url($norm_tgl);

        $explode_decrypt = explode('_',$decrypt_normtgl);
        $norm            = $explode_decrypt[0];
        $tglperiksa      = date('Y-m-d',$explode_decrypt[1]);
        $idunit          =$explode_decrypt[2];

        $params          = ['norm'=>$norm,'tglperiksa'=>$tglperiksa,'idunit'=>$idunit];
        
        $cek_sep_oldinput       = $this->cek_upload_old( 'sep',$params );
        $cek_inacbg_oldinput    = $this->cek_upload_old( 'inacbg',$params );
        $cek_rencana_oldinput   = $this->cek_upload_old( 'rencanakontrol',$params );
        $cek_rujukanbaru_oldinput   = $this->cek_upload_old( 'rujukanbaru',$params );

        $found_sep              = ( $cek_sep_oldinput ) ? 0 : $idunit;
        $found_inacbg           = ( $cek_inacbg_oldinput ) ? 0 : $idunit;
        $found_rencanakontrol   = ( $cek_rencana_oldinput ) ? 0 : $idunit;
        $found_rujukanbaru      = ( $cek_rujukanbaru_oldinput ) ? 0 : $idunit;


        $row_sep            = $this->mvclaim->tampil_sep($params,$found_sep);
        $row_inacbg         = $this->mvclaim->tampil_inacbg($params,$found_inacbg); 
        $row_rencanakontrol = $this->mvclaim->tampil_rencanakontrol($params,$found_rencanakontrol);  
        $row_rujukanbaru    = $this->mvclaim->tampil_rujukanbaru($params,$found_rujukanbaru);
        $row_dokumenlain    = $this->mvclaim->tampil_dokumenlainlain($params);

        $is_tampil_skinap       = $this->mvclaim->is_tampil_suratkontrolinap($params);
        $is_tampil_emergensi    = $this->mvclaim->is_tampil_emergensi($params);
        $is_tampil_radiologi    = $this->mvclaim->is_tampil_radiologi($params);
        $is_tampil_dokumenlainllain = $this->mvclaim->is_tampil_dokumenlainlain($params);
        $is_tampil_rencanakontrol   = $this->mvclaim->is_tampil_rencanakontrol($params);
        $is_tampil_rujukanbaru = $this->mvclaim->is_tampil_rujukanbaru($params);
        $is_tampil_skdp         = $this->mvclaim->is_tampil_skdp($params);
        
        $sep_img            = empty( $row_sep->name_image ) ? '' : convert_imageto_base64( base_url('assets/sep/'.$row_sep->name_image) );
        $inacbg_img         = empty( $row_inacbg->name_inacbg ) ? '' : convert_imageto_base64( base_url('assets/inacbg/'.$row_inacbg->name_inacbg) );
        $rencanakontrol_img = empty( $row_rencanakontrol->name_image ) ? '' : convert_imageto_base64( base_url('assets/rencanakontrol/'.$row_rencanakontrol->name_image) );
        $rujukanbaru_img    = empty( $row_rujukanbaru->name_rujukanbaru ) ? '' : convert_imageto_base64( base_url('assets/rujukanbaru/'.$row_rujukanbaru->name_rujukanbaru) );
        $dokumenlain_img    = empty( $row_dokumenlain->name_image ) ? '' : explode( ',',$row_dokumenlain->name_image );
        
        $data_bpjs              = $this->qlwebapi->api_vclaim_sep($norm,$tglperiksa,$idunit)['metadata']['response'];
        $data_resume            = $this->qlwebapi->api_vclaim_resume($norm,$tglperiksa,$idunit)['metadata']['response']; 
        $data_radiologi         = $this->qlwebapi->api_vclaim_radiologi($norm,$tglperiksa)['metadata']['response'];
        $data_nota              = $this->qlwebapi->api_vclaim_nota( $norm,$tglperiksa,$idunit )['metadata']['response'];

        $found_ro = $this->cek_pemerisaanradiologi($data_radiologi);

        // $data_riwayat_resume_skdp = '';
        if( !empty( $this->qlwebapi->api_vclaim_suratkontrol( $norm,$tglperiksa ) ) ){
            $data_riwayat_resume_skdp = $this->qlwebapi->api_vclaim_suratkontrol( $norm,$tglperiksa )['metadata']['response'];
        }else{
            $data_riwayat_resume_skdp = $data_resume;
        }

        $template_resume        = $this->template_resume($data_resume,TRUE);
        $template_suratkontrol  = $this->template_suratkontrol( $data_riwayat_resume_skdp, $params,TRUE);
        $template_suratkontrolinap = $this->template_suratkontrolinap($data_resume,$params,TRUE);
        $template_emergensi     = $this->template_emergency($data_resume,$params,TRUE);
        $template_radiologi     = $this->template_radiologi( $data_radiologi , TRUE);
        $template_nota          = $this->template_nota( $data_nota,TRUE );

        $cek_resumegabung       = $this->mvclaim->get_resume_gabung($params); 
        $cek_radiologigabung    = $this->mvclaim->get_radiologi_gabung($params); 
        $cek_billinggabung      = $this->mvclaim->get_billing_gabung($params); 

        $template_resumegabung = '';
        if( !empty($cek_resumegabung) ){
            $data_resume_gabung     = $this->qlwebapi->api_vclaim_resume($norm,$cek_resumegabung->tglgabung,$cek_resumegabung->idunit)['metadata']['response']; ;
            $template_resumegabung  = $this->template_resume($data_resume_gabung,true);
        }

        $template_radiologigabung = '';
        $found_ro_gabung = '';
        if( !empty($cek_radiologigabung) ){
            $data_ro                   = $this->qlwebapi->api_vclaim_radiologi($norm,$cek_radiologigabung->tglgabung,$cek_radiologigabung->idunit)['metadata']['response'];
            $template_radiologigabung  = $this->template_radiologi($data_ro,true);
            $found_ro_gabung = $this->cek_pemerisaanradiologi($data_ro);

        }

        $template_billinggabung = '';
        if( !empty($cek_billinggabung) ){
            $data_billing            = $this->qlwebapi->api_vclaim_nota( $norm,$cek_billinggabung->tglgabung,$cek_billinggabung->idunit )['metadata']['response'];
            $template_billinggabung  = $this->template_nota($data_billing,true);
        }

        $data_rencanakontrol = $this->qlwebapi->api_vclaim_rencanakontrol($norm,$tglperiksa)['metadata']['response'];
        $cek_rencana_kontrol = ($data_rencanakontrol['metaData']['code'] == 200) ? true : false;
        
        $jns_rujukan = akses_jenispemeriksaan($data_resume['jenisrujukan']);

        if( !empty( $data_bpjs['sep'] ) ){
            $sep = $data_bpjs['sep'];
            if( $sep['metaData']['code'] == '200' ){
                $response = $sep['response'];
                $noSep = $response['noSep'];
            }else{
                $noSep = $sep['metaData']['code'].'-'.$sep['metaData']['message'];
            }
        }else{
            $noSep = $data_bpjs['metaData']['code'].'-'.$data_bpjs['metaData']['message'];
        }

        // $nama_file_pdf = $noSep.'_'.$norm.'_'.format_tanggal($tglperiksa,'d-m-Y');
        $nama_file_pdf = $noSep;

        $data = [
            'template_billinggabung' => $template_billinggabung,
            'template_radiologigabung' => $template_radiologigabung,
            'template_resumegabung' => $template_resumegabung,
            'data_bpjs' => $data_bpjs,
            'jenisrujukan' => $jns_rujukan,
            'cek_rencana_kontrol' => $cek_rencana_kontrol,
            'row_sep'    => $sep_img,
            'row_inacbg' => $inacbg_img,
            'row_rencanakontrol' => $rencanakontrol_img,
            'rows_dokumenlainlain' => $dokumenlain_img,
            'row_rujukanbaru' => $rujukanbaru_img,
            'template_resume' => $template_resume,
            'template_suratkontrol' => $template_suratkontrol,
            'template_suratkontrolinap' =>$template_suratkontrolinap,
            'template_emergensi' => $template_emergensi,
            'template_radiologi'=> $template_radiologi,
            'template_nota' => $template_nota,
            'is_tampil_skinap' => $is_tampil_skinap,
            'is_tampil_emergensi' => $is_tampil_emergensi,
            'is_tampil_radiologi' => $is_tampil_radiologi,
            'is_tampil_dokumenlainllain' => $is_tampil_dokumenlainllain,
            'is_tampil_rencanakontrol' => $is_tampil_rencanakontrol,
            'is_tampil_rujukanbaru' => $is_tampil_rujukanbaru,
            'is_tampil_skdp'    => $is_tampil_skdp,
            'found_ro_gabung' =>$found_ro_gabung,
            'found_ro' => $found_ro
        ];


        $this->pdf->setPaper('A4', 'potrait');
        
        if( $download_pertgl == FALSE ){
            $this->pdf->filename = $nama_file_pdf.".pdf";
            $this->pdf->load_view('vclaim/download_per_sep', $data);
        }else{
           
            return $this->load->view('vclaim/download_per_sep',$data,TRUE);
          
        }

    }

    public function form_gabung()
    {
        #post
        $post       = $this->input->post();
        $norm       = $post['norm'];
        $tglperiksa = $post['tglperiksa'];
        $tipe       = $post['tipe'];
        
        #security check
        $csrf = array(
            'name' => $this->security->get_csrf_token_name(),
            'hash' => $this->security->get_csrf_hash()
        );
        
        $api_units = [];
        if( !empty($this->qlwebapi->api_vclaim_poli()) ){
            $api_units = $this->qlwebapi->api_vclaim_poli()['metadata']['response'];
        }
        
        $params = compact('norm','tglperiksa');

        $cek_resumegabung       = $this->mvclaim->get_resume_gabung($params); 
        $cek_radiologigabung    = $this->mvclaim->get_radiologi_gabung($params); 
        $cek_billinggabung      = $this->mvclaim->get_billing_gabung($params); 

        if(  $tipe == 'resume' && !empty($cek_resumegabung) ){
            
            $formgabung = '<table class="table table-bordered">';
            $formgabung .= '<tr><th>Resume Gabung</th><th>Aksi</th></tr>';
            $formgabung .= '<tr><td> Resume - '.$cek_resumegabung->tglgabung.'</td><td><button data-name="'.$csrf['name'].'" data-hash="'.$csrf['hash'].'" class="btn btn-danger btn-sm delete-resumegabung" type="button" data-tipe="'.$tipe.'" data-id="'.$cek_resumegabung->idresumegabung.'"><i class="fa fa-trash"></i></button></td></tr>';
            $formgabung .= '</table>';

        }else if( $tipe == 'radiologi' && !empty($cek_radiologigabung)){
            $formgabung = '<table class="table table-bordered">';
            $formgabung .= '<tr><th>Radiologi Gabung</th><th>Aksi</th></tr>';
            $formgabung .= '<tr><td> Radiologi - '.$cek_radiologigabung->tglgabung.'</td><td><button data-name="'.$csrf['name'].'" data-hash="'.$csrf['hash'].'" class="btn btn-danger btn-sm delete-resumegabung" type="button" data-tipe="'.$tipe.'" data-id="'.$cek_radiologigabung->idradiologigabung.'"><i class="fa fa-trash"></i></button></td></tr>';
            $formgabung .= '</table>';
        }else if( $tipe == 'billing' && !empty($cek_billinggabung) ){
            $formgabung = '<table class="table table-bordered">';
            $formgabung .= '<tr><th>Billing Gabung</th><th>Aksi</th></tr>';
            $formgabung .= '<tr><td> Billing - '.$cek_billinggabung->tglgabung.'</td><td><button data-name="'.$csrf['name'].'" data-hash="'.$csrf['hash'].'" class="btn btn-danger btn-sm delete-resumegabung" type="button" data-tipe="'.$tipe.'" data-id="'.$cek_billinggabung->idbillinggabung.'"><i class="fa fa-trash"></i></button></td></tr>';
            $formgabung .= '</table>';
        }else{

            $formgabung = '<form class="form" id="frm-resumegabung">
                <input type="hidden" name="'.$csrf['name'].'" value="'.$csrf['hash'].'">
                <input type="hidden" name="norm" value="'.$norm.'">
                <input type="hidden" name="tglperiksa" value="'.$tglperiksa.'">
                <input type="hidden" name="tipe" value="'.$tipe.'">
                <div class="form-group">
                    <label>Pilih Tanggal Periksa</label>
                    <input type="text" class="form-control ql-dateresume" name="tglperiksaresume" required>
                </div>
                <div class="form-group">
                <label>Pilih Unit (Opsional)</label>
                <select class="form-control" name="idunit">
                    <option value="">Pilih unit</option>';
                foreach($api_units as $row_unit){
                    $formgabung .= '<option value="'.$row_unit['idunit'].'">'.$row_unit['namaunit'].'</option>';
                }
                
             $formgabung .= '</select></div><div class="form-group mt-3">
                    <button class="btn btn-primary">Simpan</button>
                </div>
            </form>';
        }

        echo json_encode(['name'=>$csrf['name'],'hash'=>$csrf['hash'],'hsl'=>$formgabung]);

    }

    public function modal_close()
    {
        echo json_encode(['hash'=>$this->security->get_csrf_hash()]);
    }

    public function simpan_resumegabung(){
        $idinsert = $this->mvclaim->insert_resumegabung();
        if( $idinsert ){
            $return = ['msg'=>'success','code'=>200,'txt'=>'Resume Gabung Berhasil disimpan'];
        }else{
            $return = ['msg'=>'failed','code'=>201,'txt'=>'Resume Gabung Gagal disimpan'];
        }

        echo json_encode( $return );
    }

    public function delete_resumegabung()
    {
        $delete = $this->mvclaim->delete_resumegabung();

        if( $delete ){
            $return = ['msg'=>'success','code'=>200,'txt'=>'Berhasil Dihapus'];
        }else{
            $return = ['msg'=>'failed','code'=>201,'txt'=>'Gagal Dihapus'];
        }
        echo json_encode( $return );
    }


    public function template_pertimbangan_loop($params){

        $query_loop = $this->mvclaim->loop_listpertimbangan($params);

        $tb = '<table class="table table-striped" id="addlist" style="width:50%;">
            <thead style="background: #8392ab;">
                <tr>
                    <th class="text-white">Pertimbangan</th>
                    <th class="text-white">Aksi</th>
                </tr>
            </thead><tbody>';
                if( !empty( $query_loop ) ){
                    foreach( $query_loop as $row ){
                        $tb .= '<tr>
                                    <td>'.$row->item.'</td>
                                    <td><button type="button" class="mb-0 btn btn-danger btn-xs delete-pertimbangan" data-idlist="'.$row->idlistpertimbangan.'" data-idpertimbangan="'.$row->idpersonpertimbangan.'"><i class="fa fa-close"></i></button></td>
                                </tr>';
                    }
                }
                
        $tb .= '</tbody></table>';

        return $tb;
    }

    public function download_berkas_pertgl()
    {
        $post = $this->input->post();

        #security check
        $csrf = array(
            'name' => $this->security->get_csrf_token_name(),
            'hash' => $this->security->get_csrf_hash()
        );
        
        $tglperiksa       = format_tanggal(str_replace('/','-',$post['tglperiksa']),"d-m-Y") ;
        $jenispemeriksaan = $post['jenispemeriksaan'];
        
        $path               = FCPATH . 'assets/pdf';
        $path_jns           = $path.'/'.$jenispemeriksaan;
        $path_tglperiksa    = $path_jns.'/'.$tglperiksa;

        /**
         * Cek folder exist
         * buat folder jenis pemeriksaan dan tgl periksa
         */
        if( !is_dir( $path_jns ) ){
            mkdir($path_jns);
            chmod($path_jns, 0777);
        }

        if( !is_dir( $path_tglperiksa ) ){
            mkdir($path_tglperiksa);
            chmod($path_tglperiksa, 0777);
        }

        $list_vclaim = $this->qlwebapi->api_vclaim_list_selesai();

        foreach( $list_vclaim['response'] as $row ){
            $norm_tgl = encrypt_url($row['norm'].'_'.strtotime( format_tanggal($row['tglperiksa'] ) ).'_'.$row['idunit'] );
            
            $aktif_download_berkas = true;

            $pdf_content   = $this->downloadfiles($norm_tgl,$aktif_download_berkas);
            $path_file     = $path_tglperiksa.'/'. $row['nosep'].".pdf";

            $dompdf = new DOMPDF();
            $dompdf->load_html($pdf_content);
            $dompdf->render();
            $output = $dompdf->output();

            file_put_contents($path_file, $output);
            chmod($path_file, 0777);
        //    str_replace(' ','',preg_replace('/[:\/]/', '', $subject))
        }      

        $files_pdf = cek_isi_folder_pdf($post['tglperiksa'],$jenispemeriksaan);
        $count_file = count($files_pdf);
        $csrf['jmlfile'] = $count_file;
        $csrf['msg'] = 'success';
        echo json_encode($csrf);

    }

    public function download_zippertanggal()
    {
        
        $tglPeriksa = $this->input->post('tglPeriksa');
        $jnsPeriksa = $this->input->post('jnsPeriksa');

        $changeFormatTgl = format_tanggal(str_replace('/','-',$tglPeriksa),"d-m-Y");

        #path
        $path               = FCPATH . 'assets/pdf';
        $path_jns           = $path.'/'.$jnsPeriksa;
        $path_tglperiksa    = $path_jns.'/'.$changeFormatTgl;

        $files = cek_isi_folder_pdf($tglPeriksa,$jnsPeriksa);

        foreach ($files as $namefile){
            $this->zip->read_file( $path_tglperiksa.'/'.$namefile );  
        }

        // $this->zip->archive($path_jns.'/'.$tglperiksa.'.zip');
        $this->zip->download($changeFormatTgl.'.zip'); 
    }

    public function template_sep($data_bpjs,$params="")
    {
        ob_start();

        $csrf = array(
            'name' => $this->security->get_csrf_token_name(),
            'hash' => $this->security->get_csrf_hash()
        );

        include FCPATH .'/application/views/vclaim/template-sep.php';
        return ob_get_clean();
    }

    public function template_sep_new($params,$idunit="",$found=FALSE){
        ob_start();

        $csrf = array(
            'name' => $this->security->get_csrf_token_name(),
            'hash' => $this->security->get_csrf_hash()
        );

        if( $found ){
            $idunit = 0;
        }
        
        $row_sep = $this->mvclaim->tampil_sep($params,$idunit);
        
        
        include FCPATH .'/application/views/vclaim/template-sep-new.php';
        return ob_get_clean();
    }

    public function template_resume($data_resume,$download=FALSE)
    {
        ob_start();
        if($download){
            include FCPATH .'/application/views/vclaim/download/resume.php';
        }else{
            include FCPATH .'/application/views/vclaim/template-resume.php';
        }
        return ob_get_clean();
    }

    public function template_rencanakontrol($data_rencanakontrol)
    {
        ob_start();
        include FCPATH .'/application/views/vclaim/template-rencana-kontrol.php';
        return ob_get_clean();
    }

    public function template_rencanakontrol_new($params,$idunit="",$found=FALSE)
    {
        ob_start();

        
        if( $found ){
            $idunit = 0;
        }
        
        $row_rencanakontrol = $this->mvclaim->tampil_rencanakontrol($params,$idunit);
        include FCPATH .'/application/views/vclaim/template-rencana-kontrol-new.php';
        return ob_get_clean();
    }

    public function template_radiologi($data_radiologi,$download=FALSE)
    {
        ob_start();
        if($download){
            include FCPATH .'/application/views/vclaim/download/radiologi.php';
        }else{
            include FCPATH .'/application/views/vclaim/template-radiologi.php';
        }
        return ob_get_clean();
    }

    public function template_nota($data_nota,$download=FALSE)
    {
        ob_start();
        if( $download ){
            include FCPATH .'/application/views/vclaim/download/billing.php';
        }else{
            include FCPATH .'/application/views/vclaim/template-nota.php';
        }
        return ob_get_clean();
    }

    public function template_suratkontrol($data_suratkontrol,$params,$download=FALSE)
    {
        ob_start();
        
        $csrf = array(
            'name' => $this->security->get_csrf_token_name(),
            'hash' => $this->security->get_csrf_hash()
        );

        $loop_table_pertimbangan = $this->template_pertimbangan_loop($params);

        $query_loop = $this->mvclaim->loop_listpertimbangan($params);

        $row_skformedit = $this->mvclaim->tampil_skformedit($params);

        if( $download ){
            include FCPATH .'/application/views/vclaim/download/suratkontrol.php';
        }else{
            include FCPATH .'/application/views/vclaim/template-suratkontrol.php';
        }
        
        return ob_get_clean();
    }

    public function template_suratkontrolinap($data_suratkontrolinap,$params,$download=FALSE)
    {
        ob_start();
        $csrf = array(
            'name' => $this->security->get_csrf_token_name(),
            'hash' => $this->security->get_csrf_hash()
        );

        $row_skinap = $this->mvclaim->tampil_skinap($params);

        if( $download ){
            include FCPATH .'/application/views/vclaim/download/suratkontrolinap.php';
        }else{
            include FCPATH .'/application/views/vclaim/template-suratkontrolinap.php';
        }
        return ob_get_clean();
    }

    public function template_emergency($data_emergency,$params,$download=FALSE)
    {
        ob_start();
        $csrf = array(
            'name' => $this->security->get_csrf_token_name(),
            'hash' => $this->security->get_csrf_hash()
        );

        $row_emergency = $this->mvclaim->tampil_emergency($params);
        
        if( $download ){
            include FCPATH .'/application/views/vclaim/download/emergensi.php';
        }else{
            include FCPATH .'/application/views/vclaim/template-emergency.php';
        }
        return ob_get_clean();
    }

    public function template_incbg($params,$idunit="",$found=FALSE)
    {
        ob_start();

        
        if( $found ){
            $idunit = 0;
        }

        $row_inacbg = $this->mvclaim->tampil_inacbg($params,$idunit);


        include FCPATH .'/application/views/vclaim/template-inacbg.php';
        return ob_get_clean();
    }

    public function template_rujukanbaru($params,$idunit="",$found=FALSE)
    {
        ob_start();

        
        if( $found ){
            $idunit = 0;
        }
        
        $row_rujukanbaru = $this->mvclaim->tampil_rujukanbaru($params,$idunit);
        include FCPATH .'/application/views/vclaim/template-rujukanbaru.php';
        return ob_get_clean();
    }

    public function template_dokumenlainlain($params)
    {
        ob_start();

        $row_dokumenlain = $this->mvclaim->tampil_dokumenlainlain($params);

        // echo '<pre>';
        // print_r( $row_dokumenlain );
        // echo '</pre>';
        
        include FCPATH .'/application/views/vclaim/template-suratlainlain.php';
        return ob_get_clean();
    }

    /**
     * TEST PDF
     */
    public function template_testpdf($params,$idunit="",$found=FALSE)
    {
        ob_start();

        
        if( $found ){
            $idunit = 0;
        }
        
        $row_testpdf = $this->mvclaim->tampil_testpdf($params,$idunit);

        include FCPATH .'/application/views/vclaim/template-testpdf.php';
        return ob_get_clean();
    }
} 
?>
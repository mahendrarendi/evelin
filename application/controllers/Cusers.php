<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Cusers extends CI_Controller 
{
    protected $tb_users      = 'user';
    protected $tb_user_level = 'user_level';

    function __construct(){
	    parent::__construct();
        
        $this->load->model('mauth');
        
        if(!$this->mauth->current_user()){
			redirect('login_evelin');
		}

        if(!is_superadmin()){
            redirect('dashboard');
        }

        $this->load->model('msqlbasic');

        $this->load->library('form_validation');

        $this->load->library('encryption'); 
    }
    

    public function list()
    {

        $csrf = array(
            'name' => $this->security->get_csrf_token_name(),
            'hash' => $this->security->get_csrf_hash()
        );

        $data = [
            'content_view'  => 'user/v_list_user',
            'title_page'    => 'List Users',
            'mode'          => '',
            'active_menu'   => 'users',
            'csrf'          => $csrf,
            'list_users'    => $this->get_list_users(),
            'active_sub_menu' => 'listuser',
            'active_menu_level' => '',
            'plugins'       => [PLUGIN_DATATABLE,PLUGIN_SWEETALERT],
            'scripts_js'    => ['js_users'],
        ];

        $this->load->view('v_index',$data);
    }

    public function tambah($id=null)
    {
        $csrf = array(
            'name' => $this->security->get_csrf_token_name(),
            'hash' => $this->security->get_csrf_hash()
        );

        $userid     = ( isset( $id ) ) ? (int) decrypt_url($id) : '';
        $get_user   = ( isset( $id ) ) ? $this->get_row_user($userid) : '';

        $data = [
            'content_view'  => 'user/v_tambah',
            'title_page'    => 'Tambah Pengguna',
            'mode'          => '',
            'active_menu'   => 'users',
            'active_sub_menu' => 'listuser',
            'active_menu_level' => '',
            'csrf'          => $csrf,
            'userid'        => $userid,
            'get_user'      => $get_user,
            'level_users'   => get_level_user(),
            'plugins'       => [PLUGIN_SELECT2],
            'scripts_js'    => ['js_users'],
        ];

        $this->load->view('v_index',$data);
    }

    public function get_list_users()
    {
        $user   = $this->tb_users;
        $level  = $this->tb_user_level;
        
        $sql = "SELECT a.*,b.* FROM " . $user ." a ";
        $sql .= " LEFT JOIN ". $level . " b ON b.idlevel = a.level";

        $query = $this->msqlbasic->custom_query( $sql );
        return $query;
    }

    public function get_row_user($userid)
    {
        $user   = $this->tb_users;
        $query  = $this->msqlbasic->row_db($user,['iduser'=>$userid]);
        return $query;
    }

    public function get_row_level($idlevel)
    {
        $level      = $this->tb_user_level;
        $query      = $this->msqlbasic->row_db($level,['idlevel'=>$idlevel]);
        return $query;
    }

    public function simpan()
    {
        $post       = $this->input->post();
        $userid     = ( isset( $post['hid_userid'] ) ) ? (int) decrypt_url($post['hid_userid']) : '' ;

        $array_pass = '';
        $array_confirm = '';
        if( !isset($post['hid_userid']) ){
            $array_pass = array(
                'field' => 'password',
                'label' => 'Password',
                'rules' => 'required',
                'errors' => array(
                        'required' => 'Mohon isi kolom %s Terlebih dahulu ',
                )
            );

            $array_confirm = array(
                    'field' => 'confirm_password',
                    'label' => 'Konfirmasi Password',
                    'rules' => 'required',
                    'errors' => array(
                        'required' => 'Mohon isi kolom %s Terlebih dahulu ',
                    )
            );
        }

        $config = array(
            array(
                'field' => 'level',
                'label' => 'Level User',
                'rules' => 'required',
                'errors' => array(
                    'required' => 'Mohon Pilih kolom %s Terlebih dahulu ',
                ),
            ),
            array(
                'field' => 'nama',
                'label' => 'Nama',
                'rules' => 'required',
                'errors' => array(
                    'required' => 'Mohon isi kolom %s Terlebih dahulu ',
                ),
            ),
            array(
                    'field' => 'username',
                    'label' => 'Username',
                    'rules' => 'required',
                    'errors' => array(
                        'required' => 'Mohon isi kolom %s Terlebih dahulu ',
                    ),
                ),
            array(
                    'field' => 'email',
                    'label' => 'Email',
                    'rules' => 'required',
                    'errors' => array(
                        'required' => 'Mohon isi kolom %s Terlebih dahulu ',
                    ),
            ),
            $array_pass,
            $array_confirm
        );

        

        $this->form_validation->set_rules($config);

        if ($this->form_validation->run() == FALSE){
            $return = ['msg'=>'danger','txt'=>validation_errors()];
            echo json_encode($return);
        }else{
            $nama       = $this->security->sanitize_filename($post['nama']);
            $username   = $this->security->sanitize_filename($post['username']);
            $email      = $this->security->sanitize_filename($post['email']);
            $password   = $this->security->sanitize_filename($post['password']);
            $confirm_password = $this->security->sanitize_filename($post['confirm_password']);
            $level_user = (int) $this->security->sanitize_filename($post['level']);

            if( empty($userid) ){
                /**
                 * Insert User
                 */
                if( !empty($password) || !empty($confirm_password) ){
                    if( $password == $confirm_password ){
                        $data   = [
                            'nama'          => $nama,
                            'email'         => $email,
                            'username'      => $username,
                            'password'      => password_hash($password, PASSWORD_DEFAULT),
                            'created_at'    => waktu_indonesia(),
                            'level'         => $level_user,
                        ];
                        /** cek data insert */
                        $cek_email  = $this->msqlbasic->row_db($this->tb_users,['email'=>$email]);
                        $username   = $this->msqlbasic->row_db($this->tb_users,['username'=>$username]);
                        
                        if( !empty($cek_email) ){
                            $return = ['msg'=>'danger','txt'=>'Email Telah Terdaftar'];
                            echo json_encode($return);
                            die();
                        }elseif(!empty($username)){
                            $return = ['msg'=>'danger','txt'=>'Username telah Terdaftar'];
                            echo json_encode($return);
                            die();
                        }

                        $insert = $this->msqlbasic->add_db($this->tb_users,$data);
                        if( $insert > 0 ){
                            $return = ['msg'=>'success','txt'=>'Data Berhasil di Simpan','url'=>base_url('listuser')];
                            echo json_encode($return);
                        }else{
                            $return = ['msg'=>'danger','txt'=>'Data Gagal di Simpan','test'=>$insert];
                            echo json_encode($return);
                        }
                    }else{
                        $return = ['msg'=>'danger','txt'=>'Password tidak sama silahkan Ulangi Kembali'];
                        echo json_encode($return);
                    }
                }else{
                    $return = ['msg'=>'danger','txt'=>'Password Harap diisi dengan Benar !'];
                    echo json_encode($return);
                }

            }else{
                /**
                 * Update User
                 */
                if( !empty($password) || !empty($confirm_password) ){
                    if( $password == $confirm_password ){
                        $data   = [
                            'nama'          => $nama,
                            'email'         => $email,
                            'username'      => $username,
                            'password'      => password_hash($password, PASSWORD_DEFAULT),
                            'level'         => $level_user,
                        ];
                        
                        $update = $this->msqlbasic->update_db($this->tb_users,['iduser'=>$userid],$data);

                        if( $update > 0 ){
                            $return = ['msg'=>'success','txt'=>'Data Berhasil di Update','url'=>base_url('listuser')];
                            echo json_encode($return);
                        }else{
                            $return = ['msg'=>'danger','txt'=>'Data Gagal di Update'];
                            echo json_encode($return);
                        }
                    }else{
                        $return = ['msg'=>'danger','txt'=>'Password tidak sama silahkan Ulangi Kembali'];
                        echo json_encode($return);
                    }
                }else{
                    $data   = [
                        'nama'          => $nama,
                        'email'         => $email,
                        'username'      => $username,
                        'level'         => $level_user,
                    ];

                    $update = $this->msqlbasic->update_db($this->tb_users,['iduser'=>$userid],$data);

                    $return = ['msg'=>'success','txt'=>'Data Berhasil di Update','url'=>base_url('listuser')];
                    echo json_encode($return);
                    

                } // end !empty password

            } // end check userid


        } // end validation

    }

    public function delete()
    {
        
        $post = $this->input->post();   
        $id   = decrypt_url($post['id']);

        $query = $this->msqlbasic->delete_db($this->tb_users,['iduser'=>$id]);

        if( $query ){
            $return = ['msg'=>'success','txt'=>'Data Berhasil dihapus'];
        }else{
            $return = ['msg'=>'danger','txt'=>'Data Gagal dihapus'];
        }

        echo json_encode( $return );
    }

    public function list_level()
    {
        $csrf = array(
            'name' => $this->security->get_csrf_token_name(),
            'hash' => $this->security->get_csrf_hash()
        );

        $data = [
            'content_view'  => 'user/v_list_level',
            'title_page'    => 'List Level',
            'mode'          => '',
            'active_menu'   => 'users',
            'active_sub_menu' => 'listlevel',
            'active_menu_level' => '',
            'csrf'          => $csrf,
            'level_users'   => get_level_user(),
            'plugins'       => [PLUGIN_DATATABLE,PLUGIN_SWEETALERT],
            'scripts_js'    => ['js_users'],
        ];

        $this->load->view('v_index',$data);
    }

    public function tambah_level($id=null)
    {
        $csrf = array(
            'name' => $this->security->get_csrf_token_name(),
            'hash' => $this->security->get_csrf_hash()
        );

        $idlevel    = ( isset( $id ) ) ? (int) decrypt_url($id) : '';
        $get_level  = ( isset( $id ) ) ? $this->get_row_level($idlevel) : '';

        $data = [
            'content_view'  => 'user/v_tambah_level',
            'title_page'    => 'Tambah Level',
            'mode'          => '',
            'active_menu'   => 'users',
            'active_sub_menu' => 'listlevel',
            'active_menu_level' => '',
            'csrf'          => $csrf,
            'id_level'      =>$idlevel,
            'get_level'     =>$get_level,
            'plugins'       => [PLUGIN_SELECT2],
            'scripts_js'    => ['js_users'],
        ];

        $this->load->view('v_index',$data);
    }

    public function simpan_level()
    {
        $post    = $this->input->post();
        $idlevel = ( isset( $post['hid-id'] ) ) ? (int) decrypt_url($post['hid-id']) : '' ;

        $config = array(
            array(
                'field' => 'nama-level',
                'label' => 'Nama Level',
                'rules' => 'required',
                'errors' => array(
                    'required' => 'Mohon isi kolom %s Terlebih dahulu ',
                ),
            )
        );

        $this->form_validation->set_rules($config);

        if ($this->form_validation->run() == FALSE){
            $return = ['msg'=>'danger','txt'=>validation_errors()];
            echo json_encode($return);
        }else{
            $nama_level = $this->security->sanitize_filename($post['nama-level']);

            $data = ['level_name'=>$nama_level];

            if( !isset($post['hid-id']) ){
   
                $insert = $this->msqlbasic->add_db($this->tb_user_level,$data);
                
                if( $insert > 0 ){
                    $return = ['msg'=>'success','txt'=>'Data Berhasil di Simpan','url'=>base_url('listlevel')];
                    
                }else{
                    $return = ['msg'=>'danger','txt'=>'Data Gagal di Simpan'];
                }
                
                echo json_encode($return);

            }else{
                $update = $this->msqlbasic->update_db($this->tb_user_level,['idlevel'=>$idlevel],$data);
                $return = ['msg'=>'success','txt'=>'Data Berhasil di Update','url'=>base_url('listlevel')];
                echo json_encode($return);
            }
        }
    }

    public function delete_level()
    {
        $post       = $this->input->post();   
        $idlevel    = decrypt_url($post['id']);

        $query = $this->msqlbasic->delete_db($this->tb_user_level,['idlevel'=>$idlevel]);

        if( $query ){
            $return = ['msg'=>'success','txt'=>'Data Berhasil dihapus'];
        }else{
            $return = ['msg'=>'danger','txt'=>'Data Gagal dihapus'];
        }

        echo json_encode( $return );
    }
} 
?>
<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class C404 extends CI_Controller 
{
    function __construct(){
	    parent::__construct();

        $this->load->model('mauth');
        
        if(!$this->mauth->current_user()){
			redirect('login_evelin');
		}
        
    }

    public function index()
    {
        $data = [
            'content_view'  => 'v_404',
            'title_page'    => '404 Not Found',
            'mode'          => '',
            'active_menu'   => '',
            'active_sub_menu' => '',
            'active_menu_level' => '',
            'plugins'       => [],
            'scripts_js'    => [],
        ];

        $this->load->view('v_index',$data);
    }
}
?>
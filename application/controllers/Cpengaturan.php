<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Cpengaturan extends CI_Controller
{
    protected $key_bulanpengajuan = '_bulan_pengajuan';
    protected $key_aktifkanpengajuan = '_aktifkan_pengajuan';

    function __construct()
    {
        parent::__construct();

        # model
        $this->load->model('mpengaturan');
    }

    public function index()
    {
        $data               = $this->setting_viewpengaturan();
        $data['oke']        = 'test';
        $data['plugins']    = [PLUGIN_DATATABLE,PLUGIN_SELECT2,PLUGIN_DATEPICKER,PLUGIN_SWEETALERT,PLUGIN_ZOOMIMAGE];
        $data['scripts_js'] = ['js_pengaturan'];

        $this->parser->parse('v_index',$data);
    }

    public function setting_viewpengaturan()
    {
        $csrf = array(
            'name' => $this->security->get_csrf_token_name(),
            'hash' => $this->security->get_csrf_hash()
        );

        $row_pengajuan  = $this->mpengaturan->get_pengaturan($this->key_bulanpengajuan);
        $row_aktifpengajuan  = $this->mpengaturan->get_pengaturan($this->key_aktifkanpengajuan);
        
        $bulanpengajuan = '';
        if( !empty( $row_pengajuan ) ){
            $bulanpengajuan   = $row_pengajuan->value_pengaturan;
        }

        $aktifpengajuan = '';
        if( !empty( $row_pengajuan ) ){
            $aktifpengajuan   = $row_aktifpengajuan->value_pengaturan;
        }
        
        $data = [
            'content_view'  => 'pengaturan/v_pengaturan',
            'title_page'    => 'Pengaturan',
            'mode'          => '',
            'active_menu'   => 'pengaturan',
            'active_sub_menu' => '',
            'active_menu_level' => '',
            'csrf' => $csrf,
            'bulanpengajuan' => $bulanpengajuan,
            'aktifpengajuan' => $aktifpengajuan,
        ];

        return $data;
    }

    public function simpan_pengajuan()
    {
        $post             = $this->input->post();
        $bulan_pengajuan  = $post['bulanpengajuan'];
        $aktifkanpengajuan = isset($post['aktifkanpengajuan']) ? $post['aktifkanpengajuan'] : '';  
                
        $cek_pengajuan  = $this->mpengaturan->get_pengaturan($this->key_bulanpengajuan);
        $cek_aktifpengajuan  = $this->mpengaturan->get_pengaturan($this->key_aktifkanpengajuan);

        if( !empty( $cek_pengajuan ) || !empty( $cek_aktifpengajuan )  ){
            $this->mpengaturan->update_pengaturan($cek_pengajuan->idpengaturan,$this->key_bulanpengajuan,$bulan_pengajuan);
            $this->mpengaturan->update_pengaturan($cek_aktifpengajuan->idpengaturan,$this->key_aktifkanpengajuan,$aktifkanpengajuan);
            
        }else{
            $this->mpengaturan->add_pengaturan($this->key_bulanpengajuan,$bulan_pengajuan);
            $this->mpengaturan->add_pengaturan($this->key_aktifkanpengajuan,$aktifkanpengajuan);
        }

        $return = ['msg'=>'success','code'=>200,'txt'=>'Berhasil di Simpan'];
        

        echo json_encode( $return );

    }
}

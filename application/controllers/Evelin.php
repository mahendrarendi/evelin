<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Evelin extends CI_Controller
{
    function __construct(){
        parent::__construct();
        $this->load->model('mauth');
        
        if(!$this->mauth->current_user()){
			redirect('login_evelin');
		}
    }

    public function index()
    {
        $this->dashboard();
    }

    public function dashboard()
    {
        $data = [
            'content_view'  => 'beranda/v_dashboard',
            'title_page'    => 'Dashboard',
            'mode'          => '',
            'active_menu'   => 'dashboard',
            'active_sub_menu' => '',
            'active_menu_level' => '',
            'plugins'       => [],
            'scripts_js'    => [],
        ];

        $this->load->view('v_index',$data);
    }
}
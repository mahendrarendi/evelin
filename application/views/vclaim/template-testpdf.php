<style>
#frm-rencanakontrol .form-inline {
  display: inline-block;
}
</style>
<?php 
    $class_hidden = empty($row_testpdf->name_image) ? 'd-block' : 'd-none';
?>
<div class="row">
    
    <?php if( !empty($row_testpdf->name_image) ): ?>
        <div class="title">
            <h4>Rencana Kontrol</h4>
        </div>
    <?php endif; ?>

    <?php if(is_superadmin() || is_klaim()): ?>
    <div class="col-md-6">
        <form class="form <?= $class_hidden; ?>"  id="frm-testpdf"  enctype="multipart/form-data" >
        
            <!-- add idunit -->
            <input type="hidden" class="idunit-hidden" value="<?= $idunit; ?>" name="idunit">

            <div class="form-group">
                <div class="form-inline">
                    <label style="font-size:unset;">Silahkan Upload File TEST <span class="required">*</span></label>
                    <input  required accept="image/*"  type="file" name="fileimage" class="form-control">
                </div>
                <div class="form-inline">
                    <button type="submit" class="btn btn-danger mb-0">Upload</button>
                </div>
            </div>
        </form>
        
    </div>
    <div class="col-md-6">
        <div class="card bg-danger text-white">
            <div class="card-body">
                <p class="mb-0"><strong>(*) Catatan Upload File TEST:</strong></p>
                <ul class="mb-0">
                    <li>Upload  File Maximal <i>8MB</i></li>
                    <li>Tipe File Harus Image <i>(.PDF)</i></li>
                </ul>
            </div>
        </div> 
    </div>
   
    <?php endif; ?>
</div>
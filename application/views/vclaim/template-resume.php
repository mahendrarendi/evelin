<div class="row">
    <div class="col-md-12">
        <?php 
            
            $pasien = $data_resume['identitas'];
            $vital  = $data_resume['vitalsign'];
            $tind   = $data_resume['tindakan'];
            $lab    = $data_resume['laboratorium'];
            $rad    = $data_resume['radiologi'];
            $obat   = $data_resume['obat'];
            $diag   = $data_resume['diagnosa'];

            $ttd = $pasien['dokter'].' RSU Queen Latifa Yogyakarta '.$pasien['tglperiksa'] .' SIP : '.$pasien['sip_dokter'];

            $dtv =''; $dtt =''; $dtl =''; $dtr =''; $dto=''; $dtd ='';
            foreach($vital as $row){ $dtv.=$row['namaicd'] .': '.$row['nilai'].' '.$row['satuan'].', '; }
            foreach($tind as $row){ $dtt.=$row['icd'] .': '.$row['namaicd'].'<br>'; }
            foreach($lab as $row){ $dtl.=( ($row['namaicd'] == null) ? '' : $row['namaicd'] ) .': '.$row['nilai'].' , '; }
            foreach($rad as $row){ $dtr.=( ($row['namaicd'] == null) ? '' : $row['namaicd'] ) .'<br>'; }
            foreach($diag as $row){ $dtd.=( ($row['namaicd'] == null) ? '' : $row['icd'] ).' '.$row['namaicd'].'<br>'; }

            $keteranganradiologi = $data_resume['ketradiologi']['keteranganradiologi'].( ( $data_resume['ketradiologi']['saranradiologi'] == '' ) ? '':'<b>Saran:</b>' ).$data_resume['ketradiologi']['saranradiologi'];
        ?>
    
    </div>
    <div class="banner-header">
        <img class="img-banner" src="<?= base_url('assets/img/headerresume.svg'); ?>" alt="resume">
    </div>
    <hr style="border: 1px solid black !important;opacity: 1;">
    <table class="table textcolor-table table-noborder">
        <tr>
            <th width="15%">Tgl.Periksa</th>
            <td width="5px">:</td>
            <td><?= $pasien['tglperiksa'];?></td>
        </tr>
        <tr>
            <th>Klinik</th>
            <td width="5px">:</td>
            <td><?= $pasien['namaunit']; ?></td>
        </tr>
        <tr>
            <th>No.RM/Nama</th>
            <td width="5px">:</td>
            <td><?= $pasien['norm'].'/'.$pasien['identitas'].' '.$pasien['namalengkap']; ?></td>
        </tr>
        <tr>
            <th>Tgl.Lahir</th>
            <td width="5px">:</td>
            <td><?= $pasien['tanggallahir']; ?></td>
        </tr>
    </table>
    <div class="title-header text-center mb-3">
        <h5 style="color:black;text-transform: uppercase;">Resume Pelayanan Rawat Jalan</h5>
    </div>
    <div class="content-body">
        <div class="data-subyektif mb-4">
            <h6 style="color:black;text-transform: uppercase;">Data Subyektif :</h6>
            <hr>
            <?= $data_resume['ketradiologi']['anamnesa']; ?>
        </div>
        <div class="data-obyektif mb-4">
            <h6 style="color:black;text-transform: uppercase;">Data Obyektif</h6>
            <hr>
            <?= $dtv.'<br>'.$data_resume['ketradiologi']['keterangan']; ?>
        </div>
        <div class="data-diagnosa mb-4">
            <h6 style="color:black;text-transform: uppercase;">Data Diagnosa</h6>
            <hr>
            <?// $dtd.' '.$data_resume['ketradiologi']['diagnosa']; ?>
            <?= $data_resume['ketradiologi']['diagnosa']; ?>
        </div>
        <div class="data-tindakan mb-4">
            <h6 style="color:black;text-transform: uppercase;">Data Tindakan</h6>
            <hr>
            <?= empty( $dtt ) ? '-' : $dtt;  ?>
        </div>
        <div class="data-laboratorium mb-4">
            <h6 style="color:black;text-transform: uppercase;">Data Laboratorium</h6>
            <hr>
            <?= empty( $dtl ) ? '-' : $dtl; ?>
        </div>
        <div class="data-radiologi mb-4">
            <h6 style="color:black;text-transform: uppercase;">Data Radiologi</h6>
            <hr>
            <p><?= empty( $dtr ) ? '-' : $dtr; ?></p>
            <?= 'Keterangan Radiologi : '. $keteranganradiologi;?>
        </div>
        <div class="data-resep">
            <h6 style="color:black;text-transform: uppercase;">Data Resep</h6>
            <hr>
            <?php if( !empty( $data_resume['grup'] ) ): ?>
            <table class="table table-noborder textcolor-table">
                <tbody>
                    <?= resep_obat($data_resume['grup'],$obat); ?>
                </tbody>
            </table>
            <?php else: ?>
                -
            <?php endif; ?>
        </div>
        <div class="ttd-dpjp">
            <div style="text-align:center;padding-left:60%;">
            DOKTER 
            <br>
                <?= convert_to_qrcode($ttd,'100x100'); ?>
            <br> 
            <?=$pasien['dokter'] ?>
        </div>
        </div>
    </div>
</div>
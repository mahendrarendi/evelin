<div class="p-0">
              <div class="table-responsives">
                <table class="table td-nospace table-hover table-striped table-bordered align-items-center mb-0 qldt">
                  <thead class="bgheader">
                    <tr>
                      <th class="text-uppercase text-secondary text-xxs font-weight-bolder  ps-2" width="10%">No RM</th>
                      <th class="text-uppercase text-secondary text-xxs font-weight-bolder  ps-2">Nama</th>
                      <th class="text-uppercase text-secondary text-xxs font-weight-bolder  ps-2">No.SEP</th>
                      <th class="text-uppercase text-secondary text-xxs font-weight-bolder  ps-2">No.Kartu</th>
                      <th class="text-uppercase text-secondary text-xxs font-weight-bolder  ps-2">Poli</th>
                      <th class="text-uppercase text-secondary text-xxs font-weight-bolder  ps-2">Jenis Layanan</th>
                      <th class="text-uppercase text-secondary text-xxs font-weight-bolder  ps-2">Tanggal Periksa</th>
                      <th width="10%" class="text-center text-uppercase text-secondary text-xxs font-weight-bolder "></th>
                    </tr>
                  </thead>
                  <tbody>
                  
                  
                    <div class="security-check">
                        <input type="hidden" class="nonce" name="<?= $csrf['name'] ?>" value="<?= $csrf['hash']; ?>">
                    </div>
                  

                   <?php

                    $nosep_kosong = []; 
                    $nosep_isi = [];
                    $datavalidasi = [];

                    if( $listvclaim['code'] == '200' ) :
                      
                      if( !empty( $aktifpengajuan ) || is_superadmin() || is_klaim() || is_poliumum() || is_igd() || is_pendaftaran() ):
                      $panjang_nosep = 19;       
                      foreach( $listvclaim['response'] as $row ): 
                        
                        $nosep_jumlah = (int) strlen(str_replace(' ','',$row['nosep']));

                        $bg                    = '';
                        $color                 = '';
                        $validasi_jumlah_nosep = '';

                        if( $nosep_jumlah != $panjang_nosep )
                        {
                            $bg = 'style="background:#f53636;"';
                            $color = 'style="color:white;"'; 
                            $nosep_kosong[] =$row['nosep'];

                            if( $nosep_jumlah <= $panjang_nosep ){
                              $validasi_jumlah_nosep = '<p class="mb-0 mt-3 text-xs">[-] No SEP Harus 19 Digit</p>';
                            }
                            else
                            {
                              $validasi_jumlah_nosep = '<p class="mb-0 mt-3 text-xs">[-] No SEP Kelebihan, NO SEP Harus 19 Digit</p>';
                            }

                        }
                        else
                        {
                            $nosep_isi[] =$row['nosep'];
                        }

                        $params = [
                          'norm'=>$row['norm'],
                          'tglperiksa'=>$row['tglperiksa'],
                          'idunit'=>$row['idunit'],
                        ];
                        
                        /** CEK Validasi Data SEP */
                        $noSep = $row['nosep'];
                        $query_validasi = $this->mvclaim->get_validasi_klaim($noSep,$params);

                        $cek_validasi = false;
                        if( !is_bpjs() )
                        {
                          if( $nosep_jumlah === $panjang_nosep )
                          {
                            if( !empty( $query_validasi ) )
                            {
                                $bg = 'style="background:#007300;"';
                                $color = 'style="color:white;"';
                                $cek_validasi = true;
                                $datavalidasi[] = $query_validasi->sep;
                            }
                          }
                        }
                          
                        if( is_bpjs() )
                        {
                          if( !empty($query_validasi->sep) )
                          {
                              if( $row['nosep'] == $query_validasi->sep )
                              {
                                $row;
                              }
                          }
                          else
                          {
                              continue;
                          }
                        }
                        /** END Cek Validasi data SEP */

                       

                    ?>
                      
                    <tr <?= $bg; ?>>
                      <td <?= $color; ?>>
                      <p class="text-md font-weight-bold mb-0">
                        <span class="badge badge-sm bg-gradient-warning"><?= $row['norm']; ?></span>
                      </p>
                      <input type="hidden" name="norm" value="<?= $row['norm']; ?>">
                      <input type="hidden" name="tglperiksa" value="<?= $row['tglperiksa']; ?>">
                      <input type="hidden" name="sepklaim" value="<?= $row['nosep']; ?>">
                      <input type="hidden" name="idunit" value="<?= $row['idunit']; ?>">
                    </td>
                      <td <?= $color; ?>>
                        <p class="text-xs font-weight-bold mb-0"><?= $row['namalengkap']; ?></p>
                      </td>
                      <td <?= $color; ?>>
                        <p class="text-xs font-weight-bold mb-0"><?= $row['nosep']; ?></p>
                        <?= $validasi_jumlah_nosep; ?>
                      </td>
                      <td <?= $color; ?>><p class="text-xs font-weight-bold mb-0"><?= $row['nojkn'] ?></p></td>
                      <td <?= $color; ?>><p class="text-xs font-weight-bold mb-0"><?= $row['poli'] ?></p></td>
                      <td <?= $color; ?>><p class="text-xs font-weight-bold mb-0"><?= $row['jenispemeriksaan']; ?></p></td>
                      <td <?= $color; ?>><p class="text-xs font-weight-bold mb-0">
                        
                        <?php if( is_superadmin() || is_klaim() ): 
                            
                            if( $cek_validasi == true  ) : ?>
                              
                              <p class="text-xs font-weight-bold mb-0"><span class="badge badge-sm bg-gradient-info">Valid</span></p>
                            
                            <?php else: ?>
                              
                              <p class="text-xs font-weight-bold mb-0"><span class="badge badge-sm bg-gradient-danger">Belum Valid</span></p>
                            
                            <?php endif; ?>
                        
                        <?php endif; ?>

                        
                        <div class="text-xxs font-weight-bold mb-0 mt-2">
                          <?= format_tanggal($row['tglperiksa'],'d-m-Y') ?><br><em><?= ($row['idstatuskeluar'] == '2') ? 'Status : Selesai' : ''; ?></em>
                        </div>
                        
                        <?php if( ( !empty( $row['nosep'] ) || $row['nosep'] != 0 ) AND is_superadmin() || is_klaim() ): ?>
                          <?= html_checkbox_validasi($cek_validasi); ?>
                        <?php endif; ?>

                      </td>
                      <td class="align-middle">
                  
                        <a style="width:100%;" href="<?= base_url('detailclaim/'.encrypt_url($row['norm'].'_'.strtotime( format_tanggal($row['tglperiksa'] ) ).'_'.$row['idunit'] ));?>" class="mb-2 btn btn-success btn-xs mb-0 font-weight-bold" data-toggle="tooltip" >
                          <i class="fa fa-eye"></i> Detail
                        </a>
                        
                        <?php if(is_superadmin() || is_klaim()): ?>
                          <a   href="<?= base_url('downloadfiles/'.encrypt_url($row['norm'].'_'.strtotime( format_tanggal($row['tglperiksa'] ) ).'_'.$row['idunit'] ));?>" class="mb-2 btn btn-warning btn-xs mb-0 font-weight-bold download-filelists" data-toggle="tooltip" >
                            <i class="fa fa-download"></i> 
                          </a>
                          <a data-jr="<?= $row['jenisrujukan']; ?>" data-name="<?= $csrf['name'] ?>" data-hash="<?= $csrf['hash']; ?>"  href="javascript:void(0)" data-norm="<?= $row['norm']; ?>" data-tglperiksa="<?= $row['tglperiksa']; ?>" data-idunit="<?= $row['idunit']; ?>" class="mb-2 btn btn-primary btn-xs mb-0 font-weight-bold detail-kekurangan" data-toggle="tooltip" >
                            <i class="fa fa-warning"></i> 
                          </a>
                        <?php endif; ?>

                      </td>
                    </tr>

                        <?php endforeach; ?>
                      <?php endif; ?>
                    <?php endif; ?>

                  </tbody>
                </table>
                </div>
              </div>
              <?php if(is_klaim()): ?>
                
                <div>
                  <p class="mb-2 text-xs font-weight-bold" style="color:red;">* Disclaimer: </p>
                  <p class="mb-0 text-xs"  style="color:black;">
                    <span style="color:red;">[!]</span> Hitungan Nomor SEP Kosong ini Terdiri dari hitungan seperti, SEP Tidak Diisi, Jumlah No SEP Kelebihan, Jumlah No SEP Kurang, No. SEP 0 atau 00 atau 0000, dst 
                  </p>
                  <p class="mb-0 text-xs"  style="color:black;">
                    <span style="color:red;">[!]</span> Hitungan Nomor SEP Validasi Merupakan hitungan dari data yang sudah divalidasi 
                  </p>
                  <p class="mb-0 text-xs"  style="color:black;">
                    <span style="color:red;">[!]</span> Hitungan Nomor SEP Isi Merupakan hitungan dari jumlah SEP yang terisi dengan benar yaitu 19 digit
                  </p>
                </div>

                <p class="mb-0 mt-4" style="font-weight:bold;">Jumlah Nomor SEP Kosong : <?= count( $nosep_kosong ); ?></p>
                <p class="mb-0" style="font-weight:bold;">Jumlah Nomor SEP Validasi : <?= count( $datavalidasi ); ?></p>
                <p class="mb-0" style="font-weight:bold;">Jumlah Total Nomor SEP ISI : <?= count( $nosep_isi ); ?></p>
              <?php endif; ?>
            </div>
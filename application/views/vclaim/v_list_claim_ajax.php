<div class="row">
    <div class="col-md-12">
        <div class="card">
         
            <div class="card-header pb-0">
                <?php 
                  if( isset($_GET['oke']) ){

                    echo '<pre>';
                    print_r($testvclaim);
                    echo '</pre>';
                  }
                ?>
                <h6>Data Vclaim</h6>
                <div class="super-vclaim" data-value="<?= ( is_klaim() || is_igd() || is_poliumum() || is_superadmin() || is_pendaftaran() ) ? true  : false ; ?>"></div>
                <div class="login-vclaim-user" data-value="<?= ( is_klaim() ) ? true  : false ; ?>"></div>
                <div class="filter-claim">

                    
                    <?php if( !empty( $bulanpengajuan ) ): ?>
                        <div class="bulanpengajuan-vclaim" data-bulan="<?= $bulanpengajuan; ?>"></div>
                    <?php endif; ?>
                    <?php if( !empty( $aktifpengajuan ) ): ?>
                        <div class="aktifpengajuan-vclaim" data-aktif="<?= $aktifpengajuan; ?>"></div>
                        
                    <?php endif; ?>

                  <form class="form" id="frm-filter-claim">
                    <ul class="list-inline">
                      <li>
                        <div class="form-group">
                          <label>Tanggal</label>
                          <input type="text" autocomplete="off" class="form-control vclaimtgl" name="tanggal">
                        </div>
                      </li>
                      <li>
                        <div class="form-group">
                          <label>Jenis Layanan</label>
                          <select name="jenis-layanan" class="form-control qlselect2">
                            <option value="rajal" <?= ( isset($_GET['jenis-layanan']) && $_GET['jenis-layanan'] == 'rajal' ) ? 'selected' : ''; ?>>Rawat Jalan</option>
                            <option disabled value="rajalnap" <?= ( isset($_GET['jenis-layanan']) && $_GET['jenis-layanan'] == 'rajalnap' ) ? 'selected' : ''; ?>>Rawat Inap (Pengembangan)</option>
                          </select>
                        </div>
                      </li>
                      <li>
                        <div class="form-group">
                          <button style="display:none;" class="btn btn-warning filter"> <i class="fa fa-search"></i> Cari</button>
                          <a href="javascript:void(0)" data-name="<?= $csrf['name'];?>" data-hash="<?= $csrf['hash']; ?>" class="btn btn-warning list-ajaxtb"> <i class="fa fa-search"></i> Cari</a>
                          <a href="javascript:void(0)" class="btn btn-primary reset-refresh"> <i class="fa fa-refresh"></i> Refresh</a>
                        </div>
                      </li>
                    </ul>
                  </form>
                </div>
            </div>
            <div class="card-body">

              <!-- Download Berkas Pertanggal -->
              <?php if(is_klaim() || is_superadmin() ): ?>
                <div class="downloadall-berkas-tgl"></div>
              <?php endif; ?>
              <!-- end Download Berkas Pertanggal -->

            <div class="wp"></div>

        </div>
    </div>
</div>

<!-- Modal -->
<div id="modalEvelin" class="modal fade" data-bs-backdrop="static" data-bs-keyboard="false" tabindex="-1" aria-labelledby="staticBackdropLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
        <div class="modal-header">
            <h5 class="modal-title" id="exampleModalLongTitle">Modal title</h5>
            
        </div>
        <div class="modal-body">
            ...
        </div>
        <div class="modal-footer">
            <button type="button" class="btn btn-secondary modalCloseList" data-dismiss="modal">Close</button>
        </div>
        </div>
    </div>
    </div>
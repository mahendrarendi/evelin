<style>
#frm-sep .form-inline {
  display: inline-block;
}
</style>
<?php 
    $class_hidden = empty($row_sep->name_image) ? 'd-block' : 'd-none';
?>
<div class="row">

    <div class="security-check">
        <input type="hidden" class="nonce" name="<?= $csrf['name'] ?>" value="<?= $csrf['hash']; ?>">
        <input type="hidden" class="norm" name="norm" value="<?= $params['norm']; ?>">
        <input type="hidden" class="tglperiksa" name="tglperiksa" value="<?= $params['tglperiksa']; ?>">
    </div>
    
    <?php if( !empty($row_sep) ): ?>
        <div class="title">
            <h4>SEP</h4>
        </div>
    <?php endif; ?>

    <?php if(is_superadmin() || is_klaim()): ?>
    <div class="col-md-6">
        <form class="form <?= $class_hidden; ?>"  id="frm-sep"  enctype="multipart/form-data" >
            
            <!-- add idunit -->
            <input type="hidden" class="idunit-hidden" value="<?= $idunit; ?>" name="idunit">

            <div class="form-group">
                <div class="form-inline">
                    <label style="font-size:unset;">Silahkan Upload File SEP <span class="required">*</span></label>
                    <input  required accept="image/*"  type="file" name="fileimage" class="form-control">
                </div>
                <div class="form-inline">
                    <button type="submit" class="btn btn-danger mb-0">Upload</button>
                </div>
            </div>
        </form>

        <?php if( !empty($row_sep->name_image) ): ?>
        <div class="show-image">
            <div class="form-group" data-id="sep">
                <button class="btn btn-primary btn-sm editfile-upload-image" type="button"><i class="fa fa-edit"></i> Edit</button>
                <button class="btn btn-danger btn-sm deletefile-upload-image" data-id="<?= $row_sep->idsep; ?>" type="button"> <i class="fa fa-trash"></i> Hapus</button>
            </div>
            <img class="rounded img-fluid" src="<?= base_url('assets/sep/'.$row_sep->name_image); ?>">
        </div>
        <?php endif; ?>
        
    </div>
    <div class="col-md-6">
        <div class="card bg-danger text-white">
            <div class="card-body">
                <p class="mb-0"><strong>(*) Catatan Upload SEP:</strong></p>
                <ul class="mb-0">
                    <li>Upload  File Maximal <i>8MB</i></li>
                    <li>Tipe File Harus Image <i>(.jpg atau .jpeg)</i></li>
                </ul>
            </div>
        </div> 
    </div>
    <?php else: ?>
        <?php if( !empty($row_sep->name_image) ): ?>
        <div class="show-image">
            <img class="rounded img-fluid" src="<?= base_url('assets/sep/'.$row_sep->name_image); ?>">
        </div>
        <?php else: ?>
            <div class="alert alert-danger text-white" role="alert">
                Data File Rujukan Baru Belum Di upload
            </div>
        <?php endif; ?>
    <?php endif; ?>
</div>
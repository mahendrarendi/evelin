<style>
#frm-rujukanbaru .form-inline {
  display: inline-block;
}
</style>
<?php 
    $class_hidden = empty($row_rujukanbaru->name_rujukanbaru) ? 'd-block' : 'd-none';
?>
<div class="row">
    
    <?php if( !empty($row_rujukanbaru->name_rujukanbaru) ): ?>
        <div class="title">
            <h4>Surat Rujukan Baru</h4>
        </div>
    <?php endif; ?>

    <?php if(is_superadmin() || is_klaim()): ?>
    <div class="col-md-6">
        <form class="form <?= $class_hidden; ?>"  id="frm-rujukanbaru"  enctype="multipart/form-data" >
            
            <!-- add idunit -->
            <input type="hidden" class="idunit-hidden" value="<?= $idunit; ?>" name="idunit">

            <div class="form-group">
                <div class="form-inline">
                    <label style="font-size:unset;">Silahkan Upload File Rujukan Baru <span class="required">*</span></label>
                    <input  required accept="image/*"  type="file" name="filerujukanbaru" class="form-control">
                </div>
                <div class="form-inline">
                    <button type="submit" class="btn btn-danger mb-0">Upload</button>
                </div>
            </div>
        </form>

        <?php if( !empty($row_rujukanbaru->name_rujukanbaru) ): ?>
        <div class="showrujukanbaru">
            <div class="form-group">
                <button class="btn btn-primary btn-sm editfile-rujukanbaru" type="button"><i class="fa fa-edit"></i> Edit</button>
                <button class="btn btn-danger btn-sm deletefile-rujukanbaru" data-id="<?= $row_rujukanbaru->idrujukanbaru; ?>" type="button"> <i class="fa fa-trash"></i> Hapus</button>
            </div>
            <img class="rounded img-fluid" src="<?= base_url('assets/rujukanbaru/'.$row_rujukanbaru->name_rujukanbaru); ?>">
        </div>
        <?php endif; ?>
        
    </div>
    <div class="col-md-6">
        <div class="card bg-danger text-white">
            <div class="card-body">
                <p class="mb-0"><strong>(*) Catatan Upload Rujukan Baru:</strong></p>
                <ul class="mb-0">
                    <li>Upload  File Maximal <i>8MB</i></li>
                    <li>Tipe File Harus Image <i>(.jpg atau .jpeg)</i></li>
                </ul>
            </div>
        </div> 
    </div>
    <?php else: ?>
        <?php if( !empty($row_rujukanbaru->name_rujukanbaru) ): ?>
        <div class="showrujukanbaru">
            <img class="rounded img-fluid" src="<?= base_url('assets/rujukanbaru/'.$row_rujukanbaru->name_rujukanbaru); ?>">
        </div>
        <?php else: ?>
            <div class="alert alert-danger text-white" role="alert">
                Data File Rujukan Baru Belum Di upload
            </div>
        <?php endif; ?>
    <?php endif; ?>
</div>
<style>
#frm-dokumenlainlain .form-inline {
  display: inline-block;
}
</style>
<?php 
    $class_hidden = empty($row_dokumenlain->name_image) ? 'd-block' : 'd-none';
?>
<div class="row">
    
    <?php if( !empty($row_dokumenlain->name_image) ): ?>
        <div class="title">
            <h4>Dokumen Lain Lain</h4>
        </div>
    <?php endif; ?>

    <?php if(is_superadmin() || is_klaim() || is_pendaftaran()): ?>
    <div class="col-md-6">
        <form class="form"  id="frm-dokumenlainlain"  enctype="multipart/form-data" >
            <div class="form-group">
                <div class="form-inline">
                    <label style="font-size:unset;">Silahkan Upload File Dokumen Lain Lain <span class="required">*</span></label>
                    <input  required accept="image/jpg, image/jpeg"  type="file" multiple="multiple" name="fileimagelainlain[]" class="form-control fileimagelainlain">
                </div>
                <div class="form-inline">
                    <button type="submit" class="btn btn-danger mb-0">Upload</button>
                </div>
            </div>
        </form> 
    </div>
    <div class="col-md-6">
        <div class="card bg-danger text-white">
            <div class="card-body">
                <p class="mb-0"><strong>(*) Catatan Upload Dokumen:</strong></p>
                <ul class="mb-0">
                    <li>Upload  File Maximal <i>8MB</i></li>
                    <li>Tipe File Harus Image <i>(.jpg atau .jpeg)</i></li>
                </ul>
            </div>
        </div> 
    </div>

    <!-- GRID FILE -->
    <?php if( !empty($row_dokumenlain->name_image) ): ?>
        <div class="row mt-4"> 
        <?php 
            $images = explode(',',$row_dokumenlain->name_image);
            foreach( $images as $key => $row ):?> 
                <div class="col-md-3">
                    <div class="group-image">
                        <a target="_blank" href="<?=base_url('assets/dokumenlainlain/'.$row); ?>">
                            <img class="img-responsive img-thumbnail " src="<?= base_url('assets/dokumenlainlain/'.$row); ?>">
                        </a>
                        <div class="card">
                            <button type="button" data-id="<?= $row_dokumenlain->iddokumenlainlain; ?>" data-keyimage="<?= $key; ?>" class="delete-imageslain btn btn-danger btn-sm rounder-circle mb-0 mt-2"><i class="fa fa-trash"></i> Hapus</button>
                        </div>
                    </div>
                </div>
                <?php endforeach; ?>
            </div>
        <?php endif; ?>
    <!-- END GRID FILE -->

    <?php else: ?>
        <?php if( !empty($row_dokumenlain->name_image) ): ?>
        <div class="row">
            <?php
            $images = explode(',',$row_dokumenlain->name_image);
            foreach( $images as $key => $row ):?> 
                <div class="col-md-6 mb-3">
                    <div class="group-image">
                        <a target="_blank" href="<?=base_url('assets/dokumenlainlain/'.$row); ?>">
                            <img class="img-responsive rounded img-fluid " src="<?= base_url('assets/dokumenlainlain/'.$row); ?>">
                        </a>
                    </div>
                </div>
            <?php endforeach; ?>
        </div>
        <?php else: ?>
            <div class="alert alert-danger text-white" role="alert">
                Data File Dokumen Lain  Belum Di upload
            </div>
        <?php endif; ?>
    <?php endif; ?>
</div>
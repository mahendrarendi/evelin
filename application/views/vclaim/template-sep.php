<?php 

$noSep          = '';
$tglSep         = '';
$jnsPelayanan   = '';
$poli           = '';
$kelasRawat     = '';

$penjamin       = '';
$catatan        = '';

$noKartu        = ''; 
$nama           = ''; 
$tglLahir       = ''; 
$kelamin        = ''; 
$jnsPeserta     = ''; 
$hakKelas       = ''; 
$noMr           = '';

$kdDPJP         = '';
$nmDPJP         = '';

$notif_katarak  = '';

$klsRawatHak    = '';
$rujukan_response   = "";
$peserta_rujukan    = "";
$mr_rujukan         = "";
$telepon_rujukan    = "";
$provPerujuk        = "";
$diagnosa_rujukan   = "";
$diagnosa_kode      = "";
$diagnosa_nama      = "";
$prb                = "";
$diagnosa_klinis    = "";

if( isset($_GET['test']) ){

    echo '<pre>';
    print_r( $data_bpjs );
    echo '</pre>';
}

if( !empty( $data_bpjs['sep'] ) && $data_bpjs['sep']['metaData']['code'] == 200 ){

    $datasep        = $data_bpjs['sep'];


    if( $datasep['metaData']['code'] == '200' ){
        
        
        $datarujukan    = $data_bpjs['rujukan'];

        $sep_response   = $datasep['response'];
        $noSep          = $sep_response['noSep'];
        $tglSep         = $sep_response['tglSep'];
        $jnsPelayanan   = $sep_response['jnsPelayanan'];
        $poli           = $sep_response['poli'];
        $kelasRawat     = $sep_response['kelasRawat'];

        $penjamin       = $sep_response['penjamin'];
        $catatan        = $sep_response['catatan'];

        $peserta        = $sep_response['peserta']; 
        $noKartu        = $peserta['noKartu']; 
        $nama           = $peserta['nama']; 
        $tglLahir       = $peserta['tglLahir']; 
        $kelamin        = jenis_kelamin($peserta['kelamin']); 
        $jnsPeserta     = $peserta['jnsPeserta']; 
        $hakKelas       = $peserta['hakKelas']; 
        $noMr           = $peserta['noMr'];

        $dpjp           = $sep_response['dpjp'];
        $kdDPJP         = $dpjp['kdDPJP'];
        $nmDPJP         = $dpjp['nmDPJP'];


        $klsRawat       = $sep_response['klsRawat'];
        $klsRawatHak    = $klsRawat['klsRawatHak'];

        $katarak        = $sep_response['katarak'];
        $notif_katarak  = ( $katarak ) ? '*PASIEN OPERASI KATARAK' : '' ;

        if(!empty($data_bpjs['rekon'])){ 
            $rekon          = $data_bpjs['rekon'];
            $response_rekon = $rekon['response'];

            if( empty($response_rekon['sep']) ){
                $rekon_provPerujuk = $response_rekon['provPerujuk'];

                if(!empty($sep_response['diagnosa']) ){
                    $diagnosa_klinis= $sep_response['diagnosa'] ;
                }else{
                    $diagnosa_klinis    = $response_rekon['diagnosa'] ;
                }

                $provPerujuk        = $rekon_provPerujuk['nmProviderPerujuk'];
            }else{
                $rekon_provPerujuk = $response_rekon['sep']['provPerujuk'];

                if(!empty($sep_response['diagnosa']) ){
                    $diagnosa_klinis= $sep_response['diagnosa'] ;
                }else{
                    $diagnosa_klinis    = $response_rekon['sep']['diagnosa'] ;
                }
                
                $provPerujuk        = $rekon_provPerujuk['nmProviderPerujuk'];
            }
            

        }else if( $datarujukan['metaData']['code'] == '200' ){
            #rujukan
            $rujukan_response   = $datarujukan['response']['rujukan'];
            $peserta_rujukan    = $rujukan_response['peserta'];
            $mr_rujukan         = $peserta_rujukan['mr'];
            $telepon_rujukan    = $mr_rujukan['noTelepon'];

            $provPerujuk        = $rujukan_response['provPerujuk']['nama'];
               
            $diagnosa_rujukan   = $rujukan_response['diagnosa'];
            $diagnosa_kode      = $diagnosa_rujukan['kode'];
            $diagnosa_nama      = $diagnosa_rujukan['nama'];

            if(!empty($sep_response['diagnosa']) ){
                $diagnosa_klinis= $sep_response['diagnosa'] ;
            }else{
                $diagnosa_klinis= $diagnosa_kode.' - '.$diagnosa_nama;
            }
    
            
            $informasi          = $peserta_rujukan['informasi']; 
            $prb                = $informasi['prolanisPRB']; 

        }else{
            $rujukan_response   = "";
            $peserta_rujukan    = "";
            $mr_rujukan         = "";
            $telepon_rujukan    = "";
            $provPerujuk        = "";
            $diagnosa_rujukan   = "";
            $diagnosa_kode      = "";
            $diagnosa_nama      = "";
            $prb                = "";
            $diagnosa_klinis    = "";
        }
    }
}

?>

<div class="row">
    <div class="col-md-12">
        <div class="banner-header mb-4 sep-header-prolanis">
            <img class="img-banner" src="<?= base_url('assets/img/header-sep-1.png'); ?>" alt="SEP">
        </div>
        <div class="sep-header-prolanis" style="color:black;text-align: center;">
            <?= $prb; ?>
        </div>
        <div class="security-check">
            <input type="hidden" class="nonce" name="<?= $csrf['name'] ?>" value="<?= $csrf['hash']; ?>">
            <input type="hidden" class="norm" name="norm" value="<?= $params['norm']; ?>">
            <input type="hidden" class="tglperiksa" name="tglperiksa" value="<?= $params['tglperiksa']; ?>">
        </div>

        <input type="hidden" class="sepklaim" name="sepklaim" value="<?= $noSep; ?>">
        
    </div>
    <div class="row">
    <div class="col-md-7">
        <div class="table-responsive">
            <table class="table tb-ql">
                <tbody>
                    <tr>
                        <th>No.SEP</th>
                        <td style="padding-right: 5px;padding-left: 25px;">:</td>
                        <td><?= $noSep; ?></td>
                    </tr>
                    <tr>
                        <th>Tgl.SEP</th>
                        <td style="padding-right: 5px;padding-left: 25px;">:</td>
                        <td><?= $tglSep; ?></td>
                    </tr>
                    <tr>
                        <th>No.Kartu</th>
                        <td style="padding-right: 5px;padding-left: 25px;">:</td>
                        <td><?= $noKartu .' ( MR. '.$noMr.')'; ?></td>
                    </tr>
                    <tr>
                        <th>Nama Peserta</th>
                        <td style="padding-right: 5px;padding-left: 25px;">:</td>
                        <td><?= $nama ?></td>
                    </tr>
                    <tr>
                        <th>Tgl.Lahir</th>
                        <td style="padding-right: 5px;padding-left: 25px;">:</td>
                        <td><?= $tglLahir.' Kelamin : '.$kelamin; ?></td>
                    </tr>
                    <tr>
                        <th>No.Telepon</th>
                        <td style="padding-right: 5px;padding-left: 25px;">:</td>
                        <td><?= $telepon_rujukan; ?></td>
                    </tr>
                    <tr>
                        <th>Sub/Spesialis</th>
                        <td style="padding-right: 5px;padding-left: 25px;">:</td>
                        <td><?= $poli;   ?></td>
                    </tr>
                    <tr>
                        <th>Dokter</th>
                        <td style="padding-right: 5px;padding-left: 25px;">:</td>
                        <td><?= $nmDPJP; ?></td>
                    </tr>
                    <tr>
                        <th>Faskes Perujuk</th>
                        <td style="padding-right: 5px;padding-left: 25px;">:</td>
                        <td><?= $provPerujuk; ?></td>
                    </tr>
                    <tr>
                        <th>Diagnosa Awal</th>
                        <td style="padding-right: 5px;padding-left: 25px;">:</td>
                        <td><?= $diagnosa_klinis; ?></td>
                    </tr>
                    <tr>
                        <th>Catatan</th>
                        <td style="padding-right: 5px;padding-left: 25px;">:</td>
                        <td><?= $catatan; ?></td>
                    </tr>
                </tbody>
            </table>
        </div>
    </div>
    <div class="col-md-5">
        <?php
            if( !empty($notif_katarak) ){
                echo '<div class="ket-op" style="color:black;">'.$notif_katarak.'</div>';
            }
        ?>
        <div class="table-reponsive">
            <table class="table tb-ql">
                <tbody>
                    <tr>
                        <th>Peserta</th>
                        <td style="padding-right: 5px;padding-left: 10px;padding-bottom: 30px;">:</td>
                        <td><?= $jnsPeserta; ?></td>
                    </tr>
                    <tr>
                        <th>Jns.Rawat</th>
                        <td style="padding-right: 5px;padding-left: 10px;">:</td>
                        <td><?= $jnsPelayanan; ?></td>
                    </tr>
                    <tr>
                        <th>Jns.Kunjungan</th>
                        <td style="padding-right: 5px;padding-left: 10px;">:</td>
                        <td>
                            
                        <?php 
                        if( !empty($catatan) && $catatan != 'EMERGENCY'  ){
                            echo '-';
                        }else if($catatan === 'EMERGENCY'){
                            echo  '- Konsultasi Dokter (Pertama)';
                        }else{
                            if( akses_jenispemeriksaan($data_array['jns_rujukan']) == 'skdp' && $data_array['cek_rencana_kontrol'] == TRUE ) {
                                    echo '- Kunjungan Kontrol (Ulangan)';
                            }else{
                                    echo  '- Konsultasi Dokter (Pertama)';
                            } 
                        } 
                        
                        ?>
                        </td>
                    </tr>
                    <tr>
                        <th></th>
                        <td style="padding-right: 5px;padding-left: 10px;">:</td>
                        <td>
                            <?php 
                                if( !empty($catatan) && $catatan != 'EMERGENCY'  ){
                                    echo '-';
                                }else if($catatan === 'EMERGENCY'){
                                    echo  '-';
                                }else{
                                    if( akses_jenispemeriksaan($data_array['jns_rujukan']) == 'skdp' && $data_array['cek_rencana_kontrol'] == TRUE ) {
                                            echo '- Prosedur tidak berkelanjutan';
                                    }else{
                                            echo  '-';
                                    } 
                                } 
                            
                            ?>
                        </td>
                    </tr>
                    <tr>
                        <th>Poli Perujuk</th>
                        <td style="padding-right: 5px;padding-left: 10px;">:</td>
                        <td>-</td>
                    </tr>
                    <tr>
                        <th>Kls.Hak</th>
                        <td style="padding-right: 5px;padding-left: 10px;">:</td>
                        <td><?= $hakKelas; ?></td>
                    </tr>
                    <tr>
                        <th>Kls.Rawat</th>
                        <td style="padding-right: 5px;padding-left: 10px;">:</td>
                        <td><?= '-'; ?></td>
                    </tr>
                    <tr>
                        <th>Penjamin</th>
                        <td style="padding-right: 5px;padding-left: 10px;"></td>
                        <td></td>
                    </tr>
                </tbody>
            </table>
        </div>
        
    </div>
    <div class="col-md-8">
        <div class="catatan mb-3">
            *Saya menyetujui BPJS Kesehatan untuk : 
            <ol class="list-footnote" type="a" style="margin:0;padding-left: 15px;">
                <li>Membuka dan atau menggunakan informasi medis pasien untuk keperluan administrasi, pembayaran asuransi atau jaminan pembiayaan kesehatan.</li>
               <li>memberikan akses informasi atau riwayat pelayanan kepada dokter/tenaga medis pada RSU QUEEN LATIFA untuk kepentingan pemeliharaan kesehatan, pengobatan, penyembuhan,dan perawatan pasien</li>
            </ol>
            *Saya mengetahui dan memahami :
            <ol class="list-footnote" type="a"  style="margin:0;padding-left: 15px;">
                <li>Rumah sakit dapat melakukan koordinasi dengan PT Jasa Raharja/PT Taspen/PT ASABRI/BPJS Ketenagakerjaan atau penjamin lainya. jika peserta merupakan pasien yang mengalami kecelakaan lalulintas dan/atau kecelakaan kerja</li>
                <li>SEP bukan sebagai bukti penjaminan peserta</li>
            </ol>

            **Dengan tampilnya luaran SEP elektronik ini merupakan hasil validasi terhadap eligibilitas Pasien secara elektronik <br>
            &nbsp;&nbsp; (validasi finger print atau biometrik / sistem validasi lain) <br>
            &nbsp;&nbsp; dan selanjutnya Pasien dapat mengakses pelayanan kesehatan rujukan sesuai ketentuan berlaku. <br>
            &nbsp;&nbsp; Kebenaran dan keaslian atas informasi data Pasien menjadi tanggung jawab penuh FKRTL <br>
        </div>
        <div class="footnote">
            Cetakan ke 
            <?php //if( !empty($row_sep) ){ $cetakan = empty($row_sep->cetakan) ? '-' : $row_sep->cetakan; }else{$cetakan = '';} ?>
            <?php if( !empty($row_sep) ){ $cetakan = empty($row_sep->cetakan) ? $data_array['tglcetak_sep'] : $row_sep->cetakan; }else{$cetakan = $data_array['tglcetak_sep'];} ?>
            <?php if( is_klaim() || is_superadmin() ): ?>
            <div class="form-group">
                <input type="text" value="<?= $cetakan; ?>" class="form-control" name="cetakan" placeholder="input Example: 2 07-12-2022 13:15:24" autocomplete="off">
            </div>
            <?php else: ?>
                <?= $cetakan;  ?>
            <?php endif; ?>
        </div>
    </div>
    <div class="col-md-4">
        <div class="ttd">
            <p class="mb-0">Pasien/Keluarga Pasien</p>
            <div class="qrcode-ttd">
                <?php if( !empty( $data_bpjs['sep'] ) && $data_bpjs['sep']['metaData']['code'] == 200 ): ?>
                <?= convert_to_qrcode($noKartu,"120x120"); ?>
                <?php endif; ?>
            </div>
            <p class="mb-0"><?= strtoupper( $nama ); ?></p>
        </div>
    </div>
    </div>
</div>
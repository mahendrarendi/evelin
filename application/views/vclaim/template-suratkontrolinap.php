<style>
.tglmulai,.sd,.tglselesai {
    display: inline-block;
}
</style>
<div class="row">
    <div class="col-md-12">
        <div class="banner-header">
            <img class="img-banner" src="<?= base_url('assets/img/kwitansibig.svg'); ?>" alt="resume">
        </div>

        <h4 class="mt-2 mb-3 text-center"><?= strtoupper('Surat Kontrol'); ?></h4>
        <?php 
        $identitas      = $data_suratkontrolinap['identitas'];
        
        $dokter         = $identitas['dokter'];

        $atasnama       = $identitas['identitas'];
        $namalengkap    = $identitas['namalengkap'];
        $gabung_nama    = $atasnama.' '.$namalengkap;
        $alamat         = $identitas['alamat'];
        $norm           = $identitas['norm'];
        $jeniskelamin   =  ucfirst( $identitas['jeniskelamin'] );
        $tglperiksa      = $identitas['tglperiksa'];
        $dokter         = $identitas['dokter'];

        $ttd = $identitas['dokter'].' RSU Queen Latifa Yogyakarta '.$identitas['tglperiksa'] .' SIP : '.$identitas['sip_dokter'];


        $jenisrujukan   = $data_suratkontrolinap['jenisrujukan'];

        $datapendaftaran = $data_suratkontrolinap['datapendaftaran'];
        $nojkn           = $datapendaftaran['nojkn'];

        #cek db skinap
        $row_skinap         = (array) $row_skinap;

        $tglmulaidirawat    = $row_skinap['tglmulaidirawat'] ?? '';
        $tglselesaidirawat  = $row_skinap['tglselesaidirawat'] ?? '';
        $ruang              = $row_skinap['ruang'] ?? '';
        $diagnosis          = $row_skinap['diagnosis'] ?? '';
        $terapi             = $row_skinap['terapi'] ?? '';
        $tglkontrol         = $row_skinap['tglkontrol'] ?? '';
        $jam                = $row_skinap['jam'] ?? '';
        ?>

        <div class="table-responsive">
            <table class="table table-striped">
                <tbody>
                    <tr>
                        <td width="20%">Nama Pasien</td>
                        <td width="5px">:</td>
                        <td><?= $gabung_nama; ?></td>
                    </tr>
                    <tr>
                        <td>Dirawat sejak tanggal</td>
                        <td width="5px">:</td>
                        <td>
                            <div class="tglmulai">
                            <?php if(is_superadmin() || is_igd() || is_poliumum() || is_klaim()): ?>
                                <input type="date" class="form-control" name="tglmulaidirawat" value="<?= $tglmulaidirawat; ?>">
                            <?php else: ?>
                                <?= $tglmulaidirawat ?>
                            <?php endif; ?>
                            </div>
                            <div class="sd px-5">s.d.</div>
                            <div class="tglselesai">
                                <?php if( is_superadmin() || is_igd() || is_poliumum() || is_klaim()): ?>
                                    <input type="date" class="form-control" name="tglselesaidirawat" value="<?= $tglselesaidirawat; ?>">
                                <?php else: ?>
                                    <?= $tglselesaidirawat; ?>
                                <?php endif; ?>
                            </div>
                        </td>
                    </tr>
                    <tr>
                        <td>Di Ruang</td>
                        <td width="5px">:</td>
                        <td>
                        <?php if(is_superadmin() || is_igd() || is_poliumum() || is_klaim()): ?>
                            <input type="text" autocomplete="off" class="form-control" name="ruang" value="<?= $ruang; ?>">
                        <?php else: ?>
                            <?= $ruang; ?>
                        <?php endif; ?>
                        </td>
                    </tr>
                    <tr>
                        <td>Diagnosis</td>
                        <td width="5px">:</td>
                        <td>
                            <?php if( is_superadmin() || is_igd() || is_poliumum() || is_klaim()): ?>
                            <input type="text" autocomplete="off" class="form-control" name="diagnosis" value="<?= $diagnosis; ?>">
                            <?php else: ?>
                                <?= $diagnosis; ?>
                            <?php endif; ?>
                        </td>
                    </tr>
                    <tr>
                        <td>Terapi</td>
                        <td width="5px">:</td>
                        <td>
                            <?php if( is_superadmin() || is_igd() || is_poliumum() || is_klaim() ): ?>    
                                <input type="text" autocomplete="off" class="form-control" name="terapi" value="<?= $terapi; ?>">
                            <?php else: ?>
                                <?= $terapi; ?>
                            <?php endif; ?>
                        </td>
                    </tr>
                    <tr>
                        <td>Kontrol Tanggal</td>
                        <td width="5px">:</td>
                        <td>
                            <?php if( is_superadmin() || is_igd() || is_poliumum() || is_klaim()): ?>
                            <input type="date" value="<?= $tglkontrol; ?>" class="form-control" name="tglkontrol" value="<?= format_tanggal( str_replace('/','-', $tglperiksa) ); ?>">
                            <?php else: ?>
                                <?= $tglkontrol; ?>
                            <?php endif; ?>  
                        </td>
                    </tr>
                    <tr>
                        <td>Jam</td>
                        <td width="5px">:</td>
                        <td>
                            <?php if( is_superadmin() || is_igd() || is_poliumum() || is_klaim() ): ?>
                            <input type="time" class="form-control" name="jam" value="<?= $jam; ?>">
                            <?php else: ?>
                                <?= $jam; ?>
                            <?php endif; ?>
                        </td>
                    </tr>
                </tbody>
            </table>
        </div>
        <div class="ttd-kontrol" style="text-align:right;">
            <div class="tgl-ttd">Sleman, <?= format_tanggal( $tglselesaidirawat,'d/m/Y' ); ?></div>
            <br>
            <?= convert_to_qrcode($ttd,'100x100'); ?>
            <br>
            <div class="name-ttd">(<?= $dokter; ?>)</div>
        </div>
    </div>
</div>
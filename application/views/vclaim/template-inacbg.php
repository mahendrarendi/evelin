<style>
#frm-inacbg .form-inline {
  display: inline-block;
}
</style>
<div class="row">

    <?php if( !empty($row_inacbg) ): ?>
        <div class="title">
            <h4>FIle INACBG</h4>
        </div>
    <?php endif; ?>

    <?php if( is_superadmin() || is_klaim()): ?>
    <div class="col-md-6">
        
        <?php 
            $class_hidden = empty($row_inacbg) ? 'd-block' : 'd-none';
        ?>

        <form class="form <?= $class_hidden; ?>"  id="frm-inacbg"  enctype="multipart/form-data" >
            
            <!-- add idunit -->
            <input type="hidden" class="idunit-hidden" value="<?= $idunit; ?>" name="idunit">
            
            <div class="form-group">
                <div class="form-inline">
                    <label style="font-size:unset;">Silahkan Upload File Inacbg <span class="required">*</span></label>
                    <input  required accept="application/pdf" type="file" name="fileinacbg" class="form-control">
                </div>
                <div class="form-inline">
                    <button type="submit" class="btn btn-danger mb-0">Upload</button>
                </div>
            </div>
        </form>

        <?php if( !empty($row_inacbg) ): ?>
        <div class="showinacbg">
            <div class="form-group">
                <button class="btn btn-primary btn-sm editfile-inacbg" type="button"><i class="fa fa-edit"></i> Edit</button>
                <button class="btn btn-danger btn-sm deletefile-inacbg" data-id="<?= $row_inacbg->idinacbg; ?>" type="button"> <i class="fa fa-trash"></i> Hapus</button>
            </div>
            <img class="rounded img-fluid" src="<?= base_url('assets/inacbg/'.$row_inacbg->name_inacbg); ?>">
        </div>
        <?php endif; ?>
        
    </div>
    <div class="col-md-6">
        <div class="card bg-danger text-white">
            <div class="card-body">
                <p class="mb-0"><strong>(*) Catatan Upload InaCBG:</strong></p>
                <ul class="mb-0">
                    <li>Upload  File Maximal <i>8MB</i></li>
                    <li>Tipe File Harus 1 FILE PDF <i>(PDF)</i></li>
                </ul>
            </div>
        </div> 
    </div>
    <?php else: ?>
        <?php if( !empty($row_inacbg) ): ?>
        <div class="showinacbg">
            <img class="rounded img-fluid" src="<?= base_url('assets/inacbg/'.$row_inacbg->name_inacbg); ?>">
        </div>
        <?php else: ?>
            <div class="alert alert-danger text-white" role="alert">
                Data File INACBG Belum Di upload
            </div>
        <?php endif; ?>
    <?php endif; ?>
</div>
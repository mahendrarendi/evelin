<div class="row">
    <div class="col-md-12">
        <?php 
            $a = $data_nota;

            $userkasir = $a['userkasir'];
            $validasikasir = 'Divalidasi Oleh : '.$userkasir;
            
            $t = $data_nota['jenis'];
            $n = 1;
            $i = 'klaim';

            $e = $a;
            // $r = ( 3 == $n ? "rajal" : 4 == $n ? "ranap" : "");
            $l = "";    
            $o = "";
            $d = "";
            $s = "";
            $g = "";
            $p = "";
            $u = "";
            $c = 0;
            $h = 0;
            $_ = 0;

            $l = ($g = "rajal" != $t ? "width:100%;" : "width:100%;") . ("rajal" != $t ? "font-size:normal;" : "font-size:normal;");
            $o = "rajal" != $t ? "font-size:normal;" : "font-size:normal;";
            $d = "rajal" != $t ? "20" : "7.6";
            $s = "rajal" != $t ? "2" : "0.2";

            $p .=
                '<div style="' .
                $g .
                'float: none;"><img class="mb-4" src="' .
                base_url() ."assets/img/kwitansibig".'.svg" />
                <table border="0" class="mb-4 textcolor-table" style="' . $l .'">';
                
                if( null != $a['infotagihan'] && "rajal" != $t ){
                    $p .= '<tr><td>Nama</td><td colspan="4">' . $a['infotagihan'][0]['namalengkap'] . "</td></tr>";
                    $p .= "<tr><td>Umur</td><td>" . $a['infotagihan'][0]['usia'] . "</td><td>No RM</td><td>" . $a['infotagihan'][0]['norm'] . "</td></tr>";
                    $p .= '<tr><td>Alamat</td><td colspan="4">' . $a['infotagihan'][0]['alamat'] . "</td></tr>";
                    $p .= '<tr><td>Ruang/Kelas</td><td colspan="4">' . $a['infotagihan'][0]['ruangkelas'] . "</td></tr>";
                    $p .= "<tr><td>Tanggal Masuk</td><td>" . $a['infotagihan'][0]['waktumasuk'] . "</td><td>Tanggal Keluar</td><td>" . $a['infotagihan'][0]['waktukeluar'] . "</td></tr>";
                    $p .= '<tr><td>Dirawat Selama</td><td colspan="4">' . $a['infotagihan'][0]['dirawatselama'] . "</td></tr>";
                    $p .= '<tr><td>Dokter Penanggung Jawab</td><td colspan="4">' . $a['infotagihan'][0]['dokterdbjp'] . "</td></tr>";
                    $p .= '<tr><td>Kelas Perawatan/Kelas Jaminan</td><td colspan="4">' . $a['infotagihan'][0]['kelas'] . "</td></tr>";
                }else{
                    $p .= "<tr><td>Nama</td><td>" . $a['infotagihan'][0]['namalengkap'] . "</td></tr>";
                    $p .= "<tr><td>Alamat</td><td>" . $a['infotagihan'][0]['alamat'] . "</td></tr>";
                    $p .= "<tr><td>Penanggung</td><td>" . ( !empty($a['infotagihan'][0]['penanggung']) ) ? $a['infotagihan'][0]['penanggung'] : '' . "</td></tr>";
                    $p .= "<tr><td>No RM</td><td>" . $a['infotagihan'][0]['norm'] . "</td></tr>";
                    $p .= '</table><table class="table table-bordered textcolor-table"  style="' . $l .'">';
                    $p .= '<tr style="background: #212229;color: white !important;" ><td class="text-white">No</td><td  class="text-white">Item</td><td  class="text-white">Jml</td><td  class="text-white">Nominal</td></tr>';

                }

                
            foreach( $a['detailtagihan'] as $x => $row_detail ){
                ($y = $x - 1).
                ($u != $row_detail['jenistarif'] && 
                    ("" != $u && ($p .= '<tr><td align="right" colspan="3">:: Subtotal ::</td><td align="right">' . convertToRupiah(round($c)) . "</td></tr>").
                    ( $p .= '<tr><td style="background: #6c757d;color: white !important;" colspan="4">' . $row_detail['jenistarif'] . "</td></tr>"). ($c = 0) ) ).

                ($c += (intval($row_detail['sewa']) + intval($row_detail['nonsewa'])) * floatval($row_detail['jumlah']) * (intval($row_detail['kekuatan']) / intval($row_detail['kekuatanbarang'])));
                
                ($h +=
                    "Diskon" == $row_detail['jenistarif'] ? 0 : (intval($row_detail['sewa']) + intval($row_detail['nonsewa'])) * floatval($row_detail['jumlah']) * (intval($row_detail['kekuatan']) / intval($row_detail['kekuatanbarang'])));
                
                (1 == $row_detail['iskenapajak'])
                    ? (
                        ($p .=
                          "<tr><td>".++$_."</td><td>".
                          $row_detail['nama'] .
                          (intval($row_detail['kekuatan']) / intval($row_detail['kekuatanbarang']) > 1
                              ? " [" . convertToRupiah(intval($row_detail['kekuatan'])) . "/" . convertToRupiah(intval($row_detail['kekuatanbarang']) . "]")
                              : "") .
                          '</td><td align="right" style="font-size:' .
                          $o .
                          '">' .
                          hapus3digitkoma((int)$row_detail['jumlah']) .
                          '</td><td align="right">' .
                          convertToRupiah( intval($row_detail['jasaoperator']) * floatval($row_detail['jumlah']) * ( intval($row_detail['kekuatan']) / intval($row_detail['kekuatanbarang']) ) ) ."</td></tr>").
                        ($p .=
                            '<tr><td>'.++$_.'</td><td>' .
                            " Akomodasi " .
                            $row_detail['nama'] .
                            (intval($row_detail['kekuatan']) / intval( $row_detail['kekuatanbarang']) > 1
                                ? " [" . convertToRupiah( intval($row_detail['kekuatan']) ) . "/" . convertToRupiah(intval($row_detail['kekuatanbarang']) . "]")
                                : "") .
                            '</td><td align="right" style="font-size:' .
                            $o .
                            '">' .
                            hapus3digitkoma((int)$row_detail['jumlah']) .
                            '</td><td align="right">' .
                            convertToRupiah(intval($row_detail['akomodasi']) * intval($row_detail['jumlah']) * (intval($row_detail['kekuatan']) / intval($row_detail['kekuatanbarang']))) .
                            "</td></tr>")
                        )
                    : ($p .=
                          "<tr><td>".++$_."</td><td>".
                          $row_detail['nama'] .
                          (intval($row_detail['kekuatan']) / intval($row_detail['kekuatanbarang']) > 1
                              ? " [" . convertToRupiah(intval($row_detail['kekuatan'])) . "/" . convertToRupiah(intval($row_detail['kekuatanbarang']) . "]")
                              : "") .
                          '</td><td align="right" style="font-size:' .
                          $o .
                          '">' .
                          hapus3digitkoma((int)$row_detail['jumlah']) .
                          '</td><td align="right">' .
                          convertToRupiah(
                              (intval($row_detail['sewa']) + intval($row_detail['nonsewa'])) * floatval($row_detail['jumlah']) *
                              (intval($row_detail['kekuatan']) / intval($row_detail['kekuatanbarang']))
                          ) .
                          "</td></tr>");
                ($u = $row_detail['jenistarif']);
            }

                if (
                        (
                            ($p .= '<tr><td align="right" colspan="3">:: Subtotal ::</td><td align="right">' . convertToRupiah(round($c)) . "</td></tr>").
                            ($p  .= '<tr><td align="right" colspan="3">== Total ==</td><td align="right">' . convertToRupiahBulat($h) . "</td></tr>").
                            ($p  .= '</table><table class="textcolor-table" border="0" style="' . $l . 'width="100%";font-family: "Palatino Linotype", "Book Antiqua", Palatino, serif;color: #333333;">').
                            ("jaminan" == $i || "mandiri" == $i || "klaim" == $i) 
                        )
                    ){
                        ($p .= '<tr><td>No Nota</td><td align="right">' . $a['infotagihan'][0]['idtagihan'] . "</td></tr>").
                            ($p .= '<tr><td>Waktu Tagih</td><td align="right">' . $a['infotagihan'][0]['waktutagih'] . "</td></tr>").
                            ($p .= '<tr><td>Waktu Bayar</td><td align="right">' . format_tanggal($a['infotagihan'][0]['waktutagih'],'Y-m-d').' '.format_tanggal($a['infotagihan'][0]['waktubayar'],'H:i:s') . "</td></tr>").
                            ($p .= '<tr><td>Nominal</td><td align="right">' . convertToRupiahBulat($h) . "</td></tr>").
                            ($p .= '<tr><td>Pembulatan</td><td align="right">' . convertToRupiah(bulatkanLimaRatusan($h) - $h) . "</td></tr>").
                            ($p .= '<tr><td>Tot.Tagihan</td><td align="right">' . convertToRupiah(bulatkanLimaRatusan($h)) . "</td></tr>").
                            ($p .= '<tr><td>Dibayar</td><td align="right">' . convertToRupiah(bulatkanLimaRatusan($h)) . "</td></tr>").
                            ($p .= '<tr><td>Jenis Tagihan</td><td align="right">' . ("mandiri" == $i ? "tagihan" : $a['infotagihan'][0]['jenistagihan']) . "</td></tr>");
                    }else if (1 == $n || 3 == $n){
                        foreach ($a['infotagihan'] as $row_detail){
                            intval(3 == $n ? $h : $row_detail['nominal']) - (intval($row_detail['dibayar']) + intval($row_detail['potongan']))
                            ($p .= '<tr><td>No Nota</td><td align="right">' . $row_detail['idtagihan'] . "</td></tr>")
                            ($p .= '<tr><td>Waktu Tagih</td><td align="right">' . $row_detail['waktutagih'] . "</td></tr>")
                            ($p .= '<tr><td>Waktu Bayar</td><td align="right">' . format_tanggal($row_detail['waktutagih'],'Y-m-d').' '.format_tanggal($row_detail['waktubayar'],'H:i:s') . "</td></tr>")
                            ($p .= '<tr><td>Nominal</td><td align="right">' . convertToRupiah($row_detail['nominal']) . "</td></tr>")
                            ($p .= '<tr><td>Potongan</td><td align="right">' . convertToRupiahBulat($row_detail['potongan']) . "</td></tr>")
                            ($p .= '<tr><td>Pembulatan</td><td align="right">' . convertToRupiahBulat($row_detail['pembulatan']) ."</td></tr>")
                            ($p .= '<tr><td>Tot.Tagihan</td><td align="right">' . convertToRupiahBulat(intval($row_detail['nominal']) + intval($row_detail['pembulatan']) - intval($row_detail['potongan'])) . "</td></tr>")
                            ("jknnonpbi" == $row_detail['jenistagihan'] && "jknpbi" == $row_detail['jenistagihan']) || ($p .= '<tr><td>Dibayar</td><td align="right">' . convertToRupiah($row_detail['dibayar']) . "</td></tr>")
                            ($p .= ( $row_detail['kekurangan'] > 0) ? '<tr><td>Kekurangan</td><td align="right">' . convertToRupiah($row_detail['kekurangan']) . "</td></tr>" : "")
                            ($p .= ( $row_detail['kembalian'] > 0) ? '<tr><td>Kembalian</td><td align="right">' . convertToRupiah($row_detail['kembalian']) . "</td></tr>" : "")
                            ($p .= '<tr><td>Jenis Tagihan</td><td align="right">' . $row_detail['jenistagihan'] . "</td></tr>");
                        }                           
                    }else {
                        $e = "";
                            $r = "";
                            $m = "";
                            $f = "";
                            $b = 0;
                            $k = 0;
                            $v = 0;
                            $T = 0;
                            $ll = 0;
                        
                        foreach ($a['infotagihan'] as $row_detail){

                        ($r .= $row_detail['idtagihan'] . " | ");
                                ($m .=$row_detail['waktutagih'] . " | ");
                                ($f .= $row_detail['waktubayar'] . " | ");
                                ($b += intval($row_detail['nominal']));
                                ($k += intval($row_detail['dibayar']));
                                ($v += intval($row_detail['potongan']));
                                ($T += intval($row_detail['pembulatan']));
                                ($ll += intval($row_detail['kekurangan']));
                                ($e = $row_detail['jenistagihan']);
                        ($b -= $ll);
                            ($p .= '<tr><td>No Nota</td><td align="right">' . $r . "</td></tr>");
                            ($p .= '<tr><td>Waktu Tagih</td><td align="right">' . $m . "</td></tr>");
                            ($p .= '<tr><td>Waktu Bayar</td><td align="right">' . format_tanggal($m,'Y-m-d').' '.format_tanggal($f,'H:i:s') . "</td></tr>");
                            ($p .= '<tr><td>Nominal</td><td align="right">' . convertToRupiahBulat($b) . "</td></tr>");
                            ( $v > 0 )
                                ? (($p .= '<tr><td>Potongan</td><td align="right">' . convertToRupiahBulat($v) . "</td></tr>")
                                  ($p .= '<tr><td>Pembulatan</td><td align="right">' . convertToRupiahBulat($T) . "</td></tr>")
                                  ($p .= '<tr><td>Tot.Tagihan</td><td align="right">' . convertToRupiahBulat($b + $T - $v) . "</td></tr>"))
                                : (($p .= '<tr><td>Pembulatan</td><td align="right">' . convertToRupiahBulat(T) . "</td></tr>") 
                                ($p .= '<tr><td>Tot.Tagihan</td><td align="right">' . convertToRupiahBulat($b + $T) . "</td></tr>"));
                            ("rajal" != t)
                                ? ($p .= "jknnonpbi" == $row_detail['jenistagihan'] || "jknpbi" == $row_detail['jenistagihan'] ? "" : '<tr><td>Dibayar</td><td align="right">' . convertToRupiah($k) . "</td></tr>")
                                : (($p .= '<tr><td>Dibayar</td><td align="right">' . convertToRupiah($k) . "</td></tr>")
                                  ($p .= ($row_detail['kekurangan'] > 0 ) ? '<tr><td>Kekurangan</td><td align="right">' . convertToRupiah($row_detail['kekurangan']) . "</td></tr>" : "")
                                  ($p .= ($row_detail['kembalian'] > 0 ) ? '<tr><td>Kembalian</td><td align="right">' . convertToRupiah($row_detail['kembalian']) . "</td></tr>" : ""));
                            ($p .= '<tr><td>Jenis Tagihan</td><td align="right">' . $e . "</td></tr>");

                        }
                    }
                    ($p .=
                    ("jknpbi" !== $a['infotagihan'][0]['jenispembayaran'] && "jknnonpbi" != $a['infotagihan'][0]['jenispembayaran'] && "asuransi" != $a['infotagihan'][0]['jenispembayaran'])
                    ? '<tr><td>Cara Bayar</td><td align="right">' . $a['infotagihan'][0]['jenispembayaran'] . "</td></tr>"
                    : "");
                    ($p .= "</table></div>");

                    $p .= '<div class="validasikasir" style="font-weight:bold;margin-top:20px;color:black;"><p>'.$validasikasir.'</p></div>';

                    echo $p;
        ?>
    </div>
</div>
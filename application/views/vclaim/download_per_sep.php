<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <style>.page_break { page-break-after: always; }</style>
    </head>

    <body>

        <div class="page_break">
            <?php if( !empty($row_inacbg) ): ?>
                <?= '<img src="'.$row_inacbg.'" width="100%">';?>
            <?php else: ?>
                <?= 'MAAF FILE INACBG BELUM DIUPLOAD <br> SILAHKAN KONTAK RSU QUEEN LATIFA YOGYAKARTA' ?>
            <?php endif; ?>
        </div>

        <div class="page_break">
            <?= $template_resume; ?>
        </div>

        <?php if( !empty($template_resumegabung) ): ?>
            <div class="page_break">
                <?= $template_resumegabung; ?>
            </div>
        <?php endif; ?>

        <div class="page_break">
            <?php /*if( !empty($row_sep) ): ?>
                <?= '<img src="'.$row_sep.'" width="100%">';?>
            <?php else: ?>
                <?= 'MAAF FILE SEP BELUM DIUPLOAD <br> SILAHKAN KONTAK RSU QUEEN LATIFA YOGYAKARTA' ?>
            <?php endif; */?>
            <?= $template_sep; ?>
        </div>

        <!-- Jenis Rujukan -->
        <?php if( $jenisrujukan == 'skdp' || $jenisrujukan == 'Kontrol post ranap' ):  ?>    

            <?php if( $is_tampil_rencanakontrol != '1' ): ?>
            <div class="page_break">
                <?php /* if( !empty($row_rencanakontrol) ): ?>
                    <?= '<img src="'.$row_rencanakontrol.'" width="100%">';?>
                <?php else: ?>
                    <?= 'MAAF FILE RENCANA KONTROL BELUM DIUPLOAD <br> SILAHKAN KONTAK RSU QUEEN LATIFA YOGYAKARTA' ?>
                <?php endif; */?>
                <?= $template_rencanakontrol; ?>
            </div>
            <?php endif; ?>
            
            <?php if( $is_tampil_skdp != '1' ): ?>
                <?php if($jenisrujukan == 'skdp'): ?>
                    <div class="page_break">
                        <?= $template_suratkontrol; ?>
                    </div>
                <?php endif; ?>
            <?php endif; ?>

        <?php else: ?>
            <?php if( $jenisrujukan == 'rujukanbaru' ):  ?>   
                 
                <?php if( $is_tampil_rujukanbaru != '1' ): ?>
                <div class="page_break">
                    <?php if( !empty($row_rujukanbaru) ): ?>
                        <?= '<img src="'.$row_rujukanbaru.'" width="100%">'; ?>
                    <?php else: ?>
                        <?= 'MAAF FILE RUJUKAN BARU BELUM DIUPLOAD <br> SILAHKAN KONTAK RSU QUEEN LATIFA YOGYAKARTA' ?>
                    <?php endif; ?>
                </div>
                <?php endif; ?>

            <?php endif; ?>

        <?php endif; ?>

        <!-- end Jenis Rujukan -->

        <?php if( $is_tampil_skinap != '1'): ?>
            <div class="page_break">
                <?= $template_suratkontrolinap; ?>
            </div>
        <?php endif; ?>

        <?php if( $is_tampil_emergensi != '1'): ?>
            <div class="page_break">
                <?= $template_emergensi; ?>
            </div>
        <?php endif; ?>

        <?php if( $is_tampil_radiologi != '1'): ?>
            
            <?php if($found_ro): ?>
            <div class="page_break">
                <?= $template_radiologi; ?>
            </div>
            <?php endif; ?>

            <?php if( $found_ro_gabung ): ?>
                <?php if( !empty($template_radiologigabung) ): ?>
                    <div class="page_break">
                        <?= $template_radiologigabung; ?>
                    </div>
                <?php endif; ?>
            <?php endif; ?>

        <?php endif; ?>

        <?php if( $is_tampil_dokumenlainllain != '1'): ?>
        <div class="page_break">
            <?php 
            if( !empty($rows_dokumenlainlain) ){
                $img = '<div class="doklain">';
                foreach( $rows_dokumenlainlain as $image ){
                    $img .= '<div class="doklain-item" style="margin-bottom:15px;">
                                <img width="100%" class="img" src="'.convert_imageto_base64( base_url('assets/dokumenlainlain/'.$image) ).'">
                            </div>';
                }
                $img .= '</div>';
            }else{
                $img = 'MAAF FILE DOKUMEN PENDUKUNG BELUM DIUPLOAD <br> SILAHKAN KONTAK RSU QUEEN LATIFA YOGYAKARTA';
            }
                echo $img;
            ?>
        </div>
        <?php endif; ?>


        <div class="<?= !empty($template_billinggabung) ? 'page_break' : 'page_breaks'; ?>">
            <?= $template_nota; ?>
        </div>

        <?php if( !empty($template_billinggabung) ): ?>
            <div class="page_breaks">
                <?= $template_billinggabung; ?>
            </div>
        <?php endif; ?>

    </body>
</html>
<?php 
$noSuratKontrol = '';
$namaPoliTujuan = '';
$namaDokter     = '';
$tglRencanaKontrol = '';

$diagnosa       ='';

$noKartu        = '';
$nama           = '';
$tglLahir       = '';
$kelamin        = '';

$code_error = '';
$error_message = '';
$datatgl_terbit_cetak = '';



if(  !empty($data_rencanakontrol['response']['noSuratKontrol']) ){

    $response       = $data_rencanakontrol['response'];
    $noSuratKontrol = $response['noSuratKontrol'];
    $namaPoliTujuan = $response['namaPoliTujuan'];
    $namaDokter     = $response['namaDokter'];
    $tglRencanaKontrol = $response['tglRencanaKontrol'];
    
    $sep            = $response['sep'];
    $diagnosa       = $sep['diagnosa'];
    
    $peserta        = $sep['peserta'];
    $noKartu        = $peserta['noKartu'];
    $nama           = $peserta['nama'];
    $tglLahir       = $peserta['tglLahir'];
    $kelamin        = jenis_kelamin( $peserta['kelamin'] );

    $datatgl_terbit_cetak = $this->qlwebapi->api_vclaim_tanggal_rekon($noSuratKontrol);
    

}else if( !empty($data_rencanakontrol['metaData']) && $data_rencanakontrol['metaData']['code'] == 200 ){
    $response       = $data_rencanakontrol['response'];
    $noSuratKontrol = $response['nosuratsep'];
    $namaPoliTujuan = $response['polisep'];
    $namaDokter     = $response['dpjpsep'];
    $tglRencanaKontrol = $response['tglSep'];
    
    $diagnosa       = $response['diagnosa'];
    
    $peserta        = $response['peserta'];
    $noKartu        = $peserta['noKartu'];
    $nama           = $peserta['nama'];
    $tglLahir       = $peserta['tglLahir'];
    $kelamin        = jenis_kelamin( $peserta['kelamin'] );
    $datatgl_terbit_cetak = $this->qlwebapi->api_vclaim_tanggal_rekon($noSuratKontrol);
}else{
    if( empty($data_rencanakontrol['metaData']) ){
        $code_error     = $data_rencanakontrol['response']['metaData']['code'];
        $error_message  = $data_rencanakontrol['response']['metaData']['message'];
    }
}

if( isset($_GET['test']) ){

    echo '<pre>';
    print_r( $testdata );
    echo '</pre>';
}

?>

<?php if(!empty($code_error)): ?>
    <div class="alert alert-danger" style="color:white;" role="alert"> Error Code : <?= $code_error ?> - <?= $error_message; ?> </div>
<?php endif; ?>

<div class="row">
    <div class="col-md-12">
        <div class="banner-header mb-4 rekon-header">
            <img class="img-banner" src="<?= base_url('assets/img/rencana-kontrol.png'); ?>" alt="resume">
        </div>
        <div class="rekon-header">
            <div class="norekon"><?= 'No. '. $noSuratKontrol; ?></div>
        </div>
    </div>
    <div class="col-md-12">
        <div class="table-responsive">
            <table class="table tb-ql">
                <tbody>
                    <tr>
                        <th width="15%">Kepada YTH</th>
                        <td></td>
                        <td>
                            <p class="mb-0"><?= $namaDokter; ?></p>
                            <p class="mb-0"><?= 'Sp./Sub. '.$namaPoliTujuan; ?></p>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="3">Mohon Pemeriksaan dan Penanganan Lebih Lanjut :</td>
                    </tr>
                    <tr>
                        <th>No.Kartu</th>
                        <td>:</td>
                        <td><?= $noKartu; ?></td>
                    </tr>
                    <tr>
                        <th>Nama peserta</th>
                        <td>:</td>
                        <td><?= $nama.' ('.$kelamin.') '; ?></td>
                    </tr>
                    <tr>
                        <th>Tgl.Lahir</th>
                        <td>:</td>
                        <td><?= format_tanggal_indonesia( $tglLahir ); ?></td>
                    </tr>
                    <tr>
                        <th>Diagnosa</th>
                        <td>:</td>
                        <td><?= $diagnosa; ?></td>
                    </tr>
                    <tr>
                        <th>Rencana Kontrol</th>
                        <td>:</td>
                        <td><?= format_tanggal_indonesia( $tglRencanaKontrol ); ?></td>
                    </tr>
                </tbody>
            </table>
        </div>
        <div class="keterangan">
            Demikian atas bantuanya,diucapkan banyak terima kasih.
        </div>
       
    </div>
    <div class="col-md-8">
        <div class="footnote">
            <?php 

            if(!empty( $row_rekon )){
                $tglentri        = $row_rekon->tglentri;
                $keterangancetak = $row_rekon->keterangancetak;
            }else if( !empty($datatgl_terbit_cetak['response']) ){
                $tglentri        = $datatgl_terbit_cetak['response']['tglTerbit'];
                $keterangancetak = $datatgl_terbit_cetak['response']['tanggal_cetak'];
            }else{
                $tglentri = "";
                $keterangancetak = "";
            }
            ?>
            Tgl.Entri : 
            <?php if( is_klaim() || is_superadmin() ): ?>
            <div class="form-group">
                <input type="date" value="<?= $tglentri; ?>" autocomplete="off" class="form-control" name="tglentri">
            </div>
            <?php else: ?>
                <?= $tglentri; ?>
            <?php endif; ?>
            | 
            Tgl.Cetak:
            <?php if( is_klaim() || is_superadmin() ): ?>
            <div class="form-group">
                <input type="input" value="<?= $keterangancetak; ?>" autocomplete="off" placeholder="Tanggal Cetak dan Waktu Cetak" class="form-control" name="keterangancetak"> 
            </div>
            <?php else: ?>
                <?= $keterangancetak; ?>
            <?php endif; ?>
            
        </div>
    </div>
    <div class="col-md-4">
        <div class="ttd-dpjp">
            <p class="mb-0">Mengetahui DPJP,</p> 
            <div class="qrcode-ttd">
                <?php if( ( !empty($data_rencanakontrol['response']['metaData']) && $data_rencanakontrol['response']['metaData']['code'] == 200 ) || ( !empty($data_rencanakontrol['metaData']) && $data_rencanakontrol['metaData']['code'] == 200) ):?>
                <?= convert_to_qrcode($namaDokter,"100x100"); ?>
                <?php endif; ?>
            </div>
            <p class="mb-0"><?= $namaDokter; ?> </p>
        </div>
    </div>
</div>
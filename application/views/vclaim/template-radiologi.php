<div class="row">
    <div class="col-md-12">
        <?php 

            $t = $data_radiologi;

            $a = '<div style="float: none;"><img class="mb-4" style="width:9cm" src="'.base_url()."/assets/img/".$t['header'].'" /></div>';
            $e = $t['identitas']['keteranganradiologi'] . ("" == $t['identitas']['saranradiologi'] ? "" : "<b>Saran:</b>"). $t['identitas']['saranradiologi'];
            $i = "";
            $d = "";
            
            foreach( $t['diagnosa'] as $dg ){ $i .= '<span style="display:block;">' . $dg['icd'] . " " . $dg['namaicd']; }
            foreach( $t['elektromedik'] as $em ){ $d .= '<span style="display:block;">' . $em['icd'] . " " . $em['namaicd']; }

            $r = $t['identitas'];
            $s = '<div style="border-top:1px solid #3a3a3a;"></div>';
            $o = 'style="width:100%; text-align:left; font-size:medium;font-family: "Palatino Linotype", "Book Antiqua", "Palatino";color: #333333;"';
           
            $ttd = $t['dokterpenilai'].' RSU Queen Latifa Yogyakarta '.$r['waktu'] .' SIP : '.$t['sipdokterpenilai'];

            $a .= "<table  class='textcolor-table'>";
            $a .= "<tr><td>N.Periksa/N.RM </td><td>:</td> <td >" . $r['idpemeriksaan'] . "/" . $r['norm'] . '</td><td>D. Pengirim</td> <td>:</td><td style="height:auto;width:35%;">' . $r['dokter'] . "</td><tr>";
            $a .= '<tr><td>Nama</td><td>:</td> <td style="height:auto;width:35%;">' . $r['identitas'] . " " . $r['namalengkap'] . '</td><td>Asal</td>   <td>:</td><td style="height:auto;width:35%;">' . $r['namaunit'] . "</td><tr>";
            $a .= "<tr><td>J. Kelamin</td><td>:</td> <td>" . $r['jeniskelamin'] . "</td><td>T. Periksa</td><td>:</td><td>" . $r['waktu'] . "</td><tr>";
            $a .= "<tr><td>Usia</td><td>:</td> <td>" . $r['usia'] . '</td><td>Alamat</td> <td>:</td><td style="height:auto;width:35%;">' . $r['alamat'] . "</td><tr>";
            $a .= "</table>";
            $a .= $s;
            $a .= "<table ".$o." class='textcolor-table'><tr ><th>Diagnosis Klinis</th><th>Pemeriksaan Radiologis</th></tr>";
            $a .= '<tr><td style="height:auto;width:50%;">' . $i . '</td><td style="height:auto;width:50%;">' . $d . "</td></tr></table>";
            $a .= $s;
            $a .= '<table class="textcolor-table">'  . ( empty( $t['ketradiologi']['keteranganradiologi'] ) ? $e : $t['ketradiologi']['keteranganradiologi'] ) . "</td></tr></table>";
            $a .= '<table class="textcolor-table" style="margin-top:10px;margin-left:65%; text-align:center;"><tr><td>Dokter Penilai<br>'.convert_to_qrcode($ttd,'100x100').'<br></td></tr><tr><td>' . $t['dokterpenilai'] . "<br>" . $t['sipdokterpenilai'] .  "</td></tr></table>";

            echo $a;
        ?>
    </div>
</div>
<style>
.banner-inline {
    display: inline-block;
    width: 49.3333%;
    vertical-align: middle;
}

.banner-inline img {
    width: 80% !important;
}
.data-groupadd .inline-data {
    display: inline-block;
    vertical-align: middle;
    width: 49.333%;
}

.data-groupadd .inline-data button {
    margin: 0;
}
.tabledata tr td {
    white-space: normal;
}
.tglmulairujukan,.sd,.tglselesairujukan {
    display: inline-block;
}
</style>
<div class="row">
    <div class="col-md-12">

        <?php
        
            $identitas      = $data_suratkontrol['identitas'];
            $atasnama       = $identitas['identitas'];
            $namalengkap    = $identitas['namalengkap'];
            $gabung_nama    = $atasnama.' '.$namalengkap;
            $alamat         = $identitas['alamat'];
            $norm           = $identitas['norm'];
            $jeniskelamin   =  ucfirst( $identitas['jeniskelamin'] );
            $tglperiksa     = $identitas['tglperiksa'];
            $dokter         = $identitas['dokter'];

            $diag   = $data_suratkontrol['diagnosa'];
            $obat   = $data_suratkontrol['obat'];

            $dtd = '';
            foreach($diag as $row){ $dtd.=( ($row['namaicd'] == null) ? '' : $row['icd'] ).' '.$row['namaicd'].'<br>'; }

            // $diagnosas      = $dtd.' '.$data_suratkontrol['ketradiologi']['diagnosa'];
            $diagnosas      = $data_suratkontrol['ketradiologi']['diagnosa'];
            
            $terapis = '';
            if( !empty($obat) ){
                $terapis        = resep_obat($data_suratkontrol['grup'],$obat);
            }


            $ttd = $identitas['dokter'].' RSU Queen Latifa Yogyakarta '.$identitas['tglperiksa'] .' SIP : '.$identitas['sip_dokter'];


            $jenisrujukan   = $data_suratkontrol['jenisrujukan'];

            $datapendaftaran = $data_suratkontrol['datapendaftaran'];
            $nojkn           = $datapendaftaran['nojkn'];

            $skdp           = empty( $row_skformedit->skdp ) ? $jenisrujukan : $row_skformedit->skdp;
            $nokontrol      = $row_skformedit->nokontrol ?? '';
            $diagnosa       = empty($row_skformedit->diagnosa) ? str_replace('<br>',',',$diagnosas) : $row_skformedit->diagnosa;
            $terapi         = empty($row_skformedit->terapi) ? strip_tags($terapis) : $row_skformedit->terapi;
            $tglkontrolkembali = $row_skformedit->tglkontrolkembali ?? '';
            $tglmulairujukan   = $row_skformedit->tglmulairujukan ?? '';
            $tglselesairujukan = $row_skformedit->tglselesairujukan ?? '';

        ?>
        
        <div class="banner-header">
            <div class="banner-inline">
                <img class="img-banner" src="<?= base_url('assets/img/headerresume.svg'); ?>" alt="resume">
            </div>
            <div class="banner-inline">
                <table class="table table-bordered table-striped">
                    <tbody>
                        <tr>
                            <td style="color:black;">SKDP : 
                                <?php if( is_superadmin() || is_klaim() || is_igd() || is_poliumum() ): ?>
                                    <input type="text" autocomplete="off" value="<?= $skdp ?>" name="skdp" class="form-control">
                                <?php else: ?>
                                    <?= $skdp; ?>
                                <?php endif; ?>
                            </td>
                            <td style="color:black;">NO.KONTROL : 
                                <?php if( is_superadmin() || is_klaim() || is_igd() || is_poliumum() ): ?>
                                    <input type="text" autocomplete="off" value="<?= $nokontrol; ?>" name="nokontrol" class="form-control">
                                <?php else: ?>
                                    <?= $nokontrol; ?>
                                <?php endif; ?>
                            </td>
                        </tr>
                    </tbody>
                </table>
            </div>
        </div>
        <hr class="mt-0" style="border: 1px solid black !important;opacity: 1;">

        <h6 style="color:black;" class="text-center mb-3"><?= strtoupper('Surat Keterangan Dalam Perawatan'); ?></h6>

        <div class="row">
            <div class="col-md-8">
                <div class="table-responsive">
                    <table class="table table-striped">
                            <tr>
                                <th>Nama Pasien</th>
                                <td width="5px">:</td>
                                <td><?= $gabung_nama; ?></td>
                            </tr>
                            <tr>
                                <th>Nomor Kartu BPJS</th>
                                <td>:</td>
                                <td><?= $nojkn; ?></td>
                            </tr>
                            <tr>
                                <th>Alamat</th>
                                <td>:</td>
                                <td style="white-space: normal;"><?= $alamat; ?></td>
                            </tr>
                            <tr>
                                <th>Diagnosa</th>
                                <td>:</td>
                                <td>
                                    <?php if(  is_superadmin() || is_klaim() || is_igd()  || is_poliumum() ): ?>
                                        <textarea name="diagnosa" class="form-control" autocomplete="off" rows="4" value="<?= $diagnosa; ?>"><?= $diagnosa; ?></textarea>
                                    <?php else: ?>
                                        <?= $diagnosa; ?>
                                    <?php endif; ?>
                                </td>
                            </tr>
                            <tr>
                                <th>Terapi</th>
                                <td>:</td>
                                <td>
                                    <?php if(  is_superadmin() || is_klaim() || is_igd() || is_poliumum() ): ?>
                                        <textarea name="terapi" class="form-control" autocomplete="off"  rows="4" value="<?= $terapi; ?>"><?= $terapi; ?></textarea>
                                    <?php else: ?>
                                        <?= $terapi; ?>
                                    <?php endif; ?>
                                </td>
                            </tr>
                            <tr>
                                <th>Kontrol Kembali Tanggal</th>
                                <td>:</td>
                                <td>
                                    <?php if(  is_superadmin() || is_klaim() || is_igd() || is_poliumum() ): ?>
                                    <input value="<?= $tglkontrolkembali; ?>" type="date" autocomplete="off" class="form-control" name="tglkontrolkembali">
                                    <?php else: ?>
                                        <?= $tglkontrolkembali; ?>
                                    <?php endif; ?>
                                </td>
                            </tr>
                            <tr>
                                <th>Tanggal Surat Rujukan</th>
                                <td>:</td>
                                <td>
                                        <div class="tglmulairujukan">
                                            <?php if( is_superadmin() || is_klaim() || is_igd() || is_poliumum()): ?>
                                                <input value="<?= $tglmulairujukan; ?>" type="date" autocomplete="off" class="form-control" name="tglmulairujukan">
                                            <?php else: ?>
                                                <?= $tglmulairujukan; ?>
                                            <?php endif; ?>
                                        </div>
                                        
                                        <div class="sd px-5">s.d.</div>
                                        
                                        <div class="tglselesairujukan">
                                            <?php if(  is_superadmin() || is_klaim() || is_igd() || is_poliumum() ): ?>
                                                <input type="date" class="form-control" name="tglselesairujukan" value="<?= $tglselesairujukan; ?>">
                                            <?php else: ?>
                                                <?= $tglselesairujukan; ?>
                                            <?php endif; ?>
                                        </div>
                                </td>
                            </tr>
                    </table>
                </div>
            </div>
            <div class="col-md-4">
                <div class="table-responsive">
                    <table class="table table-striped">
                            <tr>
                                <th>No. RM</th>
                                <td width="5px">:</td>
                                <td><?= $norm; ?></td>
                            </tr>
                            <tr>
                                <th>Jenis Kelamin</th>
                                <td>:</td>
                                <td><?= $jeniskelamin; ?></td>
                            </tr>
                    </table>
                </div>
            </div>

            <div class="col-md-12 mt-4">
                <p>Pasien tersebut diatas masih dalam perawatan Dokter Spesialis di RSU Queen Latifa dengan pertimbangan sebagai berikut :</p>
                
                <?php if( is_superadmin() || is_klaim() || is_igd() || is_poliumum()): ?>
                <div class="data-groupadd">
                    <div class="form-group">
                        <div class="inline-data">
                            <input type="text" autocomplete="off" placeholder="Tambahkan Pertimbangan Dokter ...." name="addpertimbangan" class="form-control">
                        </div>
                        <div class="inline-data">
                            <button class="btn btn-primary" type="button" id="addpertimbangan"><i class="fa fa-plus-circle"></i></button>
                        </div>
                    </div>
                    <div class="tabledata mt-4">
                        <?= $loop_table_pertimbangan; ?>
                    </div>
                </div>
                <?php else: ?>
                    <ol class="pertimbangan-listul">
                        <?php 
                        if( !empty($query_loop)  ):
                            foreach( $query_loop as $row ):  ?>
                                <li><?= $row->item; ?></li>
                            <?php endforeach; ?>
                        <?php else: ?>
                            <li>-</li>
                        <?php endif; ?>
                    </ol>
                <?php endif; ?>
                <p>Surat Keterangan ini digunakan untuk 1 (satu) kali kunjungan pasien <br> dengan diagnosa sebagaimana diatas pada tanggal <strong><?= ( empty($tglkontrolkembali) ? '...' : format_tanggal($tglkontrolkembali,"d/m/Y") ); ?></strong></p>
            </div>

            <table class="table">
                <tr>
                    <td style="width:60%;"><strong>NB. Surat ini wajib dibawa saat kontrol</strong></td>
                    <td>
                        Yogyakarta, <?= $tglperiksa; ?>
                        <br>
                        Dokter Penanggung Jawab Pasien (DPJP)
                        <br>
                        <?= convert_to_qrcode($ttd,'100x100'); ?>
                        <br>
                        <?= $dokter; ?>
                    </td>
                </tr>
            </table>
        </div>

    </div>
</div>
<style>
.banner-inline {
    display: inline-block;
    width: 49.3333%;
    vertical-align: middle;
}

.banner-inline img {
    width: 80% !important;
}
.data-groupadd .inline-data {
    display: inline-block;
    vertical-align: middle;
    width: 49.333%;
}

.data-groupadd .inline-data button {
    margin: 0;
}
.tabledata tr td {
    white-space: normal;
}
.tglmulairujukan,.sd,.tglselesairujukan {
    display: inline-block;
}
.sk-row .pdf-tb tr th, .sk-row .pdf-tb tr td{
    text-align:left;
    font-size: 12px;
    /* border:black thin solid; */
    max-width:50%;
}
.sk-row div p {
    margin : 0 !important;
}
.sk-row div hr {
    border: 1px solid black !important;
}
.sk-row div h5{
    margin-bottom:15px;
    font-size:16px;
}
.sk-row div, .sk-row div p {
    font-size: 12px !important;
}

.sk-row table{
    width:100%;
    table-layout:fixed;
}
</style>
<div class="row sk-row">
    <div class="col-md-12">

        <?php
            $identitas      = $data_suratkontrol['identitas'];
            $atasnama       = $identitas['identitas'];
            $namalengkap    = $identitas['namalengkap'];
            $gabung_nama    = $atasnama.' '.$namalengkap;
            $alamat         = $identitas['alamat'];
            $norm           = $identitas['norm'];
            $jeniskelamin   =  ucfirst( $identitas['jeniskelamin'] );
            $tglperiksa     = $identitas['tglperiksa'];
            $dokter         = $identitas['dokter'];

            $diag   = $data_suratkontrol['diagnosa'];
            $obat   = $data_suratkontrol['obat'];

            $dtd = '';
            foreach($diag as $row){ $dtd.=( ($row['namaicd'] == null) ? '' : $row['icd'] ).' '.$row['namaicd'].'<br>'; }

            // $diagnosas      = $dtd.' '.$data_suratkontrol['ketradiologi']['diagnosa'];
            $diagnosas      = $data_suratkontrol['ketradiologi']['diagnosa'];
            
            $terapis = '';
            if( !empty($obat) ){
                $terapis        = resep_obat($data_suratkontrol['grup'],$obat);
            }


            // echo '<pre>';
            // print_r( $terapis );
            // echo '</pre>';

            $ttd = $identitas['dokter'].' RSU Queen Latifa Yogyakarta '.$identitas['tglperiksa'] .' SIP : '.$identitas['sip_dokter'];


            $jenisrujukan   = $data_suratkontrol['jenisrujukan'];

            $datapendaftaran = $data_suratkontrol['datapendaftaran'];
            $nojkn           = $datapendaftaran['nojkn'];

            $skdp           = empty( $row_skformedit->skdp ) ? $jenisrujukan : $row_skformedit->skdp;
            $nokontrol      = $row_skformedit->nokontrol ?? '';
            $diagnosa       = empty($row_skformedit->diagnosa) ? str_replace('<br>',',',$diagnosas) : $row_skformedit->diagnosa;
            $terapi         = empty($row_skformedit->terapi) ? strip_tags($terapis) : $row_skformedit->terapi;
            $tglkontrolkembali = $row_skformedit->tglkontrolkembali ?? '';
            $tglmulairujukan   = $row_skformedit->tglmulairujukan ?? '';
            $tglselesairujukan = $row_skformedit->tglselesairujukan ?? '';
        ?>
        
        <div class="banner-header">
            <div class="banner-inline">
                <img class="img-banner" src="<?= convert_imageto_base64 ( base_url('assets/img/header_resume.jpg') ); ?>" alt="resume">
            </div>
            <div class="banner-inline">
                <table class="table table-bordered table-striped pdf-tb">
                    <tbody>
                        <tr>
                            <td style="color:black;">SKDP : 
                                <?= $skdp; ?>
                            </td>
                            <td style="color:black;">NO.KONTROL : 
                                <?= $nokontrol; ?>
                            </td>
                        </tr>
                    </tbody>
                </table>
            </div>
        </div>

        <hr class="mt-0" style="border: 1px solid black !important;opacity: 1;">

        <h5 style="color:black; text-align:center;"><?= strtoupper('Surat Keterangan Dalam Perawatan'); ?></h5>

        <div class="row">
            <div class="col-md-8">
                <div class="table-responsive" style="margin-bottom:15px;">
                    <table class="table table-striped pdf-tb ">
                            <tr>
                                <th>Nama Pasien</th>
                                <td style="width:2% !important;">:</td>
                                <td><?= $gabung_nama; ?></td>
                                <th>No. RM</th>
                                <td style="width:2% !important;">:</td>
                                <td><?= $norm; ?></td>
                            </tr>
                            <tr>
                                <th>Nomor Kartu BPJS</th>
                                <td>:</td>
                                <td><?= $nojkn; ?></td>
                                <th>Jenis Kelamin</th>
                                <td>:</td>
                                <td><?= $jeniskelamin; ?></td>
                            </tr>
                            <tr>
                                <th>Alamat</th>
                                <td>:</td>
                                <td style="white-space: normal;"><?= $alamat; ?></td>
                                <th></th>
                                <td></td>
                                <td></td>
                            </tr>
                            <tr>
                                <th>Diagnosa</th>
                                <td>:</td>
                                <td>
                                    <?= $diagnosa; ?>
                                </td>
                                <th></th>
                                <td></td>
                                <td></td>
                            </tr>
                            <tr>
                                <th>Terapi</th>
                                <td>:</td>
                                <td>
                                    <?= $terapi; ?>
                                </td>
                                <th></th>
                                <td></td>
                                <td></td>
                            </tr>
                            <tr>
                                <th>Kontrol Kembali Tanggal</th>
                                <td>:</td>
                                <td>
                                    <?= $tglkontrolkembali; ?>
                                </td>
                                <th></th>
                                <td></td>
                                <td></td>
                            </tr>
                            <tr>
                                <th>Tanggal Surat Rujukan</th>
                                <td>:</td>
                                <td>
                                        <div class="tglmulairujukan">
                                            <?= $tglmulairujukan; ?>
                                        </div>
                                        
                                        <div class="sd px-5">s.d.</div>
                                        
                                        <div class="tglselesairujukan">
                                            <?= $tglselesairujukan; ?>
                                        </div>
                                </td>
                                <th></th>
                                <td></td>
                                <td></td>
                            </tr>
                    </table>
                </div>
            </div>


            <div class="col-md-12 mt-4">
                <p>Pasien tersebut diatas masih dalam perawatan Dokter Spesialis di RSU Queen Latifa dengan pertimbangan sebagai berikut :</p>
                
                <ol class="pertimbangan-listul">
                    <?php 
                    if( !empty($query_loop)  ):
                        foreach( $query_loop as $row ):  ?>
                            <li><?= $row->item; ?></li>
                        <?php endforeach; ?>
                    <?php else: ?>
                        <li>-</li>
                    <?php endif; ?>
                </ol>

                <p>Surat Keterangan ini digunakan untuk 1 (satu) kali kunjungan pasien <br> dengan diagnosa sebagaimana diatas pada tanggal <strong><?= ( empty($tglkontrolkembali) ? '...' : format_tanggal($tglkontrolkembali,"d/m/Y") ); ?></strong></p>
            
            </div>

            <div style="margin:15px 0px;"><p><strong>NB. Surat ini wajib dibawa saat kontrol</strong></p></div>

            <table class="table  pdf-tb">
                <tr>
                    <td style="width:70% !important;"></td>
                    <td>
                        Yogyakarta, <?= $tglperiksa; ?>
                        <br>
                        Dokter Penanggung Jawab Pasien (DPJP)
                        <br>
                        <img src="<?= convert_imageto_base64(convert_to_qrcode_pdf( str_replace(".","",$ttd) )); ?>" alt="Tanda Tangan Dokter">
                        <br>
                        <?= $dokter; ?>
                    </td>
                </tr>
            </table>
        </div>

    </div>
</div>
<style>
.tglmulai,.sd,.tglselesai {
    display: inline-block;
}
.suratinap-row .pdf-tb tr th, .suratinap-row .pdf-tb tr td{
    text-align:left;
    font-size: 12px;
    /* border:black thin solid; */
    max-width:50%;
}
.suratinap-row div p {
    margin : 0 !important;
}
.suratinap-row div hr {
    border: 1px solid black !important;
}
.suratinap-row div h5{
    margin-bottom:15px;
    font-size:16px;
}
.suratinap-row div, .suratinap-row div p {
    font-size: 12px !important;
}

.suratinap-row table{
    width:100%;
    table-layout:fixed;
}
</style>
<div class="row suratinap-row">
    <div class="col-md-12">
        <div class="banner-header">
            <img  width="50%" class="img-banner" src="<?= convert_imageto_base64( base_url('assets/img/header_resume.jpg') ); ?>" alt="resume">
        </div>
        <hr>

        <h4 style="text-align:center;"><?= strtoupper('Surat Kontrol'); ?></h4>
        <?php 
        $identitas      = $data_suratkontrolinap['identitas'];
        
        $dokter         = $identitas['dokter'];

        $atasnama       = $identitas['identitas'];
        $namalengkap    = $identitas['namalengkap'];
        $gabung_nama    = $atasnama.' '.$namalengkap;
        $alamat         = $identitas['alamat'];
        $norm           = $identitas['norm'];
        $jeniskelamin   =  ucfirst( $identitas['jeniskelamin'] );
        $tglperiksa      = $identitas['tglperiksa'];
        $dokter         = $identitas['dokter'];

        $jenisrujukan   = $data_suratkontrolinap['jenisrujukan'];

        $datapendaftaran = $data_suratkontrolinap['datapendaftaran'];
        $nojkn           = $datapendaftaran['nojkn'];

        $ttd = $identitas['dokter'].' RSU Queen Latifa Yogyakarta '.$identitas['tglperiksa'] .' SIP : '.$identitas['sip_dokter'];


        #cek db skinap
        $row_skinap         = (array) $row_skinap;

        $tglmulaidirawat    = $row_skinap['tglmulaidirawat'] ?? '';
        $tglselesaidirawat  = $row_skinap['tglselesaidirawat'] ?? '';
        $ruang              = $row_skinap['ruang'] ?? '';
        $diagnosis          = $row_skinap['diagnosis'] ?? '';
        $terapi             = $row_skinap['terapi'] ?? '';
        $tglkontrol         = $row_skinap['tglkontrol'] ?? '';
        $jam                = $row_skinap['jam'] ?? '';
        ?>

        <div class="table-responsive">
            <table class="table table-striped">
                <tbody>
                    <tr>
                        <td>Nama Pasien</td>
                        <td>:</td>
                        <td><?= $gabung_nama; ?></td>
                    </tr>
                    <tr>
                        <td width="20%">Dirawat sejak tanggal</td>
                        <td width="2%">:</td>
                        <td>
                            <div class="tglmulai">
                                <?= $tglmulaidirawat ?>
                            </div>
                            <div class="sd px-5">s.d.</div>
                            <div class="tglselesai">
                                    <?= $tglselesaidirawat; ?>
                            </div>
                        </td>
                    </tr>
                    <tr>
                        <td>Di Ruang</td>
                        <td width="5px">:</td>
                        <td>
                            <?= $ruang; ?>
                        </td>
                    </tr>
                    <tr>
                        <td>Diagnosis</td>
                        <td width="5px">:</td>
                        <td>
                            <?= $diagnosis; ?>
                        </td>
                    </tr>
                    <tr>
                        <td>Terapi</td>
                        <td width="5px">:</td>
                        <td>
                                <?= $terapi; ?>
                        </td>
                    </tr>
                    <tr>
                        <td>Kontrol Tanggal</td>
                        <td width="5px">:</td>
                        <td>
                                <?= $tglkontrol; ?>
                        </td>
                    </tr>
                    <tr>
                        <td>Jam</td>
                        <td width="5px">:</td>
                        <td>
                            <?= $jam; ?>
                        </td>
                    </tr>
                </tbody>
            </table>
        </div>
        <div class="ttd-kontrol" style="text-align:right;">
            <div class="tgl-ttd">Sleman, <?= $tglperiksa; ?></div>
            <br>
            <img src="<?= convert_imageto_base64(convert_to_qrcode_pdf( str_replace(".","",$ttd) )); ?>" alt="Tanda Tangan Dokter">
            <br>
            <div class="name-ttd">(<?= $dokter; ?>)</div>
        </div>
    </div>
</div>
<style>
.banner-inline {
    display: inline-block;
    width: 49.3333%;
    vertical-align: middle;
}

.banner-inline img {
    width: 80% !important;
}
.emg-row .pdf-tb tr th, .emg-row .pdf-tb tr td{
    text-align:left;
    font-size: 12px;
    /* border:black thin solid; */
    max-width:50%;
}
.emg-row div p {
    margin : 0 !important;
}
.emg-row div hr {
    border: 1px solid black !important;
}
.emg-row div h5{
    margin-bottom:15px;
    font-size:16px;
}
.emg-row div, .emg-row div p {
    font-size: 12px !important;
}

.emg-row table{
    width:100%;
    table-layout:fixed;
}
</style>
<div class="row emg-row">
    <div class="col-md-12">
        <div class="banner-header">
            <div class="banner-inline">
                <img class="img-banner" src="<?= convert_imageto_base64(base_url('assets/img/header_resume.jpg')); ?>" alt="resume">
            </div>
            <div class="banner-inline">
                <h4 style="color:black;font-weight: bold;">SURAT KETERANGAN EMERGENCY</h4>
            </div>
        </div>
        <hr class="mt-0" style="border: 1px solid black !important;opacity: 1;">

        <?php 
            $identitas      = $data_emergency['identitas'];
            $atasnama       = $identitas['identitas'];
            $namalengkap    = $identitas['namalengkap'];
            $gabung_nama    = $atasnama.' '.$namalengkap;
            $alamat         = $identitas['alamat'];
            $norm           = $identitas['norm'];
            $usia           = $identitas['usia'];
            $jeniskelamin   =  ucfirst( $identitas['jeniskelamin'] );
            $tglperiksa     = $identitas['tglperiksa'];
            $dokter         = $identitas['dokter'];

            $jenisrujukan   = $data_emergency['jenisrujukan'];

            $datapendaftaran = $data_emergency['datapendaftaran'];
            $nojkn           = $datapendaftaran['nojkn'];

            $ttd = $identitas['dokter'].' RSU Queen Latifa Yogyakarta '.$identitas['tglperiksa'] .' SIP : '.$identitas['sip_dokter'];


            $diag   = $data_emergency['diagnosa'];
            $dtd ='';
            foreach($diag as $row){ $dtd.=( ($row['namaicd'] == null) ? '' : $row['icd'] ).' '.$row['namaicd'].'<br>'; }
            // $diagnosa_sitiql = $dtd.' '.$data_emergency['ketradiologi']['diagnosa']; 
            $diagnosa_sitiql = $data_emergency['ketradiologi']['diagnosa']; 
            #cek db skinap
            $row_emergency   = (array) $row_emergency;

            $diagnosa        = ( empty($row_emergency['diagnosa']) ) ? str_replace('<br>',',', $diagnosa_sitiql) : str_replace('<br>',',', $row_emergency['diagnosa']) ;
            $tindakan        = $row_emergency['tindakan'] ?? '';
            $tglemergency    = $row_emergency['tglemergency'] ?? '';
        ?>

        <table class="table table-striped">
            <tbody>
                <tr>
                    <td colspan="3">Yang Bertanda Tangan dibawah ini :</td>
                </tr>
                <tr>
                    <td width="20%">Nama</td>
                    <td width="2%">:</td>
                    <td><?= $dokter; ?></td>
                </tr>
                <tr>
                    <td>Alamat</td>
                    <td>:</td>
                    <td>RSU QUEEN LATIFA Jl. Ring Road Barat, Mlangi, Gamping, Sleman,Yogyakarta</td>
                </tr>
                <tr>
                    <td>Bahwa pada Tanggal</td>
                    <td>:</td>
                    <td>
                            <?= ( empty($tglemergency) || $tglemergency == '0000-00-00' ) ? format_tanggal( str_replace("/","-",$tglperiksa) ) : $tglemergency; ?>
                    </td>
                </tr>
                <tr>
                    <td>Nama</td>
                    <td width="5px">:</td>
                    <td><?= $gabung_nama; ?></td>
                </tr>
                <tr>
                    <td>Umur</td>
                    <td width="5px">:</td>
                    <td><?= $usia; ?></td>
                </tr>
                <tr>
                    <td>Jenis Kelamin</td>
                    <td width="5px">:</td>
                    <td><?= $jeniskelamin; ?></td>
                </tr>
                <tr>
                    <td>Alamat</td>
                    <td width="5px">:</td>
                    <td><?= $alamat; ?></td>
                </tr>
                <tr>
                    <td>Diagnosa</td>
                    <td width="5px">:</td>
                    <td>
                        <?= $diagnosa; ?>
                    </td>
                </tr>
                <tr>
                    <td>Tindakan</td>
                    <td width="5px">:</td>
                    <td>
                        <?= $tindakan; ?>
                    </td>
                </tr>
            </tbody>
        </table>
        <div class="text-ket">
            Demikian Surat Keterangan ini dibuat agar dapat digunakan sebagaimana mestinya
        </div>
        <div class="ttd-kontrol" style="text-align: center;float: right;">
            <div class="tgl-ttd">Yogyakarta, <?= $tglperiksa ?> </div>
            <div class="tgl-ttd">Dokter IGD</div>
            <br>
            <img src="<?= convert_imageto_base64(convert_to_qrcode_pdf( str_replace(".","",$ttd) )); ?>" alt="Tanda Tangan Dokter">
            <br>
            <div class="name-ttd">( <?= $dokter; ?> )</div>
        </div>
    </div>
</div>
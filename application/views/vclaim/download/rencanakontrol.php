<?php 
$noSuratKontrol = '';
$namaPoliTujuan = '';
$namaDokter     = '';
$tglRencanaKontrol = '';

$diagnosa       ='';

$noKartu        = '';
$nama           = '';
$tglLahir       = '';
$kelamin        = '';

$code_error = '';
$error_message = '';

$datatgl_terbit_cetak = '';


// echo '<pre>';
// print_r( $data_rencanakontrol );
// echo '</pre>';

if(  !empty($data_rencanakontrol['response']['noSuratKontrol']) ){

    $response       = $data_rencanakontrol['response'];
    $noSuratKontrol = $response['noSuratKontrol'];
    $namaPoliTujuan = $response['namaPoliTujuan'];
    $namaDokter     = $response['namaDokter'];
    $tglRencanaKontrol = $response['tglRencanaKontrol'];
    
    $sep            = $response['sep'];
    $diagnosa       = $sep['diagnosa'];
    
    $peserta        = $sep['peserta'];
    $noKartu        = $peserta['noKartu'];
    $nama           = $peserta['nama'];
    $tglLahir       = $peserta['tglLahir'];
    $kelamin        = jenis_kelamin( $peserta['kelamin'] );
    $datatgl_terbit_cetak = $this->qlwebapi->api_vclaim_tanggal_rekon($noSuratKontrol);

}else if( !empty($data_rencanakontrol['metaData']) && $data_rencanakontrol['metaData']['code'] == 200 ){
    $response       = $data_rencanakontrol['response'];
    $noSuratKontrol = $response['nosuratsep'];
    $namaPoliTujuan = $response['polisep'];
    $namaDokter     = $response['dpjpsep'];
    $tglRencanaKontrol = $response['tglSep'];
    
    $diagnosa       = $response['diagnosa'];
    
    $peserta        = $response['peserta'];
    $noKartu        = $peserta['noKartu'];
    $nama           = $peserta['nama'];
    $tglLahir       = $peserta['tglLahir'];
    $kelamin        = jenis_kelamin( $peserta['kelamin'] );
    $datatgl_terbit_cetak = $this->qlwebapi->api_vclaim_tanggal_rekon($noSuratKontrol);

}else{
    if( empty($data_rencanakontrol['metaData']) ){
        $code_error     = $data_rencanakontrol['response']['metaData']['code'];
        $error_message  = $data_rencanakontrol['response']['metaData']['message'];
    }
}

?>


<style>
.rekon-header {
    display: inline-block;
    width: 49.3333333%;
    vertical-align: top !important;
    margin-bottom:20px !important;
  }
  .rekon-header .img-banner {
    width: 100% !important;
  }
.tb-ql tr, .tb-ql tr th, .tb-ql tr td {
    border: 0 !important;
    color: black;
    padding: 0;
    text-align: left !important ;
    font-size:12px !important;
    font-weight:normal !important;
}
.norekon{
    text-align:center;
    font-size:14px !important;
}
.banner-inline-new {
    display: inline-block;
    width: 49.3333%;
    vertical-align: middle;
    /* margin-top:20px; */
}
</style>

<?php if(!empty($code_error)): ?>
    <div class="alert alert-danger" style="color:white;" role="alert"> Error Code : <?= $code_error ?> - <?= $error_message; ?> </div>
<?php endif; ?>

<div class="row">
    <div class="col-md-12">
        <div class="banner-header mb-4 rekon-header">
            <img class="img-banner" src="<?= convert_imageto_base64(base_url('assets/img/rencana-kontrol.png')); ?>" alt="resume">
        </div>
        <div class="rekon-header">
            <div class="norekon"><?= 'No. '. $noSuratKontrol; ?></div>
        </div>
    </div>
    <div class="col-md-12">
        <div class="table-responsive">
            <table class="table tb-ql">
                <tbody>
                    <tr>
                        <th width="15%" style="vertical-align:top !important;">Kepada YTH</th>
                        <td></td>
                        <td>
                            <p class="mb-0" style="margin:0;padding:0;"><?= $namaDokter; ?></p>
                            <p class="mb-0" style="margin:0;padding:0;"><?= 'Sp./Sub. '.$namaPoliTujuan; ?></p>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="3">Mohon Pemeriksaan dan Penanganan Lebih Lanjut :</td>
                    </tr>
                    <tr>
                        <th>No.Kartu</th>
                        <td style="padding-right: 5px;padding-left: 5px;">:</td>
                        <td><?= $noKartu; ?></td>
                    </tr>
                    <tr>
                        <th>Nama peserta</th>
                        <td style="padding-right: 5px;padding-left: 5px;">:</td>
                        <td><?= $nama.' ('.$kelamin.') '; ?></td>
                    </tr>
                    <tr>
                        <th>Tgl.Lahir</th>
                        <td style="padding-right: 5px;padding-left: 5px;">:</td>
                        <td><?= format_tanggal_indonesia( $tglLahir ); ?></td>
                    </tr>
                    <tr>
                        <th>Diagnosa</th>
                        <td style="padding-right: 5px;padding-left: 5px;">:</td>
                        <td><?= $diagnosa; ?></td>
                    </tr>
                    <tr>
                        <th>Rencana Kontrol</th>
                        <td style="padding-right: 5px;padding-left: 5px;">:</td>
                        <td><?= format_tanggal_indonesia( $tglRencanaKontrol ); ?></td>
                    </tr>
                </tbody>
            </table>
        </div>
        <div class="keterangan" style="font-size:12px;margin: 10px 0 20px 0;">
            Demikian atas bantuanya,diucapkan banyak terima kasih.
        </div>
       
    </div>
    <div class="col-md-8 banner-inline-new">
        <div class="footnote" style="font-size:10px;">
            <?php 
                if(!empty( $row_rekon )){
                    $tglentri        = $row_rekon->tglentri;
                    $keterangancetak = $row_rekon->keterangancetak;
                }else if( !empty($datatgl_terbit_cetak['response']) ){
                    $tglentri        = $datatgl_terbit_cetak['response']['tglTerbit'];
                    $keterangancetak = $datatgl_terbit_cetak['response']['tanggal_cetak'];
                }else{
                    $tglentri = "";
                    $keterangancetak = "";
                }
            ?>
            Tgl.Entri : <?= $tglentri; ?>
            | 
            Tgl.Cetak: <?= $keterangancetak; ?>
        </div>
    </div>
    <div class="col-md-4 banner-inline-new">
        <div class="ttd-dpjp" style="text-align:center;">
            <p class="mb-0" style="margin:0;padding:0;">Mengetahui DPJP,</p> 
            <div class="qrcode-ttd" style="margin:0;padding:0;">
                <?php if( ( !empty($data_rencanakontrol['response']['metaData']) && $data_rencanakontrol['response']['metaData']['code'] == 200 ) || ( !empty($data_rencanakontrol['metaData']) && $data_rencanakontrol['metaData']['code'] == 200) ):?>
                <img style="margin:0;padding:0;" src="<?= convert_imageto_base64(convert_to_qrcode_pdf( str_replace(".","",$namaDokter) )); ?>" alt="nomor jkn">
                <?php endif; ?>
            </div>
            <p class="mb-0" style="margin:0;padding:0;"><?= $namaDokter; ?> </p>
        </div>
    </div>
</div>
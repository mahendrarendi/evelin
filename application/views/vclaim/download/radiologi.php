<style>
.rad-row .pdf-tb tr th, .rad-row .pdf-tb tr td{
    text-align:left;
    font-size: 12px;
    /* border:black thin solid; */
    max-width:50%;
}
.rad-row div p {
    margin : 0 !important;
}
.rad-row div hr {
    border: 1px solid black !important;
}
.rad-row div h5{
    margin-bottom:15px;
    font-size:16px;
}
.rad-row div, .rad-row div p {
    font-size: 12px !important;
}

.rad-row table{
    width:100%;
    table-layout:fixed;
}
</style>
<div class="row rad-row">
    <div class="col-md-12">
        <?php 
            $t = $data_radiologi;
            $a = '<div style="float: none;"><img width="35%" src="'.convert_imageto_base64( base_url("/assets/img/".$t['header'] )).'" /></div>';
            $a .= '<hr style="border:1px solid;opacity:1;">';
            $e = $t['identitas']['keteranganradiologi'] . ("" == $t['identitas']['saranradiologi'] ? "" : "<b>Saran:</b>"). $t['identitas']['saranradiologi'];
            $i = "";
            $d = "";
            
            foreach( $t['diagnosa'] as $dg ){ $i .= '<span style="display:block;">' . $dg['icd'] . " " . $dg['namaicd']; }
            foreach( $t['elektromedik'] as $em ){ $d .= '<span style="display:block;">' . $em['icd'] . " " . $em['namaicd']; }

            $r = $t['identitas'];
            $s = '<div style="border-top:1px solid #3a3a3a;"></div>';
            $o = 'style="width:100%; text-align:left; font-size:medium;font-family: "Palatino Linotype", "Book Antiqua", "Palatino";color: #333333;"';
    
            $ttd = $t['dokterpenilai'].' RSU Queen Latifa Yogyakarta '.$r['waktu'] .' SIP : '.$t['sipdokterpenilai'];

            $a .= "<table  class='pdf-tb textcolor-table'>";
            $a .= "<tr><td>N.Periksa/N.RM </td><td width='2%'>:</td> <td >" . $r['idpemeriksaan'] . "/" . $r['norm'] . '</td><td>D. Pengirim</td> <td width="2%">:</td><td style="height:auto;width:35%;">' . $r['dokter'] . "</td><tr>";
            $a .= '<tr><td>Nama</td><td>:</td> <td style="height:auto;width:35%;">' . $r['identitas'] . " " . $r['namalengkap'] . '</td><td>Asal</td>   <td>:</td><td style="height:auto;width:35%;">' . $r['namaunit'] . "</td><tr>";
            $a .= "<tr><td>J. Kelamin</td><td>:</td> <td>" . $r['jeniskelamin'] . "</td><td>T. Periksa</td><td>:</td><td>" . $r['waktu'] . "</td><tr>";
            $a .= "<tr><td>Usia</td><td>:</td> <td>" . $r['usia'] . '</td><td>Alamat</td> <td>:</td><td style="height:auto;width:35%;">' . $r['alamat'] . "</td><tr>";
            $a .= "</table>";
            $a .= $s;
            $a .= "<table ".$o." class='pdf-tb textcolor-table'><tr ><th>Diagnosis Klinis</th><th>Pemeriksaan Radiologis</th></tr>";
            $a .= '<tr><td style="height:auto;width:50%;">' . $i . '</td><td style="height:auto;width:50%;">' . $d . "</td></tr></table>";
            $a .= $s;
            $a .= '<table class="pdf-tb textcolor-table">'  . $e . "</td></tr></table>";
            $a .= '<table class="pdf-tb textcolor-table" style="margin-top:10px;margin-left:65%; text-align:center;"><tr><td>Dokter Penilai<br><img src="'.convert_imageto_base64(convert_to_qrcode_pdf( str_replace('.','',$ttd) )).'" alt="Tanda Tangan Dokter"><br></td></tr><tr><td>' . $t['dokterpenilai'] . "<br>" . $t['sipdokterpenilai'] .  "</td></tr></table>";

            echo $a;
        ?>
    </div>
</div>
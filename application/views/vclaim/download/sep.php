<?php 

$noSep          = '';
$tglSep         = '';
$jnsPelayanan   = '';
$poli           = '';
$kelasRawat     = '';

$penjamin       = '';
$catatan        = '';

$noKartu        = ''; 
$nama           = ''; 
$tglLahir       = ''; 
$kelamin        = ''; 
$jnsPeserta     = ''; 
$hakKelas       = ''; 
$noMr           = '';

$kdDPJP         = '';
$nmDPJP         = '';

$notif_katarak  = '';

$klsRawatHak    = '';
$rujukan_response   = "";
$peserta_rujukan    = "";
$mr_rujukan         = "";
$telepon_rujukan    = "";
$provPerujuk        = "";
$diagnosa_rujukan   = "";
$diagnosa_kode      = "";
$diagnosa_nama      = "";
$prb                = "";
$diagnosa_klinis    = "";

if( isset($_GET['test']) ){

    echo '<pre>';
    print_r( $data_bpjs );
    echo '</pre>';
}

if( !empty( $data_bpjs['sep'] ) && $data_bpjs['sep']['metaData']['code'] == 200 ){

    $datasep        = $data_bpjs['sep'];


    if( $datasep['metaData']['code'] == '200' ){
        
        
        $datarujukan    = $data_bpjs['rujukan'];

        $sep_response   = $datasep['response'];
        $noSep          = $sep_response['noSep'];
        $tglSep         = $sep_response['tglSep'];
        $jnsPelayanan   = $sep_response['jnsPelayanan'];
        $poli           = $sep_response['poli'];
        $kelasRawat     = $sep_response['kelasRawat'];

        $penjamin       = $sep_response['penjamin'];
        $catatan        = $sep_response['catatan'];

        $peserta        = $sep_response['peserta']; 
        $noKartu        = $peserta['noKartu']; 
        $nama           = $peserta['nama']; 
        $tglLahir       = $peserta['tglLahir']; 
        $kelamin        = jenis_kelamin($peserta['kelamin']); 
        $jnsPeserta     = $peserta['jnsPeserta']; 
        $hakKelas       = $peserta['hakKelas']; 
        $noMr           = $peserta['noMr'];

        $dpjp           = $sep_response['dpjp'];
        $kdDPJP         = $dpjp['kdDPJP'];
        $nmDPJP         = $dpjp['nmDPJP'];


        $klsRawat       = $sep_response['klsRawat'];
        $klsRawatHak    = $klsRawat['klsRawatHak'];

        $katarak        = $sep_response['katarak'];
        $notif_katarak  = ( $katarak ) ? '*PASIEN OPERASI KATARAK' : '' ;

        if(!empty($data_bpjs['rekon'])){ 
            $rekon          = $data_bpjs['rekon'];
            $response_rekon = $rekon['response'];

            if( empty($response_rekon['sep']) ){
                $rekon_provPerujuk = $response_rekon['provPerujuk'];
                $diagnosa_klinis    = $response_rekon['diagnosa'] ;
                $provPerujuk        = $rekon_provPerujuk['nmProviderPerujuk'];
            }else{
                $rekon_provPerujuk = $response_rekon['sep']['provPerujuk'];
                $diagnosa_klinis    = $response_rekon['sep']['diagnosa'] ;
                $provPerujuk        = $rekon_provPerujuk['nmProviderPerujuk'];
            }
            

        }else if( $datarujukan['metaData']['code'] == '200' ){
            #rujukan
            $rujukan_response   = $datarujukan['response']['rujukan'];
            $peserta_rujukan    = $rujukan_response['peserta'];
            $mr_rujukan         = $peserta_rujukan['mr'];
            $telepon_rujukan    = $mr_rujukan['noTelepon'];

            $provPerujuk        = $rujukan_response['provPerujuk']['nama'];
               
            $diagnosa_rujukan   = $rujukan_response['diagnosa'];
            $diagnosa_kode      = $diagnosa_rujukan['kode'];
            $diagnosa_nama      = $diagnosa_rujukan['nama'];
    
            $diagnosa_klinis    = $diagnosa_kode.' - '.$diagnosa_nama;
            
            $informasi          = $peserta_rujukan['informasi']; 
            $prb                = $informasi['prolanisPRB']; 

        }else{
            $rujukan_response   = "";
            $peserta_rujukan    = "";
            $mr_rujukan         = "";
            $telepon_rujukan    = "";
            $provPerujuk        = "";
            $diagnosa_rujukan   = "";
            $diagnosa_kode      = "";
            $diagnosa_nama      = "";
            $prb                = "";
            $diagnosa_klinis    = "";
        }
    }
}

?>

<style>

  .sep-header-prolanis {
    display: inline-block;
    width: 49.3333333%;
  }
  .sep-header-prolanis .img-banner {
    width: 100% !important;
  }

  .tb-ql tr, .tb-ql tr th, .tb-ql tr td {
    border: 0 !important;
    color: black;
    padding: 0;
    text-align: left !important ;
    font-size:12px !important;
    font-weight:normal !important;
  }

.ttd {
    text-align: center;
  }
  .ttd p {
    color: black;
    margin:0 !important;
  }


.catatan,.footnote {
    color: black;
    font-size: 7px;
  }
  .footnote{
    margin-top:20px;
  }
  .banner-inline {
    display: inline-block;
    width: 49.3333%;
    vertical-align: top !important;
    margin-top:10px;
}

.banner-inline-new {
    display: inline-block;
    width: 49.3333%;
    vertical-align: middle;
    /* margin-top:20px; */
}
.banner-inline-new p{
    font-size:12px !important;
}
/* .banner-inline img {
    width: 80% !important;
} */
</style>

<div class="row">
    <div class="col-md-12">
        <div class="banner-header mb-4 sep-header-prolanis">
            <img class="img-banner" src="<?= convert_imageto_base64 (base_url('assets/img/header-sep-1.png') ); ?>" alt="SEP">
        </div>
        <div class="sep-header-prolanis" style="color:black;text-align: center;font-size:12px;">
            <?= $prb; ?>
        </div>
        <div class="security-check">
            <input type="hidden" class="nonce" name="<?= $csrf['name'] ?>" value="<?= $csrf['hash']; ?>">
            <input type="hidden" class="norm" name="norm" value="<?= $params['norm']; ?>">
            <input type="hidden" class="tglperiksa" name="tglperiksa" value="<?= $params['tglperiksa']; ?>">
        </div>
    </div>
    <div class="col-md-7 banner-inline">
        <div class="table-responsive">
            <table class="table tb-ql">
                <tbody>
                    <tr>
                        <th>No.SEP</th>
                        <td style="padding-right: 5px;padding-left: 25px;">:</td>
                        <td><?= $noSep; ?></td>
                    </tr>
                    <tr>
                        <th>Tgl.SEP</th>
                        <td style="padding-right: 5px;padding-left: 25px;">:</td>
                        <td><?= $tglSep; ?></td>
                    </tr>
                    <tr>
                        <th>No.Kartu</th>
                        <td style="padding-right: 5px;padding-left: 25px;">:</td>
                        <td><?= $noKartu .' ( MR. '.$noMr.')'; ?></td>
                    </tr>
                    <tr>
                        <th>Nama Peserta</th>
                        <td style="padding-right: 5px;padding-left: 25px;">:</td>
                        <td><?= $nama ?></td>
                    </tr>
                    <tr>
                        <th>Tgl.Lahir</th>
                        <td style="padding-right: 5px;padding-left: 25px;">:</td>
                        <td><?= $tglLahir.' Kelamin : '.$kelamin; ?></td>
                    </tr>
                    <tr>
                        <th>No.Telepon</th>
                        <td style="padding-right: 5px;padding-left: 25px;">:</td>
                        <td><?= $telepon_rujukan; ?></td>
                    </tr>
                    <tr>
                        <th>Sub/Spesialis</th>
                        <td style="padding-right: 5px;padding-left: 25px;">:</td>
                        <td><?= $poli;   ?></td>
                    </tr>
                    <tr>
                        <th>Dokter</th>
                        <td style="padding-right: 5px;padding-left: 25px;">:</td>
                        <td><?= $nmDPJP; ?></td>
                    </tr>
                    <tr>
                        <th>Faskes Perujuk</th>
                        <td style="padding-right: 5px;padding-left: 25px;">:</td>
                        <td><?= $provPerujuk; ?></td>
                    </tr>
                    <tr>
                        <th>Diagnosa Awal</th>
                        <td style="padding-right: 5px;padding-left: 25px;">:</td>
                        <td><?= $diagnosa_klinis; ?></td>
                    </tr>
                    <tr>
                        <th>Catatan</th>
                        <td style="padding-right: 5px;padding-left: 25px;">:</td>
                        <td><?= $catatan; ?></td>
                    </tr>
                </tbody>
            </table>
        </div>
    </div>
    <div class="col-md-5 banner-inline">
        <?php
            if( !empty($notif_katarak) ){
                echo '<div class="ket-op" style="color:black;font-size:10px;">'.$notif_katarak.'</div>';
            }
        ?>
        <div class="table-reponsive">
            <table class="table tb-ql">
                <tbody>
                    <tr>
                        <th style="padding-right: 5px;padding-bottom: 30px;">Peserta</th>
                        <td style="padding-right: 5px;padding-bottom: 30px;padding-left: 10px;">:</td>
                        <td style="padding-right: 5px;padding-bottom: 30px;"><?= $jnsPeserta; ?></td>
                    </tr>
                    <tr>
                        <th>Jns.Rawat</th>
                        <td style="padding-right: 5px;padding-left: 10px;">:</td>
                        <td><?= $jnsPelayanan; ?></td>
                    </tr>
                    <tr>
                        <th>Jns.Kunjungan</th>
                        <td style="padding-right: 5px;padding-left: 10px;">:</td>
                        <td>
                            <?php 
                                if( !empty($catatan) && $catatan != 'EMERGENCY'  ){
                                    echo '-';
                                }else if($catatan === 'EMERGENCY'){
                                    echo  '- Konsultasi Dokter (Pertama)';
                                }else{
                                    if( akses_jenispemeriksaan($data_array['jns_rujukan']) == 'skdp' && $data_array['cek_rencana_kontrol'] == TRUE ) {
                                            echo '- Kunjungan Kontrol (Ulangan)';
                                    }else{
                                            echo  '- Konsultasi Dokter (Pertama)';
                                    } 
                                } 
                            
                            ?>
                        </td>
                    </tr>
                    <tr>
                        <th></th>
                        <td style="padding-right: 5px;padding-left: 10px;">:</td>
                        <td>
                        <?php 
                                if( !empty($catatan) && $catatan != 'EMERGENCY'  ){
                                    echo '-';
                                }else if($catatan === 'EMERGENCY'){
                                    echo  '-';
                                }else{
                                    if( akses_jenispemeriksaan($data_array['jns_rujukan']) == 'skdp' && $data_array['cek_rencana_kontrol'] == TRUE ) {
                                            echo '- Prosedur tidak berkelanjutan';
                                    }else{
                                            echo  '-';
                                    } 
                                } 
                            
                            ?>
                        </td>
                    </tr>
                    <tr>
                        <th>Poli Perujuk</th>
                        <td style="padding-right: 5px;padding-left: 10px;">:</td>
                        <td>-</td>
                    </tr>
                    <tr>
                        <th>Kls.Hak</th>
                        <td style="padding-right: 5px;padding-left: 10px;">:</td>
                        <td><?= $hakKelas; ?></td>
                    </tr>
                    <tr>
                        <th>Kls.Rawat</th>
                        <td style="padding-right: 5px;padding-left: 10px;">:</td>
                        <td><?= '-'; ?></td>
                    </tr>
                    <tr>
                        <th>Penjamin</th>
                        <td style="padding-right: 5px;padding-left: 10px;"></td>
                        <td></td>
                    </tr>
                </tbody>
            </table>
        </div>
        
    </div>
    <div class="col-md-8 banner-inline-new" style="margin-top:25px;">
        <div class="catatan mb-3">
            *Saya menyetujui BPJS Kesehatan untuk : 
            <ol class="list-footnote" type="a" style="margin:0;padding-left: 15px;">
                <li>Membuka dan atau menggunakan informasi medis pasien untuk keperluan administrasi, pembayaran asuransi atau jaminan pembiayaan kesehatan.</li>
               <li>memberikan akses informasi atau riwayat pelayanan kepada dokter/tenaga medis pada RSU QUEEN LATIFA untuk kepentingan pemeliharaan kesehatan, pengobatan, penyembuhan,dan perawatan pasien</li>
            </ol>
            *Saya mengetahui dan memahami :
            <ol class="list-footnote" type="a"  style="margin:0;padding-left: 15px;">
                <li>Rumah sakit dapat melakukan koordinasi dengan PT Jasa Raharja/PT Taspen/PT ASABRI/BPJS Ketenagakerjaan atau penjamin lainya. jika peserta merupakan pasien yang mengalami kecelakaan lalulintas dan/atau kecelakaan kerja</li>
                <li>SEP bukan sebagai bukti penjaminan peserta</li>
            </ol>

            **Dengan tampilnya luaran SEP elektronik ini merupakan hasil validasi terhadap eligibilitas Pasien secara elektronik <br>
            &nbsp;&nbsp; (validasi finger print atau biometrik / sistem validasi lain) <br>
            &nbsp;&nbsp; dan selanjutnya Pasien dapat mengakses pelayanan kesehatan rujukan sesuai ketentuan berlaku. <br>
            &nbsp;&nbsp; Kebenaran dan keaslian atas informasi data Pasien menjadi tanggung jawab penuh FKRTL <br>
        </div>
        <div class="footnote">
            Cetakan ke 
            <?php 
                // if( !empty($row_sep) ){ 
                //     echo $row_sep->cetakan;
                // }
            ?>
            <?php 
                if( !empty($row_sep) ){ 
                    $cetakan = empty($row_sep->cetakan) ? $data_array['tglcetak_sep'] : $row_sep->cetakan; 
                }else{
                    $cetakan = $data_array['tglcetak_sep'];
                }
                
                echo $cetakan; 
            ?>
            
        </div>
    </div>
    <div class="col-md-4 banner-inline-new">
        <div class="ttd">
            <p class="mb-0">Pasien/Keluarga Pasien</p>
            <div class="qrcode-ttd">
                <?php if( !empty( $data_bpjs['sep'] ) && $data_bpjs['sep']['metaData']['code'] == 200 ): ?>
                    <img src="<?= convert_imageto_base64(convert_to_qrcode_pdf( str_replace(".","",$noKartu) )); ?>" alt="nomor jkn">
                <?php endif; ?>
            </div>
            <p class="mb-0"><?= strtoupper( $nama ); ?></p>
        </div>
    </div>
</div>
<style>
.banner-inline {
    display: inline-block;
    width: 49.3333%;
    vertical-align: middle;
}

.banner-inline img {
    width: 80% !important;
}
</style>
<div class="row">
    <div class="col-md-12">
        <div class="banner-header">
            <div class="banner-inline">
                <img class="img-banner" src="<?= base_url('assets/img/headerresume.svg'); ?>" alt="resume">
            </div>
            <div class="banner-inline">
                <h4 style="color:black;font-weight: bold;">SURAT KETERANGAN EMERGENCY</h4>
            </div>
        </div>
        <hr class="mt-0" style="border: 1px solid black !important;opacity: 1;">

        <?php 
            $identitas      = $data_emergency['identitas'];
            $atasnama       = $identitas['identitas'];
            $namalengkap    = $identitas['namalengkap'];
            $gabung_nama    = $atasnama.' '.$namalengkap;
            $alamat         = $identitas['alamat'];
            $norm           = $identitas['norm'];
            $usia           = $identitas['usia'];
            $jeniskelamin   =  ucfirst( $identitas['jeniskelamin'] );
            $tglperiksa     = $identitas['tglperiksa'];
            $dokter         = $identitas['dokter'];

            $jenisrujukan   = $data_emergency['jenisrujukan'];

            $ttd = $identitas['dokter'].' RSU Queen Latifa Yogyakarta '.$identitas['tglperiksa'] .' SIP : '.$identitas['sip_dokter'];


            $datapendaftaran = $data_emergency['datapendaftaran'];
            $nojkn           = $datapendaftaran['nojkn'];

            $diag   = $data_emergency['diagnosa'];
            $dtd ='';
            foreach($diag as $row){ $dtd.=( ($row['namaicd'] == null) ? '' : $row['icd'] ).' '.$row['namaicd'].'<br>'; }
            // $diagnosa_sitiql = $dtd.' '.$data_emergency['ketradiologi']['diagnosa']; 
            $diagnosa_sitiql = $data_emergency['ketradiologi']['diagnosa']; 
            #cek db skinap
            $row_emergency   = (array) $row_emergency;

            $diagnosa        = ( empty($row_emergency['diagnosa']) ) ? str_replace('<br>',',', $diagnosa_sitiql) : str_replace('<br>',',', $row_emergency['diagnosa']) ;
            $tindakan        = $row_emergency['tindakan'] ?? '';
            $tglemergency    = $row_emergency['tglemergency'] ?? '';
        ?>

        <table class="table table-striped">
            <tbody>
                <tr>
                    <td colspan="3">Yang Bertanda Tangan dibawah ini :</td>
                </tr>
                <tr>
                    <td>Nama</td>
                    <td width="5px">:</td>
                    <td><?= $dokter; ?></td>
                </tr>
                <tr>
                    <td>Alamat</td>
                    <td>:</td>
                    <td>RSU QUEEN LATIFA Jl. Ring Road Barat, Mlangi, Gamping, Sleman,Yogyakarta</td>
                </tr>
                <tr>
                    <td>Bahwa pada Tanggal</td>
                    <td>:</td>
                    <td>
                        <?php if( is_superadmin() || is_igd() || is_poliumum() || is_klaim() ): ?>
                        <input style="display: inline-block;width: 30%;" value="<?= ( empty($tglemergency) || $tglemergency == '0000-00-00' ) ? format_tanggal( str_replace("/","-",$tglperiksa) ) : $tglemergency; ?>" type="date" autocomplete="off" name="tglemergency" class="form-control"> telah dilakukan pemeriksaan emergency atas pasien : 
                        <?php else: ?>
                            <?= ( empty($tglemergency) || $tglemergency == '0000-00-00' ) ? format_tanggal( str_replace("/","-",$tglperiksa) ) : $tglemergency; ?>
                        <?php endif; ?>
                    </td>
                </tr>
                <tr>
                    <td>Nama</td>
                    <td width="5px">:</td>
                    <td><?= $gabung_nama; ?></td>
                </tr>
                <tr>
                    <td>Umur</td>
                    <td width="5px">:</td>
                    <td><?= $usia; ?></td>
                </tr>
                <tr>
                    <td>Jenis Kelamin</td>
                    <td width="5px">:</td>
                    <td><?= $jeniskelamin; ?></td>
                </tr>
                <tr>
                    <td>Alamat</td>
                    <td width="5px">:</td>
                    <td><?= $alamat; ?></td>
                </tr>
                <tr>
                    <td>Diagnosa</td>
                    <td width="5px">:</td>
                    <td>
                        <?php if( is_superadmin() || is_igd() || is_poliumum() || is_klaim() ): ?>
                        <input value="<?= $diagnosa; ?>" type="text" autocomplete="off" name="diagnosa" class="form-control">
                        <?php else: ?>
                            <?= $diagnosa; ?>
                        <?php endif; ?>
                    </td>
                </tr>
                <tr>
                    <td>Tindakan</td>
                    <td width="5px">:</td>
                    <td>
                        <?php if( is_superadmin() || is_igd() || is_poliumum() || is_klaim() ): ?>
                        <input value="<?= $tindakan; ?>" type="text" autocomplete="off" name="tindakan" class="form-control">
                        <?php else: ?>
                            <?= $tindakan; ?>
                        <?php endif; ?>
                    </td>
                </tr>
            </tbody>
        </table>
        <div class="text-ket">
            Demikian Surat Keterangan ini dibuat agar dapat digunakan sebagaimana mestinya
        </div>
        <div class="ttd-kontrol" style="text-align: center;float: right;">
            <div class="tgl-ttd">Yogyakarta, <?= $tglperiksa ?> </div>
            <div class="tgl-ttd">Dokter IGD</div>
            <br>
            <?= convert_to_qrcode($ttd,'100x100'); ?>
            <br>
            <div class="name-ttd">( <?= $dokter; ?> )</div>
        </div>
    </div>
</div>
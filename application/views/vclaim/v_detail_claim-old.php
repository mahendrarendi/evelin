<style>
#frm-resumegabung .form-group {
  display: inline-block;
  width: 49.3333%;
  margin-bottom: 0;
}
</style>
<div class="row">
    <div class="col-md-12">
        <div class="card">
            <div class="card-header pb-0">
                <div class="head-title inline-left">
                    <h6>Detail Vclaim</h6>
                    <?php if(!is_bpjs()): ?>
                        <?= '<p><em>Jenis Permeriksaan : </em><span class="badge badge-sm bg-gradient-success">'.strtoupper( akses_jenispemeriksaan($jenisrujukan) ).'</span></p>'; ?>
                    <?php endif; ?>
                </div>
                <div class="backbutton inline-right">
                    <button onclick="history.back()" class="btn btn-primary back-history" type="button"> <i class="fa fa-arrow-left"></i> Back</button>
                </div>
            </div>
            
            <div class="card-body">
                
                <div class="idunit-detail" data-id="<?= $idunit; ?>"></div>

                <?php //if( !empty($data_bpjs['sep']) ): ?>
                <div class="header-tab">
                    
                    <?php if(is_superadmin() || is_klaim() || is_bpjs() ): ?>
                        <button class="btn btn-warning text-white active" data-t="all" type="button">All</button>
                        <button class="btn btn-primary" type="button" data-toggle="collapse" data-t="inacbg" data-target="#inacbg">INA-CBG</button>
                        <button class="btn btn-primary" type="button" data-toggle="collapse" data-t="resume" data-target="#resume">Resume</button>
                        <button class="btn btn-primary" type="button" data-toggle="collapse" data-t="sep" data-target="sep">SEP</button>
                    <?php endif; ?>
                    
                    <?php if( akses_jenispemeriksaan($jenisrujukan) == 'skdp' || $cek_rencana_kontrol || akses_jenispemeriksaan($jenisrujukan) == 'Kontrol post ranap' ): ?>
                        <?php if(is_superadmin() || is_klaim() || (is_bpjs() && $is_tampil_rencanakontrol != '1' ) ): ?>  
                            <button class="btn btn-primary" type="button" data-toggle="collapse" data-t="rencana-kontrol" data-target="#rencana-kontrol">Rencana Kontrol</button>
                        <?php endif; ?>
                    <?php endif; ?>

                    <?php if( akses_jenispemeriksaan($jenisrujukan) == 'skdp' ):  ?>  
                        

                        <?php if(is_superadmin() || is_klaim() || is_poliumum() || is_igd() || is_bpjs() ): ?>  
                            <button class="btn btn-primary" type="button" data-toggle="collapse" data-t="suratkontrol" data-target="#suratkontrol">Surat Kontrol</button>
                        <?php endif; ?>
                    
                    <?php elseif(akses_jenispemeriksaan($jenisrujukan) == 'rujukanbaru'): ?>
                        <?php if(is_superadmin() || is_klaim() || is_bpjs() ): ?>
                            <button class="btn btn-primary" type="button" data-toggle="collapse" data-t="rujukan-baru" data-target="#rujukan-baru">Rujukan Baru</button>
                        <?php endif; ?>
                    <?php endif; ?>
                    
                    <?php if( (is_bpjs() && $is_tampil_skinap != '1') || is_superadmin() || is_klaim() || is_igd() || is_poliumum() ): ?>
                        <button class="btn btn-primary" type="button" data-toggle="collapse" data-t="suratkontrolinap" data-target="#suratkontrolinap">Surat Kontrol Inap</button>
                    <?php endif; ?>
                    <?php if( (is_bpjs() && $is_tampil_emergensi != '1') || is_superadmin() || is_klaim() || is_igd() || is_poliumum() ): ?>
                        <button class="btn btn-primary" type="button" data-toggle="collapse" data-t="emergency" data-target="#emergency">Emergency</button>
                    <?php endif; ?>
                    <?php if( (is_bpjs() && $is_tampil_radiologi != '1') || is_superadmin() || is_klaim() ): ?> 
                        <button class="btn btn-primary" type="button" data-toggle="collapse" data-t="radiologi" data-target="#radiologi">Radiologi</button>
                    <?php endif; ?>
                    
                    <?php if( ( $is_tampil_dokumenlainllain != '1' && is_bpjs() ) || is_superadmin() || is_klaim() || is_pendaftaran() ): ?>
                        <button class="btn btn-primary" type="button" data-toggle="collapse" data-t="dokumenlainlain" data-target="#dokumenlainlain">Dokumen Lain</button>
                    <?php endif; ?>
                    
                    <?php if(is_superadmin() || is_klaim() || is_bpjs() ): ?>  
                        <button class="btn btn-primary" type="button" data-toggle="collapse" data-t="nota" data-target="#nota">Billing</button>
                    <?php endif; ?>

                    <?php if(is_superadmin() ): ?>  
                     <!-- TEST -->
                        <button class="btn btn-primary" type="button" data-toggle="collapse" data-t="testpdf" data-target="#testpdf">Test PDF</button>
                    <!-- END TEST -->
                    <?php endif; ?>

                </div>
                <div class="body-tabs">
                    <div class="row">

                    <?php if(is_superadmin() || is_klaim() || is_bpjs() ): ?>

                        <div class="col-md-12 ">                            
                            <div class="collapse show" id="inacbg">
                                <div class="card card-body mb-3">
                                    {template_incbg}
                                </div>
                            </div>
                        </div>

                        <div class="col-md-12 ">
                            <div class="collapse show" id="sep">
                                <div class="card card-body mb-3">
                                    <!-- {template_sep} -->
                                    {template_sep_new}
                                </div>
                            </div>
                        </div>

                        <div class="col-md-12 ">
                            <div class="collapse show" id="resume">
                                <div class="card card-body mb-3">
                                    
                                    <?php if(is_superadmin() || is_klaim()): ?>
                                    <!-- resume gabung -->
                                    <div class="tambah-resumegabung">
                                        <div class="form-group">
                                            <button type="button" data-tipe="resume" data-norm="{norm}" data-tglperiksa="{tglperiksa}" data-name="<?= $csrf['name'] ?>" data-hash="<?= $csrf['hash']; ?>" class="btn btn-danger btn-sm"><i class="fa fa-plus-circle"></i> Resume Gabung</button>
                                        </div>
                                    </div>
                                    <?php endif; ?>
                                        
                                    {template_resume}
                                
                                </div>

                                <?php if( !empty($template_resumegabung) ): ?>
                                    <div class="card card-body mb-3">
                                        {template_resumegabung}
                                    </div>
                                <?php endif; ?>
                            </div>
                        </div>
                    <?php endif; ?>
                        
                    <?php if( akses_jenispemeriksaan($jenisrujukan) == 'skdp' || $cek_rencana_kontrol || akses_jenispemeriksaan($jenisrujukan) == 'Kontrol post ranap' ): ?>
                        <?php if(is_superadmin() || is_klaim() || (is_bpjs() && $is_tampil_rencanakontrol != '1' ) ): ?>
                            <div class="col-md-12 ">
                                <div class="collapse show" id="rencana-kontrol">
                                    <div class="card card-body mb-3">

                                        <?= html_switch_tampil($is_tampil_rencanakontrol); ?>

                                        <!-- {template_rencanakontrol} -->
                                        {template_rencanakontrol_new}
                                    </div>
                                </div>
                            </div>
                        <?php endif; ?>
                    <?php endif; ?>

                        <?php if( akses_jenispemeriksaan($jenisrujukan) == 'skdp' ):  ?> 
                        
                        
                        <?php if(is_superadmin() || is_klaim() || is_bpjs() || is_igd() || is_poliumum() ): ?>
                            <div class="col-md-12">
                                <div class="collapse show" id="suratkontrol">
                                    <div class="card card-body mb-3">

                                        <?= html_switch_tampil($is_tampil_skdp); ?>

                                        {template_suratkontrol}
                                    </div>
                                </div>
                            </div>
                        <?php endif; ?>

                        <?php elseif(akses_jenispemeriksaan($jenisrujukan) == 'rujukanbaru'): ?>
                            <?php if(is_superadmin() || is_klaim() || is_bpjs() ): ?>
                                <div class="col-md-12">
                                    <div class="collapse show" id="rujukan-baru">
                                        <div class="card card-body mb-3">

                                            <?= html_switch_tampil($is_tampil_rujukanbaru); ?>

                                            {template_rujukanbaru}
                                        </div>
                                    </div>
                                </div>
                            <?php endif; ?>
                        <?php endif; ?>

                            <?php if( (is_bpjs() && $is_tampil_skinap != '1' ) || is_superadmin() || is_klaim() || is_igd() || is_poliumum() ): ?>

                            <div class="col-md-12">
                                <div class="collapse show" id="suratkontrolinap">
                                    <div class="card card-body mb-3">

                                        <?= html_switch_tampil($is_tampil_skinap); ?>

                                        {template_suratkontrolinap}
                                    </div>
                                </div>
                            </div>

                            <?php endif; ?>

                            <?php if( (is_bpjs() && $is_tampil_emergensi != '1') || is_superadmin() || is_klaim() || is_poliumum() || is_igd() ): ?>

                            <div class="col-md-12">
                                <div class="collapse show" id="emergency">
                                    <div class="card card-body mb-3">
                                        
                                        <?= html_switch_tampil($is_tampil_emergensi); ?>

                                        {template_emergency}
                                    </div>
                                </div>
                            </div>

                            <?php endif; ?>

                            <?php if( (is_bpjs() && $is_tampil_radiologi != '1') || is_superadmin() || is_klaim() ): ?>

                            <div class="col-md-12">
                                <div class="collapse show" id="radiologi">
                                    
                                    <?php if( $found_ro  ): ?>
                                    <div class="card card-body mb-3">

                                        <?php if(is_superadmin() || is_klaim()): ?>
                                        <!-- radiologi gabung -->
                                        <div class="tambah-radiologigabung">
                                            <div class="form-group">
                                                <button type="button" data-tipe="radiologi" data-norm="{norm}" data-tglperiksa="{tglperiksa}" data-name="<?= $csrf['name'] ?>" data-hash="<?= $csrf['hash']; ?>" class="btn btn-danger btn-sm"><i class="fa fa-plus-circle"></i> Radiologi Gabung</button>
                                            </div>
                                        </div>
                                        <?php endif; ?>
        
                                        <?= html_switch_tampil($is_tampil_radiologi); ?>

                                        {template_radiologi}
                                    </div>
                                    <?php elseif(is_klaim()): ?>

                                        <div class="card card-body mb-3">

                                            <?php if(is_superadmin() || is_klaim()): ?>
                                            <!-- radiologi gabung -->
                                            <div class="tambah-radiologigabung">
                                                <div class="form-group">
                                                    <button type="button" data-tipe="radiologi" data-norm="{norm}" data-tglperiksa="{tglperiksa}" data-name="<?= $csrf['name'] ?>" data-hash="<?= $csrf['hash']; ?>" class="btn btn-danger btn-sm"><i class="fa fa-plus-circle"></i> Radiologi Gabung</button>
                                                </div>
                                            </div>
                                            <?php endif; ?>
            
                                            <?= html_switch_tampil($is_tampil_radiologi); ?>

                                            {template_radiologi}
                                        </div>

                                    <?php endif; ?>

                                    <?php if($found_ro_gabung): ?>
                                        <?php if( !empty($template_radiologigabung) ): ?>
                                            <div class="card card-body mb-3">           
                                            {template_radiologigabung}
                                            </div>
                                        <?php endif; ?>
                                    <?php endif; ?>

                                </div>
                            </div>

                            <?php endif; ?>

                        <?php if( (is_bpjs() && $is_tampil_dokumenlainllain != '1' ) || is_superadmin() || is_klaim() || is_pendaftaran() ): ?>

                        <div class="col-md-12 ">
                            <div class="collapse show" id="dokumenlainlain">
                                <div class="card card-body mb-3">
                                    
                                    <?= html_switch_tampil($is_tampil_dokumenlainllain); ?>

                                    {template_dokumenlainlain}
                                </div>
                            </div>
                        </div>  

                        <?php endif; ?>

                        <?php if(is_superadmin() || is_klaim() || is_bpjs()): ?>
                            <div class="col-md-12 ">
                                <div class="collapse show" id="nota">
                                    <div class="card card-body mb-3">

                                        <?php if(is_superadmin() || is_klaim()): ?>
                                            <!-- Billing gabung -->
                                            <div class="tambah-billinggabung">
                                                <div class="form-group">
                                                    <button type="button" data-tipe="billing" data-norm="{norm}" data-tglperiksa="{tglperiksa}" data-name="<?= $csrf['name'] ?>" data-hash="<?= $csrf['hash']; ?>" class="btn btn-danger btn-sm"><i class="fa fa-plus-circle"></i> Billing Gabung</button>
                                                </div>
                                            </div>
                                        <?php endif; ?>

                                        {template_nota}
                                    </div>
                                    
                                    <?php if( !empty($template_billinggabung) ): ?>
                                        <div class="card card-body mb-3">
                                            {template_billinggabung}
                                        </div>
                                    <?php endif; ?>

                                </div>
                            </div>
                        <?php endif; ?>
                        
                        <?php if(is_superadmin()): ?>
                        <!-- TEST PDF -->
                        <div class="col-md-12 ">                            
                            <div class="collapse show" id="testpdf">
                                <div class="card card-body mb-3">
                                    {template_testpdf}
                                </div>
                            </div>
                        </div>
                        <!-- END TEST PDF -->
                        <?php endif; ?>

                    </div>
                </div>

                <?php //else: ?>
                    <!-- <div class="row">
                        <div class="col-md-12">
                            <div class="alert alert-danger text-white" role="alert">
                                <?// $data_bpjs['metaData']['code'].' - '.$data_bpjs['metaData']['message']; ?>
                            </div>
                        </div>
                    </div> -->
                <?php //endif; ?>

            </div>
        </div>
    </div>
</div>

<!-- MODAL -->
<?= modal_template(); ?>
<footer class="footer pt-3  ">
    <div class="container-fluid">
        <div class="row align-items-center justify-content-lg-between">

            <div class="col-lg-6 mb-lg-0 mb-4">
                <div class="copyright text-center text-sm text-muted text-lg-start">
                    © <script>
                        document.write(new Date().getFullYear())
                    </script>,
                    made with <i class="text-danger fa fa-heart"></i> by
                    <a href="https://rsu.queenlatifa.co.id/yogyakarta/" class="font-weight-bold" target="_blank">RSU Queen Latifa</a>
                    Evelin Web Apps.
                </div>
            </div>

        </div>
    </div>
</footer>
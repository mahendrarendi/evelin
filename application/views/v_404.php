<div class="row justify-content-center">
    <div class="col-md-12 text-center">
        <div class="card">
            <div class="card-body">
                <span class="display-1 d-block">404</span>
                <div class="mb-4 lead">Maaf Halaman Tidak Ditemukan :(</div>
                <a href="<?= base_url('dashboard'); ?>" class="btn btn-link">Kembali Ke Dashboard</a>
            </div>
        </div>
    </div>
</div>
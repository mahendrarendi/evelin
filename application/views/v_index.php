<!--
    =========================================================
    * Argon Dashboard 2 - v2.0.4
    =========================================================
    
    * Product Page: https://www.creative-tim.com/product/argon-dashboard
    * Copyright 2022 Creative Tim (https://www.creative-tim.com)
    * Licensed under MIT (https://www.creative-tim.com/license)
    * Coded by Creative Tim
    * Powered by Developer RSU Queen Latifa
    =========================================================
    
    * The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
    -->
    <!DOCTYPE html>
    <html lang="en">
    
    <head>
      <meta charset="utf-8" />
      <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
      <link rel="apple-touch-icon" sizes="76x76" href="<?= base_url(); ?>assets/img/apple-icon.png">
      <link rel="icon" type="image/png" href="<?= base_url(); ?>assets/img/qllogo.png">
      <title>
        Evelin RSU Queen Latifa
      </title>
      <!--     Fonts and icons     -->
      <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700" rel="stylesheet" />
      <!-- Nucleo Icons -->
      <link href="<?= base_url(); ?>assets/css/nucleo-icons.css" rel="stylesheet" />
      <link href="<?= base_url(); ?>assets/css/nucleo-svg.css" rel="stylesheet" />
      <!-- Font Awesome Icons -->
      <script src="https://kit.fontawesome.com/42d5adcbca.js" crossorigin="anonymous"></script>
      <link href="<?= base_url('assets/css/nucleo-svg.css'); ?> " rel="stylesheet" />
      <!-- CSS Files -->
      <link id="pagestyle" href="<?= base_url(); ?>assets/css/argon-dashboard.css?v=2.0.4" rel="stylesheet" />

      <!-- Evelin css -->
      <?php 
        if( !empty($plugins) ){
          foreach($plugins as $plugin){
            if(!empty( $plugin['css'] ) ){
              foreach($plugin['css'] as $css){
                echo '<link href="'.$css.'" rel="stylesheet" />';
              }
            }  
          }
        }
      ?>
      <link rel="stylesheet" href="<?= base_url(); ?>assets/css/evelin/evelin.css">
    </head>
    
    <body class="g-sidenav-show   bg-gray-100">
      <div class="min-height-300 position-absolute w-100" style="background-color:#fa830f;"></div>
      
      <!-- v_sidebar.php -->
      <?php $this->load->view('v_sidebar'); ?>

      <main class="main-content position-relative border-radius-lg ">
        
        <!-- Navbar -->
        <!-- v_header.php -->
        <?php $this->load->view('v_header'); ?>

        <!-- End Navbar -->
        <div class="container-fluid py-4">
            
            <!-- content-view -->
            <?php $this->load->view($content_view); ?>

            <!-- v_footer.php -->
            <?php $this->load->view('v_footer'); ?>

        </div>
      </main>
      
      <!--   Core JS Files   -->
      <script src="https://code.jquery.com/jquery-3.5.1.js"></script>
      <script src="<?= base_url(); ?>assets/js/core/popper.min.js"></script>
      <script src="<?= base_url(); ?>assets/js/core/bootstrap.min.js"></script>
      <script src="<?= base_url(); ?>assets/js/plugins/perfect-scrollbar.min.js"></script>
      <script src="<?= base_url(); ?>assets/js/plugins/smooth-scrollbar.min.js"></script>
      <script src="<?= base_url(); ?>assets/js/plugins/chartjs.min.js"></script>
      <script>
        var ctx1 = document.getElementById("chart-line").getContext("2d");
    
        var gradientStroke1 = ctx1.createLinearGradient(0, 230, 0, 50);
    
        gradientStroke1.addColorStop(1, 'rgba(94, 114, 228, 0.2)');
        gradientStroke1.addColorStop(0.2, 'rgba(94, 114, 228, 0.0)');
        gradientStroke1.addColorStop(0, 'rgba(94, 114, 228, 0)');
        new Chart(ctx1, {
          type: "line",
          data: {
            labels: ["Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"],
            datasets: [{
              label: "Mobile apps",
              tension: 0.4,
              borderWidth: 0,
              pointRadius: 0,
              borderColor: "#5e72e4",
              backgroundColor: gradientStroke1,
              borderWidth: 3,
              fill: true,
              data: [50, 40, 300, 220, 500, 250, 400, 230, 500],
              maxBarThickness: 6
    
            }],
          },
          options: {
            responsive: true,
            maintainAspectRatio: false,
            plugins: {
              legend: {
                display: false,
              }
            },
            interaction: {
              intersect: false,
              mode: 'index',
            },
            scales: {
              y: {
                grid: {
                  drawBorder: false,
                  display: true,
                  drawOnChartArea: true,
                  drawTicks: false,
                  borderDash: [5, 5]
                },
                ticks: {
                  display: true,
                  padding: 10,
                  color: '#fbfbfb',
                  font: {
                    size: 11,
                    family: "Open Sans",
                    style: 'normal',
                    lineHeight: 2
                  },
                }
              },
              x: {
                grid: {
                  drawBorder: false,
                  display: false,
                  drawOnChartArea: false,
                  drawTicks: false,
                  borderDash: [5, 5]
                },
                ticks: {
                  display: true,
                  color: '#ccc',
                  padding: 20,
                  font: {
                    size: 11,
                    family: "Open Sans",
                    style: 'normal',
                    lineHeight: 2
                  },
                }
              },
            },
          },
        });
      </script>
      <script>
        var get_url = window.location;
        var base_url = get_url .protocol + "//" + get_url.host + "/" + get_url.pathname.split('/')[1]+'/';
        
        function message_success(title,msg,url=''){
          Swal.fire(
              title,
              msg,
              'success'
          ).then((result) => {
            if( result.isConfirmed ){
              if( url == '' ){
                location.reload();
              }else{
                window.location.href = url;
              }
            }
          });
          
        }

        function message_failed(title,msg,url=''){
          Swal.fire(
              title,
              msg,
              'warning'
          ).then((result) => {
            if( result.isConfirmed ){
              if( url == '' ){
                location.reload();
              }else{
                window.location.href = url;
              }
            }
          });
          
        }

        var win = navigator.platform.indexOf('Win') > -1;
        if (win && document.querySelector('#sidenav-scrollbar')) {
          var options = {
            damping: '0.5'
          }
          Scrollbar.init(document.querySelector('#sidenav-scrollbar'), options);
        }

        var NavWithChild = (function() {

          // Variables

          var $nav = $('.nav-item.nav-with-child');
          setTimeout(function(){
            $nav.each(function(index, each) {

                $(each).on('click',function(event) {
                  if($(each).is('.nav-item-expanded')) {
                    $(each).removeClass('nav-item-expanded')

                  } else {
                      $(each).addClass('nav-item-expanded')
                  }
                })
              });
          },300)

          })();
      </script>
      <!-- Github buttons -->
      <script async defer src="https://buttons.github.io/buttons.js"></script>
      <!-- Control Center for Soft Dashboard: parallax effects, scripts for the example pages etc -->
      <script src="<?= base_url(); ?>assets/js/argon-dashboard.min.js?v=2.0.4"></script>

      <!-- Evelin JS Controller -->
      <?php 
        if( !empty($plugins) ){
          foreach($plugins as $plugin){
            if(!empty( $plugin['js'] ) ){
              foreach($plugin['js'] as $js){
                echo '<script src="'.$js.'"></script>';
              }
            }  
          }
        }
      ?>
      
      <script src="<?= base_url(); ?>assets/js/evelin/evelin.js"></script>

      <!-- Evelin Js -->
      <?php if( !empty($scripts_js) ):
          foreach($scripts_js as $script): ?>
          <script src="<?= base_url().'assets/js/pages/'.$script.'.js'; ?>"></script>
        <?php endforeach; ?>
      <?php endif; ?>
    
    <div class="modalloading"><!-- Place at bottom of page --></div>

    </body>
    
    </html>
<!--
=========================================================
* Argon Dashboard 2 - v2.0.4
=========================================================

* Product Page: https://www.creative-tim.com/product/argon-dashboard
* Copyright 2022 Creative Tim (https://www.creative-tim.com)
* Licensed under MIT (https://www.creative-tim.com/license)
* Coded by Creative Tim

=========================================================

* The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
-->
<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="utf-8" />
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <link rel="apple-touch-icon" sizes="76x76" href="<?= base_url(); ?>assets/img/apple-icon.png">
  <link rel="icon" type="image/png" href="<?= base_url(); ?>assets/img/qllogo.png">
  <title>
    Login Evelin RSU QL
  </title>
  <!--     Fonts and icons     -->
  <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700" rel="stylesheet" />
  <!-- Nucleo Icons -->
  <link href="<?= base_url(); ?>assets/css/nucleo-icons.css" rel="stylesheet" />
  <link href="<?= base_url(); ?>assets/css/nucleo-svg.css" rel="stylesheet" />
  <!-- Font Awesome Icons -->
  <script src="https://kit.fontawesome.com/42d5adcbca.js" crossorigin="anonymous"></script>
  <link href="<?= base_url(); ?>assets/css/nucleo-svg.css" rel="stylesheet" />
  <!-- CSS Files -->
  <link id="pagestyle" href="<?= base_url(); ?>assets/css/argon-dashboard.css?v=2.0.4" rel="stylesheet" />

  <style>
    .bg-gradient-primary {
        background-image: linear-gradient(310deg, #fb6340 0%, #f58536 100%);
    }
  </style>
  
  <link rel="stylesheet" href="<?= base_url(); ?>assets/css/evelin/evelin.css">

</head>

<body class="">

  <main class="main-content  mt-0">
    <section>
      <div class="page-header min-vh-100">
        <div class="container">
          <div class="row">
            <div class="col-xl-4 col-lg-5 col-md-7 d-flex flex-column mx-lg-0 mx-auto">
              <div class="card card-plain">
                <div class="card-header pb-0 text-start">
                  <h4 class="font-weight-bolder">Masuk ke Evelin</h4>
                  <p class="mb-0">Masukan Username dan Password untuk Login</p>
                </div>
                <div class="card-body">
                  
                  <div class="err-msgs"></div>

                  <form role="form" id="frm-login">
                    <input type="hidden" name="<?=$csrf['name'];?>" value="<?=$csrf['hash'];?>">

                    <div class="mb-3">
                      <input type="text" value="<?= isset($_COOKIE['user']) ? $_COOKIE['user'] : ''; ?>" autocomplete="off" class="form-control form-control-lg" name="username" placeholder="Username" aria-label="username">
                    </div>
                    <div class="mb-3">
                      <input type="password" value="<?= isset( $_COOKIE['pass'] ) ?  $_COOKIE['pass'] : ''; ?>" autocomplete="off" class="form-control form-control-lg" name="password" placeholder="Password" aria-label="password">
                    </div>
                    <div class="form-check form-switch">
                      <input class="form-check-input" type="checkbox" id="rememberMe">
                      <label class="form-check-label" for="rememberMe">Remember me</label>
                    </div>
                    <div class="text-center">
                      <button type="submit" class="btn btn-lg btn-warning btn-lg w-100 mt-4 mb-0">Masuk</button>
                    </div>
                  </form>
                </div>
               
              </div>
            </div>
            <div class="col-6 d-lg-flex d-none h-100 my-auto pe-0 position-absolute top-0 end-0 text-center justify-content-center flex-column">
              <div class="position-relative bg-gradient-primary h-100 m-3 px-7 border-radius-lg d-flex flex-column justify-content-center overflow-hidden" style="background-image: url('https://raw.githubusercontent.com/creativetimofficial/public-assets/master/argon-dashboard-pro/assets/img/signin-ill.jpg');
          background-size: cover;">
                <span class="mask bg-gradient-primary opacity-6"></span>
                <h4 class="mt-5 text-white font-weight-bolder position-relative">"Evelin (Electronic Claim Validation)"</h4>
                <p class="text-white position-relative">Fitur dari RSU Queen Latifa tentang Vclaim Validation Berbasis Sistem</p>
              </div>
            </div>
          </div>
        </div>
      </div>
    </section>
  </main>
  <!--   Core JS Files   -->
  <script src="https://code.jquery.com/jquery-3.5.1.js"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-cookie/1.4.1/jquery.cookie.min.js"></script>
  <script src="<?= base_url(); ?>assets/js/core/popper.min.js"></script>
  <script src="<?= base_url(); ?>assets/js/core/bootstrap.min.js"></script>
  <script src="<?= base_url(); ?>assets/js/plugins/perfect-scrollbar.min.js"></script>
  <script src="<?= base_url(); ?>assets/js/plugins/smooth-scrollbar.min.js"></script>
  <script>
    var get_url = window.location;
    var base_url = get_url .protocol + "//" + get_url.host + "/" + get_url.pathname.split('/')[1]+'/';

    var win = navigator.platform.indexOf('Win') > -1;
    if (win && document.querySelector('#sidenav-scrollbar')) {
      var options = {
        damping: '0.5'
      }
      Scrollbar.init(document.querySelector('#sidenav-scrollbar'), options);
    }
  </script>
  <!-- Github buttons -->
  <script async defer src="https://buttons.github.io/buttons.js"></script>
  <!-- Control Center for Soft Dashboard: parallax effects, scripts for the example pages etc -->
  <script src="<?= base_url(); ?>assets/js/argon-dashboard.min.js?v=2.0.4"></script>
  <script src="<?= base_url(); ?>assets/js/evelin/evelin_login.js"></script>
</body>

</html>
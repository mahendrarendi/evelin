<aside class="sidenav bg-white navbar navbar-vertical navbar-expand-xs border-0 border-radius-xl my-3 fixed-start ms-4 " id="sidenav-main" data-color="warning">
    <div class="sidenav-header">
      <i class="fas fa-times p-3 cursor-pointer text-secondary opacity-5 position-absolute end-0 top-0 d-none d-xl-none" aria-hidden="true" id="iconSidenav"></i>
      <a class="navbar-brand m-0" href=" <?= base_url('listvclaim'); ?> ">
        <img src="<?= base_url(); ?>assets/img/qllogo.png" class="navbar-brand-img h-100" alt="main_logo">
        <span class="ms-1 font-weight-bold">EVELIN RSU QL</span>
      </a>
    </div>
    <hr class="horizontal dark mt-0">
    <div class="collapse navbar-collapse  w-auto " id="sidenav-collapse-main">
      <ul class="navbar-nav">
      
      <?php /* ?>
        <li class="nav-item">
          <a class="nav-link <?= $active_menu == 'dashboard' ? 'active' : ''; ?>" href="<?= base_url('dashboard'); ?>">
            <div class="icon icon-shape icon-sm border-radius-md text-center me-2 d-flex align-items-center justify-content-center">
              <i class="ni ni-tv-2 text-primary text-sm opacity-10"></i>
            </div>
            <span class="nav-link-text ms-1">Dashboard</span>
          </a>
        </li>
      <?php */ ?>

        <li class="nav-item">
          <a class="nav-link <?= $active_menu == 'vclaim' ? 'active' : ''; ?>" href="<?= base_url('listvclaim'); ?>">
            <div class="icon icon-shape icon-sm border-radius-md text-center me-2 d-flex align-items-center justify-content-center">
              <i class="ni ni-credit-card text-success text-sm opacity-10"></i>
            </div>
            <span class="nav-link-text ms-1">Vclaim</span>
          </a>
        </li>
        
        <?php if( is_superadmin() || is_klaim() ): ?>
        <li class="nav-item">
          <a class="nav-link <?= $active_menu == 'pengaturan' ? 'active' : ''; ?>" href="<?= base_url('pengaturan'); ?>">
            <div class="icon icon-shape icon-sm border-radius-md text-center me-2 d-flex align-items-center justify-content-center">
              <i class="ni ni-settings text-dark text-sm opacity-10"></i>
            </div>
            <span class="nav-link-text ms-1">Pengaturan</span>
          </a>
        </li>
        <?php endif; ?>
        
        <?php if( is_superadmin() ): ?>
        <li class="nav-item nav-with-child <?= $active_menu == 'users' ? ' nav-item-expanded ' : ''; ?>">
          <a class="nav-link <?= $active_menu == 'users' ? 'active' : ''; ?>">
            <div class="icon icon-shape icon-sm border-radius-md text-center me-2 d-flex align-items-center justify-content-center">
              <i class="ni ni-app text-info text-sm opacity-10"></i>
            </div>
            <span class="nav-link-text ms-1">Pengguna</span>
          </a>
          <ul class="nav-item-child ">
              <li class="nav-item">
                <a class="nav-link <?= $active_sub_menu == 'listuser' ? 'active' : ''; ?>" href="<?= base_url('listuser') ?>">
                <div class="icon icon-shape icon-sm border-radius-md text-center me-2 d-flex align-items-center justify-content-center">
                  <i class="fa fa-check-circle text-warning text-sm opacity-10"></i>
                </div>
                <span class="nav-link-text ms-1">List Pengguna</span>
                </a>
              </li>
              <li class="nav-item">
                <a class="nav-link <?= $active_sub_menu == 'listlevel' ? 'active' : ''; ?>" href="<?= base_url('listlevel'); ?>">
                <div class="icon icon-shape icon-sm border-radius-md text-center me-2 d-flex align-items-center justify-content-center">
                  <i class="fa fa-check-circle text-warning text-sm opacity-10"></i>
                </div>
                  <span class="nav-link-text ms-1">Level</span>
                </a>
              </li>

            </ul>
        </li>     
        <?php endif; ?>  
        <li class="nav-item mt-3">
          <h6 class="ps-4 ms-2 text-uppercase text-xs font-weight-bolder opacity-6">Account pages</h6>
        </li>
        
        <li class="nav-item">
          <a class="nav-link <?= $active_menu == 'login' ? 'active' : ''; ?>" href="<?= base_url('clogin/logout') ?>">
            <div class="icon icon-shape icon-sm border-radius-md text-center me-2 d-flex align-items-center justify-content-center">
              <i class="fa fa-sign-out text-danger text-sm opacity-10"></i>
            </div>
            <span class="nav-link-text ms-1">Keluar</span>
          </a>
        </li>
      </ul>
    </div>

  </aside>
<div class="row">
    <div class="col-md-12">
        <div class="card">
            <div class="card-body">
            <div class="security-check">
                <input type="hidden" class="nonce" name="<?= $csrf['name'] ?>" value="<?= $csrf['hash']; ?>">
            </div>

                <ul class="nav nav-pills mb-3" id="pengaturan-tab" role="tablist">
                    <li class="nav-item" role="presentation">
                        <button class="nav-link active" id="pills-home-tab" data-bs-toggle="pill" data-bs-target="#pills-home" type="button" role="tab" aria-controls="pills-home" aria-selected="true">Pengajuan</button>
                    </li>
                    <li class="nav-item" role="presentation">
                        <button class="nav-link" id="pills-profile-tab" data-bs-toggle="pill" data-bs-target="#pills-profile" type="button" role="tab" aria-controls="pills-profile" aria-selected="false">Tentang</button>
                    </li>
                    <!-- <li class="nav-item" role="presentation">
                        <button class="nav-link" id="pills-contact-tab" data-bs-toggle="pill" data-bs-target="#pills-contact" type="button" role="tab" aria-controls="pills-contact" aria-selected="false">Contact</button>
                    </li> -->
                </ul>
                <div class="tab-content" id="pills-tabContent">
                    <div class="tab-pane fade show active" id="pills-home" role="tabpanel" aria-labelledby="pills-home-tab">
                        <div class="row">
                            <div class="col-md-4">
                                <div class="form-pengajuan">
                                    <form class="form" id="frm-pengajuan">
                                        <div class="form-group">
                                            <label class="small text-normal">Buka Bulan Pengajuan <span class="required">*</span></label>
                                            <input type="text" value="<?= $bulanpengajuan; ?>"  autocomplete="off" class="tglpengajuan form-control" name="bulanpengajuan">
                                        </div>
                                        <div class="form-group">
                                            <div class="form-check form-switch">
                                                <input class="form-check-input" <?= ( empty($aktifpengajuan) ) ? '' : 'checked'; ?> name="aktifkanpengajuan" type="checkbox" role="switch" id="aktifkanpengajuan">
                                                <label class="form-check-label" for="aktifkanpengajuan">Tutup / Buka</label>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <button type="submit" class="btn btn-primary"><i class="fa fa-save"></i> Simpan </button>
                                        </div>
                                    </form>
                                </div>  
                            </div> <!-- end col-md-4 -->
                        </div> <!-- end row -->
                    </div>

                    <div class="tab-pane fade" id="pills-profile" role="tabpanel" aria-labelledby="pills-profile-tab">
                        <p>
                            Pengaturan WEB APPS Evelin RSU QL
                        </p>
                    </div>
                </div>

            </div> <!-- end card-body -->
        </div>
    </div>
</div> 
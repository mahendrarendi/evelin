<div class="row">
    <div class="col-md-12">
        <div class="card">
            <div class="card-header pb-0">
                <h6>Tambah Pengguna</h6>
            </div>
            <div class="card-body">
                <?php 
                $level      = !empty($get_user) ? $get_user->level : '';
                $nama       = !empty($get_user) ? $get_user->nama : '';
                $username   = !empty($get_user) ? $get_user->username : '';
                $email      = !empty($get_user) ? $get_user->email : '';
                ?>
                <div class="err-msgs"></div>

                <form class="form" id="frm-add-pengguna">
                <input type="hidden" name="<?=$csrf['name'];?>" value="<?=$csrf['hash'];?>">

                    <div class="row">
                        <div class="col-md-6">
                            <?php if(!empty($userid)): ?>
                                <input type="hidden" name="hid_userid" value="<?= encrypt_url($userid); ?>" required>
                            <?php endif; ?>
                            <div class="form-group">
                                <label>Level <span class="required">*</span></label>
                                <select name="level" class="form-control qlselect2" required>
                                    <option value="">Pilih Level User</option>
                                    <?php foreach($level_users as $row): 
                                        $selected = ( $row->idlevel == $level ) ? 'selected' : '';
                                    ?>
                                        <option <?= $selected; ?> value="<?= $row->idlevel; ?>"><?= strtoupper($row->level_name); ?></option>
                                    <?php endforeach; ?>
                                </select>
                            </div>
                            <div class="form-group">
                                <label>Nama <span class="required">*</span></label>
                                <input value="<?= $nama; ?>" required type="text" autocomplete="off" placeholder="Nama" class="form-control" name="nama">
                            </div>
                            <div class="form-group">
                                <label>Username <span class="required">*</span></label>
                                <input  value="<?= $username; ?>" required type="text" autocomplete="off" placeholder="Masukan Username Tanpa Spasi" class="form-control" name="username">
                                <small style="font-size:11px;"><em><span class="required">*</span> Catatan : Masukan username tanpa spasi</em></small>
                            </div>
                            <div class="form-group">
                                <label>Email <span class="required">*</span></label>
                                <input  value="<?= $email; ?>" required type="email" autocomplete="off" placeholder="Example@mail.com" class="form-control" name="email">
                            </div>
                            
                        </div>
                        <div class="col-md-6">
                            <?php if( !empty($userid) ): ?>
                            <div class="alert alert-warning text-white">
                                Catatan : Jika Tidak ingin mengganti password Kosongkan Saja
                            </div>
                            <?php endif; ?>
                            <div class="form-group">
                                <label>Password <?= ( !empty($userid) ) ? '' : '<span class="required">*</span>'; ?> </label>
                                <input <?= ( !empty($userid) ) ? '' : 'required'; ?>  type="password" autocomplete="off" placeholder="Password" class="form-control" name="password">
                            </div>
                            <div class="form-group">
                                <label>Konfirmasi Password <?= ( !empty($userid) ) ? '' : '<span class="required">*</span>'; ?></label>
                                <input <?= ( !empty($userid) ) ? '' : 'required'; ?>  type="password" autocomplete="off" placeholder="Konfirmasi Password" class="form-control" name="confirm_password">
                                <div class="err-msg"></div>
                            </div>
                            
                            <div class="form-group">
                                <div class="btn-actionform">
                                    <a href="<?= base_url('listuser'); ?>" class="btn btn-primary"><i class="fa fa-chevron-circle-left"></i> Batal</a>
                                    <button class="btn btn-warning simpan"><i class="fa fa-save"></i> Simpan</button>
                                </div>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
<div class="row">
        <div class="col-12">
          <div class="card mb-4">
            <div class="card-header pb-0">
              <h6>Data Pengguna</h6>
              <div class="qlbutton-action">
                <a class="btn btn-warning"  href="<?= base_url('tambahuser'); ?>"><i class="fa fa-plus-circle"></i> Tambah</a>
              </div>
            </div>
            <div class="card-body">
            
              <div class="p-0">
                <table class="table table-striped table-bordered align-items-center mb-0 qldt">
                  <thead>
                    <tr>
                      <th class="text-uppercase text-secondary text-xxs font-weight-bolder opacity-7 ps-2">Nama</th>
                      <th class="text-uppercase text-secondary text-xxs font-weight-bolder opacity-7 ps-2">Username</th>
                      <th class="text-center text-uppercase text-secondary text-xxs font-weight-bolder opacity-7">Status</th>
                      <th class="text-secondary opacity-7"></th>
                    </tr>
                  </thead>
                  <tbody>
                    <?php foreach($list_users as $row): ?>
                    <tr>
                      <td>
                        <div class="d-flex px-2 py-1">
                          <div>
                            <img src="<?= base_url(); ?>assets/img/usericon.png" class="avatar avatar-sm me-3" alt="user1">
                          </div>
                          <div class="d-flex flex-column justify-content-center">
                            <h6 class="mb-0 text-sm"><?= ucfirst($row->nama); ?></h6>
                            <p class="text-xs text-secondary mb-0"><?= $row->email; ?></p>
                          </div>
                        </div>
                      </td>
                      <td>
                        <p class="text-xs font-weight-bold mb-0"><?= $row->username; ?></p>
                        <p class="text-xs text-secondary mb-0">Gunakan Username Untuk Login</p>
                      </td>
                      <td class="align-middle text-center text-sm">
                        <?php if( $row->status == active() ): ?>
                          <span class="badge badge-sm bg-gradient-success">Online</span>
                        <?php else: ?>
                          <span class="badge badge-sm bg-gradient-secondary">Offline</span>
                        <?php endif; ?>
                      </td>
                      <td class="align-middle">
                        <a href="<?= base_url('tambahuser/'.encrypt_url($row->iduser)); ?>" class="text-secondary font-weight-bold text-xs" data-toggle="tooltip" data-original-title="Edit user">
                          Edit
                        </a>
                        <?php if( $row->status != active() ): ?>
                        |
                        <a href="javascript:;" data-namehash = <?= $csrf['name']; ?> data-hash = <?= $csrf['hash']; ?> data-nama="<?= ucfirst($row->nama); ?>" data-id="<?= encrypt_url($row->iduser); ?>" class="text-danger text-secondary font-weight-bold text-xs delete-user" data-toggle="tooltip" data-original-title="Edit user">
                          <i class="fa fa-trash"></i>
                        </a>
                        <?php endif; ?>
                      </td>
                    </tr>
                    <?php endforeach; ?>
                  </tbody>
                </table>
              </div>
            </div>
          </div>
        </div>
      </div>
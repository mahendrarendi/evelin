<div class="row">
    <div class="col-md-12">
        <div class="card">
            <div class="card-header pb-0">
                <h6>Data Level</h6>
              <div class="qlbutton-action">
                <a class="btn btn-warning"  href="<?= base_url('tambahlevel'); ?>"><i class="fa fa-plus-circle"></i> Tambah</a>
              </div>
            </div>
            <div class="card-body">
            <div class="p-0">
                <table class="table table-striped table-bordered align-items-center mb-0 qldt">
                  <thead>
                    <tr>
                      <th class="text-uppercase text-secondary text-xxs font-weight-bolder  ps-2" width="10%">No</th>
                      <th class="text-uppercase text-secondary text-xxs font-weight-bolder  ps-2">Level</th>
                      <th class="text-center text-uppercase text-secondary text-xxs font-weight-bolder "></th>
                    </tr>
                  </thead>
                  <tbody>
                    <?php 
                    $no = 1;
                    foreach( $level_users  as $level): 
                      $idlevel = encrypt_url( $level->idlevel);
                    ?>
                    <tr>
                      <td>
                        <p class="text-xs text-secondary mb-0"><?= $no++; ?></p>
                      </td>
                      <td>
                        <p class="text-xs font-weight-bold mb-0"><?= $level->level_name; ?></p>
                      </td>
                      <td class="align-middle">
                        <a href="<?= base_url('tambahlevel/'.$idlevel); ?>" class="text-secondary font-weight-bold text-xs" data-toggle="tooltip" data-original-title="Edit Level">
                          Edit
                        </a>
                        |
                        <a href="javascript:;" data-namehash="<?= $csrf['name']; ?>" data-hash="<?= $csrf['hash']; ?>" data-nama="<?= $level->level_name; ?>" data-id="<?= $idlevel; ?>" class="text-danger text-secondary font-weight-bold text-xs delete-level" data-toggle="tooltip" data-original-title="Edit level">
                          <i class="fa fa-trash"></i>
                        </a>
                      </td>
                    </tr>
                    <?php endforeach; ?>
                  </tbody>
                </table>
              </div>
            </div>
        </div>
    </div>
</div>
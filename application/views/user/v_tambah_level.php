<div class="row">
    <div class="col-md-12">
        <div class="card">
            <div class="card-header pb-0">
                <h6>Tambah Level</h6>
            </div>
            <div class="card-body">
                <?php 
                    $level_name = !empty($id_level) ? $get_level->level_name : '';
                ?>
                <div class="err-msgs"></div>

                <form class="form form-inline" id="frm-add-level">
                <input type="hidden" name="<?=$csrf['name'];?>" value="<?=$csrf['hash'];?>">

                    <?php if(!empty($id_level)): ?>
                        <input type="hidden" name="hid-id" value="<?= encrypt_url($id_level); ?>">
                    <?php endif; ?>
                    <div class="form-group">
                        <label>Nama Level <span class="required">*</span></label>
                        <input value="<?= $level_name; ?>" required type="text" autocomplete="off" placeholder="Nama Level" class="form-control" name="nama-level">
                    </div>
                    <div class="form-group">
                        <div class="btn-actionform">
                            <a href="<?= base_url('listlevel'); ?>" class="btn btn-primary"><i class="fa fa-chevron-circle-left"></i> Batal</a>
                            <button class="btn btn-warning simpan"><i class="fa fa-save"></i> Simpan</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
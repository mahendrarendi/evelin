<?php 
function is_superadmin(){

  $CI =& get_instance();

  $CI->load->library('session');

  $CI->load->model('msqlbasic');

  $userid = decrypt_url( $CI->session->userdata('iduser') );

  $sql = "SELECT b.level_name FROM user a 
          LEFT JOIN user_level b ON b.idlevel = a.level 
          WHERE iduser='".$userid."' LIMIT 1";

  $is_superadmin = $CI->msqlbasic->custom_query($sql)[0]->level_name;

  $found = false;
  if( $is_superadmin == 'superadmin' ){
    $found =true;
  }
  
  return $found;

}

function user_login_header(){

  $CI =& get_instance();

  $CI->load->library('session');

  $CI->load->model('msqlbasic');

  $userid = decrypt_url( $CI->session->userdata('iduser') );

  $where  = ['iduser'=>$userid];
  $row    = $CI->msqlbasic->row_db('user',$where);
  $nama   = $row->username;


  $where_level = ['idlevel'=>session_level()];
  $row_level = $CI->msqlbasic->row_db('user_level',$where_level);
  $level  = $row_level->level_name;

  $data = ['level'=>$level,'nama'=>$nama];

  return $data;

}

function session_level()
{
  CI()->load->library('session');
  
  return CI()->session->userdata('level');
}



function CI()
{
  $CI =& get_instance();
  return $CI;
}


function is_poliumum(){

  $CI =& get_instance();

  $CI->load->library('session');

  $CI->load->model('msqlbasic');

  $userid = decrypt_url( $CI->session->userdata('iduser') );

  $where = ['idlevel'=>session_level()];
  $row_level = $CI->msqlbasic->row_db('user_level',$where);
  $is_level  = $row_level->level_name;

  $found = false;
  if( $is_level == 'poliumum' ){
    $found =true;
  }
  
  return $found;

}

function is_pendaftaran(){

  $CI =& get_instance();

  $CI->load->library('session');

  $CI->load->model('msqlbasic');

  $userid = decrypt_url( $CI->session->userdata('iduser') );

  $where = ['idlevel'=>session_level()];
  $row_level = $CI->msqlbasic->row_db('user_level',$where);
  $is_level  = $row_level->level_name;

  $found = false;
  if( $is_level == 'pendaftaran' ){
    $found =true;
  }
  
  return $found;

}

function is_bpjs(){

  $CI =& get_instance();

  $CI->load->library('session');

  $CI->load->model('msqlbasic');

  $userid = decrypt_url( $CI->session->userdata('iduser') );

  $where = ['idlevel'=>session_level()];
  $row_level = $CI->msqlbasic->row_db('user_level',$where);
  $is_level  = $row_level->level_name;

  $found = false;
  if( $is_level == 'bpjs' ){
    $found =true;
  }
  
  return $found;

}

function is_igd(){

  $CI =& get_instance();

  $CI->load->library('session');

  $CI->load->model('msqlbasic');

  $userid = decrypt_url( $CI->session->userdata('iduser') );

  $where = ['idlevel'=>session_level()];
  $row_level = $CI->msqlbasic->row_db('user_level',$where);
  $is_level  = $row_level->level_name;

  $found = false;
  if( $is_level == 'igd' ){
    $found =true;
  }
  
  return $found;

}

function is_klaim(){

  $CI =& get_instance();

  $CI->load->library('session');

  $CI->load->model('msqlbasic');

  $userid = decrypt_url( $CI->session->userdata('iduser') );

  $where = ['idlevel'=>session_level()];
  $row_level = $CI->msqlbasic->row_db('user_level',$where);
  $is_level  = $row_level->level_name;

  $found = false;
  if( $is_level == 'klaim' ){
    $found =true;
  }
  
  return $found;

}

function is_level_user(){

  $CI =& get_instance();

  $CI->load->library('session');

  $CI->load->model('msqlbasic');

  $userid = decrypt_url( $CI->session->userdata('iduser') );

  $sql = "SELECT b.level_name FROM user a 
          LEFT JOIN user_level b ON b.idlevel = a.level 
          WHERE iduser='".$userid."' LIMIT 1";

  $userlevel = $CI->msqlbasic->custom_query($sql)[0]->level_name;
  
  return $userlevel;
}

function akses_jenispemeriksaan($jenispemeriksaan){
  $skdps = ['skdp1','skdp2','skdp3','skdp4','skdp5'];
  if ( in_array($jenispemeriksaan,$skdps) ) {
    $found = 'skdp';
  }else if($jenispemeriksaan == 'rujukanbaru'){
    $found = 'rujukanbaru';
  }else{
    $found = $jenispemeriksaan;
  }
  return $found;
}
?>
<?php
/**
 * format_tanggal
 *
 * @param  string $tgl
 * @param  string $format = Y-m-d
 * @return date/datetime
 */
function format_tanggal($tgl,$format="Y-m-d"){
    $date = date_create($tgl);
    return date_format($date,$format);
}

function format_tanggal_indonesia( $tgl ){
    $bulan  = ql_namabulan( (int)format_tanggal( $tgl,'m' ) );
    $tahun  = format_tanggal( $tgl,'Y' );
    $hari   = format_tanggal( $tgl,'d' );

    $format = $hari.' '.$bulan.' '.$tahun;
    return  $format;
}

function ql_namabulan($bulan){
  $namabulan = ['1'=>'Januari', '2'=>'Februari','3'=>'Maret','4'=>'April','5'=>'Mei','6'=>'Juni','7'=>'Juli','8'=>'Agustus','9'=>'September','10'=>'Oktober','11'=>'November','12'=>'Desember'];
  $result = $namabulan[$bulan];
  return $result;
}

/**
 * waktu_indonesia
 *
 * @return datetime
 */
function waktu_indonesia(){
    date_default_timezone_set('Asia/Jakarta');
    return date('Y-m-d H:i:s');
}

/**
 * set_page_cookie
 *
 * @param  string $key
 * @param  string $value
 * @return return $_COOKIE[$key]
 */
function set_page_cookie($key,$value){
    setcookie($key, $value, time() + (86400 * 30), "/"); // 86400 = 1 day
}

/**
 * active
 *
 * @return return true
 */
function active(){
    return 1;
}

/**
 * non_active
 *
 * @return return false
 */
function non_active(){
    return 0;
}

/**
 * create_key_enscrypt
 *
 * @return return random code
 */
function create_key_enscrypt(){
    $CI =& get_instance();
    $key = bin2hex($CI->encryption->create_key(16));
    return $key;
}

/**
 * encrypt_url
 *
 * @param  string $string
 * @param  bolean $url_safe
 * @return return string
 */
function encrypt_url($string,$url_safe=TRUE){
     
    $CI =& get_instance();
    $CI->load->library('encryption'); 

    $ret = $CI->encryption->encrypt($string);

    if ($url_safe)
        {
            $ret = strtr(
                    $ret,
                    array(
                        '+' => '.',
                        '=' => '-',
                        '/' => '~'
                    )
                );
    }

    
    return $ret;
}

/**
 * decrypt_url
 *
 * @param  string $encrypt_url
 * @return return string
 */
function decrypt_url($encrypt_url){
       
    $CI =& get_instance();
    $CI->load->library('encryption'); 

    $encrypt_url = strtr(
        $encrypt_url,
        array(
            '.' => '+',
            '-' => '=',
            '~' => '/'
        )
    );

    return $CI->encryption->decrypt($encrypt_url);
}

if (!function_exists('get_level_user')){    
    /**
     * get_level_user
     *
     * @return return object
     */
    function get_level_user(){
        $CI =& get_instance();
        $CI->db->select('*');
        $CI->db->from('user_level');
        $query = $CI->db->get()->result();
        return $query;
    }
}

function jenis_kelamin($inisial){
    switch ($inisial) {
        case 'L':
            $jk = 'Laki-Laki';
            break;

        case 'P':
            $jk = 'Perempuan';
            break;
    }

    return $jk;
}

function resep_obat($grupresep,$obat){

    $cetakresep = '';
    $salin = '';

    foreach( $obat as $ob ){
        $resep = $ob;

        
        $grup = $resep['grup'];

        $totaldiresepkan = ( $resep['kekuatanperiksa'] / $resep['kekuatan']) * $resep['jumlahdiresepkan'];
        $resep['grup'] == 0 || $grup !== $resep['grup'] ? ($no = 1) : ($no += 1);
        
        $txtresep = ($resep['grup'] == 0 || $grup !== $resep['grup'] ? 'R/' : '&nbsp;&nbsp;&nbsp;').'  '.
                    $resep['namabarang'] .
                    ', jumlah: ' .
                    ( !empty( $resep['jumlahRacikan'] ) ? round( ($totaldiresepkan / $resep['jumlahRacikan']),3 ) : $resep['jumlahpemakaian']) . ' '.
                    ($resep['grup'] == '0' ? $resep['pemakaian'] == null ? '' : $resep['pemakaian'] : '');

                    // ( !empty( $resep['jumlahRacikan'] ) ? round( ($totaldiresepkan / $resep['jumlahRacikan']),3 ) : $totaldiresepkan) . ' '.
                    // ($resep['grup'] == '0' ? $resep['pemakaian'] == null ? '' : $resep['pemakaian'] : '');
    
        $txtsigna = '&nbsp;&nbsp;&nbsp;&nbsp;' . ($resep['signa'] == null ? '' : $resep['signa']);

        $txtkemasangrup = '&nbsp;&nbsp;&nbsp;&nbsp;<i>' . $resep['kemasan'] . ' dtd no ' . $resep['jumlahRacikan'];

        $txtsignagrup = '&nbsp;&nbsp;&nbsp;&nbsp;' . ($resep['signa'] == null ? '' : $resep['signa']);

        $cetakresep .=
        '<tr><td colspan="2">' .
        $txtresep .
        (($resep['grup'] == 0 || $grup != $resep['grup']) && $salin != ''
            ? "&nbsp;&nbsp;<a id='salinriwayatresep' grup='" .
            $resep['grup'] .
            "' idbp='" .
            $resep['idbarangpemeriksaan'] .
            "' resep='" .
            $txtresep .
            "' signa='" .
            $txtsigna .
            "' kemasangrup='" .
            $txtkemasangrup .
            "' signagrup='" .
            $txtsignagrup .
            "' class='btn btn-primary btn-xs'> Salin Resep</a>"
            : '') .
        ' </td></tr>';


        if ($resep['grup'] == 0) {
        $cetakresep .=
            '<tr>
                <td colspan="2">' .$txtsigna .'</td>
            </tr>';
        }
        

        foreach($grupresep as $gpr){
            if (
                $resep['grup'] != 0 &&
                $gpr['jumlahgrup'] == $no &&
                $resep['grup'] == $gpr['grup']
            ) {
                $cetakresep .= '<tr><td colspan="2">' .$txtkemasangrup.'</i></td></tr>';
                $cetakresep .='<tr><td colspan="2">' . $txtsignagrup .'</td></tr>';
            }
        }

    }

    return $cetakresep;

}

function convertToRupiah($angka){
    return number_format((float) $angka,0,',','.');
}

function hapus3digitkoma($angka){
    return $angka;
}

function convertToRupiahBulat($angka){
    return  convertToRupiah( ceil($angka / 100) * 100 );
}

function bulatkanLimaRatusan($angka){
    return ceil($angka / 500) * 500;
}

function convert_imageto_base64($url_image){
    $path = $url_image;// Modify this part (your_img.png
    $type = pathinfo($path, PATHINFO_EXTENSION);
    $data = file_get_contents($path);
    $base64 = 'data:image/' . $type . ';base64,' . base64_encode($data); 
    return $base64;
}

function convert_to_qrcode($string_url,$ukuran="80x80"){
    #generate QR Code
    $chs = urlencode( $ukuran );
    $cht = 'qr';
    $chl = urlencode( $string_url );
    $link_qrcode = "https://chart.googleapis.com/chart?chs=".$chs."&cht=".$cht."&chl=".$chl."&choe=UTF-8";
    $html_qrcode = '<img src="'.$link_qrcode.'" title="Tanda Tangan Dokter" />';
    return $html_qrcode;
}

function convert_to_qrcode_pdf($string_url,$ukuran="80x80"){
    #generate QR Code
    $chs = urlencode( $ukuran );
    $cht = 'qr';
    $chl = urlencode( $string_url );
    $link_qrcode = "https://chart.googleapis.com/chart?chs=".$chs."&cht=".$cht."&chl=".$chl."&choe=UTF-8";
    // $html_qrcode = '<img src="'.convert_imageto_base64($link_qrcode).'" title="Tanda Tangan Dokter" />';
    // return $html_qrcode;
    return $link_qrcode;
}

function html_switch_tampil($row_istampil=""){
    ob_start(); 
    if( is_superadmin() || is_klaim() ):
        $label_skinap = 'Sembunyikan File';
        switch ($row_istampil) {
            case '1':
            $label_skinap = 'Tampilkan';
                break;
            case '0':
            $label_skinap = 'Tidak Tampilkan';
                break;
        }
    ?>
        <div class="form-group">
            <div class="form-check form-switch" style="justify-content: flex-end !important;">
                <input class="form-check-input" <?= ( $row_istampil ) ? 'checked' : ''; ?> name="is_tampil" type="checkbox" role="switch" id="sembunyikanFile">
                <label class="form-check-label" for="sembunyikanFile"><?= $label_skinap; ?></label>
            </div>
        </div>
    <?php
    endif;
    return ob_get_clean();
}

function modal_template(){
    ob_start(); ?>

    <!-- Modal -->
    <div id="modalEvelin" class="modal fade" data-bs-backdrop="static" data-bs-keyboard="false" tabindex="-1" aria-labelledby="staticBackdropLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
        <div class="modal-header">
            <h5 class="modal-title" id="exampleModalLongTitle">Modal title</h5>
            
        </div>
        <div class="modal-body">
            ...
        </div>
        <div class="modal-footer">
            <button type="button" class="btn btn-secondary modalClose" data-dismiss="modal">Close</button>
        </div>
        </div>
    </div>
    </div>
    <?php return ob_get_clean();
}

function cek_isi_folder_pdf($tglPeriksa,$jnsPeriksa){
    $tglperiksa       = format_tanggal(str_replace('/','-',$tglPeriksa),"d-m-Y") ;
    $jenispemeriksaan = $jnsPeriksa;

        
    $path               = FCPATH . 'assets/pdf';
    $path_jns           = $path.'/'.$jenispemeriksaan;
    $path_tglperiksa    = $path_jns.'/'.$tglperiksa;

    $files = [];
    if(is_dir($path_tglperiksa)){

        $open = opendir( $path_tglperiksa ) or die('Folder tidak ditemukan ...!');
        
        while ( ($file = readdir($open)) !== FALSE ) {
            if($file !='.' && $file !='..'){   
                $files[]=$file;
            }
        }
        
        if (($key = array_search('.pdf', $files)) !== false) {
            unset($files[$key]);
        }
    }

    return $files;
}


function delete_older_than($dir, $max_age) {
    $list = array();
    
    $limit = time() - $max_age;

    $found = false;
    if ( is_dir($dir) ){
        if (filemtime($dir) < $limit) {
            deleteDirectory($dir);
            $found = true; 
        }
        $found = true; 
    }
    
    return $found;
  
  }
function deleteDirectory($dir) {
    system('rm -rf -- ' . escapeshellarg($dir), $retval);
    return $retval == 0; // UNIX commands return zero on success
}

function html_checkbox_validasi($cek_validasi=false){
    ob_start(); ?>

    <div class="form-check validasi-klaim-group">
        <label class="form-check-label">
        <input class="form-check-input" name="validasi_klaim" type="checkbox" <?= ( $cek_validasi ) ? 'checked' : '' ; ?>> Validasi
        </label>
    </div>

    <?php 
    return ob_get_clean();
}
?>
<?php
defined('BASEPATH') OR exit('No direct script access allowed');
defined('EVELINQL') OR define('EVELINQL',4);

class Qlwebapi
{
    private $Xconsid;//username secret
    private $Xconssecret;//password secret
    private $response;
    private $jenisaplikasi;
    private $jeniskoneksi;
    private $url;
    
    /**
     * Set Client Password
     */
    private function userpassword()
    {
        switch ($this->jenisaplikasi)
            {
                case EVELINQL:
                    $this->Xconsid      = 'QL0179R014';
                    $this->Xconssecret  = 'evelinQL';
                    $this->jeniskoneksi = 'EVELINQLYK';
                    break;
            }   
    }


    
    function createSignature($requestParameter, $uploadedJSON = '', $method = 'POST')
    {
        $this->userpassword();
        
        
        switch ($this->jenisaplikasi){
            case EVELINQL: $this->url = 'http://172.16.200.2/sirstql/api/webevelinservertest'; break;
            // case EVELINQL: $this->url = 'http://172.16.200.2/sirstql/api/webevelin'; break;

            // case APPSURVEY:  $this->url = 'http://203.30.236.119/sirstql/api/wsrsuql'; break;
            // case APMQLRALAN:  $this->url = 'http://203.30.236.119/sirstql/api/wsapmrsuql'; break;
            // case APPANTRIAN: $this->url = 'http://203.30.236.119/sirstql/api/wsrsuql'; break;
                
            // case APPSURVEY:  $this->url = 'https://sirstql.queenlatifa.co.id/sirstql/api/wsrsuql'; break;
            // case APMQLRALAN:  $this->url = 'https://sirstql.queenlatifa.co.id/sirstql/api/wsapmrsuql'; break;
            // case APPANTRIAN: $this->url = 'https://sirstql.queenlatifa.co.id/sirstql/api/wsrsuql'; break;
        }
        
        //menghitung timestamp
        date_default_timezone_set('UTC');
        $tStamp = strval(time()-strtotime('1970-01-01 00:00:00'));
        //menghitung tanda tangan dengan melakukan hash terhadap salt dengan kunci rahasia sebagai kunci
        $signature = base64_encode(hash_hmac('sha256', $this->Xconsid."&".$tStamp, $this->Xconssecret, true));
        $headers = array("Accept: application/json", "X-cons-id: ".$this->Xconsid,"X-timestamp: ".$tStamp,"X-signature: ".$signature,"X-jeniskoneksi:".$this->jeniskoneksi,"Content-Type: application/json");
        
        // echo '<pre>';
        // print_r( $signature );
        // print_r( $headers );
        // echo '</pre>';
        // die();
        
        $ch = curl_init($this->url.$requestParameter);
            if (empty(!$uploadedJSON)) 
            {
                curl_setopt($ch, CURLOPT_POST, 1);
                curl_setopt($ch, CURLOPT_POSTFIELDS, $uploadedJSON);
            }
            curl_setopt($ch, CURLOPT_TIMEOUT, 30);
            curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 10);
            curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
            curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        $data = curl_exec($ch);

        if (empty($data) or $data == "null")
        {
            $data = curl_error($ch);
        }
        curl_close($ch); 
        return json_decode($data);
    }


    function api_vclaim_list()
    {
        $this->jenisaplikasi    = EVELINQL;

        $tglperiksa     = isset($_GET['tanggal']) ? format_tanggal(str_replace("/","-",$_GET['tanggal'])) : date('Y-m-d');
        $jns_layanan    = isset($_GET['jenis-layanan']) ? $_GET['jenis-layanan'] : 'rajal';
                
        $endpoint       = '/vclaimlist';
        $uploadedJSON   = json_encode( ['tglperiksa'=>$tglperiksa,'jnslayanan'=>$jns_layanan] );

        $this->response = json_decode(
            json_encode(
                $this->createSignature($endpoint,$uploadedJSON,'POST')
            ),
            true
        );
        
        return $this->response['metadata'];
    }

    function api_vclaim_list_selesai()
    {
        $this->jenisaplikasi    = EVELINQL;

        $tglperiksa     = isset($_GET['tanggal']) ? format_tanggal(str_replace("/","-",$_GET['tanggal'])) : date('Y-m-d');
        $jns_layanan    = isset($_GET['jenis-layanan']) ? $_GET['jenis-layanan'] : 'rajal';
                
        $endpoint       = '/vclaimlistselesai';
        // $endpoint       = '/testvclaimlistselesai';

        $uploadedJSON   = json_encode( ['tglperiksa'=>$tglperiksa,'jnslayanan'=>$jns_layanan] );

        $this->response = json_decode(
            json_encode(
                $this->createSignature($endpoint,$uploadedJSON,'POST')
            ),
            true
        );
        
        return $this->response['metadata'];
    }

    function test_api_vclaim_list_selesai()
    {
        $this->jenisaplikasi    = EVELINQL;

        $tglperiksa     = isset($_GET['tanggal']) ? format_tanggal(str_replace("/","-",$_GET['tanggal'])) : date('Y-m-d');
        $jns_layanan    = isset($_GET['jenis-layanan']) ? $_GET['jenis-layanan'] : 'rajal';
                
        // $endpoint       = '/vclaimlistselesai';
        $endpoint       = '/testnew_vclaimlistselesai';

        $uploadedJSON   = json_encode( ['tglperiksa'=>$tglperiksa,'jnslayanan'=>$jns_layanan] );

        $this->response = json_decode(
            json_encode(
                $this->createSignature($endpoint,$uploadedJSON,'POST')
            ),
            true
        );
        
        return $this->response['metadata'];
    }

    

    function api_vclaim_list_selesai_ajax($tglperiksa,$jns_layanan)
    {
        $this->jenisaplikasi    = EVELINQL;
                
        $endpoint       = '/vclaimlistselesai';

        $uploadedJSON   = json_encode( ['tglperiksa'=>$tglperiksa,'jnslayanan'=>$jns_layanan] );

        $this->response = json_decode(
            json_encode(
                $this->createSignature($endpoint,$uploadedJSON,'POST')
            ),
            true
        );
        
        return $this->response['metadata'];
    }


    function api_vclaim_sep($norm,$tglperiksa,$idunit="")
    {
        $this->jenisaplikasi    = EVELINQL;
        $endpoint       = '/vclaimsep';
        $uploadedJSON   = json_encode( ['norm'=>$norm,'tglperiksa'=>$tglperiksa,'idunit'=>$idunit] );

        $this->response = json_decode(
            json_encode(
                $this->createSignature($endpoint,$uploadedJSON,'POST')
            ),
            true
        );
        return $this->response;
    }

    function api_vclaim_resume($norm,$tglperiksa,$idunit="")
    {
        $this->jenisaplikasi    = EVELINQL;
        $endpoint       = '/vclaimresume';
        $uploadedJSON   = json_encode( ['norm'=>$norm,'tglperiksa'=>$tglperiksa,'idunit'=>$idunit] );

        $this->response = json_decode(
            json_encode(
                $this->createSignature($endpoint,$uploadedJSON,'POST')
            ),
            true
        );
        return $this->response;
    }

    function api_vclaim_rencanakontrol($norm,$tglperiksa,$idunit="")
    {
        $this->jenisaplikasi    = EVELINQL;
        $endpoint               = '/vclaimrencanakontrol';
        $uploadedJSON           = json_encode( ['norm'=>$norm,'tglperiksa'=>$tglperiksa,'idunit'=>$idunit] );

        $this->response = json_decode(
            json_encode(
                $this->createSignature($endpoint,$uploadedJSON,'POST')
            ),
            true
        );
        return $this->response;
    }

    function api_vclaim_radiologi($norm,$tglperiksa,$idunit="")
    {
        $this->jenisaplikasi    = EVELINQL;
        $endpoint               = '/vclaimradiologi';
        $uploadedJSON           = json_encode( ['norm'=>$norm,'tglperiksa'=>$tglperiksa,'idunit'=>$idunit] );

        $this->response = json_decode(
            json_encode(
                $this->createSignature($endpoint,$uploadedJSON,'POST')
            ),
            true
        );
        return $this->response;
    }

    function api_vclaim_nota( $norm,$tglperiksa,$idunit="" )
    {
        $this->jenisaplikasi = EVELINQL;
        $endpoint = '/vclaimnota';
        $uploadedJSON = json_encode( ['norm'=>$norm,'tglperiksa'=>$tglperiksa,'idunit'=>$idunit] );

        $this->response = json_decode(
            json_encode(
                $this->createSignature($endpoint,$uploadedJSON,'POST')
            ),
            true
        );
        return $this->response;
    }

    function api_vclaim_poli()
    {
        $this->jenisaplikasi = EVELINQL;
        $endpoint = '/vclaimdatapoli';
        $uploadedJSON = json_encode( ['unit'=>'/'] );

        $this->response = json_decode(
            json_encode(
                $this->createSignature($endpoint,$uploadedJSON,'POST')
            ),
            true
        );
        return $this->response;
    }

    function api_vclaim_suratkontrol( $norm,$tglperiksa )
    {
        $this->jenisaplikasi = EVELINQL;
        $endpoint = '/vclaimresumesuratkontrol';
        $uploadedJSON =  json_encode( ['norm'=>$norm,'tglperiksa'=>$tglperiksa] );

        $this->response = json_decode(
            json_encode(
                $this->createSignature($endpoint,$uploadedJSON,'POST')
            ),
            true
        );
        return $this->response;
        
    }

    function api_vclaim_tanggal_rekon($nomerrekon)
    {
        $this->jenisaplikasi    = EVELINQL;

        $endpoint  = '/vclaim_tanggalrekon';

        $uploadedJSON   = json_encode( ['nomerrekon'=>$nomerrekon] );

        $this->response = json_decode(
            json_encode(
                $this->createSignature($endpoint,$uploadedJSON,'POST')
            ),
            true
        );
        
        return $this->response['metadata'];
    }

    function test_api_vclaim_suratkontrol( $norm,$tglperiksa )
    {
        $this->jenisaplikasi = EVELINQL;
        $endpoint = '/testvclaimresumesuratkontrol';
        $uploadedJSON =  json_encode( ['norm'=>$norm,'tglperiksa'=>$tglperiksa] );

        $this->response = json_decode(
            json_encode(
                $this->createSignature($endpoint,$uploadedJSON,'POST')
            ),
            true
        );
        return $this->response;
        
    }

    // function test_api_nota( $norm,$tglperiksa,$idunit="" )
    function test_api_nota()
    {
        $norujukan = '0179R0141222V002297';

        $this->jenisaplikasi = EVELINQL;
        $endpoint = '/testnota';

        // $uploadedJSON =  json_encode( ['norm'=>$norm,'tglperiksa'=>$tglperiksa,'idunit'=>$idunit] );
        $uploadedJSON =  json_encode( ['norujukan'=>$norujukan] );

        $this->response = json_decode(
            json_encode(
                $this->createSignature($endpoint,$uploadedJSON,'POST')
            ),
            true
        );
        return $this->response;
        
    }
    
}
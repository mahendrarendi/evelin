<?php
class Mauth extends CI_Model
{
    protected $tb_users = 'user';
    const SESSION_KEY   = 'iduser';

    function current_user()
	{
		if (!$this->session->has_userdata(self::SESSION_KEY)) {
			return null;
		}

		$user_id = $this->session->userdata(self::SESSION_KEY);
		$query = $this->db->get_where($this->tb_users, ['iduser' => decrypt_url($user_id)]);
		return $query->row();
	}

    function update_last_login($iduser)
	{
		$data = [
			'last_login' => waktu_indonesia(),
			'status' => active(),
		];
        return $this->db->update($this->tb_users, $data, ['iduser' => $iduser]);
	}

    function logout()
	{
        $user_id = $this->session->userdata(self::SESSION_KEY);
        $this->db->update($this->tb_users, ['status'=>non_active()], ['iduser' => decrypt_url($user_id)]);

		$this->session->unset_userdata(self::SESSION_KEY);
		return !$this->session->has_userdata(self::SESSION_KEY);
	}
}
?>
<?php
class Msqlbasic extends CI_Model
{

    function show_db($nama_table,$where=array()){

        $this->db->select('*');
        $this->db->from($nama_table);
        $this->db->where($where);
        return $query = $this->db->get()->result();
    }

    function row_db($nama_table,$where=array())
    {
        $this->db->select('*');
        $this->db->from($nama_table);
        $this->db->where($where);
        return $query = $this->db->get()->row();
    }

    function add_db($nama_table,$data=array())
    {
        $this->db->insert($nama_table, $data);
        $insert_id = $this->db->insert_id();
        return  $insert_id;
    }

    function update_db($nama_table,$where=array(),$data=array())
    {        
        $this->db->where($where);
        $this->db->update($nama_table, $data);
        return $this->db->affected_rows();
    }

    function delete_db($nama_table,$where=array())
    {
        $this->db->where($where);
        $this->db->delete($nama_table);
        return $this->db->affected_rows();
    }

    function custom_query($sql)
    {
        $query = $this->db->query($sql);
        return $query->result();
    }
}

?>
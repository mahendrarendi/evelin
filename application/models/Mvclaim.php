<?php 
class Mvclaim extends CI_Model
{
    protected $tb_sk_personpertimbangan = 'sk_person_pertimbangan';
    protected $tb_sk_listpertimbangan   = 'sk_list_pertimbangan';
    protected $tb_sk_formedit           = 'sk_formedit';
    protected $tb_sk_inap               = 'sk_inap';
    protected $tb_sk_emergency          = 'sk_emergency';
    protected $tb_inacbg                = 'inacbg';
    protected $tb_rujukanbaru           = 'rujukanbaru';
    protected $tb_radiologi             = 'radiologi';
    protected $tb_sep                   = 'sep';
    protected $tb_rencanakontrol        = 'rencanakontrol';
    protected $tb_dokumenlainlain       = 'dokumenlainlain';
    protected $tb_resumegabung          = 'resume_gabung';
    protected $tb_radiologigabung       = 'radiologi_gabung';
    protected $tb_billinggabung         = 'billing_gabung';
    protected $tb_testpdf_convert       = 'testpdf_convert';
    protected $tb_sep_new               = 'sep_new';
    protected $tb_rencanakontrol_new    = 'rencanakontrol_new';
    protected $tb_verif_klaim           = 'verif_klaim';


    public function insert_personpertimbangan()
    {
        $post        = $this->input->post();
        $item        = $post['value'];
        $norm        = $this->security->sanitize_filename($post['norm']);
        $tglperiksa  = format_tanggal(str_replace('/','-',$post['tglperiksa']));
        
        #cek data last insert
        $cek = $this->cek_personpertimbangan($norm,$tglperiksa);

        if( empty( $cek ) ){
            $data   = [
                'datecreated'=>waktu_indonesia(),
                'norm'=>$norm,
                'tglperiksa'=>$tglperiksa
            ]; 
            
            $this->db->insert($this->tb_sk_personpertimbangan, $data);
            $insert_id = $this->db->insert_id();
        }else{
            $insert_id = $cek->idpersonpertimbangan;
        }

        $data_list = [
            'idpertimbangan' => $insert_id,
            'item' => $item,
        ];

        $this->insert_listpertimbangan($data_list);

        return  $insert_id;
    }

    public function cek_personpertimbangan($norm,$tglperiksa)
    {
        $this->db->select('*');
        $this->db->from($this->tb_sk_personpertimbangan);
        $this->db->where('norm',$norm);
        $this->db->where('tglperiksa',$tglperiksa);
        return $this->db->get()->row();
    }
    
    public function insert_listpertimbangan($data_list)
    {        
        $this->db->insert($this->tb_sk_listpertimbangan, $data_list);
        $insert_id = $this->db->insert_id();
        return  $insert_id;
    }
    
    public function loop_listpertimbangan($params){
        $this->db->select('*');
        $this->db->from($this->tb_sk_personpertimbangan.' a ');
        $this->db->join($this->tb_sk_listpertimbangan.' b ', 'a.idpersonpertimbangan = b.idpertimbangan', 'left');
        if( !empty($params['norm']) ){
            $this->db->where('a.norm',$params['norm']);
            $this->db->where('a.tglperiksa',$params['tglperiksa']);
        }
        return $query = $this->db->get()->result();
    }

    public function delete_pertimbangan(){
        $post            = $this->input->post();
        $idpertimbangan  = $this->security->sanitize_filename($post['idpertimbangan']);
        $idlist          = $this->security->sanitize_filename($post['idlist']);

        $ceklis = $this->db->select('*')
                  ->from($this->tb_sk_listpertimbangan)
                  ->where('idpertimbangan',$idpertimbangan)
                  ->get()->result_array();

        if( count($ceklis) == '1' ){
            $this->db->where('idpersonpertimbangan',$idpertimbangan);
            $delete_pertimbangan = $this->db->delete($this->tb_sk_personpertimbangan);          
        }
            
        $this->db->where('idlistpertimbangan',$idlist);
        return $this->db->delete($this->tb_sk_listpertimbangan);
        
    }

    public function insert_sk_formedit()
    {
        $post        = $this->input->post();
        $tglperiksa  =  format_tanggal( str_replace('/','-', $post['tglperiksa']) ); 
        $norm        = $post['norm']; 
        $val         = $post['val']; 
        $name_elemen = $post['name_elemen']; 

        $idunit     = $post['idunit'];

        $cek = $this->db->select('*')
                ->from( $this->tb_sk_formedit )
                ->where('norm',$norm)
                ->where('tglperiksa',$tglperiksa)
                ->where('idunit',$idunit)
                ->get()->row();
        
        if( empty($cek) ){
            $cek = $this->db->select('*')
                ->from( $this->tb_sk_formedit )
                ->where('norm',$norm)
                ->where('tglperiksa',$tglperiksa)
                ->where('idunit',0)
                ->get()->row();
        }
                
        $data = [$name_elemen => $val];

        if( empty($cek) ){
            $data['norm'] = $norm;
            $data['tglperiksa'] = $tglperiksa;
            $data['idunit'] = $idunit;
            $this->db->insert($this->tb_sk_formedit,$data);
            $query = $this->db->insert_id();
        }else{
            $data['idunit'] = $idunit;
            $this->db->where('norm',$norm);
            $this->db->where('tglperiksa',$tglperiksa);
            $this->db->update($this->tb_sk_formedit,$data);
            $query =  $this->db->affected_rows();
        }

        return $query;

    }

    public function insert_suratkontrolinap()
    {
        $post        = $this->input->post();
        $tglperiksa  =  format_tanggal( str_replace('/','-', $post['tglperiksa']) ); 
        $norm        = $post['norm']; 
        $val         = $post['val']; 
        $name_elemen = $post['name_elemen']; 
        $idunit      = $post['idunit'];

        $cek = $this->db->select('*')
                ->from( $this->tb_sk_inap )
                ->where('norm',$norm)
                ->where('tglperiksa',$tglperiksa)
                ->where('idunit',$idunit)
                ->get()->row();

        if( empty($cek) ){
            $cek = $this->db->select('*')
                ->from( $this->tb_sk_inap )
                ->where('norm',$norm)
                ->where('tglperiksa',$tglperiksa)
                ->where('idunit',0)
                ->get()->row();
        }
        
        $data = [$name_elemen => $val];

        if( empty($cek) ){
            $data['norm'] = $norm;
            $data['tglperiksa'] = $tglperiksa;
            $data['idunit'] = $idunit;
            $this->db->insert($this->tb_sk_inap,$data);
            $query = $this->db->insert_id();
        }else{
            $data['idunit'] = $idunit;
            $this->db->where('norm',$norm);
            $this->db->where('tglperiksa',$tglperiksa);
            $this->db->update($this->tb_sk_inap,$data);
            $query =  $this->db->affected_rows();
        }

        return $query;

    }

    public function insert_rencanakontrol()
    {
        $post        = $this->input->post();
        $tglperiksa  =  format_tanggal( str_replace('/','-', $post['tglperiksa']) ); 
        $norm        = $post['norm']; 
        $val         = $post['val']; 
        $name_elemen = $post['name_elemen']; 
        $idunit      = $post['idunit'];

        $cek = $this->db->select('*')
                ->from( $this->tb_rencanakontrol )
                ->where('norm',$norm)
                ->where('tglperiksa',$tglperiksa)
                ->where('idunit',$idunit)
                ->get()->row();

        if( empty($cek) ){
            $cek = $this->db->select('*')
                ->from( $this->tb_rencanakontrol )
                ->where('norm',$norm)
                ->where('tglperiksa',$tglperiksa)
                ->where('idunit',0)
                ->get()->row();
        }
        
        $data = [$name_elemen => $val];

        if( empty($cek) ){
            $data['norm'] = $norm;
            $data['tglperiksa'] = $tglperiksa;
            $data['idunit'] = $idunit;
            $this->db->insert($this->tb_rencanakontrol,$data);
            $query = $this->db->insert_id();
        }else{
            $data['idunit'] = $idunit;
            $this->db->where('norm',$norm);
            $this->db->where('tglperiksa',$tglperiksa);
            $this->db->update($this->tb_rencanakontrol,$data);
            $query =  $this->db->affected_rows();
        }

        return $query;

    }

    public function insert_rencanakontrol_istampil()
    {
        $post        = $this->input->post();
        $tglperiksa  =  format_tanggal( str_replace('/','-', $post['tglperiksa']) ); 
        $norm        = $post['norm']; 
        $val         = $post['val']; 
        $name_elemen = $post['name_elemen']; 
        $idunit      = $post['idunit'];

        $cek = $this->db->select('*')
                ->from( $this->tb_rencanakontrol_new )
                ->where('norm',$norm)
                ->where('tglperiksa',$tglperiksa)
                ->where('idunit',$idunit)
                ->get()->row();

        if( empty($cek) ){
            $cek = $this->db->select('*')
                ->from( $this->tb_rencanakontrol_new )
                ->where('norm',$norm)
                ->where('tglperiksa',$tglperiksa)
                ->where('idunit',0)
                ->get()->row();
        }
        
        $data = [$name_elemen => $val];

        if( empty($cek) ){
            $data['norm'] = $norm;
            $data['tglperiksa'] = $tglperiksa;
            $data['idunit'] = $idunit;
            $this->db->insert($this->tb_rencanakontrol_new,$data);
            $query = $this->db->insert_id();
        }else{
            $data['idunit'] = $idunit;
            $this->db->where('norm',$norm);
            $this->db->where('tglperiksa',$tglperiksa);
            $this->db->update($this->tb_rencanakontrol_new,$data);
            $query =  $this->db->affected_rows();
        }

        return $query;

    }

    public function insert_rujukanbaru_showhide()
    {
        $post        = $this->input->post();
        $tglperiksa  =  format_tanggal( str_replace('/','-', $post['tglperiksa']) ); 
        $norm        = $post['norm']; 
        $val         = $post['val']; 
        $name_elemen = $post['name_elemen'];
        $idunit      = $post['idunit']; 

        $cek = $this->db->select('*')
                ->from( $this->tb_rujukanbaru )
                ->where('norm',$norm)
                ->where('tglperiksa',$tglperiksa)
                ->where('idunit',$idunit)
                ->get()->row();
        
        if( empty($cek) ){
            $cek = $this->db->select('*')
                ->from( $this->tb_rujukanbaru )
                ->where('norm',$norm)
                ->where('tglperiksa',$tglperiksa)
                ->where('idunit',0)
                ->get()->row();
        }
        
        $data = [$name_elemen => $val];

        if( empty($cek) ){
            $data['norm'] = $norm;
            $data['tglperiksa'] = $tglperiksa;
            $data['idunit'] = $idunit;
            $this->db->insert($this->tb_rujukanbaru,$data);
            $query = $this->db->insert_id();
        }else{
            $data['idunit'] = $idunit;
            $this->db->where('norm',$norm);
            $this->db->where('tglperiksa',$tglperiksa);
            $this->db->update($this->tb_rujukanbaru,$data);
            $query =  $this->db->affected_rows();
        }

        return $query;

    }

    public function insert_skdp_showhide()
    {
        $post        = $this->input->post();
        $tglperiksa  =  format_tanggal( str_replace('/','-', $post['tglperiksa']) ); 
        $norm        = $post['norm']; 
        $val         = $post['val']; 
        $name_elemen = $post['name_elemen']; 
        $idunit      = $post['idunit'];

        $cek = $this->db->select('*')
                ->from( $this->tb_sk_formedit )
                ->where('norm',$norm)
                ->where('tglperiksa',$tglperiksa)
                ->where('idunit',$idunit)
                ->get()->row();
        if( empty($cek) ){
            $cek = $this->db->select('*')
                ->from( $this->tb_sk_formedit )
                ->where('norm',$norm)
                ->where('tglperiksa',$tglperiksa)
                ->where('idunit',0)
                ->get()->row();
        }

        $data = [$name_elemen => $val];

        if( empty($cek) ){
            $data['norm'] = $norm;
            $data['tglperiksa'] = $tglperiksa;
            $data['idunit'] = $idunit;
            $this->db->insert($this->tb_sk_formedit,$data);
            $query = $this->db->insert_id();
        }else{
            $data['idunit'] = $idunit;
            $this->db->where('norm',$norm);
            $this->db->where('tglperiksa',$tglperiksa);
            $this->db->update($this->tb_sk_formedit,$data);
            $query =  $this->db->affected_rows();
        }

        return $query;

    }

    public function insert_emergency()
    {
        $post        = $this->input->post();
        $tglperiksa  =  format_tanggal( str_replace('/','-', $post['tglperiksa']) ); 
        $norm        = $post['norm']; 
        $val         = $post['val']; 
        $name_elemen = $post['name_elemen'];
        $idunit      = $post['idunit'];

        $cek = $this->db->select('*')
                ->from( $this->tb_sk_emergency )
                ->where('norm',$norm)
                ->where('tglperiksa',$tglperiksa)
                ->where('idunit',$idunit)
                ->get()->row();

        if(empty($cek)){
            $cek = $this->db->select('*')
                ->from( $this->tb_sk_emergency )
                ->where('norm',$norm)
                ->where('tglperiksa',$tglperiksa)
                ->where('idunit',0)
                ->get()->row();
        }

        $data = [$name_elemen => $val];

        if( empty($cek) ){
            $data['norm'] = $norm;
            $data['tglperiksa'] = $tglperiksa;
            $data['idunit'] = $idunit;
            $this->db->insert($this->tb_sk_emergency,$data);
            $query = $this->db->insert_id();
        }else{
            $data['idunit'] = $idunit;
           
            $this->db->where('norm',$norm);
            $this->db->where('tglperiksa',$tglperiksa);
            $this->db->update($this->tb_sk_emergency,$data);
            $query =  $this->db->affected_rows();
        }

        return $query;

    }

    public function insert_radiologi()
    {
        $post        = $this->input->post();
        $tglperiksa  =  format_tanggal( str_replace('/','-', $post['tglperiksa']) ); 
        $norm        = $post['norm']; 
        $val         = $post['val']; 
        $name_elemen = $post['name_elemen'];
        $idunit      = $post['idunit'];

        $cek = $this->db->select('*')
                ->from( $this->tb_radiologi )
                ->where('norm',$norm)
                ->where('tglperiksa',$tglperiksa)
                ->where('idunit',$idunit)
                ->get()->row();
        
        if( empty($cek) ){
            $cek = $this->db->select('*')
            ->from( $this->tb_radiologi )
            ->where('norm',$norm)
            ->where('tglperiksa',$tglperiksa)
            ->where('idunit',0)
            ->get()->row();
        }

        $data = [$name_elemen => $val];

        if( empty($cek) ){
            $data['norm'] = $norm;
            $data['tglperiksa'] = $tglperiksa;
            $data['idunit']     = $idunit;
            $this->db->insert($this->tb_radiologi,$data);
            $query = $this->db->insert_id();
        }else{
            $data['idunit']     = $idunit;
            $this->db->where('norm',$norm);
            $this->db->where('tglperiksa',$tglperiksa);
            $this->db->update($this->tb_radiologi,$data);
            $query =  $this->db->affected_rows();
        }

        return $query;

    }

    public function tampil_skformedit($params)
    {
        $query = $this->db->select('*')
                ->from( $this->tb_sk_formedit )
                ->where('norm',$params['norm'])
                ->where('tglperiksa',$params['tglperiksa'])
                ->get()->row();

        return $query;
    }

    public function tampil_skinap($params)
    {
        $query = $this->db->select('*')
                ->from( $this->tb_sk_inap )
                ->where('norm',$params['norm'])
                ->where('tglperiksa',$params['tglperiksa'])
                ->get()->row();

        return $query;
    }

    public function tampil_emergency($params)
    {
        $query = $this->db->select('*')
                ->from( $this->tb_sk_emergency )
                ->where('norm',$params['norm'])
                ->where('tglperiksa',$params['tglperiksa'])
                ->get()->row();

        return $query;
    }

    /**
     * change
     * [+] idunit
     */
    public function insert_inacbg($data,$params="")
    {
        if( !empty($params) ){
            $this->db->where('norm',$params['norm']);
            $this->db->where('tglperiksa',$params['tglperiksa']);
            $this->db->where('idunit',$params['idunit']);
            $this->db->delete( $this->tb_inacbg );
        }

        $this->db->insert($this->tb_inacbg, $data);
        $insert_id = $this->db->insert_id();
        return  $insert_id;
    }

    public function insert_upload_file_image($data,$nama_table,$params="")
    {
        if( !empty($params) ){
            $this->db->where('norm',$params['norm']);
            $this->db->where('tglperiksa',$params['tglperiksa']);
            $this->db->where('idunit',$params['idunit']);
            $this->db->delete( $nama_table );
        }

        $this->db->insert($nama_table, $data);
        $insert_id = $this->db->insert_id();
        return  $insert_id;
    }


    /**
     * change
     * [+] idunit
     */
    public function tampil_inacbg($params,$idunit=0)
    {
        $query = $this->db->select('*')
                ->from( $this->tb_inacbg )
                ->where('norm',$params['norm'])
                ->where('tglperiksa',$params['tglperiksa'])
                ->where('idunit',$idunit)
                ->get()->row(); 

        return $query;
    }

    public function cek_fileinacbg($params)
    {
        // $query = $this->tampil_inacbg($params,$params['idunit']);
        $query = $this->db->select("IF(name_inacbg = '','".false."','".true."') as name_inacbg")
                ->from( $this->tb_inacbg )
                ->where('norm',$params['norm'])
                ->where('tglperiksa',$params['tglperiksa'])
                ->where('idunit',$params['idunit'])
                ->get()->row(); 
        
        if( !empty( $query ) ){
            $found = $query->name_inacbg;
        }else{
            $found = false;
        }

        return $found;
    }

    public function tampil_upload_file_image($nama_table,$params,$idunit=0)
    {
        $query = $this->db->select('*')
                ->from( $nama_table )
                ->where('norm',$params['norm'])
                ->where('tglperiksa',$params['tglperiksa'])
                ->where('idunit',$idunit)
                ->get()->row();

        return $query;
    }

    public function delete_inacbg()
    {
        $post     = $this->input->post();
        $idinacbg = $post['idinacbg'];

        $this->db->select('*');
        $this->db->from($this->tb_inacbg);
        $this->db->where('idinacbg',$idinacbg);
        $row_inacbg = $this->db->get()->row();

        $path         = FCPATH.'assets/inacbg';
                
        $file_name    = $row_inacbg->name_inacbg;
        $path_file    = $path.'/'.$file_name;

        if(!empty($row_inacbg)){
            exec( chmod($path_file,777) );
            unlink($path_file);
        }

        $this->db->where('idinacbg',$idinacbg);
        $query = $this->db->delete( $this->tb_inacbg );
        return $query;
    }

    public function delete_upload_image()
    {
        $post     = $this->input->post();
        $idimage  = $post['idimage'];
        $elemenid  =  str_replace( 'frm-','',$post['elemenid'] );

        $this->db->select('*');
        $this->db->from($elemenid);
        $this->db->where('id'.$elemenid,$idimage);
        $row_query = $this->db->get()->row();

        $path         = FCPATH.'assets/'.$elemenid;
                
        $file_name    = $row_query->name_image;
        $path_file    = $path.'/'.$file_name;

        if(!empty($row_query)){
            exec( chmod($path_file,777) );
            unlink($path_file);
        }

        $this->db->where('id'.$elemenid,$idimage);
        $query = $this->db->delete( $elemenid );
        return $query;
    }

    public function delete_rujukanbaru()
    {
        $post           = $this->input->post();
        $idrujukanbaru  = $post['idrujukanbaru'];

        $this->db->select('*');
        $this->db->from($this->tb_rujukanbaru);
        $this->db->where('idrujukanbaru',$idrujukanbaru);
        $row_rujukanbaru = $this->db->get()->row();

        $path         = FCPATH.'assets/rujukanbaru';
                
        $file_name    = $row_rujukanbaru->name_rujukanbaru;
        $path_file    = $path.'/'.$file_name;

        if(!empty($row_rujukanbaru)){
            exec( chmod($path_file,777) );
            unlink($path_file);
        }

        $this->db->where('idrujukanbaru',$idrujukanbaru);
        $query = $this->db->delete( $this->tb_rujukanbaru );
        return $query;
    }

    public function insert_rujukanbaru($data,$params="")
    {
        if( !empty($params) ){
            $this->db->where('norm',$params['norm']);
            $this->db->where('tglperiksa',$params['tglperiksa']);
            $this->db->where('idunit',$params['idunit']);
            $this->db->delete( $this->tb_rujukanbaru );
        }

        $this->db->insert($this->tb_rujukanbaru, $data);
        $insert_id = $this->db->insert_id();
        return  $insert_id;
    }

    public function tampil_rujukanbaru($params,$idunit=0)
    {
        $query = $this->db->select('*')
                ->from( $this->tb_rujukanbaru )
                ->where('norm',$params['norm'])
                ->where('tglperiksa',$params['tglperiksa'])
                ->where('idunit',$idunit)
                ->get()->row();

        return $query;
    }

    public function cek_rujukanbaru($params)
    {
        // $query = $this->tampil_rujukanbaru($params,$params['idunit']);
        $query = $this->db->select("IF(name_rujukanbaru IS NULL or name_rujukanbaru = '','".false."','".true."') as name_rujukanbaru")
                ->from( $this->tb_rujukanbaru )
                ->where('norm',$params['norm'])
                ->where('tglperiksa',$params['tglperiksa'])
                ->where('idunit',$params['idunit'])
                ->where('is_tampil',0)
                ->get()->row(); 
        
        if( !empty( $query ) ){
            $found = $query->name_rujukanbaru;
               
        }else{
            $found = false;
        }

        return $found;
    }

    public function is_tampil_suratkontrolinap($params)
    {
        $query = $this->db->select('is_tampil')
            ->from( $this->tb_sk_inap )
            ->where('norm',$params['norm'])
            ->where('tglperiksa',$params['tglperiksa'])
            ->where('idunit',$params['idunit'])
            ->get()->row();
        
        if( empty($query) ){
            $query = $this->db->select('is_tampil')
            ->from( $this->tb_sk_inap )
            ->where('norm',$params['norm'])
            ->where('tglperiksa',$params['tglperiksa'])
            ->where('idunit',0)
            ->get()->row();
        }

        if( !empty($query) ){
            return $query->is_tampil;
        }

    }

    public function is_tampil_skdp($params)
    {
        $query = $this->db->select('is_tampil')
        ->from( $this->tb_sk_formedit )
        ->where('norm',$params['norm'])
        ->where('tglperiksa',$params['tglperiksa'])
        ->where('idunit',$params['idunit'])
        ->get()->row();

        if( empty($query) ){
            $query = $this->db->select('is_tampil')
            ->from( $this->tb_sk_formedit )
            ->where('norm',$params['norm'])
            ->where('tglperiksa',$params['tglperiksa'])
            ->where('idunit',0)
            ->get()->row();
        }

        if( !empty($query) ){
            return $query->is_tampil;
        }

    }

    public function is_tampil_emergensi($params)
    {
        $query = $this->db->select('is_tampil')
        ->from( $this->tb_sk_emergency )
        ->where('norm',$params['norm'])
        ->where('tglperiksa',$params['tglperiksa'])
        ->where('idunit',$params['idunit'])
        ->get()->row();

        if( empty($query) ){
            $query = $this->db->select('is_tampil')
            ->from( $this->tb_sk_emergency )
            ->where('norm',$params['norm'])
            ->where('tglperiksa',$params['tglperiksa'])
            ->where('idunit',0)
            ->get()->row();
        }

        if( !empty($query) ){
            return $query->is_tampil;
        }

    }

    public function is_tampil_radiologi($params)
    {
        $query = $this->db->select('is_tampil')
        ->from( $this->tb_radiologi )
        ->where('norm',$params['norm'])
        ->where('tglperiksa',$params['tglperiksa'])
        ->where('idunit',$params['idunit'])
        ->get()->row();

        if( empty($query) ){
            $query = $this->db->select('is_tampil')
            ->from( $this->tb_radiologi )
            ->where('norm',$params['norm'])
            ->where('tglperiksa',$params['tglperiksa'])
            ->where('idunit',0)
            ->get()->row();
        }

        if( !empty($query) ){
            return $query->is_tampil;
        }

    }

    public function is_tampil_rencanakontrol($params)
    {
        $query = $this->db->select('is_tampil')
        ->from( $this->tb_rencanakontrol )
        ->where('norm',$params['norm'])
        ->where('tglperiksa',$params['tglperiksa'])
        ->where('idunit',$params['idunit'])
        ->get()->row();

        if( empty($query) ){
            $query = $this->db->select('is_tampil')
            ->from( $this->tb_rencanakontrol )
            ->where('norm',$params['norm'])
            ->where('tglperiksa',$params['tglperiksa'])
            ->where('idunit',0)
            ->get()->row();
        }

        if( !empty($query) ){
            return $query->is_tampil;
        }

    }

    public function is_tampil_rencanakontrol_new($params)
    {
        $query = $this->db->select('is_tampil')
        ->from( $this->tb_rencanakontrol_new )
        ->where('norm',$params['norm'])
        ->where('tglperiksa',$params['tglperiksa'])
        ->where('idunit',$params['idunit'])
        ->get()->row();

        if( empty($query) ){
            $query = $this->db->select('is_tampil')
            ->from( $this->tb_rencanakontrol_new )
            ->where('norm',$params['norm'])
            ->where('tglperiksa',$params['tglperiksa'])
            ->where('idunit',0)
            ->get()->row();
        }

        if( !empty($query) ){
            return $query->is_tampil;
        }

    }

    public function is_tampil_rujukanbaru($params)
    {
        $query = $this->db->select('is_tampil')
        ->from( $this->tb_rujukanbaru )
        ->where('norm',$params['norm'])
        ->where('tglperiksa',$params['tglperiksa'])
        ->where('idunit',$params['idunit'])
        ->get()->row();

        if( empty($query) ){
            $query = $this->db->select('is_tampil')
            ->from( $this->tb_rujukanbaru )
            ->where('norm',$params['norm'])
            ->where('tglperiksa',$params['tglperiksa'])
            ->where('idunit',0)
            ->get()->row();
        }

        if( !empty($query) ){
            return $query->is_tampil;
        }

    }

    /**
     * change
     * [+] idunit
     */
    public function tampil_sep($params,$idunit=0)
    {
        $query = $this->db->select('*')
                ->from( $this->tb_sep )
                ->where('norm',$params['norm'])
                ->where('tglperiksa',$params['tglperiksa'])
                ->where('idunit',$idunit)
                ->get()->row();

        return $query;
    }

    public function tampil_rencanakontrol($params,$idunit=0)
    {
        $query = $this->db->select('*')
                ->from( $this->tb_rencanakontrol )
                ->where('norm',$params['norm'])
                ->where('tglperiksa',$params['tglperiksa'])
                ->where('idunit',$idunit)
                ->get()->row();

        return $query;
    }

    public function tampil_dokumenlainlain($params)
    {
        $query = $this->db->select('*')
            ->from( $this->tb_dokumenlainlain )
            ->where('norm',$params['norm'])
            ->where('tglperiksa',$params['tglperiksa'])
            ->where('idunit',$params['idunit'])
            ->get()->row();
        
        if( empty( $query ) ){
            $query = $this->db->select('*')
            ->from( $this->tb_dokumenlainlain )
            ->where('norm',$params['norm'])
            ->where('tglperiksa',$params['tglperiksa'])
            ->where('idunit',0)
            ->get()->row();
            
        }

        return $query;

    }

    public function cek_dokumenlainlain($params)
    {
        // $query = $this->tampil_dokumenlainlain($params);

        $query = $this->db->select("IF(name_image IS NULL or name_image = '','".false."','".true."') as name_image")
                ->from( $this->tb_dokumenlainlain )
                ->where('norm',$params['norm'])
                ->where('tglperiksa',$params['tglperiksa'])
                ->where('idunit',$params['idunit'])
                ->where('is_tampil',0)
                ->get()->row(); 
        
        if( !empty( $query ) ){
            $found = $query->name_image;
        }else{
            $found = false;
        }

        return $found;
    }

    public function insert_dokumenlainlain($nm_elemen="",$v_val="")
    {
        $post        = $this->input->post();
        $tglperiksa  =  format_tanggal( str_replace('/','-', $post['tglperiksa']) ); 
        $norm        = $post['norm']; 
        $val         = !empty( $v_val ) ? $v_val : $post['val']; 
        $name_elemen = !empty( $nm_elemen ) ? $nm_elemen : $post['name_elemen'];
        $idunit      = $post['idunit']; 

        $cek = $this->db->select('*')
                ->from( $this->tb_dokumenlainlain )
                ->where('norm',$norm)
                ->where('tglperiksa',$tglperiksa)
                ->where('idunit',$idunit)
                ->get()->row();
        
        if(empty($cek)){
            $cek = $this->db->select('*')
                ->from( $this->tb_dokumenlainlain )
                ->where('norm',$norm)
                ->where('tglperiksa',$tglperiksa)
                ->where('idunit',0)
                ->get()->row();
        }

        if( !empty($cek) ){ 
            $arr_name_images   = explode(',',$cek->name_image);
            $ex_val            = explode(',',$val);
            $merge_image       = array_merge($arr_name_images, $ex_val);
            
            if( $name_elemen == 'is_tampil' ){
                $data = [$name_elemen => $val];
            }else{
                $data = [$name_elemen => implode(',', $merge_image)];
            }
        }else{
            $data = [$name_elemen => $val];
        }

        if( empty($cek) ){
            $data['norm'] = $norm;
            $data['tglperiksa'] = $tglperiksa;
            $data['idunit'] = $idunit;
            $this->db->insert($this->tb_dokumenlainlain,$data);
            $query = $this->db->insert_id();
        }else{
            $data['idunit'] = $idunit;
            $this->db->where('norm',$norm);
            $this->db->where('tglperiksa',$tglperiksa);
            $this->db->update($this->tb_dokumenlainlain,$data);
            $query =  $this->db->affected_rows();
        }

        return $query;
    }

    public function delete_imagelainlain()
    {
        $post       = $this->input->post();
        $norm       = $post['norm'];
        $tglperiksa = $post['tglperiksa'];
        $dataid     = $post['dataid'];
        $keyimage   = $post['keyimage'];


        $cek = $this->db->select('*')
            ->from( $this->tb_dokumenlainlain )
            ->where('iddokumenlainlain',$dataid)
            ->get()->row();

        if( empty($cek->name_image) ){
            $this->db->where('iddokumenlainlain',$dataid);
            $query = $this->db->delete( $this->tb_dokumenlainlain );
        }else{
            $filenames = '';
            $arr_images = explode(',',$cek->name_image);

            unset($arr_images[$keyimage]);
            $reorder_arr = array_values( $arr_images );

            $path         = FCPATH.'assets/dokumenlainlain';
            
            if(!empty($cek->name_image)){
                foreach( explode( ',',$cek->name_image ) as $key => $rowname ){
                    if( $key == $keyimage ){
                        $file_name    = $rowname;
                        $path_file    = $path.'/'.$file_name;
                        unlink($path_file);
                    }
                }
            }

            if( empty($reorder_arr) ){
                $this->db->where('iddokumenlainlain',$dataid);
                $query = $this->db->delete( $this->tb_dokumenlainlain );
            }else{
                
                $this->db->where('iddokumenlainlain',$dataid);
                $this->db->update($this->tb_dokumenlainlain,['name_image'=>implode(',',$reorder_arr) ]);
                $query =  $this->db->affected_rows();
            }

            
        }

        return $query;
    }

    public function is_tampil_dokumenlainlain($params)
    {
        $query = $this->db->select('is_tampil')
        ->from( $this->tb_dokumenlainlain )
        ->where('norm',$params['norm'])
        ->where('tglperiksa',$params['tglperiksa'])
        ->where('idunit',$params['idunit'])
        ->get()->row();

        if( empty($query) ){
            $query = $this->db->select('is_tampil')
            ->from( $this->tb_dokumenlainlain )
            ->where('norm',$params['norm'])
            ->where('tglperiksa',$params['tglperiksa'])
            ->where('idunit',0)
            ->get()->row();
        }

        if( !empty($query) ){
            return $query->is_tampil;
        }

    }

    public function insert_resumegabung(){
        $post       = $this->input->post();
        $norm       = $post['norm'];
        $tglperiksa = $post['tglperiksa'];
        $tipe       = $post['tipe'];
        $idunit     = $post['idunit'];
        $tglgabung  = format_tanggal( str_replace('/','-',$post['tglperiksaresume'] ) );

        $data = compact("norm", "tglperiksa","tglgabung");
        
        if( !empty($idunit) ){
            $data['idunit'] = $idunit;
        }

        if( $tipe == 'resume' ){
            $nametable = $this->tb_resumegabung;
        }else if($tipe == 'radiologi'){
            $nametable = $this->tb_radiologigabung;
        }else if($tipe == 'billing'){
            $nametable = $this->tb_billinggabung;
        }

        $this->db->insert($nametable,$data);
        $query = $this->db->insert_id();
        return $query;
    }

    public function get_resume_gabung($params){
        $query = $this->db->select('*')
        ->from( $this->tb_resumegabung )
        ->where('norm',$params['norm'])
        ->where('tglperiksa',$params['tglperiksa'])
        ->get()->row();

        return $query;
    }

    public function delete_resumegabung()
    {
        $post       = $this->input->post();
        $idgabung   = $post['idgabung'];
        $tipe       = $post['tipe'];

        if( $tipe == 'resume' ){
            $this->db->where('idresumegabung',$idgabung);
            return $this->db->delete( $this->tb_resumegabung );
        }else if($tipe == 'radiologi'){
            $this->db->where('idradiologigabung',$idgabung);
            return $this->db->delete( $this->tb_radiologigabung );
        }else if($tipe == 'billing'){
            $this->db->where('idbillinggabung',$idgabung);
            return $this->db->delete( $this->tb_billinggabung );
        }
    }

    public function get_radiologi_gabung($params){
        $query = $this->db->select('*')
        ->from( $this->tb_radiologigabung )
        ->where('norm',$params['norm'])
        ->where('tglperiksa',$params['tglperiksa'])
        ->get()->row();

        return $query;
    }
    
    public function get_billing_gabung($params){
        $query = $this->db->select('*')
        ->from( $this->tb_billinggabung )
        ->where('norm',$params['norm'])
        ->where('tglperiksa',$params['tglperiksa'])
        ->get()->row();

        return $query;
    }

    public function get_billing_gabung_all($params){
        $query = $this->db->select('*')
        ->from( $this->tb_billinggabung )
        ->where('norm',$params['norm'])
        ->where('Month(tglperiksa)',format_tanggal($params['tglperiksa'],'m'))
        ->get()->result();

        return $query;
    }

    /**
     * NEW Script with bridging
     */
    public function insert_sep_new(){
        $post = $this->input->post();
        
        $cetakan    = $post['cetakan'];
        $norm       = $post['norm'];
        $tglperiksa = $post['tglperiksa'];
        $idunit     = $post['idunit'];

         $cek = $this->db->select('*')
                ->from( $this->tb_sep_new )
                ->where('norm',$norm)
                ->where('tglperiksa',$tglperiksa)
                ->where('idunit',$idunit)
                ->get()->row();

        if( empty($cek) ){
            $cek = $this->db->select('*')
                ->from( $this->tb_sep_new )
                ->where('norm',$norm)
                ->where('tglperiksa',$tglperiksa)
                ->where('idunit',0)
                ->get()->row();
        }
        
        $data = ['cetakan' => $cetakan];

        if( empty($cek) ){
            $data['norm'] = $norm;
            $data['tglperiksa'] = $tglperiksa;
            $data['idunit'] = $idunit;
            $this->db->insert($this->tb_sep_new,$data);
            $query = $this->db->insert_id();
        }else{
            $data['idunit'] = $idunit;
            $this->db->where('norm',$norm);
            $this->db->where('tglperiksa',$tglperiksa);
            $this->db->update($this->tb_sep_new,$data);
            $query =  $this->db->affected_rows();
        }

        return $query;
    }

    public function tampil_sep_new($params,$idunit=0)
    {
        $query = $this->db->select('*')
                ->from( $this->tb_sep_new )
                ->where('norm',$params['norm'])
                ->where('tglperiksa',$params['tglperiksa'])
                ->where('idunit',$idunit)
                ->get()->row();

        return $query;
    }

    public function insert_rencanakontrol_new(){
        $post = $this->input->post();
        $norm       = $post['norm'];
        $tglperiksa = $post['tglperiksa'];
        $idunit     = $post['idunit'];

         $cek = $this->db->select('*')
                ->from( $this->tb_rencanakontrol_new )
                ->where('norm',$norm)
                ->where('tglperiksa',$tglperiksa)
                ->where('idunit',$idunit)
                ->get()->row();

        if( empty($cek) ){
            $cek = $this->db->select('*')
                ->from( $this->tb_rencanakontrol_new )
                ->where('norm',$norm)
                ->where('tglperiksa',$tglperiksa)
                ->where('idunit',0)
                ->get()->row();
        }
        

        if( empty($cek) ){
            $this->db->insert($this->tb_rencanakontrol_new,$post);
            $query = $this->db->insert_id();
        }else{
            $this->db->where('norm',$norm);
            $this->db->where('tglperiksa',$tglperiksa);
            $this->db->update($this->tb_rencanakontrol_new,$post);
            $query =  $this->db->affected_rows();
        }

        return $query;
    }

    public function tampil_rencanakontrol_new($params,$idunit=0)
    {
        $query = $this->db->select('*')
                ->from( $this->tb_rencanakontrol_new )
                ->where('norm',$params['norm'])
                ->where('tglperiksa',$params['tglperiksa'])
                ->where('idunit',$idunit)
                ->get()->row();

        return $query;
    }

    public function insert_validasi_klaim()
    {
        $post = $this->input->post();

        $cek_uncek = $post['val'];

        $data = [
            'datecreated'=> waktu_indonesia(),
            'sep' => $post['sep'],
            'idunit' =>$post['idunit'],
            'norm' =>$post['norm'],
            'tglperiksa' =>$post['tglperiksa'],
        ];
        
        if( $cek_uncek == 'true' ){
            $this->db->insert($this->tb_verif_klaim,$data);
            $query = $this->db->insert_id();
        }else{
            unset($data['datecreated']);
           
            $this->db->where($data);
            $query = $this->db->delete($this->tb_verif_klaim);
        }


        return $query;
    }

    public function get_validasi_klaim($nosep,$params)
    {
        $query = $this->db->select('*')
                ->from( $this->tb_verif_klaim )
                ->where('norm',$params['norm'])
                ->where('tglperiksa',$params['tglperiksa'])
                ->where('idunit',$params['idunit'])
                ->where('sep',$nosep)
                ->get()->row();

        return $query;
    }

    /**
     * TEST PDF
     */
    public function tampil_testpdf($params,$idunit=0)
    {
        $query = $this->db->select('*')
                ->from( $this->tb_testpdf_convert )
                ->where('norm',$params['norm'])
                ->where('tglperiksa',$params['tglperiksa'])
                ->where('idunit',$idunit)
                ->get()->row();

        return $query;
    }

}
?>
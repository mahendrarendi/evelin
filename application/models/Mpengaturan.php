<?php
/**
 * Model Pengaturan
 * Author : MrGanteng
 */
class Mpengaturan extends CI_Model 
{
    protected $tb_pengaturan = 'pengaturan';

    public function get_pengaturan($key_png)
    {
        $this->db->select('*');
        $this->db->from($this->tb_pengaturan);
        $this->db->where('key_pengaturan',$key_png);
        return $this->db->get()->row();
    }

    public function add_pengaturan($key_png,$value_png)
    {
        $data = ['key_pengaturan'=>$key_png,'value_pengaturan'=>$value_png];
        $this->db->insert($this->tb_pengaturan, $data);
        $insert_id = $this->db->insert_id();
        return  $insert_id;
    }

    public function update_pengaturan($id_png,$key_png,$value_png)
    {
        $data = ['key_pengaturan'=>$key_png,'value_pengaturan'=>$value_png];
        $this->db->where(['idpengaturan'=>$id_png]);
        $this->db->update($this->tb_pengaturan, $data);
        return $this->db->affected_rows();
    }

    
}